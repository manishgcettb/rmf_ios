//
//  Analytics.h
//  SWAT
//
//  Created by Arsalan Habib on 12/17/13.
//
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#define kKeyAnalytics       @"analytics"
#define kFlagConsent        @"kFlagConsent"
#define kAppHasLaunchedOnce @"HasLaunchedOnce"

enum AnalyticsType
{
    GoogleAnalytics = 1,
    FlurryAnalytics,
    CrittercismAnalytics,
    CrashlyticsAnalytics
};

#define kEventCategoryUIAction              @"ui_action"

#define kEventActionButtonPress             @"button_press"

@interface Analytics : NSObject <UIAlertViewDelegate>

-(void) initializeTrackerWithTrackingId:(NSString *) trackingId forAnalyticsPlatform:(int) platform;
+(void) initAnalyticsWithTrackerId:(NSString *) trackingId;
+(void) logEvent:(NSString *) event withAction:(NSString *) action withInformation:(NSString *) info;
+(void) logException:(NSException *) exception;


@end