/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#define kKeyAnalytics       @"analytics"
#define kFlagConsent        @"kFlagConsent"
#define kAppHasLaunchedOnce @"HasLaunchedOnce"


@interface Analytics : NSObject <UIAlertViewDelegate>

+(void) initAnalyticsWithTrackerId:(NSString *) trackingId;
+(void) logEvent:(NSString *) event withAction:(NSString *) action withInformation:(NSString *) info;


@end