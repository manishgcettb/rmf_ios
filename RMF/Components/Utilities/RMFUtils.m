/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFUtils.h"
#import "RMFErrorHandler.h"
#import "RMFMacro.h"

@implementation RMFUtils

#pragma mark - Class methods declaration

/*
 * Raises exception of given exception type with given description
 */

+ (void)raiseException:(NSString *)exceptionType WithDescription:(NSString *)description {
    if (exceptionType && description) {
        [NSException raise:exceptionType format:@"%@", description];
    }
}

/*
 * Returns NSError instance based on the error code
 */

+ (NSError *)errorWithCode:(NSInteger)code {
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
    [userInfo setObject:[RMFErrorHandler getErrorMessageForSystem:code] forKey:kLocalizedDescriptionKey];
    NSError *error = [NSError errorWithDomain:[RMFErrorHandler domainForCode:code] code:code userInfo:userInfo];
    return error;
}

/*
 * Checks whether system manager is configured properly
 */

+ (BOOL)isSystemManagerConfigured:(RMFSystemManager *)systemManager {
    if (systemManager) {
        // Check if active controller instance is available
        if (systemManager.activeController) {
            // Check if active parser instance is available
            if (systemManager.activeParser) {
                return YES;
            }
            else {
                [RMFUtils raiseException:NSInternalInconsistencyException WithDescription:kActiveParserNotFound];
            }
        }
        else {
            [RMFUtils raiseException:NSInternalInconsistencyException WithDescription:kActiveControllerNotFound];
        }
    }
    else {
        [RMFUtils raiseException:NSInternalInconsistencyException WithDescription:kSystemManagerNotFound];
    }
    return NO;
}

@end
