/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <Foundation/Foundation.h>
#import "RMFSystemManager.h"

@interface RMFUtils : NSObject

#pragma mark - Class methods declaration

// Raises exception of given exception type with given description
+ (void)raiseException:(NSString *)exceptionType WithDescription:(NSString *)description;

// Returns NSError instance based on the error code
+ (NSError *)errorWithCode:(NSInteger)code;

// Checks whether system manager is configured properly
+ (BOOL)isSystemManagerConfigured:(RMFSystemManager *)systemManager;

@end
