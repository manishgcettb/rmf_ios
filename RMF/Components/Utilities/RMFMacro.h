/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFLogger.h"
#import "RMFLog.h"

#ifndef RMF_RMFMacro_h
#define RMF_RMFMacro_h

#pragma mark - RMFConfiguration

// Keys of acceptable content type
#define kContentTypeJson            @"text/json"

#define CONFIG_FILE_NAME @"rmfconfiguration.json"

// Key of RMF configuration dictionary
#define MIME_TYPE @"mime-type"
#define CONFIG_URL @"config-url"
#define SYSTEM_IP @"system-ip"
#define PROFILE_VERSION @"rmf-version"
#define PROFILE_NAME @"rmf-name"
#define PORT @"port"
#define PORT_INFO @"port-info"

#define DEFAULT_ZONE @"Default-ZoneName"
#define COMPANY_NAME @"company-name"
#define COMPANY_URL @"company-url"
#define PROFILE_DESC @"profile-description"
#define APP_CONFIG @"appConfiguration"
#define SUPPORTED_SOCIAL_LINKS @"supportedsociallinks"
#define PROMO_URL @"promoURL"
#define PRIVACY_POLICY_URL @"privacyPolicyURL"
#define SCREEN_NAV_FLOW @"screennavigationflow"
#define TERMS_OF_USE_URL @"TermOfUseURL"
#define APP_VERSION @"appVersion"
#define KEEP_ME_LOGIN @"keepMeLogin"
#define SUPPORT_EMAIL @"supportEmail"
#define SUPPORT_PHONE @"supportPhone"
#define SYSTEM @"system"
#define SYSTEM_VERSION @"systemversion"
#define SPONSORED_URL @"sponsoredURL"
#define ABOUT_US @"aboutUs"
#define PRIVACY_POLICY @"privacyPolicy"
#define HELP @"help"
#define NOTIFICATION_ENABLED @"notificationEnable"
#define ANALYTICS_ENABLED @"analyticsEnable"
#define IS_SANDBOX_ENABLED @"isSandboxEnabled"
#define IS_DATABASE_ENABLED @"isDatabaseEnabled"

#define NOTIFICATION_TYPE @"notification"

#define PORTRAIT @"portrait"
#define LANDSCAPE  @"landscape"

#define KUMO @"kumo"
#define SCG @"scg"
#define ZD @"zd"

#define NAVIGATION_FLOW_FIRST_TIME @"firsTimeFlow"
#define NAVIGATION_FLOW_FOR_ADMIN @"userRoleAdmin"
#define NAVIGATION_FLOW_FOR_NORMAL @"userRoleNormal"

#define kAppDomain @"com.ruckuswireless.rmf.errordomain"
#define kEmptyText @""
#define kRequestTimeOutInterval       100

#define kCacheCountLimit     100
#define kCacheTotalCostLimit 1500000

//HTTP header key
#define kAcceptKey           @"Accept"
#define kContentTypeKey      @"Content-Type"
#define kKUMOContentTypeKey  @"application/vnd.yang.data+json"
#define kSCGContentTypeKey   @""
#define kZDContentTypeKey    @""


#pragma mark  -  WLAN

#pragma mark - WLAN DB MACRO

#define EVENT_ENTITY @"RMFEvent"
#define WLAN_ENTITY @"RMFWlan"
#define WLAN_ADV_ENTITY @"RMFWlanAdvNwCustomization"
#define WLAN_BASIC_ENTITY @"RMFWlanBasicNwCustomization"

#define kWlanNameAttribute                  @"name"
#define kWlanPskPassPhraseAttribute         @"pskPassphrase"
#define kWlanSsidAttribute                  @"ssid"
#define kWlanSecurityAttribute              @"wlanSecurity"
#define kWlanTypeAttribute                  @"wlanType"
#define kWlanAdvCustomizationAttribute      @"wlanAdvCustmization"
#define kWlanBasicCustomizationAttribute    @"wlanBasicNwCustomization"
#define kEnableBandBalancingAttribute       @"enableBandBalancing"
#define kMaxClientOnWlanPerRadioAttribute   @"maxClientOnWlanPerRadio"

#define kEventTimeAttribute                 @"time"
#define kEventTypeAttribute                 @"eventType"
#define kEventSeverityAttribute             @"severity"
#define kEventDescriptionAttribute          @"eventDescription"
#define kEventTypeIdAttribute               @"eventTypeId"

#define kWlanStartIndexValue        1
#define kWlanFrameSize        10

// Clients key
#define kClientsKey @"clients"
#define kClientsDataKey     @"client-data"
#define kClientMacAddressKey        @"mac-address"
#define kClientAuthMethodKey        @"auth-method"
#define kClientConnecitonDurationKey        @"connection-duration"
#define kClientIPKey        @"ip"
#define kClientOSTypeKey        @"os-type"
#define kClientStatusKey        @"status"
#define kClientWlanKey        @"wlan"

// Event keys
#define kEventTimeKey           @"time"
#define kEventTypeKey           @"event-type"
#define kEventSeverityKey       @"severity"
#define kEventDescriptionKey    @"description"
#define kEventTypeIdKey         @"event-type-id"

//Wlan keys
#define kBssidKey           @"bssid"
#define kWlansKey           @"wlans"
#define kTotalCountKey      @"total-count"
#define kWlanStatusKey      @"wlan-status"
#define kServicesKey        @"services"
#define kWlanServiceKey     @"wlan-service"
#define kWlanNameKey        @"name"
#define kWlanDescriptionKey @"description"
#define kWlanEnabledKey     @"enabled"
#define kWlanSssidKey                   @"ssid"
#define kWlanTypeKey                    @"wlan-type"
#define kWlanSecurityKey                @"wlan-security"
#define kBasicNwCustomizationKey        @"basic-network-customization"
#define kVlanIdKey                      @"vlanId"
#define kAdvCustomizationKey            @"advanced-customization"
#define kMaxClientOnWlanPerRadio        @"max-clients-onWlan-perRadio"
#define kEnableBandBalancingKey         @"enableBandBalancing"
#define kPskPassPhraseKey               @"psk-passphrase"


#pragma mark  -  AP
//AP keys

#define kAPStartIndexValue        1
#define kAPFrameSize        10

#define kApsKey           @"aps"
#define kAPProvisioningData           @"provisioning-data"

#define kAPWlanStatus @"wlan-status"
#define kAPWlanTotalCount @"total-count"
#define kAPWlans @"wlans"

#define kAPExtendedStatus @"extended-status"
#define kAPInternalIP @"internal-ip"
#define kAPExternalIP @"external-ip"
#define kAPHops @"hops"
#define kAPTunnelType @"tunnel-type"
#define kAPName @"name"
#define kAPUptime @"uptime"

#define kAPRadioStatus @"radio-status"
#define kAPTxPower @"tx-power"
#define kAPChannel @"channel"
#define kAPChannelWidth @"channel-width"
#define kAPWlanCount @"wlan-count"
#define kAPMode @"mode"

#define kAPStatus @"status"
#define kAPModel @"model"
#define kAPMacAddress @"mac-address"
#define kAPRegistrationStatus @"registration-status"
#define kAPConnectionStatus @"connection-status"
#define kAPConfigStatus @"config-status"
#define kAPLastSeenByVscg @"last-seen-by-vscg"
#define kAPSWver @"SWver"
#define kAPLocation @"location"


#define kApsKey                     @"aps"
#define kProvisioningData           @"provisioning-data"
#define kAPName                     @"name"
#define kOffset        @"offset"
#define kLimit       @"limit"

#define kSelectParametersLabel @"label"
#define kSelectParametersExpression @"expression"
#define kSelectParametersResultType @"result-type"
#define kSelectResultTypeString @"string"
#define kSelectResultParametersValue @"value"


#define kSelectLabelName @"Name"
#define kSelectLabelSerialNumber @"Serial Number"
#define kSelectLabelMacAddress @"MAC Address"
#define kSelectLabelSSID @"SSID"
#define kSelectLabelWLANCount @"WLAN Counts"

#define kSelectExpressionName @"extended-status/name"
#define kSelectExpressionSerialNumber @"serial-number"
#define kSelectExpressionMacAddress @"status/mac-address"
#define kSelectExpressionSSID @"wlan-status/wlans/ssid"
#define kSelectExpressionWLANCount @"wlan-status/total-count"

#define kAPSortByName               @"name"
#define kAPSortByMacAddress         @"mac-address"
#define kAPSortBySerialNumber       @"serial-number"
#define kAPSortByWlanCount          @"total-count"
#define kAPSortByVenue              @"venue"

#define kStartQuery @"start-query"
#define kForeach @"foreach"
#define kSelect @"select"
#define kSortBy @"sort-by"
#define kVenue @"venue"

#define kLimit @"limit"
#define kOffset @"offset"
#define kStartQueryResult @"start-query-result"
#define kFetchQueryResult @"fetch-query-result"
#define kQueryHandle @"query-handle"
#define kQueryResult @"query-result"
#define kResult @"result"
#define kImageUrlkey @"image_url"

#define kAPCreatedSuccessfully  @"AP Created successfully"
#define kAPListReceivedSuccessfully  @"AP list received successfully"
#define kAPDetailReceivedSuccessfully  @"AP detail received successfully"
#define kAPEditedSuccessfully  @"AP edited successfully"
#define kAPDeletedSuccessfully  @"AP deleted successfully"
#define kAPImageDownloadedSuccessfully  @"AP image downloaded successfully"
#define kAPImageUploadedSuccessfully  @"AP image uploaded successfully"

#pragma mark  -  Venue and Tenant


//RMF request parameter keys

#define kUserNameKey         @"user_name"
#define kPasswordKey         @"password"
#define kVenueNameKey        @"venue_name"
#define kTenantNameKey       @"tenant_name"
#define kSerialNumberKey     @"serial-number"
#define kNewWlanInfoKey      @"wlan_info"
#define kNewAPInfoKey        @"ap_info"
#define kNewVenueInfoKey     @"venue_info"
#define kMacAddressInfoKey   @"mac_address"
#define kAPKey               @"ap"

#pragma mark  -  Error code

//key of RMF error codes
#define kUrlNotFoundError              1
#define kRequestParamNotFound          2
#define kAPIResponseIsNil              3
#define kParsingFailedError            3
#define kNetworkUnreachableError       4

//Key of RMF error message
#define kUrlNotFound                @"Invalid parameter not satisfying:URL"
#define kBaseUrlNotFound            @"Invalid parameter not satisfying:baseURL"
#define kPortNotFound               @"Invalid parameter not satisfying:port"
#define kConfigDataNotFound         @"Invalid parameter not satisfying:configData"
#define kSystemManagerNotFound      @"Invalid parameter not satisfying:systemManager"
#define kActiveControllerNotFound   @"Invalid parameter not satisfying:activeController"
#define kActiveParserNotFound       @"Invalid parameter not satisfying:activeParser"
#define kSystemTypeNotDefined       @"System type not defined"
#define kRequestParamNotFoundKey    @"Request parameters not found"
#define kLocalizedDescriptionKey    @"NSLocalizedDescriptionKey"
#define kLocalizedDescription    @"NSLocalizedDescription"

#define kCouldNotConnectToServer    @"Could not connect to the server"



#define kTNNeeded              @"Tenant name is required"
#define kTNVNNeeded            @"Tenant name and venue name are required"
#define kTNVINeeded            @"Tenant name and new venue information are required"
#define kTNVNSNNeeded          @"Tenant name venue name and serial number are required"
#define kTNVNAINeeded          @"Tenant name, venue name and new AP information are required"
#define kTNWSNNeeded           @"Tenant name and wlan service name are required"
#define kTNWINeeded            @"Tenant name and new wlan information are required"
#define kTNMANeeded            @"Tenant name and mac address are required"

#define kParsingFailed         @"Parsing failure error"
#define kNetworkUnreachable    @"Network unreachable"

// Error domains
#define kErrorDomainName  @""
#define kErrorDomainApplication @"com.ruckuswireless.rmf.errordomain"
#define BETWEEN(value, min, max) (value <= max && value >= min)

//RMF Error Macros

//HTTP code description for informational status codes
#define kHTTPCodeFor1XXInformationalUnknownDesc   @"Undefined 1XX status code"
#define kHTTPCodeForContinueDesc  @"This means that the server has received the request headers, and that the client should proceed to send the request body (in the case of a request for which a body needs to be sent;"
#define kHTTPCodeForSwitchingProtocolsDesc @"This means the requester has asked the server to switch protocols and the server is acknowledging that it will do so."
#define kHTTPCodeForProcessingDesc @"This code indicates that the server has received and is processing the request, but no response is available yet."

//HTTP code description for success status codes
#define kHTTPCodeFor2XXSuccessUnknownDesc @"Undefined 2XX status code"
#define kHTTPCodeForOKDesc @"Successful HTTP requests."
#define kHTTPCodeForCreatedDesc @"The request has been fulfilled and resulted in a new resource being created."
#define kHTTPCodeForAcceptedDesc @"The request has been accepted for processing, but the processing has not been completed."
#define kHTTPCodeForNonAuthoritativeInformationDesc @"The server successfully processed the request, but is returning information that may be from another source."
#define kHTTPCodeForNoContentDesc @"The server successfully processed the request, but is not returning any content."
#define kHTTPCodeForResetContentDesc @"The server successfully processed the request, but is not returning any content."
#define kHTTPCodeForPartialContentDesc @"The server is delivering only part of the resource due to a range header sent by the client."
#define kHTTPCodeForMultiStatusDesc @"The message body that follows is an XML message and can contain a number of separate response codes, depending on how many sub-requests were made."
#define kHTTPCodeForAlreadyReportedDesc @"The members of a DAV binding have already been enumerated in a previous reply to this request, and are not being included again."
#define kHTTPCodeForIMUsedDesc @"The server has fulfilled a request for the resource, and the response is a representation of the result of one or more instance-manipulations applied to the current instance"

//HTTP code descrption for redirectional status codes
#define kHTTPCodeFor3XXSuccessUnknownDesc @"Undefined 3XX status code"
#define kHTTPCodeForMultipleChoicesDesc @"Indicates multiple options for the resource that the client may follow."
#define kHTTPCodeForMovedPermanentlyDesc @"This and all future requests should be directed to the given URI."
#define kHTTPCodeForFoundDesc @"This is an example of industry practice contradicting the standard."
#define kHTTPCodeForSeeOtherDesc @"The server has received the data and the redirect should be issued with a separate GET message."
#define kHTTPCodeForNotModifiedDesc @"Indicates that the resource has not been modified since the version specified by the request headers If-Modified-Since or If-Match."
#define kHTTPCodeForUseProxyDesc @"The requested resource is only available through a proxy, whose address is provided in the response. "
#define kHTTPCodeForSwitchProxyDesc @"No longer used. Originally meant \"Subsequent requests should use the specified proxy.\""
#define kHTTPCodeForTemporaryRedirectDesc @"In this case, the request should be repeated with another URI; however, future requests should still use the original URI."
#define kHTTPCodeForPermanentRedirect @"The request, and all future requests should be repeated using another URI."

//HTTP code descrption for client error status codes
#define kHTTPCodeFor4XXSuccessUnknownDesc @"Undefined 4XX status code"
#define kHTTPCodeForBadRequestDesc @"The request cannot be fulfilled due to bad syntax."
#define kHTTPCodeForUnauthorisedDesc @"Similar to 403 Forbidden, but specifically for use when authentication is required and has failed or has not yet been provided."
#define kHTTPCodeForPaymentRequired @"Reserved for future use."
#define kHTTPCodeForForbiddenDesc @"The request was a valid request, but the server is refusing to respond to it."
#define kHTTPCodeForNotFoundDesc @"The requested resource could not be found but may be available again in the future."
#define kHTTPCodeForMethodNotAllowedDesc @"A request was made of a resource using a request method not supported by that resource;"
#define kHTTPCodeForNotAcceptableDesc @"The requested resource is only capable of generating content not acceptable according to the Accept headers sent in the request."
#define kHTTPCodeForProxyAuthenticationRequiredDesc @"The client must first authenticate itself with the proxy."
#define kHTTPCodeForRequestTimeoutDesc @"The client did not produce a request within the time that the server was prepared to wait."
#define kHTTPCodeForConflictDesc @"Indicates that the request could not be processed because of conflict in the request, such as an edit conflict in the case of multiple updates."
#define kHTTPCodeForGoneDesc @"Indicates that the resource requested is no longer available and will not be available again."
#define kHTTPCodeForLengthRequiredDesc @"The request did not specify the length of its content, which is required by the requested resource."
#define kHTTPCodeForPreconditionFailedDesc @"The server does not meet one of the preconditions that the requester put on the request."
#define kHTTPCodeForRequestEntityTooLargeDesc @"The request is larger than the server is willing or able to process."
#define kHTTPCodeForRequestURITooLongDesc @"The URI provided was too long for the server to process."
#define kHTTPCodeForUnsupportedMediaTypeDesc @"The request entity has a media type which the server or resource does not support."
#define kHTTPCodeForRequestedRangeNotSatisfiableDesc @"The client has asked for a portion of the file, but the server cannot supply that portion."
#define kHTTPCodeForExpectationFailedDesc @"The server cannot meet the requirements of the Expect request-header field."
#define kHTTPCodeForIamATeapotDesc @"This code was defined in 1998 as one of the traditional IETF April Fools' jokes, in RFC 2324, Hyper Text Coffee Pot Control Protocol, and is not expected to be implemented by actual HTTP servers."
#define kHTTPCodeForAuthenticationTimeoutDesc @"Not a part of the HTTP standard, 419 Authentication Timeout denotes that previously valid authentication has expired."
#define kHTTPCodeForMethodFailureSpringFramework @"Not part of the HTTP standard, but defined by Spring in the HttpStatus class to be used when a method failed."
#define kHTTPCodeForMisdirectedRequestDesc @"The request was directed at a server that is not able to produce a response (for example because a connection reuse)."
#define kHTTPCodeForEnhanceYourCalmTwitterDesc @"Not part of the HTTP standard, but returned by the Twitter Search and Trends API when the client is being rate limited."
#define kHTTPCodeForUnprocessableEntityDesc @"The request was well-formed but was unable to be followed due to semantic errors."
#define kHTTPCodeForLockedDesc @"The resource that is being accessed is locked."
#define kHTTPCodeForFailedDependencyDesc @"The request failed due to failure of a previous request (e.g., a PROPPATCH)."
#define kHTTPCodeForMethodFailureWebDawDesc @"Indicates the method was not executed on a particular resource within its scope because some part of the method's execution failed causing the entire method to be aborted."
#define kHTTPCodeForUpgradeRequiredDesc @"The client should switch to a different protocol such as TLS/1.0."
#define kHTTPCodeForPreconditionRequiredDesc @"The origin server requires the request to be conditional."
#define kHTTPCodeForTooManyRequestsDesc @"The user has sent too many requests in a given amount of time. Intended for use with rate limiting schemes."
#define kHTTPCodeForRequestHeaderFieldsTooLargeDesc @"The server is unwilling to process the request because either an individual header field, or all the header fields collectively, are too large."
#define kHTTPCodeForLoginTimeoutDesc @"A Microsoft extension. Indicates that your session has expired."
#define kHTTPCodeForNoResponseNginxDesc @"Used in Nginx logs to indicate that the server has returned no information to the client and closed the connection (useful as a deterrent for malware)."
#define kHTTPCodeForRetryWithMicrosoftDesc @"A Microsoft extension. The request should be retried after performing the appropriate action."
#define kHTTPCodeForBlockedByWindowsParentalControlsDesc @"A Microsoft extension. This error is given when Windows Parental Controls are turned on and are blocking access to the given webpage."
#define kHTTPCodeForRedirectMicrosoftDesc @"Used in Exchange ActiveSync if there either is a more efficient server to use or the server can't access the users' mailbox."
#define kHTTPCodeForUnavailableForLegalReasonsDesc @"Intended to be used when resource access is denied for legal reasons, e.g. censorship or government-mandated blocked access.";
#define kHTTPCodeForRequestHeaderTooLargeNginxDesc @"Nginx internal code similar to 431 but it was introduced earlier."
#define kHTTPCodeForCertErrorNginxDesc @"Nginx internal code used when SSL client certificate error occurred to distinguish it from 4XX in a log and an error page redirection."
#define kHTTPCodeForNoCertNginxDesc @"Nginx internal code used when client didn't provide certificate to distinguish it from 4XX in a log and an error page redirection."
#define kHTTPCodeForHTTPToHTTPSNginxDesc @"Nginx internal code used for the plain HTTP requests that are sent to HTTPS port to distinguish it from 4XX in a log and an error page redirection."
#define kHTTPCodeForTokenExpiredOrInvalidDesc @"Returned by ArcGIS for Server. A code of 498 indicates an expired or otherwise invalid token."
#define kHTTPCodeForClientClosedRequestNginxDesc @"Used in Nginx logs to indicate when the connection has been closed by client while the server is still processing its request, making server unable to send a status code back."

//HTTP code descrption for server error status codes
#define kHTTPCodeFor5XXSuccessUnknownDesc @"Undefined 5XX status code";
#define kHTTPCodeForInternalServerErrorDesc  @"A generic error message, given when no more specific message is suitable."
#define kHTTPCodeForNotImplementedDesc   @"The server either does not recognize the request method, or it lacks the ability to fulfill the request. Usually this implies future availability (e.g., a new feature of a web-service API)."
#define kHTTPCodeForBadGatewayDesc  @"The server was acting as a gateway or proxy and received an invalid response from the upstream server."
#define kHTTPCodeForServiceUnavailableDesc   @"The server is currently unavailable (because it is overloaded or down for maintenance). Generally, this is a temporary state. Sometimes, this can be permanent as well on test servers."
#define kHTTPCodeForGatewayTimeoutDesc  @"The server was acting as a gateway or proxy and did not receive a timely response from the upstream server."
#define kHTTPCodeForHTTPVersionNotSupportedDesc   @"The server does not support the HTTP protocol version used in the request."
#define kHTTPCodeForVariantAlsoNegotiatesDesc  @"Transparent content negotiation for the request results in a circular reference."
#define kHTTPCodeForInsufficientStorageDesc  @"The server is unable to store the representation needed to complete the request."
#define kHTTPCodeForLoopDetectedDesc  @"The server detected an infinite loop while processing the request (sent in lieu of 208 Not Reported)."
#define kHTTPCodeForBandwidthLimitExceededDesc   @"This status code, while used by many servers, is not specified in any RFCs."
#define kHTTPCodeForNotExtendedDesc  @"Further extensions to the request are required for the server to fulfill it."
#define kHTTPCodeForNetworkAuthenticationRequiredDesc   @"The client needs to authenticate to gain network access."
#define kHTTPCodeForConnectionTimedOutDesc  @"The server connection timed out."
#define kHTTPCodeForNetworkReadTimeoutErrorUnknownDesc  @"Used by Microsoft HTTP proxies to signal a network read timeout behind the proxy to a client in front of the proxy."
#define kHTTPCodeForNetworkConnectTimeoutErrorUnknownDesc @"This status code is not specified in any RFCs, but is used by Microsoft HTTP proxies to signal a network connect timeout behind the proxy to a client in front of the proxy."

//KUMO error specific descrption
#define kKUMOCodeForUrlNotFoundDesc   @"KUMO Error: Invalid parameter not satisfying:URL"
#define kKUMOCodeForAPIResponseIsNilDesc @"KUMO Error: API response coming nil"
#define kKUMOCodeForParsingFailedErrorDesc @"KUMO Error: Parsing failure error"
#define kKUMOCodeForNetworkUnreachableErrorDesc @"KUMO Error: Netwrok is unreachable"
#define kKUMOCodeForBaseUrlNotFoundDesc @"KUMO Error: Invalid parameter not satisfying:BaseURL"
#define kKUMOCodeForPortNotFoundDesc @"KUMO Error: Invalid parameter not satisfying:port"
#define kKUMOCodeForConfigDataNotFoundDesc @"KUMO Error: Invalid parameter not satisfying:config data"
#define kKUMOCodeForSystemManagerNotFoundDesc @"KUMO Error: System manager not found"
#define kKUMOCodeForActiveControllerNotFoundDesc @"KUMO Error: Active controller not found"
#define kKUMOCodeForActiveParserNotFoundDesc  @"KUMO Error: Active parser not found"
#define kKUMOCodeForSystemTypeNotDefinedDesc @"KUMO Error: System time not defined"
#define kKUMOCodeForRequestParamNotFoundKeyDesc  @"KUMO Error: Request parameters not found"
#define kKUMOCodeForTenantNameNotFoundDesc  @"KUMO Error: Tenant name is required"
#define kKUMOCodeForWlanNameNotFoundDesc  @"KUMO Error: Wlan name is required"
#define kKUMOCodeForWlanSsidNotFoundDesc  @"KUMO Error: Wlan ssid is required"
#define kKUMOCodeForImageUrlNotFoundDesc  @"KUMO Error: Image download url is required"
#define kKUMOCodeForImageAndUrlNotFoundDesc  @"KUMO Error: Image upload url and image are required"
#define kKUMOCodeForTenantAndVenueNameNotFoundDesc  @"KUMO Error: Tenant name and venue name are required"
#define kKUMOCodeForTenantAndVenueInfoNotFoundDesc  @"KUMO Error: Tenant name and new venue information are required"
#define kKUMOCodeForTenantAndVenueNameAndSerialNumberNotFoundDesc  @"KUMO Error: Tenant name venue name and serial number are required"
#define kKUMOCodeForTenantAndVenueNameAndAPInfoNotFoundDesc @"KUMO Error: Tenant name, venue name and AP information are required"
#define kKUMOCodeForTenantAndWlanNameNotFoundDesc  @"KUMO Error: Tenant name and wlan service name are required"
#define kKUMOCodeForTenantAndWlanInfoNotFoundDesc  @"KUMO Error: Tenant name and new wlan information are required"
#define kKUMOCodeForTenantAndMacAddressNotFoundDesc  @"KUMO Error: Tenant name and mac address are required"
#define kKUMOCodeForAPIndexNotSetDesc @"KUMO Error: Start index and size not set"
#define kKUMOCodeForUserAssociationNotSetDesc @"KUMO Error: User association is not set"
#define kKUMOCodeForUserTypeNotSetDesc @"KUMO Error: User type is not set"
#define kKUMOCodeForWrongRootType @"KUMO Error: Root Type is wrong"
#define kKUMOCodeForWrongAPFiler @"KUMO Error: Wrong filter value for AP list"

// Database
#define kKUMOCodeForDatabaseInstanceNotFound @"KUMO Error: Database instance not found"
#define kKUMOCodeForDatabaseImageUrlNotFound @"KUMO Error: AP image url can not be nil"
#define kKUMOCodeForDatabaseInputParameterNotFound @"KUMO Error: Input parameter can not be nil"




//ZD error specific descrption
#define kZDCodeForUrlNotFoundDesc   @"ZD Error: Invalid parameter not satisfying:URL"
#define kZDCodeForAPIResponseIsNilDesc @"ZD Error: API response coming nil"
#define kZDCodeForParsingFailedErrorDesc @"ZD Error: Parsing failure error"
#define kZDCodeForNetworkUnreachableErrorDesc @"ZD Error: Netwrok is unreachable"
#define kZDCodeForBaseUrlNotFoundDesc @"ZD Error: Invalid parameter not satisfying:BaseURL"
#define kZDCodeForPortNotFoundDesc @"ZD Error: Invalid parameter not satisfying:port"
#define kZDCodeForConfigDataNotFoundDesc @"ZD Error: Invalid parameter not satisfying:config data"
#define kZDCodeForSystemManagerNotFoundDesc @"ZD Error: System manager not found"
#define kZDCodeForActiveControllerNotFoundDesc @"ZD Error: Active controller not found"
#define kZDCodeForActiveParserNotFoundDesc  @"ZD Error: Active parser not found"
#define kZDCodeForSystemTypeNotDefinedDesc @"ZD Error: System time not defined"
#define kZDCodeForRequestParamNotFoundKeyDesc  @"ZD Error: Request parameters not found"
#define kZDCodeForTenantNameNotFoundDesc  @"ZD Error: Tenant name is required"
#define kZDCodeForTenantAndVenueNameNotFoundDesc  @"ZD Error: Tenant name and venue name are required"
#define kZDCodeForTenantAndVenueInfoNotFoundDesc  @"ZD Error: Tenant name and new venue information are required"
#define kZDCodeForTenantAndVenueNameAndSerialNumberNotFoundDesc  @"ZD Error: Tenant name venue name and serial number are required"
#define kZDCodeForTenantAndVenueNameAndAPInfoNotFoundDesc @"ZD Error: Tenant name, venue name and new AP information are required"
#define kZDCodeForTenantAndWlanNameNotFoundDesc  @"ZD Error: Tenant name and wlan service name are required"
#define kZDCodeForTenantAndWlanInfoNotFoundDesc  @"ZD Error: Tenant name and new wlan information are required"
#define kZDCodeForTenantAndMacAddressNotFoundDesc  @"ZD Error: Tenant name and mac address are required"
#define kZDCodeForAPIndexNotSetDesc @"ZD Error: Start and last index not set"

#define kZDCodeForWrongAPFiler @"ZD Error: Wrong filter value for AP list"


//SCG error specific descrption
#define kSCGCodeForUrlNotFoundDesc   @"SCG Error: Invalid parameter not satisfying:URL"
#define kSCGCodeForAPIResponseIsNilDesc @"SCG Error: API response coming nil"
#define kSCGCodeForParsingFailedErrorDesc @"SCG Error: Parsing failure error"
#define kSCGCodeForNetworkUnreachableErrorDesc @"SCG Error: Netwrok is unreachable"
#define kSCGCodeForBaseUrlNotFoundDesc @"SCG Error: Invalid parameter not satisfying:BaseURL"
#define kSCGCodeForPortNotFoundDesc @"SCG Error: Invalid parameter not satisfying:port"
#define kSCGCodeForConfigDataNotFoundDesc @"SCG Error: Invalid parameter not satisfying:config data"
#define kSCGCodeForSystemManagerNotFoundDesc @"SCG Error: System manager not found"
#define kSCGCodeForActiveControllerNotFoundDesc @"SCG Error: Active controller not found"
#define kSCGCodeForActiveParserNotFoundDesc  @"SCG Error: Active parser not found"
#define kSCGCodeForSystemTypeNotDefinedDesc @"SCG Error: System time not defined"
#define kSCGCodeForRequestParamNotFoundKeyDesc  @"SCG Error: Request parameters not found"
#define kSCGCodeForTenantNameNotFoundDesc  @"SCG Error: Tenant name is required"
#define kSCGCodeForTenantAndVenueNameNotFoundDesc  @"SCG Error: Tenant name and venue name are required"
#define kSCGCodeForTenantAndVenueInfoNotFoundDesc  @"SCG Error: Tenant name and new venue information are required"
#define kSCGCodeForTenantAndVenueNameAndSerialNumberNotFoundDesc  @"SCG Error: Tenant name venue name and serial number are required"
#define kSCGCodeForTenantAndVenueNameAndAPInfoNotFoundDesc @"SCG Error: Tenant name, venue name and new AP information are required"
#define kSCGCodeForTenantAndWlanNameNotFoundDesc  @"SCG Error: Tenant name and wlan service name are required"
#define kSCGCodeForTenantAndWlanInfoNotFoundDesc  @"SCG Error: Tenant name and new wlan information are required"
#define kSCGCodeForTenantAndMacAddressNotFoundDesc  @"SCG Error: Tenant name and mac address are required"
#define kSCGCodeForAPIndexNotSetDesc @"SCG Error: Start and last index not set"

#define kSCGCodeForWrongAPFiler @"SCG Error: Wrong filter value for AP list"



#define kAPListNotCalled @"Please call apList before calling apListNextChunk"
#define kAPIndexNotSet @"Start and last index not set"


#pragma mark -- Database

//RMF DB Macros
#define NOTIFICATION_ENTITY @"RMFNotification"
#define ASSOCIATION_ENTITY  @"RMFRootAssociation"
#define USER_ENTITY         @"RMFUser"
#define USER_NAME           @"userName"
#define USER_FULL_NAME      @"fullName"
#define USER_EMAIL          @"email"
#define USER_PASSWORD       @"password"
#define USER_ROLE           @"roleType"
#define USER_PH_NO          @"phoneNumber"
#define USER_ADDRESS        @"address"


#define DATABASE_NAME       @"RMFDataModel"
#define STORE_MODE_VERSION  @"NSStoreModelVersionHashes"
#define DATABASE_FILE_NAME  @"RMFDataModel.sqlite"
#define READ_STATUS         @"read_status"

#pragma mark - AP


#define AP_ENTITY @"RMFAccessPoint"
#define AP_WLAN_STATUS_ENTITY @"RMFAPWlanStatus"
#define AP_EXTENDED_STATUS_ENTITY @"RMFAPExtendedStatus"
#define AP_RADIO_STATUS_ENTITY @"RMFAPRadioStatus"
#define AP_STATUS_ENTITY @"RMFAPStatus"

#define kAPKeySerialNumber @"serialNumber"
#define kAPKeySsid @"ssid"
#define kAPKeyRootAssociation @"rootAssociation"
#define kAPKeyImage @"imageUrl"
#define kAPKeyWlanStatus @"rmfAPWlanStatus"
#define kAPKeyExtendedStatus @"rmfAPExtendedStatus"
#define kAPKeyRadioStatus @"rmfAPRadioStatus"
#define kAPKeyStatus @"rmfAPStatus"

#define kAPKeyWlanStatusTotalCount @"totalCount"
#define kAPKeyWlanStatusWlans @"wlans"

#define kAPKeyExtendedStatusInternalIp @"internalIp"
#define kAPKeyExtendedStatusExternalIp @"externalIp"
#define kAPKeyExtendedStatusHops @"hops"
#define kAPKeyExtendedStatusTunnelType @"tunnelType"
#define kAPKeyExtendedStatusName @"name"
#define kAPKeyExtendedStatusUptime @"uptime"

#define kAPKeyRadioStatusTxPower @"txPower"
#define kAPKeyRadioStatusChannel @"channel"
#define kAPKeyRadioStatusChannelWidth @"channelWidth"
#define kAPKeyRadioStatusWlanCount @"wlanCount"
#define kAPKeyRadioStatusMode @"mode"

#define kAPKeyStatusModel @"model"
#define kAPKeyStatusMacAddress @"macAddress"
#define kAPKeyStatusRegistrationStatus @"registrationStatus"
#define kAPKeyStatusConnectionStatus @"connectionStatus"
#define kAPKeyStatusConfigStatus @"configStatus"
#define kAPKeyStatusLastSeenByVscg @"lastSeenByVscg"
#define kAPKeyStatusSwver @"swver"
#define kAPKeyStatusLocation @"location"

#pragma mark -


#pragma mark - Push Notifications
//Keys used for Push Notification

#define NOTIFICATION_RECEIVED       @"NotificationReceived"
#define NOTIFICATION_REGISTER       @"NotificationRegister"
#define NOTIFICATION_REGISTER_FAIL  @"NotificationRegisterFail"
#define DEVICE_TOKEN                @"device_token"


#define kRMFClickEvent          @"This event occured due to click gesture"
#define kRMFNavigationEvent     @"Thus event occured due to navigation gesture"
#define kRMFTransitionEvent     @"This event occured due to app changing states i.e from foreground to background or vice versa"
#define kRMFOrientationEvent    @"This event occured due to app changing orientations i.e.from landscape to portrait or vice versa"
#define kRMFNonUIEvent    @"This event is to report some non UI event"

#pragma mark - RMFBundle

#define RMFBUNDLE_NAME       @"RMFBundle.bundle"
#define RMFBUNDLE_PATH       [[[NSBundle mainBundle] resourcePath]stringByAppendingPathComponent:RMFBUNDLE_NAME]
#define RMFBUNDLE           [NSBundle bundleWithPath:RMFBUNDLE_PATH]

#define RMFBUNDLE_DATA_WITH_FILE_NAME(filename) [NSData dataWithContentsOfFile:[[RMFBUNDLE resourcePath] stringByAppendingPathComponent:filename]]


#pragma mark - macro to check object

#define OBJ_NOT_NIL(obj) ((obj != nil && ![obj isKindOfClass:[NSNull class]]) ? YES : NO)

#define STRING_NOT_NIL(STRING) ((STRING != nil && ![STRING isEqual:[NSNull null]]) ? YES : NO)

#define TRIM_STRING(STRING) [STRING stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]

#define IS_EMPTY_STRING(STRING) ((!STRING_NOT_NIL(STRING) && [TRIM_STRING(STRING) length] == 0) ? YES : NO)


// Logger macros
#define RMF_LOG_INFO(class, method, string)  RMFLogInfo(@"%@ : %@ : %@", class, method, string)
#define RMF_LOG_VERBOSE(class, method, string)  RMFLogVerbose(@"%@ : %@ : %@", class, method, string)
#define RMF_LOG_ERROR(class, method, string)  RMFLogError(@"%@ : %@ : %@", class, method, string)
#define RMF_LOG_WARNING(class, method, string)  RMFLogWarn(@"%@ : %@ : %@", class, method, string)
#define RMF_LOG_DEBUG(class, method, string)  RMFLogDebug(@"%@ : %@ : %@", class, method, string)

#define REQUEST_SUCCESS(info) [NSString stringWithFormat: @"%@ received succesfully", info]
#define REQUEST_FAILURE(info, description) [NSString stringWithFormat: @"Error fetching %@: %@", info, description]

#define ADD_REQUEST_SUCCESS(info) [NSString stringWithFormat: @"New %@ created succesfully", info]
#define ADD_REQUEST_FAILURE(info, description) [NSString stringWithFormat: @"Error creating a new %@: %@", info, description]

#define EDIT_REQUEST_SUCCESS(info) [NSString stringWithFormat: @"%@ edited succesfully", info]
#define EDIT_REQUEST_FAILURE(info, description) [NSString stringWithFormat: @"Error editing %@: %@", info, description]
#define DELETE_REQUEST_FAILURE(info, description) [NSString stringWithFormat: @"Error deleting %@: %@", info, description]


#define SHARED_INSTANCE_CREATED(info) [NSString stringWithFormat: @"Shared instance of %@ created successfully", info]
#define DELETE_REQUEST_SUCCESS(info) [NSString stringWithFormat: @"%@ deleted succesfully", info]
#define DELETE_REQUEST_FAILURE(info, description) [NSString stringWithFormat: @"Error deleting %@: %@", info, description]

#define IMAGE_UPLOAD_FAILURE(info, description) [NSString stringWithFormat: @"Error uploading %@: image: %@", info, description]
#define IMAGE_DOWNLOAD_FAILURE(info, description) [NSString stringWithFormat: @"Error downloading %@: image: %@", info, description]

#define IMAGE_DOWNLOAD_SUCCESS(info) [NSString stringWithFormat: @"%@ image downloaded", info]
#define IMAGE_UPLOAD_SUCCESS(info) [NSString stringWithFormat: @"%@ image uploaded", info]


// Request description keys
#define kSummaryStatistics   @"summary statistics"
#define kEventDetail         @"event detail"
#define kEventList           @"event list"
#define kClientDetails       @"client details"
#define kClientList          @"client list"
#define kWlan                @"wlan"
#define kWlanServiceDetails  @"wlan service detail"
#define kWlanServiceList     @"wlan service list"
#define kWlanStatus          @"wlan status"
#define kAP                  @"AP"
#define kAPImage             @"AP image"
#define kAPDetail            @"AP detail"
#define kAPList              @"AP list"
#define kVenue               @"venue"
#define kVenueDetails        @"venue details"
#define kVenueList           @"venue list"
#define kTenantDetails       @"tenant details"
#define kTenantList          @"tenant list"
#define kKumoController      @"kumo controller"
#define kWarning             @"Warning"
#define kKumoHelper      @"kumo helper"



#pragma mark - Notification plugin & Push Notification

#define NOTIFICATION_KEY            @"key"

#pragma mark - RootAssociation

#define WLAN        @"wlan"
#define ERR_MSG     @"error_message"

#pragma mark - WifiManager

#define WWAN_CONNECTED_ERR      @"internet connected via WWAN"
#define WWAN_ERR_CODE           1
#define NO_INTERNET_ERR         @"not connected to internet"
#define NO_INTERNET_ERR_CODE    2
#define WIFI_CONNECTED_ERR      @"internet connected via WIFI"
#define WIFI_ERR_CODE           3
#define START_INDEX             0

#pragma mark - API's

#define KUMO_QUERY_URL    @"/api/query"

#define ADD_NEW_AP(tenant, venue) [NSString stringWithFormat: @"/api/running/tenants/tenant/%@/venues/venue/%@/aps", tenant, venue]
#define AP_LIST(tenant, venue) [NSString stringWithFormat: @"/api/running/tenants/tenant/%@/venues/venue/%@/aps", tenant, venue]
#define AP_DETAILS(tenant, venue, serialNumber) [NSString stringWithFormat: @"/api/operational/tenants/tenant/%@/venues/venue/%@/aps/ap/%@", tenant, venue, serialNumber]

#define EDIT_AP(tenant, venue) [NSString stringWithFormat: @"/api/running/tenants/tenant/%@/venues/venue/%@/aps", tenant, venue]
#define DELETE_AP(tenant, venue) [NSString stringWithFormat: @"/api/running/tenants/tenant/%@/venues/venue/%@/aps", tenant, venue]


#define EVENT_LIST(tenantName, venueName)     [NSString stringWithFormat: @"/api/operational/tenants/tenant/%@/venues/venue/%@/Event-list", tenantName, venueName]
#define CLIENT_DETAILS(tenantName, venueName)       [NSString stringWithFormat: @"/api/operational/tenants/tenant/%@/clients/client-data/%@", tenantName, macAddress]
#define CLIENT_LIST(tenantName) [NSString stringWithFormat: @"/api/operational/tenants/tenant/%@/clients", tenantName]
#define ADD_WLAN(tenantName) [NSString stringWithFormat: @"/api/running/tenants/tenant/%@/services", tenantName]
#define WLAN_SERVICE_DETAILS(tenantName, wlanServiceName) [NSString stringWithFormat: @"/api/running/tenants/tenant/%@/services/wlan-service/%@", tenantName, wlanServiceName]
#define WLAN_SERVICE_LIST(tenantName) [NSString stringWithFormat: @"/api/running/tenants/tenant/%@/services", tenantName]
#define WLAN_STATUS(tenantName, venueName, serialNumber) [NSString stringWithFormat: @"/api/running/tenants/tenant/%@/venues/venue/%@/aps/ap/%@/wlan-status", tenantName, venueName, serialNumber]
#define ADD_VENUE(tenantName) [NSString stringWithFormat: @"/api/running/tenants/tenant/%@/venues", tenantName]
#define VENUE_DETAILS(tenantName, venueName) [NSString stringWithFormat: @"/api/running/tenants/tenant/%@/venues/venue/%@", tenantName, venueName]
#define VENUE_LIST(tenantName) [NSString stringWithFormat: @"/api/running/tenants/tenant/%@/venues", tenantName]
#define TENANT_DETAILS(tenantName) [NSString stringWithFormat: @"/api/running/tenants/tenant/%@", tenantName]
#define TENANT_LIST @"/api/running/tenants"

// query desc for yang
#define WLAN_LIST_FETCH_URL  @"/tenants/tenant/services/wlan-service"
#define CLINT_LIST_YANG_URL  @"/tenants/tenant/clients/client-data"

// TODO: Update event list fetch url
#define EVENT_LIST_FETCH_URL  @""


#define QUERY_API @"/api/query"
#define QUERY_INDEXED_AP_FOR_ALL_TENANT_VENUE [NSString stringWithFormat:@"/tenants/tenant/venues/venue/aps/ap"]
#define QUERY_INDEXED_AP(tenant, venue) [NSString stringWithFormat: @"/tenants/tenant/%@/venues/venue/%@/aps/ap", tenant, venue]

#define QUERY_INDEXED_WLAN_SERVICE [NSString stringWithFormat:@"/tenants/tenant/services/wlan-service"]


#pragma mark - Logger's Messages
//Wifi manager loggers message

#define WIFI_MANAGER_INIT       @"Wifi manager initialized"
#define SIM_CARD_AVAILABLITY    @"Checking sim card availabilty"
#define WIFI_ENABLED            @"Checking if wifi is enabled or not"
#define INTERNET_VIA_WIFI       @"Checking internet connectivity via Wifi"
#define INTERNET_VIA_WWAN       @"Checking internet connectivity via WWAN"

// System manager messages
#define SHARED_ACTIVE_CONTROLLER    @"shared instance of active controller"
#define SHARED_ACTIVE_PARSER        @"shared instance of active parser"

//Session Provider messages
#define kSessionProvider    @"Session Provider"

//Base Controller

#define CONFIGURATION_URL   @"Configration url is :"
#define BASE_URL            @"Base url is :"
#define WARNING             @"Warning :"
#define ERROR               @"Error :"

//Navigation flow
#define USER_ROLE_ADMIN     @"User role - Admin"
#define USER_ROLE_NORMAL    @"User role - Normal"



//Database Manager
#define kDBManager              @"RMFDB Manager"
#define OBJECT_MODEL_CREATED    @"Creating managed object model"
#define PERSISTANCE_CREATED     @"Creating Persistant store coordinator"
#define MODEL_CONTEXT_CREATED   @"Creating managed object context"
#define DATABASE_INIT           @"initializing DB with name"
#define DATABASE_INIT_PATH      @"initializing DB with path"
#define DATA_CLEANED            @"Core data Cleaned"

#define OBJECT_NIL_ERROR        @"Object is nil"
#define OBJECT_NIL_CODE         1121

//DBACCESS Layer
#define kDBAccess                   @"RMFDBAccessLayer"
#define CLEAN_CACHE_DATA            @"Cache data deleted"
#define DATABASE_MANAGER_CREATED    @"Database manager created"
#define DB_PATH                     @"Path of DB :"
#define DELETE_ALL_NOTIFICATION     @"Deleting all notifications"
#define DELETE_A_NOTIFICATION       @"Deleting a notification"
#define UPDATE_A_NOTIFICATION       @"Updating notifications"
#define SAVE_NOTIFICATION           @"Saving notification object"
#define SAVE_USER                   @"user profile saved"
#define SAVE_USER_ERROR             @"User profile can not be saved"
#define UPDATE_USER                 @"User profile updated sucessfully"
#define UPDATE_USER_ERROR           @"User profile can not be updated"
#define DELETE_USER                 @"User profile deleted sucessfully"

#define ALL_NOTIFICATIONS           @"Notifications are:"
#define FETCH_USER_LOG              @"User is:"
#define USER_INFO_NOT_FETCHED       @"Userinfo not fetched"
#define DELETE_A_ASSOCIATION        @"Updating notifications"
#define SAVE_ASSOCIATION            @"Updating notifications"
#define DELETE_ASSOCIATION          @"Updating notifications"
#define CURRENT                     @"current"

#define kLogInsertAPInDatabase  @"Inserting AP objects in database"
#define kLogGetAPListFromDatabase  @"Fetching AP objects from database"
#define kLogUpdateAPInDatabase  @"Updating AP objects in database"
#define kLogDeleteAPFromDatabase  @"Deleting AP objects from database : "
#define kLogDeleteAllAPFromDatabase  @"Deleting all AP objects from database"
#define kLogUpdateAPImageUrlInDatabase  @"Updating AP image url in database"
#define kLogGetAPImageUrlFromDatabase  @"Fetching AP image url from database"
#define kLogGetAPDetailsFromDatabase  @"Fetching AP detail from database"

#define kLogInsertWLANInDatabase          @"Inserting WLAN objects in database"
#define kLogGetWLANListFromDatabase       @"Fetching WLAN objects from database"
#define kLogUpdateWLANInDatabase          @"Updating WLAN objects in database"
#define kLogDeleteWLANFromDatabase        @"Deleting WLAN objects from database : "
#define kLogDeleteAllWLANFromDatabase     @"Deleting all WLAN objects from database"
#define kLogGetWLANDetailsFromDatabase    @"Fetching WLAN detail from database"

#define kLogInsertEventInDatabase          @"Inserting event objects in database"
#define kLogGetEventListFromDatabase       @"Fetching event objects from database"
#define kLogUpdateEventInDatabase          @"Updating event objects in database"
#define kLogDeleteEventFromDatabase        @"Deleting event objects from database : "
#define kLogDeleteAllEventFromDatabase     @"Deleting all event objects from database"
#define kLogGetEventDetailsFromDatabase    @"Fetching event detail from database"

//Request Manager
#define RESPONSE_SUCCESSFUL         @"Response received successfully"

//Kumo Parser

#define kKumoParser         @"KumoParser"
#define ACCESSPOINT_List    @"Access Point info after parsing response :"
#define EVENT_List           @"Event list after parsing response :"
#define WLAN_List           @"Wlan service list after parsing response :"
#define CLIENT_List           @"Clients list after parsing response :"
#define CLIENT_Details           @"Clients details after parsing response :"
#define DELETE_ACCESSPOINT    @"Delete status after parsing response :"

//Encryption

#define ENCRYPTION_KEY      @"e8ffc7e56311679f12b6fc91aa77a5eb"

#endif
