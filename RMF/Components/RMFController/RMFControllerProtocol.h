/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <Foundation/Foundation.h>

@protocol RMFControllerProtocol <NSObject>

@optional

#pragma mark - Login

// Makes login request with request parameters and returns the network response
- (void)loginRequestWithParameters:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion;

// Makes logout request with request parameters and returns the network response
- (void)logoutRequestWithResponse:(void (^)(NSError *error, id data))completion;

#pragma mark - AP

// Makes network request to fetch AP list with request parameters and returns the network response
- (void)apList:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion;

// Makes network request to fetch filtered and sorted AP list with request parameters and returns the network response
- (void)apList:(NSDictionary *)requestParams sortBy:(int)sort filterOn:(NSDictionary *)filter response:(void (^)(NSError *error, id data))completion;

// Makes network request to fetch AP details with request parameters and returns the network response
- (void)apDetail:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion;

// Makes network request to add new AP with request parameters and returns the network response
- (void)createAP:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion;

// Makes network request to edit AP with request parameters and returns the network response
- (void)editAP:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion;

// Makes network request to delete AP with request parameters and returns the network response
- (void)deleteAP:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion;

#pragma mark - Wlan

// Makes network request to fetch Wlan status with request parameters and returns the network response
- (void)wlanStatus:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion;

// Makes network request to fetch Wlan service list with request parameters and returns the network response
- (void)wlanServiceList:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion;

// Makes network request to fetch Wlan service details with request parameters and returns the network response
- (void)wlanServiceDetails:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion;

// Makes network request to add new Wlan with request parameters and returns the network response
- (void)createWlan:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion;

// Makes network request to delete Wlan with request parameters and returns the network response
- (void)deleteWlan:(NSDictionary *)requestParam response:(void (^)(NSError *error, id data))completion;


// Makes network request to edit Wlan with request parameters and returns the network response
- (void)editWlan:(NSDictionary *)requestParam response:(void (^)(NSError *error, id data))completion;

// Makes network request to fetch Wlan list based on given filtering and sorting criteria
- (void)wlanList:(NSDictionary *)requestParams sortBy:(int)sort filterOn:(NSDictionary *)filter response:(void (^)(NSError *error, id data))completion;

#pragma mark - Events

// Makes network request to fetch event list with request parameters and returns the network response
- (void)eventList:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion;

// Makes network request to fetch event details with request parameters and returns the network response
- (void)eventDetails:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion;

// Create query to fetch event list based on filtering and sorting criteria provided and make network request to fetch it
- (void)eventList:(NSDictionary *)requestParams sortBy:(int)sort filterOn:(NSDictionary *)filter response:(void (^)(NSError *error, id data))completion;

// Creates a new instance of RMFRequestOperationManager initialise with the base URL
- (void)initRequestOperationManager;

#pragma mark - Tenants

// Makes network request to fetch tenant list with request parameters and returns the network response
- (void)tenantListWithresponse:(void (^)(NSError *error, id data))completion;

// Makes network request to fetch tenant details with request parameters and returns the network response
- (void)tenantDetails:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion;

#pragma mark - Venues

// Makes network request to fetch venue list with request parameters and returns the network response
- (void)venueList:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion;

// Makes network request to fetch venue details with request parameters and returns the network response
- (void)venueDetails:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion;

// Makes network request to add new venue with request parameters and returns the network response
- (void)createVenue:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion;

#pragma mark - Client

// Makes network request to fetch Client list based on given filtering and sorting criteria
- (void)clientListFilteredBy:(NSDictionary *)filteringParameters sortBy:(NSArray *)sortingParameters limit:(NSNumber *)limit offset:(NSNumber *)offset response:(void (^)(NSError *, id))completion;

// Makes network request to fetch client list with request parameters and returns the network response
- (void)clientList:(NSDictionary *)requestParams response:(void (^)(NSError *, id))completion;

// Makes network request to fetch client details with request parameters and returns the network response
- (void)clientDetails:(NSDictionary *)requestParams response:(void (^)(NSError *, id))completion;

// Makes network request to fetch active/blocked client list  with request parameters and returns the network response
- (void)clientList:(NSDictionary *)requestParams clientType:(int)clientType response:(void (^)(NSError *error, id data))completion;

#pragma mark - Summary statistics

// Makes network request to fetch summaryStatistics for tenant
- (void)summaryStatisticsForTenant:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion;

// Makes network request to fetch summaryStatistics for var
- (void)summaryStatisticsForVarWithResponse:(void (^)(NSError *error, id data))completion;

// Makes network request to download image
- (void)downloadImage:(NSString *)imageUrl response:(void (^)(NSError *error, id data))completion;

// Makes network request to upload image
- (void)uploadImage:(NSData *)image imageUrl:(NSString *)imageUrl response:(void (^)(NSError *error, id data))completion;

@end
