/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFMacro.h"
#import "RMFUtils.h"
#import "RMFSCGController.h"
#import "RMFConfiguration.h"

#import "RMFLog.h"
#import "RMFLogger.h"

@implementation RMFSCGController
{
	// base url used for making network request on SCG
	NSString *baseURL;
}

// Stores the singleton instance of SCG controller
static RMFSCGController *sharedRMFSCGController = nil;

#pragma mark - Class methods implementation

/*
 * Creates a singleton instance of RMFScgController
 */
+ (RMFSCGController *)sharedRMFSCGController {
	if (!OBJ_NOT_NIL(sharedRMFSCGController)) {
		sharedRMFSCGController = [[RMFSCGController alloc] init];
	}

	return sharedRMFSCGController;
}

#pragma mark - Instance method implementation

/*
 * initialise the singleton instance of RMFScgController
 */
- (RMFSCGController *)init {
	if (!OBJ_NOT_NIL(sharedRMFSCGController)) {
		sharedRMFSCGController = [super init];
		if (sharedRMFSCGController) {
			[self initRequestOperationManager];
		}
	}
	return sharedRMFSCGController;
}

/*
 * Get an instance of requestOperationManager and initialise it with the base url
 */

- (void)initRequestOperationManager {
	if (sharedRMFSCGController) {
		if ([self createBaseUrl]) {
			// If new base url is same as the existing url then don't do anything
			if (![baseURL isEqualToString:[self createBaseUrl]]) {
				baseURL = [self createBaseUrl];
				RMFConfiguration *config = [RMFConfiguration sharedRMFConfiguration];
				NSString *contentType = [config contentTypeForActiveController];

				sharedRMFSCGController.requestOperationManager = [[RMFRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];

				if (OBJ_NOT_NIL(sharedRMFSCGController.requestOperationManager)) {
					NSString *logDescription = [NSString stringWithFormat:@"Request operation manager : %@", sharedRMFSCGController.requestOperationManager];
					RMF_LOG_DEBUG([self class], NSStringFromSelector(_cmd), logDescription);

					// Set request serializer
					sharedRMFSCGController.requestOperationManager.requestSerializer = [AFHTTPRequestSerializer serializer];

					// Set response serializer
					sharedRMFSCGController.requestOperationManager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
					sharedRMFSCGController.requestOperationManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:contentType];
				}
			}
		}
		else {
			NSString *logDescription = [NSString stringWithFormat:@" %@ %@", WARNING, kBaseUrlNotFound];
			RMF_LOG_WARNING([self class], NSStringFromSelector(_cmd), logDescription);
			[RMFUtils raiseException:NSInternalInconsistencyException WithDescription:kBaseUrlNotFound];
		}
	}
}

@end
