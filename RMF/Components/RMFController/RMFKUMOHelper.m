/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */


#import "RMFKUMOHelper.h"
#import "RMFMacro.h"
#import "RMFUtils.h"

@implementation RMFKUMOHelper

// Stores the singleton instance of KUMO Helper
static RMFKUMOHelper *sharedRMFKUMOHelper = nil;

#pragma mark - Class methods implementation

/*
 * Creates a singleton instance of RMFKUMOHelper
 */

+ (RMFKUMOHelper *)sharedRMFKUMOHelper {
	if (!OBJ_NOT_NIL(sharedRMFKUMOHelper)) {
		sharedRMFKUMOHelper = [[RMFKUMOHelper alloc] init];
		RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), SHARED_INSTANCE_CREATED(kKumoHelper));
	}
	return sharedRMFKUMOHelper;
}

#pragma mark - Instance method implementation

/*
 * initialise the singleton instance of RMFKUMOHelper
 */

- (RMFKUMOHelper *)init {
	if (!OBJ_NOT_NIL(sharedRMFKUMOHelper)) {
		sharedRMFKUMOHelper = [super init];
	}
	return sharedRMFKUMOHelper;
}

#pragma mark - Common query APIs

/*
 * Creates foreach statement
 */
- (NSString *)foreachStatement:(NSDictionary *)filter {
	NSString *foreach = QUERY_INDEXED_AP_FOR_ALL_TENANT_VENUE;
	if (OBJ_NOT_NIL(filter) && [filter count]) {
		NSArray *filterKey = [filter allKeys];
		NSMutableArray *filterArray = [[NSMutableArray alloc] init];
		for (NSString *key in filterKey) {
			[filterArray addObject:[NSString stringWithFormat:@"%@ = '%@'", key, [filter objectForKey:key]]];
		}
		NSString *joinedFilterString = [filterArray componentsJoinedByString:@","];
		foreach = [NSString stringWithFormat:@"%@[%@]", QUERY_INDEXED_AP_FOR_ALL_TENANT_VENUE, joinedFilterString];
	}
	return foreach;
}

/*
 * Creates select statement
 */
- (NSMutableArray *)selectStatementWithLabel:(NSArray *)label expression:(NSArray *)expression resultType:(NSArray *)resultType filterString:(NSString *)filter {
	NSMutableArray *selectStatementsArray = nil;

	if (OBJ_NOT_NIL(label) && OBJ_NOT_NIL(resultType) && OBJ_NOT_NIL(expression) && [label count] == [resultType count] && [label count] == [expression count]) {
		// Allocate memory to hold select statement array
		selectStatementsArray = [[NSMutableArray alloc] init];

		for (int index = 0; index < [label count]; index++) {
			NSMutableDictionary *selectStatementDictionary = [[NSMutableDictionary alloc] init];

			// If filter is available add it to the expression
			if (filter) {
				[selectStatementDictionary setObject:[NSString stringWithFormat:@"%@%@", [expression objectAtIndex:index], filter] forKey:kSelectParametersExpression];
			}
			// Create expression without filter
			else {
				[selectStatementDictionary setObject:[expression objectAtIndex:index] forKey:kSelectParametersExpression];
			}
			// Add label
			[selectStatementDictionary setObject:[label objectAtIndex:index] forKey:kSelectParametersLabel];

			// Add result type
			[selectStatementDictionary setObject:[resultType objectAtIndex:index] forKey:kSelectParametersResultType];

			// Add select this statement to the select statements array
			[selectStatementsArray addObject:selectStatementDictionary];
		}
	}
	return selectStatementsArray;
}

/*
 * Creates query
 */
- (NSDictionary *)createQuery:(NSString *)foreach select:(NSArray *)select sortBy:(NSArray *)sortBy limit:(NSNumber *)limit offset:(NSNumber *)offset {
	NSMutableDictionary *queryDictionary = nil;
	NSMutableDictionary *queryParam = [[NSMutableDictionary alloc] init];

	// Add foreach statement
	if (foreach) {
		[queryParam setObject:foreach forKey:kForeach];
	}

	// Add select statement
	if (select) {
		[queryParam setObject:select forKey:kSelect];

		// Add sort by statement
		if (sortBy) {
			[queryParam setObject:sortBy forKey:kSortBy];
		}

		// Add limit and offset value
		if (limit && offset) {
			[queryParam setObject:limit forKey:kLimit];
			[queryParam setObject:offset forKey:kOffset];
		}

		// Create query dictionary
		queryDictionary = [[NSMutableDictionary alloc] init];

		if (queryParam) {
			[queryDictionary setObject:queryParam forKey:kStartQuery];
		}
	}

	return queryDictionary;
}

@end
