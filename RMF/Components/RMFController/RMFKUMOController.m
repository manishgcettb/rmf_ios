/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFMacro.h"
#import "RMFUtils.h"
#import "RMFKUMOController.h"
#import "RMFConfiguration.h"
#import "RMFSystemManager.h"
#import "RMFLogger.h"
#import "RMFSessionProvider.h"
#import "RMFLog.h"
#import "RMFErrorHandler.h"
#import "RMFAPPlugin.h"
#import "RMFKUMOSandboxController.h"
#import "RMFKUMOHelper.h"
#import "RMFWlanPlugin.h"
#import "RMFAPClientPlugin.h"

#define IS_SANDBOX_SUPPORTED_ENABLED ([RMFConfiguration sharedRMFConfiguration].isSandboxEnabled)

@interface RMFKUMOController ()
@property (nonatomic, strong) RMFSessionProvider *sessionProvider;
@end

@implementation RMFKUMOController
{
	// base url used for making network request on KUMO
	NSString *baseURL;
}

// Stores the singleton instance of KUMO controller
static RMFKUMOController *sharedRMFKUMOController = nil;

#pragma mark - Class methods implementation

/*
 * Creates a singleton instance of RMFKUMOController
 */

+ (RMFKUMOController *)sharedRMFKUMOController {
	if (!OBJ_NOT_NIL(sharedRMFKUMOController)) {
		sharedRMFKUMOController = [[RMFKUMOController alloc] init];
		RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), SHARED_INSTANCE_CREATED(kKumoController));
	}
	return sharedRMFKUMOController;
}

#pragma mark - Instance method implementation

/*
 * initialize the singleton instance of RMFKUMOController
 */

- (RMFKUMOController *)init {
	if (!OBJ_NOT_NIL(sharedRMFKUMOController)) {
		sharedRMFKUMOController = [super init];

		if (sharedRMFKUMOController) {
			self.sessionProvider = [RMFSessionProvider sharedSessionProvider];
			[self initRequestOperationManager];
		}
	}
	return sharedRMFKUMOController;
}

/*
 * Get an instance of requestOperationManager and initialise it with the base url
 */

- (void)initRequestOperationManager {
	if (sharedRMFKUMOController) {
		if ([self createBaseUrl]) {
			// If new base url is same as the existing url then don't do anything
			if (![baseURL isEqualToString:[self createBaseUrl]]) {
				baseURL = [self createBaseUrl];
				RMFConfiguration *config = [RMFConfiguration sharedRMFConfiguration];
				NSString *contentType = [config contentTypeForActiveController];

				sharedRMFKUMOController.requestOperationManager = [[RMFRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];

				if (OBJ_NOT_NIL(sharedRMFKUMOController.requestOperationManager)) {
					RMFLogInfo(@"%@ : %@ : %@", [self class],
					           NSStringFromSelector(_cmd),
					           [NSString stringWithFormat:@"KUMO request Operation Manager : %@", (sharedRMFKUMOController.requestOperationManager)]);

					// Set request serializer
					sharedRMFKUMOController.requestOperationManager.requestSerializer = [AFJSONRequestSerializer serializer];
					[sharedRMFKUMOController.requestOperationManager.requestSerializer setValue:contentType forHTTPHeaderField:kAcceptKey];
					[sharedRMFKUMOController.requestOperationManager.requestSerializer setValue:contentType forHTTPHeaderField:kContentTypeKey];

					//Remove this once login API is implemented
					[sharedRMFKUMOController.requestOperationManager.requestSerializer setAuthorizationHeaderFieldWithUsername:self.sessionProvider.userName password:self.sessionProvider.password];

					// Set response serializer
					sharedRMFKUMOController.requestOperationManager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
					sharedRMFKUMOController.requestOperationManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:kContentTypeJson, contentType, nil];
				}
			}
		}
		else {
			RMFLogWarn(@"%@ : %@", kWarning, kBaseUrlNotFound);
			[RMFUtils raiseException:NSInternalInconsistencyException WithDescription:kBaseUrlNotFound];
		}
	}
}

#pragma mark - Login

/*
 * Creates network request to perform login operation on KUMO Controller
 */

- (void)loginRequestWithParameters:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion {
	//TODO: Implement login request for KUMO
	if (!IS_SANDBOX_SUPPORTED_ENABLED) {
	}
	else {
		// Mocked APIs response to be exposed...
	}
}

/*
 * Creates network request to perform logout operation on KUMO Controller
 */

- (void)logoutRequestWithResponse:(void (^)(NSError *error, id data))completion {
	//TODO: Implement logout request for KUMO
	if (!IS_SANDBOX_SUPPORTED_ENABLED) {
	}
	else {
		// Mocked APIs response to be exposed...
	}
}

#pragma mark - tenant

/*
 * Creates network request to fetch tenant list
 */

- (void)tenantListWithresponse:(void (^)(NSError *error, id data))completion {
	if (!IS_SANDBOX_SUPPORTED_ENABLED) {
		[self.requestOperationManager get:TENANT_LIST withParameters:nil andResponse: ^(NSError *error, id data) {
		    if (OBJ_NOT_NIL(completion)) {
		        completion(error, data);

		        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), REQUEST_SUCCESS(kTenantList));
			}
		}];
	}
	else {
		// Mocked APIs response to be exposed...
	}
}

/*
 * Creates network request to fetch tenant's details
 */

- (void)tenantDetails:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion {
	NSError *error = nil;
	if (requestParams) {
		NSString *tenantName = [requestParams objectForKey:kTenantNameKey];
		if (tenantName) {
			if (!IS_SANDBOX_SUPPORTED_ENABLED) {
				[self.requestOperationManager get:TENANT_DETAILS(tenantName) withParameters:nil andResponse: ^(NSError *error, id data) {
				    if (OBJ_NOT_NIL(completion)) {
				        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), REQUEST_SUCCESS(kTenantDetails));
				        completion(error, data);
					}
				}];
			}
			else {
				// Mocked APIs to be exposed...
			}
		}
		else {
			error = [RMFUtils errorWithCode:RMFCodeForTenantNameNotFound];
			if (OBJ_NOT_NIL(completion)) {
				RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kTenantDetails, error.description));
				completion(error, nil);
			}
		}
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForRequestParamNotFound];
		if (OBJ_NOT_NIL(completion)) {
			RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kTenantDetails, error.description));

			completion(error, nil);
		}
	}
}

#pragma mark - venue

/*
 * Creates network request to fetch venue list
 */

- (void)venueList:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion {
	NSError *error = nil;
	if (requestParams) {
		NSString *tenantName = [requestParams objectForKey:kTenantNameKey];
		if (tenantName) {
			if (!IS_SANDBOX_SUPPORTED_ENABLED) {
				[self.requestOperationManager get:VENUE_LIST(tenantName) withParameters:nil andResponse: ^(NSError *error, id data) {
				    if (OBJ_NOT_NIL(completion)) {
				        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), REQUEST_SUCCESS(kVenueList));
				        completion(error, data);
					}
				}];
			}
			else {
				// Mocked APIs to be exposed...
			}
		}
		else {
			error = [RMFUtils errorWithCode:RMFCodeForTenantNameNotFound];
			if (OBJ_NOT_NIL(completion)) {
				RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kVenueList, error.description));
				completion(error, nil);
			}
		}
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForRequestParamNotFound];
		if (OBJ_NOT_NIL(completion)) {
			RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kVenueList, error.description));
			completion(error, nil);
		}
	}
}

/*
 * Creates network request to fetch venue detail
 */

- (void)venueDetails:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion {
	NSError *error = nil;
	if (requestParams) {
		NSString *tenantName = [requestParams objectForKey:kTenantNameKey];
		NSString *venueName = [requestParams objectForKey:kVenueNameKey];

		if (tenantName && venueName) {
			if (!IS_SANDBOX_SUPPORTED_ENABLED) {
				[self.requestOperationManager get:VENUE_DETAILS(tenantName, venueName) withParameters:nil andResponse: ^(NSError *error, id data) {
				    if (OBJ_NOT_NIL(completion)) {
				        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), REQUEST_SUCCESS(kVenueDetails));
				        completion(error, data);
					}
				}];
			}
			else {
				// Mocked APIs response to be shared...
			}
		}
		else {
			error = [RMFUtils errorWithCode:RMFCodeForTenantAndVenueNameNotFound];
			if (OBJ_NOT_NIL(completion)) {
				RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kVenueDetails, error.description));
				completion(error, nil);
			}
		}
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForRequestParamNotFound];
		if (OBJ_NOT_NIL(completion)) {
			RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kVenueDetails, error.description));
			completion(error, nil);
		}
	}
}

/*
 * Creates network request to add new venue
 */

- (void)createVenue:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion {
	NSError *error = nil;
	if (requestParams) {
		NSString *tenantName = [requestParams objectForKey:kTenantNameKey];
		NSMutableDictionary *venueInfoDictionary = [requestParams objectForKey:kNewVenueInfoKey];

		if (tenantName && venueInfoDictionary) {
			if (!IS_SANDBOX_SUPPORTED_ENABLED) {
				[self.requestOperationManager post:ADD_VENUE(tenantName) withParameters:venueInfoDictionary andResponse: ^(NSError *error, id data) {
				    if (OBJ_NOT_NIL(completion)) {
				        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), ADD_REQUEST_SUCCESS(kVenue));
				        completion(error, data);
					}
				}];
			}
			else {
				// Mocked APIs response to be shared...
			}
		}
		else {
			error = [RMFUtils errorWithCode:RMFCodeForTenantAndVenueInfoNotFound];
			if (OBJ_NOT_NIL(completion)) {
				RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), ADD_REQUEST_FAILURE(kVenue, error.description));
				completion(error, nil);
			}
		}
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForRequestParamNotFound];
		if (OBJ_NOT_NIL(completion)) {
			RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), ADD_REQUEST_FAILURE(kVenue, error.description));
			completion(error, nil);
		}
	}
}

#pragma mark - AP

/*
 * Creates network request to fetch AP list
 */
- (void)apList:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion {
	NSError *error = nil;
	if (requestParams) {
		NSString *tenantName = [requestParams objectForKey:kTenantNameKey];
		NSString *venueName = [requestParams objectForKey:kVenueNameKey];

		if (tenantName && venueName) {
			if (!IS_SANDBOX_SUPPORTED_ENABLED) {
				[self.requestOperationManager get:AP_LIST(tenantName, venueName) withParameters:nil andResponse: ^(NSError *error, id data) {
				    if (OBJ_NOT_NIL(completion)) {
				        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), REQUEST_SUCCESS(kAPList));
				        completion(error, data);
					}
				}];
			}
			else {
				// Mocked APIs response
				[[RMFKUMOSandboxController sharedInstance] apList: ^(NSError *error, id data) {
				    if (OBJ_NOT_NIL(completion)) {
				        completion(error, data);
					}
				}];
			}
		}
		else {
			error = [RMFUtils errorWithCode:RMFCodeForTenantAndVenueNameNotFound];

			if (OBJ_NOT_NIL(completion)) {
				RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kAPList, error.description));
				completion(error, nil);
			}
		}
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForRequestParamNotFound];
		if (OBJ_NOT_NIL(completion)) {
			RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kAPList, error.description));
			completion(error, nil);
		}
	}
}

/*
 * Creates network request to fetch AP list
 */
- (void)apList:(NSDictionary *)requestParams sortBy:(int)sort filterOn:(NSDictionary *)filter response:(void (^)(NSError *error, id data))completion {
	NSError *error = nil;
	NSString *tenantName = nil;
	NSString *venueName = nil;

#pragma unused(tenantName)
#pragma unused(venueName)

	NSNumber *limit = nil;
	NSNumber *offset = nil;


	if (requestParams) {
		tenantName = [requestParams objectForKey:kTenantNameKey];
		venueName = [requestParams objectForKey:kVenueNameKey];
		limit = [requestParams objectForKey:kLimit];
		offset = [requestParams objectForKey:kOffset];
	}

	// Create foreach string
	NSString *foreach = [[RMFKUMOHelper sharedRMFKUMOHelper] foreachStatement:filter];

	// Create select query string
	NSArray *selectList = [[RMFKUMOHelper sharedRMFKUMOHelper] selectStatementWithLabel:[NSArray arrayWithObjects:kSelectLabelName, kSelectLabelSerialNumber, kSelectLabelMacAddress, kSelectLabelSSID, kSelectLabelWLANCount, nil] expression:[NSArray arrayWithObjects:kSelectExpressionName, kSelectExpressionSerialNumber, kSelectExpressionMacAddress, kSelectExpressionSSID, kSelectExpressionWLANCount, nil] resultType:[NSArray arrayWithObjects:@[kSelectResultTypeString], @[kSelectResultTypeString], @[kSelectResultTypeString], @[kSelectResultTypeString], @[kSelectResultTypeString], nil] filterString:nil];

	// Create sorted array
	NSArray *sortedBy = nil;
	switch (sort) {
		case kSortingParameterName:
			sortedBy = [NSArray arrayWithObjects:kAPSortByName, nil];
			break;

		case kSortingParameterMacAddress:
			sortedBy = [NSArray arrayWithObjects:kAPSortByMacAddress, nil];
			break;

		case kSortingParameterSerialNumber:
			sortedBy = [NSArray arrayWithObjects:kAPSortBySerialNumber, nil];
			break;

		case kSortingParameterWlanCount:
			sortedBy = [NSArray arrayWithObjects:kAPSortByWlanCount, nil];
			break;
	}

	// Create final query
	NSDictionary *query = [[RMFKUMOHelper sharedRMFKUMOHelper] createQuery:foreach select:selectList sortBy:sortedBy limit:limit offset:offset];

	if (!IS_SANDBOX_SUPPORTED_ENABLED) {
		[self fetchHandleResponseAndData:query response: ^(NSError *error, id data) {
		    if (OBJ_NOT_NIL(completion)) {
		        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), REQUEST_SUCCESS(kAPList));
		        completion(error, data);
			}
		}];
	}
	else {
		// Mocked APIs response
		[[RMFKUMOSandboxController sharedInstance] apList: ^(NSError *error, id data) {
		    if (OBJ_NOT_NIL(completion)) {
		        completion(error, data);
			}
		}];
	}
}

/*
 * Creates network request to fetch AP details
 */
- (void)apDetail:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion {
	NSError *error = nil;
	if (requestParams) {
		NSString *tenantName = [requestParams objectForKey:kTenantNameKey];
		NSString *venueName = [requestParams objectForKey:kVenueNameKey];
		NSString *serialNumber = [requestParams objectForKey:kSerialNumberKey];

		if (tenantName && venueName && serialNumber) {
			if (!IS_SANDBOX_SUPPORTED_ENABLED) {
				[self.requestOperationManager get:AP_DETAILS(tenantName, venueName, serialNumber) withParameters:nil andResponse: ^(NSError *error, id data) {
				    if (OBJ_NOT_NIL(completion)) {
				        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), REQUEST_SUCCESS(kAPDetail));
				        completion(error, data);
					}
				}];
			}
			else {
				// Mocked APIs response
				[[RMFKUMOSandboxController sharedInstance] apDetail: ^(NSError *error, id data) {
				    if (OBJ_NOT_NIL(completion)) {
				        completion(error, data);
					}
				}];
			}
		}
		else {
			error = [RMFUtils errorWithCode:RMFCodeForTenantAndVenueNameAndSerialNumberNotFound];
			if (OBJ_NOT_NIL(completion)) {
				RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kAPDetail, error.description));
				completion(error, nil);
			}
		}
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForRequestParamNotFound];
		if (OBJ_NOT_NIL(completion)) {
			RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kAPDetail, error.description));
			completion(error, nil);
		}
	}
}

/*
 * Creates network request to add new AP
 */
- (void)createAP:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion {
	NSError *error = nil;
	if (requestParams) {
		NSString *tenantName = [requestParams objectForKey:kTenantNameKey];
		NSString *venueName = [requestParams objectForKey:kVenueNameKey];
		NSDictionary *serialNumber = [NSDictionary dictionaryWithObjectsAndKeys:[requestParams objectForKey:kSerialNumberKey], kSerialNumberKey, nil];
		NSDictionary *apInfoDictionary = [NSDictionary dictionaryWithObjectsAndKeys:serialNumber, kAPKey, nil];

		if (tenantName && apInfoDictionary && venueName) {
			if (!IS_SANDBOX_SUPPORTED_ENABLED) {
				[self.requestOperationManager post:ADD_NEW_AP(tenantName, venueName) withParameters:apInfoDictionary andResponse: ^(NSError *error, id data) {
				    if (OBJ_NOT_NIL(completion)) {
				        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), ADD_REQUEST_SUCCESS(kAP));
				        completion(error, data);
					}
				}];
			}
			else {
				// Mocked APIs response...
			}
		}
		else {
			error = [RMFUtils errorWithCode:RMFCodeForTenantAndVenueNameAndAPInfoNotFound];
			if (OBJ_NOT_NIL(completion)) {
				RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), ADD_REQUEST_FAILURE(kAP, error.description));
				completion(error, nil);
			}
		}
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForRequestParamNotFound];
		if (OBJ_NOT_NIL(completion)) {
			RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), ADD_REQUEST_FAILURE(kAP, error.description));
			completion(error, nil);
		}
	}
}

/*
 * Creates network request to edit AP
 */
- (void)editAP:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion {
	NSError *error = nil;

	if (requestParams) {
		NSString *tenantName = [requestParams objectForKey:kTenantNameKey];
		NSString *venueName = [requestParams objectForKey:kVenueNameKey];
		NSDictionary *serialNumber = [NSDictionary dictionaryWithObjectsAndKeys:[requestParams objectForKey:kSerialNumberKey], kSerialNumberKey, nil];
		NSDictionary *apInfoDictionary = [NSDictionary dictionaryWithObjectsAndKeys:serialNumber, kAPKey, nil];

		if (tenantName && apInfoDictionary && venueName) {
			if (!IS_SANDBOX_SUPPORTED_ENABLED) {
				[self.requestOperationManager post:EDIT_AP(tenantName, venueName) withParameters:apInfoDictionary andResponse: ^(NSError *error, id data) {
				    if (OBJ_NOT_NIL(completion)) {
				        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), EDIT_REQUEST_SUCCESS(kAP));
				        completion(error, data);
					}
				}];
			}
			else {
				// Mocked APIs response...
			}
		}
		else {
			error = [RMFUtils errorWithCode:RMFCodeForTenantAndVenueNameAndAPInfoNotFound];
			if (OBJ_NOT_NIL(completion)) {
				RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), EDIT_REQUEST_FAILURE(kAP, error.description));
				completion(error, nil);
			}
		}
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForRequestParamNotFound];
		if (OBJ_NOT_NIL(completion)) {
			RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), ADD_REQUEST_FAILURE(kAP, error.description));
			completion(error, nil);
		}
	}
}

/*
 * Creates network request to delete AP
 */
- (void)deleteAP:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion {
	NSError *error = nil;

	if (requestParams) {
		NSString *tenantName = [requestParams objectForKey:kTenantNameKey];
		NSString *venueName = [requestParams objectForKey:kVenueNameKey];
		NSDictionary *serialNumber = [NSDictionary dictionaryWithObjectsAndKeys:[requestParams objectForKey:kSerialNumberKey], kSerialNumberKey, nil];
		NSDictionary *apInfoDictionary = [NSDictionary dictionaryWithObjectsAndKeys:serialNumber, kAPKey, nil];

		if (tenantName && apInfoDictionary && venueName) {
			if (!IS_SANDBOX_SUPPORTED_ENABLED) {
				[self.requestOperationManager post:DELETE_AP(tenantName, venueName) withParameters:apInfoDictionary andResponse: ^(NSError *error, id data) {
				    if (OBJ_NOT_NIL(completion)) {
				        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), DELETE_REQUEST_SUCCESS(kAP));
				        completion(error, data);
					}
				}];
			}
			else {
			}
		}
		else {
			error = [RMFUtils errorWithCode:RMFCodeForTenantAndVenueNameAndAPInfoNotFound];
			if (OBJ_NOT_NIL(completion)) {
				RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), DELETE_REQUEST_FAILURE(kAP, error.description));
				completion(error, nil);
			}
		}
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForRequestParamNotFound];
		if (OBJ_NOT_NIL(completion)) {
			RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), ADD_REQUEST_FAILURE(kAP, error.description));
			completion(error, nil);
		}
	}
}

#pragma mark - Image Upload/Download

/*
 * Creates network request to download image
 */

- (void)downloadImage:(NSString *)imageUrl response:(void (^)(NSError *error, id data))completion {
	NSError *error = nil;

	if (imageUrl) {
		//TODO: implement network request to download image
	}
	else {
		if (completion) {
			RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), IMAGE_DOWNLOAD_FAILURE(kEmptyText, error.description));
			error = [RMFUtils errorWithCode:RMFCodeForImageUrlNotFound];
			completion(error, nil);
		}
	}
}

/*
 * Creates network request to upload image
 */

- (void)uploadImage:(NSData *)image imageUrl:(NSString *)imageUrl response:(void (^)(NSError *error, id data))completion {
	NSError *error = nil;

	if (imageUrl && image) {
		//TODO: implement network request to upload image
	}
	else {
		if (completion) {
			RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), IMAGE_UPLOAD_FAILURE(kEmptyText, error.description));
			error = [RMFUtils errorWithCode:RMFCodeForImageAndUrlNotFound];
			completion(error, nil);
		}
	}
}

#pragma mark - wlan

/*
 * Creates network request to fetch Wlan status
 */

- (void)wlanStatus:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion {
	NSError *error = nil;
	if (requestParams) {
		NSString *tenantName = [requestParams objectForKey:kTenantNameKey];
		NSString *venueName = [requestParams objectForKey:kVenueNameKey];
		NSString *serialNumber = [requestParams objectForKey:kSerialNumberKey];

		if (tenantName && venueName && serialNumber) {
			if (!IS_SANDBOX_SUPPORTED_ENABLED) {
				[self.requestOperationManager get:WLAN_STATUS(tenantName, venueName, serialNumber) withParameters:nil andResponse: ^(NSError *error, id data) {
				    if (OBJ_NOT_NIL(completion)) {
				        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), REQUEST_SUCCESS(kWlanStatus));
				        completion(error, data);
					}
				}];
			}
			else {
				// Mocked APIs response
				[[RMFKUMOSandboxController sharedInstance] wlanStatus: ^(NSError *error, id data) {
				    if (OBJ_NOT_NIL(completion)) {
				        completion(error, data);
					}
				}];
			}
		}
		else {
			error = [RMFUtils errorWithCode:RMFCodeForTenantAndVenueNameAndSerialNumberNotFound];
			if (OBJ_NOT_NIL(completion)) {
				RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kWlanStatus, error.description));
				completion(error, nil);
			}
		}
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForRequestParamNotFound];
		if (OBJ_NOT_NIL(completion)) {
			RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kWlanStatus, error.description));
			completion(error, nil);
		}
	}
}

/*
 * Creates network request to fetch wlan service list
 */

- (void)wlanServiceList:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion {
	NSError *error = nil;
	if (requestParams) {
		NSString *tenantName = [requestParams objectForKey:kTenantNameKey];

		if (tenantName) {
			if (!IS_SANDBOX_SUPPORTED_ENABLED) {
				[self.requestOperationManager get:WLAN_SERVICE_LIST(tenantName) withParameters:nil andResponse: ^(NSError *error, id data) {
				    if (OBJ_NOT_NIL(completion)) {
				        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), REQUEST_SUCCESS(kWlanServiceList));
				        completion(error, data);
					}
				}];
			}
			else {
				// Mocked APIs response
				[[RMFKUMOSandboxController sharedInstance] wlanList: ^(NSError *error, id data) {
				    if (OBJ_NOT_NIL(completion)) {
				        completion(error, data);
					}
				}];
			}
		}
		else {
			error = [RMFUtils errorWithCode:RMFCodeForTenantNameNotFound];
			if (OBJ_NOT_NIL(completion)) {
				if (completion) {
					RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kWlanServiceList, error.description));
					completion(error, nil);
				}
			}
		}
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForRequestParamNotFound];
		if (OBJ_NOT_NIL(completion)) {
			RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kWlanServiceList, error.description));
			completion(error, nil);
		}
	}
}

/*
 * Creates network request to fetch wlan service details
 */

- (void)wlanServiceDetails:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion {
	NSError *error = nil;
	if (requestParams) {
		NSString *tenantName = [requestParams objectForKey:kTenantNameKey];
		NSString *wlanServiceName = [requestParams objectForKey:kWlanNameKey];

		if (tenantName && wlanServiceName) {
			if (!IS_SANDBOX_SUPPORTED_ENABLED) {
				[self.requestOperationManager get:WLAN_SERVICE_DETAILS(tenantName, wlanServiceName) withParameters:nil andResponse: ^(NSError *error, id data) {
				    if (OBJ_NOT_NIL(completion)) {
				        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), REQUEST_SUCCESS(kWlanServiceDetails));
				        completion(error, data);
					}
				}];
			}
			else {
				// Mocked APIs response
				[[RMFKUMOSandboxController sharedInstance] wlanDetails: ^(NSError *error, id data) {
				    if (OBJ_NOT_NIL(completion)) {
				        completion(error, data);
					}
				}];
			}
		}
		else {
			error = [RMFUtils errorWithCode:RMFCodeForTenantAndWlanNameNotFound];
			if (OBJ_NOT_NIL(completion)) {
				RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kWlanServiceDetails, error.description));
				completion(error, nil);
			}
		}
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForRequestParamNotFound];
		if (OBJ_NOT_NIL(completion)) {
			RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kWlanServiceDetails, error.description));
			completion(error, nil);
		}
	}
}

/*
 * Creates network request to add new wlan
 */

- (void)createWlan:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion {
	NSError *error = nil;
	if (requestParams) {
		NSString *tenantName = [requestParams objectForKey:kTenantNameKey];
		NSMutableDictionary *wlanInfoDictionary = [requestParams objectForKey:kNewWlanInfoKey];

		if (tenantName && wlanInfoDictionary) {
			if (!IS_SANDBOX_SUPPORTED_ENABLED) {
				[self.requestOperationManager post:ADD_WLAN(tenantName) withParameters:wlanInfoDictionary andResponse: ^(NSError *error, id data) {
				    if (OBJ_NOT_NIL(completion)) {
				        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), ADD_REQUEST_SUCCESS(kWlan));
				        completion(error, data);
					}
				}];
			}
			else {
				// Mocked APIs to be exposed...
			}
		}
		else {
			error = [RMFUtils errorWithCode:RMFCodeForTenantAndWlanInfoNotFound];
			if (OBJ_NOT_NIL(completion)) {
				RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), ADD_REQUEST_FAILURE(kWlan, error.description));
				completion(error, nil);
			}
		}
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForRequestParamNotFound];
		if (OBJ_NOT_NIL(completion)) {
			RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), ADD_REQUEST_FAILURE(kWlan, error.description));
			completion(error, nil);
		}
	}
}

/*
 * Creates network request to delete wlan
 */

- (void)deleteWlan:(NSDictionary *)requestParam response:(void (^)(NSError *error, id data))completion {
	NSError *error = nil;
	if (requestParam) {
		NSString *wlanName = [requestParam objectForKey:kWlanNameKey];

		if (wlanName) {
			//TODO: implement network requet to delete wlan
		}
		else {
			error = [RMFUtils errorWithCode:RMFCodeForWlanNameNotFound];
			if (completion) {
				RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), DELETE_REQUEST_FAILURE(kWlan, error.description));
				completion(error, nil);
			}
		}
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForRequestParamNotFound];
		if (completion) {
			RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), DELETE_REQUEST_FAILURE(kWlan, error.description));
			completion(error, nil);
		}
	}
}

/*
 * Creates network request to edit wlan
 */

- (void)editWlan:(NSDictionary *)requestParam response:(void (^)(NSError *error, id data))completion {
	NSError *error = nil;
	if (requestParam) {
		NSString *wlanName = [requestParam objectForKey:kWlanNameKey];
		NSString *tenantName = [requestParam objectForKey:kTenantNameKey];
		NSString *ssid = [requestParam objectForKey:kWlanSssidKey];
		NSString *wlanType = [requestParam objectForKey:kWlanTypeKey];
		NSString *wlanSecurity = [requestParam objectForKey:kWlanSecurityKey];
		NSString *pskPassphrase = [requestParam objectForKey:kPskPassPhraseKey];

		if (wlanName && tenantName && ssid && wlanType && wlanSecurity && pskPassphrase) {
			//TODO: implement network requet to edit wlan
		}
		else {
			error = [RMFUtils errorWithCode:RMFCodeForTenantAndWlanInfoNotFound];
			if (completion) {
				RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), EDIT_REQUEST_FAILURE(kWlan, error.description));
				completion(error, nil);
			}
		}
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForRequestParamNotFound];
		if (completion) {
			RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), EDIT_REQUEST_FAILURE(kWlan, error.description));
			completion(error, nil);
		}
	}
}

#pragma mark - client Methods

/*
 * Create query to fetch wlan list based on filtering and sorting riteria provided and make network
 * request to fetch it
 */

- (void)clientListFilteredBy:(NSDictionary *)filteringParameters sortBy:(NSArray *)sortingParameters limit:(NSNumber *)limit offset:(NSNumber *)offset response:(void (^)(NSError *, id))completion {
	NSString *filterString = kEmptyText;
	NSString *forEachStatement = nil;

	// Create filter string
	if (filteringParameters) {
		NSString *macAddressFilter = nil;
		NSString *ipAddressFilter = nil;
		NSString *wlanFilter = nil;
		NSString *osTypeFilter = nil;
		NSString *statusFilter = nil;

		if ([filteringParameters objectForKey:kClientMacAddressKey]) {
			macAddressFilter = [NSString stringWithFormat:@"%@=%@", kClientMacAddressKey, [filteringParameters objectForKey:kClientMacAddressKey]];
		}

		if ([filteringParameters objectForKey:kClientIPKey]) {
			ipAddressFilter = [NSString stringWithFormat:@"%@=%@", kClientIPKey, [filteringParameters objectForKey:kClientIPKey]];
		}

		if ([filteringParameters objectForKey:kClientWlanKey]) {
			wlanFilter = [NSString stringWithFormat:@"%@=%@", kClientWlanKey, [filteringParameters objectForKey:kClientWlanKey]];
		}

		if ([filteringParameters objectForKey:kClientOSTypeKey]) {
			osTypeFilter = [NSString stringWithFormat:@"%@=%@", kClientOSTypeKey, [filteringParameters objectForKey:kClientOSTypeKey]];
		}

		if ([filteringParameters objectForKey:kClientStatusKey]) {
			osTypeFilter = [NSString stringWithFormat:@"%@=%@", kClientStatusKey, [filteringParameters objectForKey:kClientStatusKey]];
		}

		NSArray *filterStringComponents = [[NSArray alloc] initWithObjects:macAddressFilter, ipAddressFilter, wlanFilter, osTypeFilter, statusFilter, nil];
		NSString *joinedString = [filterStringComponents componentsJoinedByString:@","];

		if (joinedString) {
			filterString = [NSString stringWithFormat:@"[%@]", joinedString];
		}
	}

	// create for each statement for client list ..
	forEachStatement = [NSString stringWithFormat:@"%@%@", CLINT_LIST_YANG_URL, filterString];

	NSArray *clientResponseParams = [NSArray arrayWithObjects:kClientAuthMethodKey, kClientMacAddressKey, kClientStatusKey, kClientIPKey, kClientWlanKey, kClientOSTypeKey, kClientConnecitonDurationKey, nil];

	// Create select statement for client list ..
	NSMutableArray *selectStatementsArray = nil;

	for (NSString *clientResponseParam in clientResponseParams) {
		NSMutableDictionary *selectStatementDictionary = [[NSMutableDictionary alloc] init];

		[selectStatementDictionary setObject:clientResponseParam forKey:kSelectParametersExpression];

		// Add label
		[selectStatementDictionary setObject:clientResponseParam forKey:kSelectParametersLabel];

		// Add result type
		[selectStatementDictionary setObject:[NSString stringWithFormat:kSelectResultTypeString] forKey:kSelectParametersResultType];

		// Add select this statement to the select statements array
		[selectStatementsArray addObject:selectStatementDictionary];
	}

	// Create request parameter dictionary
	NSDictionary *requestParameters = [[RMFKUMOHelper sharedRMFKUMOHelper] createQuery:forEachStatement select:selectStatementsArray sortBy:sortingParameters limit:limit offset:offset];

	// Call Query api to fetch response
	[self fetchHandleResponseAndData:requestParameters response: ^(NSError *error, id data) {
	    if (completion) {
	        completion(error, data);
		}
	}];
}

/*
 * Creates network request to fetch client list
 */

- (void)clientList:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion {
	NSError *error = nil;
	if (requestParams) {
		NSString *tenantName = [requestParams objectForKey:kTenantNameKey];

		if (tenantName) {
			if (!IS_SANDBOX_SUPPORTED_ENABLED) {
				[self.requestOperationManager get:CLIENT_LIST(tenantName) withParameters:nil andResponse: ^(NSError *error, id data) {
				    if (OBJ_NOT_NIL(completion)) {
				        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), REQUEST_SUCCESS(kClientList));
				        completion(error, data);
					}
				}];
			}
			else {
				// Mocked APIs to be exposed...
			}
		}
		else {
			error = [RMFUtils errorWithCode:RMFCodeForTenantNameNotFound];
			if (OBJ_NOT_NIL(completion)) {
				RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kClientList, error.description));
				completion(error, nil);
			}
		}
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForRequestParamNotFound];
		if (OBJ_NOT_NIL(completion)) {
			RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kClientList, error.description));
			completion(error, nil);
		}
	}
}

/*
 * Makes network request to fetch active/blocked client list  with request parameters and returns the network response
 */
- (void)clientList:(NSDictionary *)requestParams clientType:(int)clientType response:(void (^)(NSError *error, id data))completion {
	NSError *error = nil;
	if (requestParams) {
		NSString *tenantName = [requestParams objectForKey:kTenantNameKey];

		if (tenantName) {
			if (!IS_SANDBOX_SUPPORTED_ENABLED) {
				switch (clientType) {
					case kClientTypeActive:
						// TODO: create request for active client
						break;

					case kClientTypeBlocked:
						// TODO: create request for blocked client
						break;

					default:
						break;
				}
				[self.requestOperationManager get:CLIENT_LIST(tenantName) withParameters:nil andResponse: ^(NSError *error, id data) {
				    if (OBJ_NOT_NIL(completion)) {
				        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), REQUEST_SUCCESS(kClientList));
				        completion(error, data);
					}
				}];
			}
			else {
				// Mocked APIs to be exposed...
			}
		}
		else {
			error = [RMFUtils errorWithCode:RMFCodeForTenantNameNotFound];
			if (OBJ_NOT_NIL(completion)) {
				RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kClientList, error.description));
				completion(error, nil);
			}
		}
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForRequestParamNotFound];
		if (OBJ_NOT_NIL(completion)) {
			RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kClientList, error.description));
			completion(error, nil);
		}
	}
}

/*
 * Creates network request to fetch client details
 */

- (void)clientDetails:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion {
	NSError *error = nil;
	if (requestParams) {
		NSString *tenantName = [requestParams objectForKey:kTenantNameKey];
		NSString *macAddress = [requestParams objectForKey:kMacAddressInfoKey];

		if (tenantName && macAddress) {
			if (!IS_SANDBOX_SUPPORTED_ENABLED) {
				[self.requestOperationManager get:CLIENT_DETAILS(tenantName, venueName) withParameters:nil andResponse: ^(NSError *error, id data) {
				    if (OBJ_NOT_NIL(completion)) {
				        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), REQUEST_SUCCESS(kClientDetails));
				        completion(error, data);
					}
				}];
			}
			else {
				// Mocked APIs to be exposed...
			}
		}
		else {
			error = [RMFUtils errorWithCode:RMFCodeForTenantAndMacAddressNotFound];
			if (OBJ_NOT_NIL(completion)) {
				RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kClientDetails, error.description));
				completion(error, nil);
			}
		}
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForRequestParamNotFound];
		if (OBJ_NOT_NIL(completion)) {
			RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kClientDetails, error.description));
			completion(error, nil);
		}
	}
}

#pragma mark - event Methods

/*
 * Creates network request to fetch event list
 */
//RMF-413 resolved
- (void)eventList:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion {
	NSError *error = nil;
	if (requestParams) {
		NSString *tenantName = [requestParams objectForKey:kTenantNameKey];
		NSString *venueName = [requestParams objectForKey:kVenueNameKey];

		if (tenantName && venueName) {
			if (!IS_SANDBOX_SUPPORTED_ENABLED) {
				[self.requestOperationManager get:EVENT_LIST(tenantName, venueName) withParameters:nil andResponse: ^(NSError *error, id data) {
				    if (OBJ_NOT_NIL(completion)) {
				        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), REQUEST_SUCCESS(kEventList));
				        completion(error, data);
					}
				}];
			}
			else {
				// Mocked APIs to be exposed...
			}
		}
		else {
			error = [RMFUtils errorWithCode:RMFCodeForTenantAndVenueNameNotFound];
			if (OBJ_NOT_NIL(completion)) {
				RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kEventList, error.description));
				completion(error, nil);
			}
		}
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForRequestParamNotFound];
		if (OBJ_NOT_NIL(completion)) {
			RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kEventList, error.description));
			completion(error, nil);
		}
	}
}

/*
 * Creates network request to fetch event details
 */

- (void)eventDetails:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion {
	NSError *error = nil;
	if (requestParams) {
		if (!IS_SANDBOX_SUPPORTED_ENABLED) {
			// TODO:: Network call for event details , so we can get alarm details by checking its type..
		}
		else {
			// Mocked APIs to be exposed...
		}
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForRequestParamNotFound];
		if (OBJ_NOT_NIL(completion)) {
			RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kEventDetail, error.description));
			completion(error, nil);
		}
	}
}

/*
 * Create query to fetch event list based on filtering and sorting criteria provided and make network
 * request to fetch it
 */

- (void)eventList:(NSDictionary *)requestParams sortBy:(int)sort filterOn:(NSDictionary *)filter response:(void (^)(NSError *error, id data))completion {
	NSNumber *limit = [requestParams objectForKey:kLimit];
	NSNumber *offset = [requestParams objectForKey:kOffset];

	// Create foreach statement
	NSString *foreach = EVENT_LIST_FETCH_URL;
	if (OBJ_NOT_NIL(filter) && [filter count]) {
		NSArray *filterKey = [filter allKeys];
		NSMutableArray *filterArray = [[NSMutableArray alloc] init];
		for (NSString *key in filterKey) {
			[filterArray addObject:[NSString stringWithFormat:@"%@='%@'", key, [filter objectForKey:key]]];
		}
		NSString *joinedFilterString = [filterArray componentsJoinedByString:@","];
		foreach = [NSString stringWithFormat:@"%@[%@]", EVENT_LIST_FETCH_URL, joinedFilterString];
	}

	// Create select statement
	NSArray *eventResponseParams = [NSArray arrayWithObjects:kEventTimeKey, kEventTypeKey, kEventSeverityKey, kEventDescriptionKey, kEventTypeIdKey, nil];
	NSMutableArray *selectStatementsArray = [[NSMutableArray alloc] init];

	for (NSString *eventResponseParam in eventResponseParams) {
		NSMutableDictionary *selectStatementDictionary = [[NSMutableDictionary alloc] init];
		[selectStatementDictionary setObject:[NSString stringWithFormat:@"%@", eventResponseParam] forKey:kSelectParametersExpression];
		[selectStatementDictionary setObject:[NSString stringWithFormat:@"%@", eventResponseParam] forKey:kSelectParametersLabel];
		[selectStatementDictionary setObject:[[NSArray alloc] initWithObjects:[NSString stringWithFormat:@"%@", kSelectResultTypeString], nil] forKey:kSelectParametersResultType];
		[selectStatementsArray addObject:selectStatementDictionary];
	}

	// Create sort by array
	NSArray *sortedBy = nil;
	switch (sort) {
		// TODO: Add cases for event sorting parameters
		default:
			break;
	}

	// Create request parameter dictionary
	NSDictionary *requestParameters = [[RMFKUMOHelper sharedRMFKUMOHelper] createQuery:foreach select:selectStatementsArray sortBy:sortedBy limit:limit offset:offset];

	if (!IS_SANDBOX_SUPPORTED_ENABLED) {
		// Call Query api to fetch response
		[self fetchHandleResponseAndData:requestParameters response: ^(NSError *error, id data) {
		    if (completion) {
		        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), REQUEST_SUCCESS(kEventList));
		        completion(error, data);
			}
		}];
	}
	else {
		// Mocked APIs to be exposed...
	}
}

#pragma mark - Summary statistics

/*
 * Creates network request to fetch summaryStatistics
 */

- (void)summaryStatisticsForVarWithResponse:(void (^)(NSError *error, id data))completion {
	NSError *error = nil;

	NSMutableDictionary *queryJsonDictionary = nil;
	//TODO: create json dictionary

	// Create query json

	if (!IS_SANDBOX_SUPPORTED_ENABLED) {
		[self fetchHandleResponseAndData:queryJsonDictionary response: ^(NSError *error, id data) {
		    if (completion) {
		        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), REQUEST_SUCCESS(kSummaryStatistics));
		        completion(error, data);
			}
		}];
	}
	else {
		// Mocked APIs to be exposed...
	}
}

/*
 * Creates network request to fetch summaryStatistics for tenant
 */

- (void)summaryStatisticsForTenant:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion {
	NSError *error = nil;
	if (requestParams) {
		NSString *tenantName = [requestParams objectForKey:kTenantNameKey];
		if (tenantName) {
			NSMutableDictionary *queryJsonDictionary = nil;
			//TODO: create json dictionary

			// Create query json
			if (!IS_SANDBOX_SUPPORTED_ENABLED) {
				[self fetchHandleResponseAndData:queryJsonDictionary response: ^(NSError *error, id data) {
				    if (completion) {
				        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), REQUEST_SUCCESS(kSummaryStatistics));
				        completion(error, data);
					}
				}];
			}
			else {
				// Mocked Responses to be populated
			}
		}
		else {
			error = [RMFUtils errorWithCode:RMFCodeForTenantNameNotFound];
			if (OBJ_NOT_NIL(completion)) {
				RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kSummaryStatistics, error.description));
				completion(error, nil);
			}
		}
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForRequestParamNotFound];
		if (OBJ_NOT_NIL(completion)) {
			RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), REQUEST_FAILURE(kSummaryStatistics, error.description));
			completion(error, nil);
		}
	}
}

#pragma mark - Query APIs

/*
 * Create query to fetch wlan list based on filtering and sorting criteria provided and make network
 * request to fetch it
 */

- (void)wlanList:(NSDictionary *)requestParams sortBy:(int)sort filterOn:(NSDictionary *)filter response:(void (^)(NSError *error, id data))completion {
	NSNumber *limit = [requestParams objectForKey:kLimit];
	NSNumber *offset = [requestParams objectForKey:kOffset];

	// Create foreach statement
	NSString *foreach = WLAN_LIST_FETCH_URL;
	if (OBJ_NOT_NIL(filter) && [filter count]) {
		NSArray *filterKey = [filter allKeys];
		NSMutableArray *filterArray = [[NSMutableArray alloc] init];
		for (NSString *key in filterKey) {
			[filterArray addObject:[NSString stringWithFormat:@"%@='%@'", key, [filter objectForKey:key]]];
		}
		NSString *joinedFilterString = [filterArray componentsJoinedByString:@","];
		foreach = [NSString stringWithFormat:@"%@[%@]", WLAN_LIST_FETCH_URL, joinedFilterString];
	}

	// Create select statement
	NSArray *wlanResponseParams = [NSArray arrayWithObjects:kWlanNameKey, kWlanDescriptionKey, kWlanSssidKey, kWlanEnabledKey, kWlanTypeKey, kPskPassPhraseKey, kWlanSecurityKey, nil];
	NSMutableArray *selectStatementsArray = [[NSMutableArray alloc] init];

	for (NSString *wlanResponseParam in wlanResponseParams) {
		NSMutableDictionary *selectStatementDictionary = [[NSMutableDictionary alloc] init];
		[selectStatementDictionary setObject:[NSString stringWithFormat:@"%@", wlanResponseParam] forKey:kSelectParametersExpression];
		[selectStatementDictionary setObject:[NSString stringWithFormat:@"%@", wlanResponseParam] forKey:kSelectParametersLabel];
		[selectStatementDictionary setObject:[[NSArray alloc] initWithObjects:[NSString stringWithFormat:@"%@", kSelectResultTypeString], nil] forKey:kSelectParametersResultType];
		[selectStatementsArray addObject:selectStatementDictionary];
	}

	// Create sort by array
	NSArray *sortedBy = nil;
	switch (sort) {
		case kWlanSortingParameterName:
			sortedBy = [NSArray arrayWithObjects:kWlanNameKey, nil];
			break;

		case kWlanSortingParameterSsid:
			sortedBy = [NSArray arrayWithObjects:kWlanSssidKey, nil];
			break;

		default:
			break;
	}

	// Create request parameter dictionary
	NSDictionary *requestParameters = [[RMFKUMOHelper sharedRMFKUMOHelper] createQuery:foreach select:selectStatementsArray sortBy:sortedBy limit:limit offset:offset];

	if (!IS_SANDBOX_SUPPORTED_ENABLED) {
		// Call Query api to fetch response
		[self fetchHandleResponseAndData:requestParameters response: ^(NSError *error, id data) {
		    if (completion) {
		        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), REQUEST_SUCCESS(kWlanServiceList));
		        completion(error, data);
			}
		}];
	}
	else {
		// Mocked APIs to be exposed...
	}
}

#pragma mark - Query API helpers

/*
 * Creates query handle by using the handle response json
 */

- (void)createQueryHandle:(id)input response:(void (^)(NSError *error, NSDictionary *data))completion {
	NSMutableDictionary *queryHandleDictionary = nil;

	// Create query handle
	id typeChecker = input;
	if ([typeChecker isKindOfClass:[NSDictionary class]]) {
		NSDictionary *queryHandleDictionary = (NSDictionary *)typeChecker;

		typeChecker = [queryHandleDictionary objectForKey:kStartQueryResult];
		if (typeChecker && [typeChecker isKindOfClass:[NSDictionary class]]) {
			queryHandleDictionary = [[NSMutableDictionary alloc] init];
			[queryHandleDictionary setValue:typeChecker forKey:kFetchQueryResult];
			if (completion) {
				completion(nil, queryHandleDictionary);
				return;
			}
		}
	}

	if (completion) {
		completion([RMFUtils errorWithCode:RMFCodeForParsingFailedError], nil);
	}
}

/*
 * Creates network request to fetch handle response json, creates query handle and fetch data
 */

- (void)fetchHandleResponseAndData:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion {
	// Make request to fetch handle response json
	[self.requestOperationManager post:KUMO_QUERY_URL withParameters:requestParams andResponse: ^(NSError *error, id data) {
	    if (data) {
	        // Create query handle from handle response json
	        if (!IS_SANDBOX_SUPPORTED_ENABLED) {
	            [self createQueryHandle:data response: ^(NSError *error, NSDictionary *data) {
	                if (data) {
	                    // Make request to fetch data
	                    [self.requestOperationManager post:KUMO_QUERY_URL withParameters:data andResponse: ^(NSError *error, id data) {
	                        if (completion) {
	                            completion(error, data);
							}
						}];
					}
	                else {
	                    if (completion) {
	                        completion(error, data);
						}
					}
				}];
			}
	        else {
	            // Mocked APIs to be exposed...
			}
		}
	    else {
	        if (completion) {
	            completion(error, data);
			}
		}
	}];
}

@end
