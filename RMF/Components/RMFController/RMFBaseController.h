/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFRequestOperationManager.h"
#import "RMFControllerProtocol.h"

@interface RMFBaseController : NSObject <RMFControllerProtocol>

// Stores and returns the instance of RMFRequestOperationManager
@property (nonatomic, strong) RMFRequestOperationManager *requestOperationManager;

// Creates the base URL using IP and port number stored in configuration and returns it
- (NSString *)createBaseUrl;

@end
