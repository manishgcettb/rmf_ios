/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFBaseController.h"
#import "RMFConfiguration.h"
#import "RMFMacro.h"
#import "RMFUtils.h"

#import "RMFLogger.h"
#import "RMFLog.h"

@implementation RMFBaseController

/*
 * Creates the base URL using IP and port number available in the configuration data
 */

- (NSString *)createBaseUrl {
	NSString *baseUrl = nil;
	RMFConfiguration *configuration = [RMFConfiguration sharedRMFConfiguration];

	if (OBJ_NOT_NIL(configuration)) {
		// If IP address is available create the base URL by appending the port number
		if (configuration.systemIP && ![configuration.systemIP isEqualToString:kEmptyText]) {
			if (configuration.port && ![configuration.port isEqualToString:kEmptyText]) {
				baseUrl = [NSString stringWithFormat:@"http://%@:%@", configuration.systemIP, configuration.port];
				// Store base URL in config URL
				configuration.configUrl = baseUrl;

				NSString *logDescription = [NSString stringWithFormat:@"%@ %@", CONFIGURATION_URL, configuration.configUrl];
				RMF_LOG_VERBOSE([self class], NSStringFromSelector(_cmd), logDescription);
			}
			else {
				// If config URL is available then use it as the base URL
				if (configuration.configUrl && ![configuration.configUrl isEqualToString:kEmptyText]) {
					baseUrl = configuration.configUrl;
					NSString *logDescription = [NSString stringWithFormat:@"%@ %@", BASE_URL, configuration.configUrl];
					RMF_LOG_VERBOSE([self class], NSStringFromSelector(_cmd), logDescription);
				}
				else {
					NSString *logDescription = [NSString stringWithFormat:@"%@ %@", WARNING, kPortNotFound];
					RMF_LOG_WARNING([self class], NSStringFromSelector(_cmd), logDescription);
					[RMFUtils raiseException:NSInternalInconsistencyException WithDescription:kPortNotFound];
				}
			}
		}

		// If config URL is available then use it as the base URL
		else if (configuration.configUrl && ![configuration.configUrl isEqualToString:kEmptyText]) {
			baseUrl = configuration.configUrl;
		}

		// Raise exception
		else {
			NSString *logDescription = [NSString stringWithFormat:@" %@ %@", WARNING, kBaseUrlNotFound];
			RMF_LOG_WARNING([self class], NSStringFromSelector(_cmd), logDescription);
			[RMFUtils raiseException:NSInternalInconsistencyException WithDescription:kBaseUrlNotFound];
		}
	}
	else {
		NSString *logDescription = [NSString stringWithFormat:@" %@ %@", WARNING, kConfigDataNotFound];
		RMF_LOG_WARNING([self class], NSStringFromSelector(_cmd), logDescription);
		[RMFUtils raiseException:NSInternalInconsistencyException WithDescription:kConfigDataNotFound];
	}
	return baseUrl;
}

@end
