/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */


#import <Foundation/Foundation.h>

@interface RMFKUMOHelper : NSObject

// Returns singleton instance of KUMO Helper
+ (RMFKUMOHelper *)sharedRMFKUMOHelper;

// Creates foreach statement
- (NSString *)foreachStatement:(NSDictionary *)filter;

// Creates select statement
- (NSMutableArray *)selectStatementWithLabel:(NSArray *)label expression:(NSArray *)expression resultType:(NSArray *)resultType filterString:(NSString *)filter;

// Creates query statement
- (NSDictionary *)createQuery:(NSString *)foreach select:(NSArray *)select sortBy:(NSArray *)sortBy limit:(NSNumber *)limit offset:(NSNumber *)offset;


@end
