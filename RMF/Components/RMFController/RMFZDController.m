/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFMacro.h"
#import "RMFUtils.h"
#import "RMFZDController.h"
#import "RMFConfiguration.h"
#import "RMFLog.h"
#import "RMFLogger.h"

@implementation RMFZDController
{
    // base url used for making network request on ZD
    NSString *baseURL;
}

// Stores the singleton instance of RMFZDController
static RMFZDController *sharedRMFZDController = nil;

#pragma mark - Class methods implementation

/*
 * Creates a singleton instance of RMFZDController
 */
+ (RMFZDController *)sharedRMFZDController {
    if (!OBJ_NOT_NIL(sharedRMFZDController)) {
        sharedRMFZDController = [[RMFZDController alloc] init];
    }
    
    return sharedRMFZDController;
}

#pragma mark - Instance method implementation

/*
 * initialise the singleton instance of RMFZDController
 */
- (RMFZDController *)init {
    if (!OBJ_NOT_NIL(sharedRMFZDController)) {
        sharedRMFZDController = [super init];
        
        if (sharedRMFZDController) {
            [self initRequestOperationManager];
        }
    }
    
    return sharedRMFZDController;
}

/*
 * Get an instance of requestOperationManager and initialise it with the base url
 */

- (void)initRequestOperationManager {
    if (sharedRMFZDController) {
        if ([self createBaseUrl]) {
            // If new base url is same as the existing url then don't do anything
            if (![baseURL isEqualToString:[self createBaseUrl]]) {
                baseURL = [self createBaseUrl];
                RMFConfiguration *config = [RMFConfiguration sharedRMFConfiguration];
                NSString *contentType = [config contentTypeForActiveController];
                
                sharedRMFZDController.requestOperationManager = [[RMFRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
                
                if (OBJ_NOT_NIL(sharedRMFZDController.requestOperationManager)) {
                    // Set request serializer
                    sharedRMFZDController.requestOperationManager.requestSerializer = [AFHTTPRequestSerializer serializer];
                    
                    // Set response serializer
                    sharedRMFZDController.requestOperationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
                    sharedRMFZDController.requestOperationManager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:contentType];
                    
                    NSString *strDesc = [NSString stringWithFormat:@"ZD requestOperationManager : %@", (sharedRMFZDController.requestOperationManager)];
                    RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), strDesc);
                }
            }
        }
        else {
            NSString *strDesc = [NSString stringWithFormat:@" %@ %@", WARNING, kBaseUrlNotFound];
            RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), strDesc);
            [RMFUtils raiseException:NSInternalInconsistencyException WithDescription:kBaseUrlNotFound];
        }
    }
}

@end
