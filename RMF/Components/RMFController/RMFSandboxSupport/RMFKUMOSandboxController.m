/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFKUMOSandboxController.h"
#import "RMFSystemManager.h"
#import "RMFMacro.h"

#define AP_LIST_JSON                          @"aplist.json"
#define AP_DETAIL_JSON                        @"apdetail.json"
#define WLAN_LIST_JSON                        @"wlanservice.json"
#define WLAN_DETAIL_JSON                      @"wlanservicedetail.json"
#define WLAN_STATUS_JSON                      @"wlanstatus.json"
#define ERROR_FILE_NOT_FOUND                  @"file not found in bundle"

@interface RMFKUMOSandboxController ()
// Stores RMFSystemManager instance
@property (nonatomic, strong) RMFSystemManager *systemManager;

- (void)jsonForFileName:(NSString *)filename response:(void (^)(NSError *error, id data))completion;

@end


@implementation RMFKUMOSandboxController

+ (RMFKUMOSandboxController *)sharedInstance {
    static RMFKUMOSandboxController *_sharedInstace = nil;
    
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstace = [[RMFKUMOSandboxController alloc] init];
    });
    
    return _sharedInstace;
}

- (id)init {
    if (self = [super init]) {
        // set default properties...
        self.systemManager = [RMFSystemManager sharedRMFSystemManager];
    }
    return self;
}

#pragma - Mocked APIs to be Exposed...

/*
 * Return local data for apList in RMFAPPlugin
 */
- (void)apList:(void (^)(NSError *error, id data))completion {
    [self jsonForFileName:AP_LIST_JSON response: ^(NSError *error, id data) {
        if (OBJ_NOT_NIL(completion)) {
            completion(error, data);
        }
    }];
}

/*
 * Return local data for apDetail in RMFAPPlugin
 */
- (void)apDetail:(void (^)(NSError *error, id data))completion {
    [self jsonForFileName:AP_DETAIL_JSON response: ^(NSError *error, id data) {
        if (OBJ_NOT_NIL(completion)) {
            completion(error, data);
        }
    }];
}

/*
 * Return local data for wlanList in RMFWlanPlugin
 */
- (void)wlanList:(void (^)(NSError *error, id data))completion {
    [self jsonForFileName:WLAN_LIST_JSON response: ^(NSError *error, id data) {
        if (OBJ_NOT_NIL(completion)) {
            completion(error, data);
        }
    }];
}

/*
 * Return local data for wlanDetails in RMFWlanPlugin
 */
- (void)wlanDetails:(void (^)(NSError *error, id data))completion {
    [self jsonForFileName:WLAN_DETAIL_JSON response: ^(NSError *error, id data) {
        if (OBJ_NOT_NIL(completion)) {
            completion(error, data);
        }
    }];
}

/*
 * Return local data for wlanStatus in RMFWlanPlugin
 */
- (void)wlanStatus:(void (^)(NSError *error, id data))completion {
    [self jsonForFileName:WLAN_STATUS_JSON response: ^(NSError *error, id data) {
        if (OBJ_NOT_NIL(completion)) {
            completion(error, data);
        }
    }];
}

/*
 * Parse local file from RMFBundle
 */
- (void)jsonForFileName:(NSString *)filename response:(void (^)(NSError *error, id data))completion {
    NSData *bundleData = RMFBUNDLE_DATA_WITH_FILE_NAME(filename);
    if (bundleData != nil) {
        NSError *error = nil;
        id jsonObj = [NSJSONSerialization JSONObjectWithData:bundleData options:NSJSONReadingMutableContainers error:&error];
        if (error) {
            if (OBJ_NOT_NIL(completion)) {
                completion(error, nil);
            }
        }
        else if (OBJ_NOT_NIL(completion)) {
            completion(error, jsonObj);
        }
    }
    else if (OBJ_NOT_NIL(completion)) {
        completion([self sandboxError:AP_DETAIL_JSON], nil);
    }
}

- (NSError *)sandboxError:(NSString *)fileName {
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
    [userInfo setObject:[NSString stringWithFormat:@"%@ %@", fileName, ERROR_FILE_NOT_FOUND] forKey:kLocalizedDescriptionKey];
    return [NSError errorWithDomain:ERROR_FILE_NOT_FOUND code:9999 userInfo:userInfo];
}

@end
