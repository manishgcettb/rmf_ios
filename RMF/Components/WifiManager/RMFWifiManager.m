/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFWifiManager.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import <ifaddrs.h>
#import <net/if.h>
#import <SystemConfiguration/CaptiveNetwork.h>
#import <CoreTelephony/CoreTelephonyDefines.h>
#import "RMFMacro.h"

#import "RMFLogger.h"
#import "RMFLog.h"


#define TEST_URL    @"www.google.com"

@implementation RMFWifiManager

static RMFWifiManager *sharedRMFWifiManager;

#pragma mark - Class methods declaration

/*
 * Creates a singleton instance
 */
+ (RMFWifiManager *)sharedInstance {
    if (!OBJ_NOT_NIL(sharedRMFWifiManager)) {
        sharedRMFWifiManager = [[RMFWifiManager alloc] init];
        
        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), @"Shared instance of wifi manager created");
    }
    return sharedRMFWifiManager;
}

#pragma mark - Instance methods implementation

/*
 * initialise the singleton instance
 */
- (RMFWifiManager *)init {
    if (!OBJ_NOT_NIL(sharedRMFWifiManager)) {
        sharedRMFWifiManager = [super init];
        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), WIFI_MANAGER_INIT);
    }
    
    return sharedRMFWifiManager;
}

//To detect sim card
- (BOOL)isSimCardAvailable {
    RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), SIM_CARD_AVAILABLITY);
    
    BOOL isSimCardAvailable = true;
    CTTelephonyNetworkInfo *info = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = info.subscriberCellularProvider;
    
    if (carrier.mobileNetworkCode == nil || [carrier.mobileNetworkCode isEqualToString:@""]) {
        isSimCardAvailable = false;
    }
    return isSimCardAvailable;
}

//To check Wifi is on or off
- (BOOL)isWiFiEnabled {
    RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), WIFI_ENABLED);
    
    NSCountedSet *cset = [NSCountedSet new];
    
    struct ifaddrs *interfaces;
    
    if (!getifaddrs(&interfaces)) {
        for (struct ifaddrs *interface = interfaces; interface; interface = interface->ifa_next) {
            if ((interface->ifa_flags & IFF_UP) == IFF_UP) {
                [cset addObject:[NSString stringWithUTF8String:interface->ifa_name]];
            }
        }
    }
    
    return [cset countForObject:@"awdl0"] > 1 ? YES : NO;
}

- (NetworkStatus)getNetStatus {
    Reachability *hostReach = [Reachability reachabilityWithHostName:TEST_URL];
    NetworkStatus netStatus = [hostReach currentReachabilityStatus];
    return netStatus;
}

//To check Net Connected via WIFI

- (NSError *)internetConnectedViaWifi:(NetworkStatus)netStatus {
    RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), INTERNET_VIA_WIFI);
    NSError *error = nil;
    switch (netStatus) {
        case ReachableViaWiFi:
            error = nil;
            break;
            
        case ReachableViaWWAN:
            error = [[NSError alloc] initWithDomain:WWAN_CONNECTED_ERR code:WWAN_ERR_CODE userInfo:nil]
            ;
            break;
            
        case NotReachable:
            error = [[NSError alloc] initWithDomain:NO_INTERNET_ERR code:NO_INTERNET_ERR_CODE userInfo:nil]
            ;
            break;
            
        default:
            break;
    }
    
    return error;
}

//To check net connected via WWAN

- (NSError *)internetConnectedViaWWAN:(NetworkStatus)netStatus {
    RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), INTERNET_VIA_WWAN);
    
    NSError *error = nil;
    switch (netStatus) {
        case ReachableViaWiFi:
            error = [[NSError alloc] initWithDomain:WIFI_CONNECTED_ERR code:WIFI_ERR_CODE userInfo:nil]
            ;
            break;
            
        case ReachableViaWWAN:
            error = nil;
            break;
            
        case NotReachable:
            error = [[NSError alloc] initWithDomain:NO_INTERNET_ERR code:NO_INTERNET_ERR_CODE userInfo:nil]
            ;
            break;
            
        default:
            break;
    }
    
    return error;
}

@end
