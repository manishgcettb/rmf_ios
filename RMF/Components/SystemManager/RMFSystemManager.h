/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFControllerProtocol.h"
#import "RMFParsingProtocol.h"

@interface RMFSystemManager : NSObject

// Returns singleton instance of RMFSystemManager
+ (RMFSystemManager *)sharedRMFSystemManager;

// Returns active parser instance
- (id <RMFParsingProtocol> )activeParser;

// Returns active controller instance
- (id <RMFControllerProtocol> )activeController;

// Updates active controller instance with new IP and port value
- (void)updateActiveController;

@end
