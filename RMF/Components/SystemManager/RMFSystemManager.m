/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFSystemManager.h"
#import "RMFMacro.h"
#import "RMFSCGController.h"
#import "RMFZDController.h"
#import "RMFKUMOController.h"
#import "RMFConfiguration.h"
#import "RMFControllerProtocol.h"
#import "RMFParsingProtocol.h"
#import "RMFKUMOParser.h"
#import "RMFZDParser.h"
#import "RMFSCGParser.h"
#import "RMFLogger.h"
#import "RMFLog.h"

extern int ddLogLevel;

extern int ddLogLevel;
// Stores the singleton instance of RMFSystemManager
static RMFSystemManager *sharedRMFSystemManager = nil;

@implementation RMFSystemManager

/*
 * Creates a singleton instance of RMFSystemManager
 */

+ (RMFSystemManager *)sharedRMFSystemManager {
    if (!OBJ_NOT_NIL(sharedRMFSystemManager)) {
        sharedRMFSystemManager = [[RMFSystemManager alloc] init];
    }
    return sharedRMFSystemManager;
}

/*
 * initialise the singleton instance of RMFSystemManager
 */

- (RMFSystemManager *)init {
    if (!OBJ_NOT_NIL(sharedRMFSystemManager)) {
        sharedRMFSystemManager = [super init];
        
        if (sharedRMFSystemManager) {
            // Configure CocoaLumberjack - (enable and start recording logs )
            [[RMFLogger sharedRMFLogger] enableRMFLogger];
        }
    }
    return sharedRMFSystemManager;
}

/*
 * Returns the singleton instance of active controller
 */

- (id <RMFControllerProtocol> )activeController {
    id <RMFControllerProtocol> activeController = nil;
    
    RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), SHARED_ACTIVE_CONTROLLER);
    
    RMFConfiguration *config = [RMFConfiguration sharedRMFConfiguration];
    switch (config.system) {
        case kSystemTypeSCG:
            activeController = [RMFSCGController sharedRMFSCGController];
            break;
            
        case kSystemTypeZD:
            activeController = [RMFZDController sharedRMFZDController];
            break;
            
        case kSystemTypeKUMO:
            activeController = [RMFKUMOController sharedRMFKUMOController];
            break;
            
        default:
            break;
    }
    
    return activeController;
}

/*
 * Returns the singleton instance of active parser
 */

- (id <RMFParsingProtocol> )activeParser {
    id <RMFParsingProtocol> activeParser = nil;
    
    RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), SHARED_ACTIVE_PARSER);
    
    
    RMFConfiguration *config = [RMFConfiguration sharedRMFConfiguration];
    switch (config.system) {
        case kSystemTypeSCG:
            activeParser = [RMFSCGParser sharedRMFSCGParser];
            break;
            
        case kSystemTypeZD:
            activeParser = [RMFZDParser sharedRMFZDParser];
            break;
            
        case kSystemTypeKUMO:
            activeParser = [RMFKUMOParser sharedRMFKUMOParser];
            break;
            
            
        case kSystemTypeNone:
            break;
            
        default:
            break;
    }
    
    return activeParser;
}

/*
 * If there is a change in system IP/port then first update these values in the
 * system configuration instance then call this method
 */

- (void)updateActiveController {
    id <RMFControllerProtocol> activeController = [self activeController];
    if (activeController) {
        [activeController initRequestOperationManager];
    }
}

@end
