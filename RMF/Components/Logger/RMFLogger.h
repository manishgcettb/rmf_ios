/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */
#import <Foundation/Foundation.h>
#import "CocoaLumberjack.h"

typedef enum {
	DDLogger = 1,
	OTHER
}loggerType;


extern int ddLogLevel;

@interface RMFLogger : NSObject

@property (nonatomic, strong) DDFileLogger *fileLogger;

@property (nonatomic, assign) BOOL isRecordingLogs;

// all intializaiton will be done here in this method for logger..
- (void)intializeLogger;

+ (RMFLogger *)sharedRMFLogger;

// to check if logger  has been started or not..
- (BOOL)isLoggerAlreadyStarted;
- (NSArray *)retrieveLogFilePaths;

#pragma mark - enable/disable logger
- (void)enableRMFLogger;
- (void)disableRMFLogger;

@end
