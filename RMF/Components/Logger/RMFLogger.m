/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */
#import "RMFLogger.h"
#import "RMFMacro.h"
#import "RMFLog.h"
#import "RMFConfiguration.h"

#import <MessageUI/MFMailComposeViewController.h>

// by default log level will be DDLogLevelVerbose, if user do not set it..
int ddLogLevel = DDLogLevelVerbose;

static RMFLogger *sharedRMFLogger = nil;

@interface RMFLogger ()


@end


@implementation RMFLogger

/*
 * Creates a singleton instance of RMFSystemManager
 */
+ (RMFLogger *)sharedRMFLogger {
	if (!OBJ_NOT_NIL(sharedRMFLogger)) {
		sharedRMFLogger = [[RMFLogger alloc] init];
	}
	return sharedRMFLogger;
}

// initialise the logger so that it can get all required things to start with ..
- (void)intializeLogger {
	self.isRecordingLogs = NO;
	// by default log level will be DDLogLevelVerbose, if user do not set it..
	ddLogLevel = [[RMFConfiguration sharedRMFConfiguration] logLevelRMF];
}

- (BOOL)isLoggerAlreadyStarted {
	if (self.isRecordingLogs)
		return YES;
	return NO;
}

#pragma mark - Logger Helper methods

- (void)startAndConfigureLogger {
	// set where logs will be saved in file
	[self saveLogsToFile];

	[DDLog addLogger:[DDASLLogger sharedInstance] withLevel:ddLogLevel];
	[DDLog addLogger:[DDTTYLogger sharedInstance] withLevel:ddLogLevel];
	self.isRecordingLogs = YES;

	// assign color to logs..
	DDMakeColor(204, 255, 255);
}

- (void)stopRecordingLogs {
	if (self.isRecordingLogs)
		self.isRecordingLogs = NO;

	[DDLog removeAllLoggers];
}

// capture logs into file
- (void)saveLogsToFile {
	// Writing to file ..
	self.fileLogger = [[DDFileLogger alloc] init];
	self.fileLogger.rollingFrequency = 60 * 60 * 24; // 24 hour rolling
	self.fileLogger.logFileManager.maximumNumberOfLogFiles = 5;

	RMFLogInfo(@"log file at: %@", [[self.fileLogger currentLogFileInfo] filePath]);

	[DDLog addLogger:self.fileLogger];
}

#pragma mark - enable/disable logger
- (void)enableRMFLogger {
	// Enable logger

	// log level to set..
	ddLogLevel = [[RMFConfiguration sharedRMFConfiguration] logLevelRMF];

	// Now you may start configuring logger
	[self startAndConfigureLogger];
}

- (void)disableRMFLogger {
	// Disable it
	if (ddLogLevel != DDLogLevelOff) {
		ddLogLevel = DDLogLevelOff;

		// now you may stop recording logs
		[self stopRecordingLogs];
	}
}

#pragma mark -
#pragma mark - retrieve paths of logs files
- (NSArray *)retrieveLogFilePaths {
	DDLogFileManagerDefault *defaultLogFileManager =  [[DDLogFileManagerDefault alloc] init];

	return [defaultLogFileManager sortedLogFilePaths];
}

@end
