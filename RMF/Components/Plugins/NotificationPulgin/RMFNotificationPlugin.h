/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */
#import <Foundation/Foundation.h>
#import "RMFNotifications.h"
#import <UIKit/UIKit.h>
#import "RMFDBAccessLayer.h"
#import "RMFConfiguration.h"



@interface RMFNotificationPlugin : NSObject

#pragma mark -
#pragma mark PROPERTY

@property(nonatomic,strong)NSString *deviceToken;

#pragma mark -
#pragma mark METHODS

//Used to register Application for Push Notification
-(void)registerPushNotificationForApplication:(UIApplication*)application;

//Used to save notification after receiving the response
-(NSError *)receivedNotification:(NSDictionary*)dictionary atDate:(NSDate *)date key:(NSString *)key;

//To fetch All saved notifications
-(NSArray*)getAllNotifications;

//To delete all saved notifications
-(NSError*)deleteAllNotification;

//To delete a notification
-(NSError*)deleteNotification:(RMFNotifications*)notification;

//To update a notification
-(NSError*)updateNotification:(RMFNotifications*)notification;

//Return Shared instance
+ (RMFNotificationPlugin *)sharedRMFNotificationPlugin;
@end
