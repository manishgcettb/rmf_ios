/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */
#import "RMFNotificationPlugin.h"
#import "RMFMacro.h"
#import "RMFConstants.h"


static RMFNotificationPlugin *sharedRMFNotificationPlugin;

@implementation RMFNotificationPlugin

#pragma mark - Class methods declaration

/*
 * Creates a singleton instance of RMFLoginPlugin
 */
+ (RMFNotificationPlugin *)sharedRMFNotificationPlugin {
	if (!OBJ_NOT_NIL(sharedRMFNotificationPlugin)) {
		sharedRMFNotificationPlugin = [[RMFNotificationPlugin alloc] init];
	}
	return sharedRMFNotificationPlugin;
}

#pragma mark - Instance methods implementation

/*
 * initialize the singleton instance of RMFLoginPlugin
 */
- (RMFNotificationPlugin *)init {
	if (!OBJ_NOT_NIL(sharedRMFNotificationPlugin)) {
		sharedRMFNotificationPlugin = [super init];
		[[NSNotificationCenter defaultCenter] addObserver:self
		                                         selector:@selector(receiveNotification:)
		                                             name:NOTIFICATION_REGISTER
		                                           object:nil];
		[[NSNotificationCenter defaultCenter] addObserver:self
		                                         selector:@selector(receiveNotification:)
		                                             name:NOTIFICATION_REGISTER_FAIL
		                                           object:nil];

		[[NSNotificationCenter defaultCenter] addObserver:self
		                                         selector:@selector(receiveNotification:)
		                                             name:NOTIFICATION_RECEIVED object:nil];
	}

	return sharedRMFNotificationPlugin;
}

//Used to register Application for Push Notification
- (void)registerPushNotificationForApplication:(UIApplication *)application {
	RMFConfiguration *config            = [RMFConfiguration sharedRMFConfiguration];

	if (config.notificationEnabled) {
		if ([application respondsToSelector:@selector(isRegisteredForRemoteNotifications)]) {
			// iOS 8 Notifications
			[application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];

			[application registerForRemoteNotifications];
		}
		else {
			// iOS < 8 Notifications
			[application registerForRemoteNotificationTypes:
			 (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
		}
	}
}

//Used to save notification after receiving the response
- (NSError *)receivedNotification:(NSDictionary *)dictionary atDate:(NSDate *)date key:(NSString *)key {
	RMFDBAccessLayer *databaseManager = [RMFDBAccessLayer sharedRMFDBAccessLayer];
	RMFConfiguration *config = [RMFConfiguration sharedRMFConfiguration];
	RMFNotifications *notification = [databaseManager insertNotifications];
	notification.message = [dictionary valueForKey:key];
	notification.notificationTime   = date;
	notification.readStatus = [NSNumber numberWithInt:kUnRead];
	notification.type = [NSString stringWithFormat:@"%d", config.system];
	return [databaseManager saveNotificationObject:notification];
}

//To fetch All saved notifications
- (NSArray *)getAllNotifications {
	NSArray *notifications = nil;
	RMFDBAccessLayer *databaseManager = [RMFDBAccessLayer sharedRMFDBAccessLayer];
	notifications = [databaseManager getNotifications];

	return notifications;
}

//To delete all saved notifications
- (NSError *)deleteAllNotification {
	NSError *error = nil;
	RMFDBAccessLayer *databaseManager = [RMFDBAccessLayer sharedRMFDBAccessLayer];
	error = [databaseManager deleteAllNotifications];

	return error;
}

//To delete a notification
- (NSError *)deleteNotification:(RMFNotifications *)notification {
	NSError *error = nil;
	if (notification) {
		RMFDBAccessLayer *databaseManager = [RMFDBAccessLayer sharedRMFDBAccessLayer];
		error = [databaseManager deleteNotifications:notification];
	}
	return error;
}

//To update a notification
- (NSBatchUpdateResult *)updateNotification:(RMFNotifications *)notification {
	NSBatchUpdateResult *result = nil;
	if (notification) {
		RMFDBAccessLayer *databaseManager = [RMFDBAccessLayer sharedRMFDBAccessLayer];
		result = [databaseManager updateNotifications:notification];
	}
	return result;
}

#pragma mark-
#pragma mark Push Notification Methods

//Handle Push Notification
- (void)receiveNotification:(NSNotification *)notification {
	if ([[notification name] isEqualToString:NOTIFICATION_REGISTER]) {
		RMF_LOG_VERBOSE([self class], NSStringFromSelector(_cmd), @"Registering notification");
		self.deviceToken = [[notification userInfo] valueForKey:DEVICE_TOKEN];
	}
	else if ([[notification name] isEqualToString:NOTIFICATION_REGISTER_FAIL]) {
		RMF_LOG_VERBOSE([self class], NSStringFromSelector(_cmd), @"Registering notification Failed");
	}
	else {
		RMF_LOG_VERBOSE([self class], NSStringFromSelector(_cmd), @"Receive notifications");
		[self receivedNotification:[notification userInfo] atDate:[NSDate date] key:NOTIFICATION_KEY];
	}
}

@end
