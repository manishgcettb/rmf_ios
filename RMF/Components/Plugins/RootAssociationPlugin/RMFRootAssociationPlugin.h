/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <Foundation/Foundation.h>
#import "RMFConstants.h"
#import "RMFBasePlugin.h"
#import "RMFRootAssociation.h"

@interface RMFRootAssociationPlugin : RMFBasePlugin

//get association data of a root from server
-(void) fetchParentAssoication:(int) type root:(NSDictionary *) root response:(void(^)(NSError *error, NSDictionary *associationList))completion;

//Shared instance
+ (RMFRootAssociationPlugin *)sharedRMFRootAssociationPlugin;

//Clear association data
- (void) clear;

//get association data of a root
- (RMFRootAssociation *)refreshforRoot:(NSString *)root ;
@end
