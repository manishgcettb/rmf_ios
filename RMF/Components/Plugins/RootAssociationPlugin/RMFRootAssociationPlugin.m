/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFRootAssociationPlugin.h"
#import "RMFSystemManager.h"
#import "RMFDBAccessLayer.h"
#import "RMFConfiguration.h"
#import "RMFMacro.h"
#import "RMFErrorHandler.h"
#import "RMFUtils.h"

@interface RMFRootAssociationPlugin ()
@property (nonatomic, strong) RMFSystemManager *systemManager;
@end

static RMFRootAssociationPlugin *rmfRootAssociationPlugin = nil;


@implementation RMFRootAssociationPlugin

- (void)fetchParentAssoication:(int)type root:(NSDictionary *)root response:(void (^)(NSError *error, NSDictionary *associationList))completion {
    // Error Keys check are handled in respective system controller
    
    switch (type) {
        case (KUMOSystem)kTypeVAR:
        {
            
            RMFConfiguration *configuration = [RMFConfiguration sharedRMFConfiguration];
            
            if (configuration.userType == kUserRoleSuper) {
                // Request to get the details
                [self tenantList:root response:completion];
            } else {
                if (completion) {
                    NSError *error = [NSError errorWithDomain:[RMFErrorHandler getErrorMessageForSystem:RMFCodeForWrongRootType] code:RMFCodeForWrongRootType userInfo:nil];
                    completion(error, nil);
                }
            }
        }
            break;
            
        case (KUMOSystem)kTypeTenant:
            
            // Request to get the details
            
            [self venueList:root response:completion];
            
            break;
            
        case (KUMOSystem)kTypeVenue:
            
            // Request to get the details
            if ([[root allKeys] containsObject:WLAN]) {
                [self wlanList:root response:completion];
            }
            if ([[root allKeys] containsObject:kAPKey]) {
                [self apList:root response:completion];
            }
            break;
            
        case (SCGSystem)kTypeZone:
            break;
            
        default:
            break;
    }
}

#pragma mark - Class methods declaration

/*
 * Creates a singleton instance of RMFWlanPlugin
 */

+ (RMFRootAssociationPlugin *)sharedRMFRootAssociationPlugin {
    if (!OBJ_NOT_NIL(rmfRootAssociationPlugin)) {
        rmfRootAssociationPlugin = [[RMFRootAssociationPlugin alloc] init];
    }
    
    return rmfRootAssociationPlugin;
}

#pragma mark - Instance methods implementation

/*
 * initialize the singleton instance of RMFLoginPlugin
 */

- (RMFRootAssociationPlugin *)init {
    if (!OBJ_NOT_NIL(rmfRootAssociationPlugin)) {
        rmfRootAssociationPlugin = [super init];
        
        // Root association plugin instance will be created successfully only when the system manager is configured properly
        if (rmfRootAssociationPlugin && [RMFUtils isSystemManagerConfigured:[RMFSystemManager sharedRMFSystemManager]]) {
            self.systemManager = [RMFSystemManager sharedRMFSystemManager];
        }
    }
    return rmfRootAssociationPlugin;
}

/*
 * Requests network controller to fetch tenant list, sends the response to the parser and returns list of tenant after parsing
 */

- (void)tenantList:(NSDictionary *)requestParams response:(void (^)(NSError *error,  NSDictionary *assoicationList))completion {
    // Call network controller to load tenant list from the server
    [self.systemManager.activeController tenantListWithresponse:completion];
}

/*
 * Requests network controller to fetch Venue list, sends the response to the parser and returns list of Venue after parsing
 */

- (void)venueList:(NSDictionary *)requestParams response:(void (^)(NSError *error,  NSDictionary *assoicationList))completion {
    // Call network controller to load venue list from the server
    [self.systemManager.activeController venueList:requestParams response:completion];
}

/*
 * Requests network controller to fetch AP list, sends the response to the parser and returns list of AP's after parsing
 */

- (void)apList:(NSDictionary *)requestParams response:(void (^)(NSError *error,  NSDictionary *assoicationList))completion {
    // Call network controller to load AP list from the server
    [self.systemManager.activeController apList:requestParams response:completion];
}

/* Requests network controller to fetch wlan's list, sends the response to the parser and returns list of wlan's after parsing
 */

- (void)wlanList:(NSDictionary *)requestParams response:(void (^)(NSError *error,  NSDictionary *assoicationList))completion {
    // Call network controller to load wlan list from the server
    [self.systemManager.activeController wlanServiceList:requestParams response:completion];
}

#pragma mark - DB operation

//Clear association data
- (void)clear {
    RMFDBAccessLayer *dbAccesslayer = [RMFDBAccessLayer sharedRMFDBAccessLayer];
    [dbAccesslayer deleteAllAssociation];
}

//get association data of a root
- (RMFRootAssociation *)refreshforRoot:(NSString *)root {
    RMFRootAssociation *rootAssociation = nil;
    RMFDBAccessLayer *dbAccesslayer = [RMFDBAccessLayer sharedRMFDBAccessLayer];
    NSArray *associationArray = [dbAccesslayer getRootAssociation:root];
    if (associationArray && [associationArray count] > 0) {
        rootAssociation = [associationArray objectAtIndex:0];
    }
    return rootAssociation;
}

@end
