/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFEventPlugin.h"
#import "RMFMacro.h"
#import "RMFEvent.h"
#import "RMFUtils.h"
#import "RMFConfiguration.h"
#import "RMFDBAccessLayer.h"

@interface RMFEventPlugin ()
// Stores RMFSystemManager instance
@property (nonatomic, strong) RMFSystemManager *systemManager;

@end


@implementation RMFEventPlugin

// Stores the singleton instance of RMFEventPlugin
static RMFEventPlugin *sharedRMFEventPlugin = nil;

#pragma mark - Class methods declaration

/*
 * Creates a singleton instance of RMFEventPlugin
 */

+ (RMFEventPlugin *)sharedRMFEventPlugin {
	if (!OBJ_NOT_NIL(sharedRMFEventPlugin)) {
		sharedRMFEventPlugin = [[RMFEventPlugin alloc] init];
	}
	return sharedRMFEventPlugin;
}

#pragma mark - Instance methods implementation

/*
 * initialize the singleton instance of RMFEventPlugin
 */

- (RMFEventPlugin *)init {
	if (!OBJ_NOT_NIL(sharedRMFEventPlugin)) {
		sharedRMFEventPlugin = [super init];

		// Event plugin instance will be created successfully only when the system manager is configured properly
		if (sharedRMFEventPlugin && [RMFUtils isSystemManagerConfigured:[RMFSystemManager sharedRMFSystemManager]]) {
			self.systemManager = [RMFSystemManager sharedRMFSystemManager];
		}
	}

	return sharedRMFEventPlugin;
}

/*
 * Makes network request to fetch Event list with request parameters and returns the network response
 */

- (void)eventList:(NSDictionary *)requestParams response:(void (^)(NSError *error, NSArray *eventList))completion {
	// Call network controller to load Event list from the server
	[self.systemManager.activeController eventList:requestParams response: ^(NSError *error, id data) {
	    if (data) {
	        // Response received from controller, send it for parsing
	        [self.systemManager.activeParser parseEventListWithInput:data output: ^(NSError *error, NSArray *eventList) {
	            if (completion) {
	                completion(error, eventList);
	                RMF_LOG_DEBUG([self class], NSStringFromSelector(_cmd), REQUEST_SUCCESS(kEventList));
				}
                
                // If database is enabled store event list in the database
                if ([[RMFConfiguration sharedRMFConfiguration] isDatabaseEnabled]) {
                    for (RMFEvent *event in eventList) {
                        [[RMFDBAccessLayer sharedRMFDBAccessLayer] insertEvent:event];
                    }
                }

			}];
		}
	    else if (completion) {
	        completion(error, nil);
	        NSString *logDescription = [NSString stringWithFormat:@"%@ : %@", ERROR, error.description];
	        RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
            
            NSArray *eventList = nil;
            if ([[RMFConfiguration sharedRMFConfiguration] isDatabaseEnabled]) {
                eventList = [[RMFDBAccessLayer sharedRMFDBAccessLayer] eventList];
            }
            completion(error, eventList);
		}
	}];
}

/*
 * Makes network request to fetch Event details with request parameters and returns the network response
 */
- (void)eventDetails:(NSDictionary *)requestParams response:(void (^)(NSError *error,  RMFEvent *event))completion {
	// Call network controller to load Event details from the server
	[self.systemManager.activeController eventDetails:requestParams response: ^(NSError *error, id data) {
	    if (data) {
	        // Response received from controller, send it for parsing
	        [self.systemManager.activeParser parseEventDetailsWithInput:data output: ^(NSError *error, RMFEvent *event) {
	            if (completion) {
	                completion(error, event);
	                RMF_LOG_DEBUG([self class], NSStringFromSelector(_cmd), REQUEST_SUCCESS(kEventDetail));
				}
                
                // If database is enabled update wlan entry in the database
                if ([[RMFConfiguration sharedRMFConfiguration] isDatabaseEnabled]) {
                    [[RMFDBAccessLayer sharedRMFDBAccessLayer] updateEvent:event];
                }

			}];
		}
	    else if (completion) {
           
            RMFEvent *event = nil;
            if ([[RMFConfiguration sharedRMFConfiguration] isDatabaseEnabled]) {
                event = [[RMFDBAccessLayer sharedRMFDBAccessLayer] eventDetails:[[requestParams objectForKey:kEventTypeIdKey] integerValue]];
            }
            completion(error, event);

	        completion(error, nil);
	        NSString *logDescription = [NSString stringWithFormat:@"%@ : %@", ERROR, error.description];
	        RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
		}
	}];
}

/*
 * Requests network controller to fetch event list based on given filtering and sorting criteria,sends the
 * response to the parser and returns the parsed response
 */

- (void)eventList:(NSDictionary *)requestParams sortBy:(kEventSortingParameter)sortingParameter filterOn:(NSDictionary *)filter response:(void (^)(NSError *, id))completion {
    [self.systemManager.activeController eventList:requestParams sortBy:sortingParameter filterOn:filter response: ^(NSError *error, id data) {
        if (data) {
            [self.systemManager.activeParser parseEventList:data output: ^(NSError *error, NSArray *eventList) {
                if (completion) {
                    completion(error, eventList);
                    RMF_LOG_VERBOSE([self class], NSStringFromSelector(_cmd), REQUEST_SUCCESS(kEventList));
                }
                
                // If database is enabled store event list in the database
                if ([[RMFConfiguration sharedRMFConfiguration] isDatabaseEnabled]) {
                    for (RMFEvent *event in eventList) {
                        [[RMFDBAccessLayer sharedRMFDBAccessLayer] insertEvent:event];
                    }
                }

            }];
        }
        else {
            if (completion) {
                
                NSArray *eventList = nil;
                
                if ([[RMFConfiguration sharedRMFConfiguration] isDatabaseEnabled]) {
                    eventList = [[RMFDBAccessLayer sharedRMFDBAccessLayer] eventList];
                }
                completion(error, eventList);
                
                NSString *logDescription = [NSString stringWithFormat:@"%@ : %@", ERROR, error.description];
                RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
            }
        }
    }];
}

@end
