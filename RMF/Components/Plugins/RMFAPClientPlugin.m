/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFAPClientPlugin.h"
#import "RMFMacro.h"
#import "RMFClientDetail.h"
#import "RMFUtils.h"


@interface RMFAPClientPlugin ()
// Stores RMFSystemManager instance
@property (nonatomic, strong) RMFSystemManager *systemManager;

@end


@implementation RMFAPClientPlugin

// Stores the singleton instance of RMFClientPlugin
static RMFAPClientPlugin *sharedRMFClientPlugin = nil;

#pragma mark - Class methods declaration

/*
 * Creates a singleton instance of RMFClientPlugin
 */

+ (RMFAPClientPlugin *)sharedRMFClientPlugin {
	if (!OBJ_NOT_NIL(sharedRMFClientPlugin)) {
		sharedRMFClientPlugin = [[RMFAPClientPlugin alloc] init];
	}
	return sharedRMFClientPlugin;
}

#pragma mark - Instance methods implementation

/*
 * initialize the singleton instance of RMFClientPlugin
 */

- (RMFAPClientPlugin *)init {
	if (!OBJ_NOT_NIL(sharedRMFClientPlugin)) {
		sharedRMFClientPlugin = [super init];

		// Client plugin instance will be created successfully only when the system manager is configured properly
		if (sharedRMFClientPlugin && [RMFUtils isSystemManagerConfigured:[RMFSystemManager sharedRMFSystemManager]]) {
			self.systemManager = [RMFSystemManager sharedRMFSystemManager];
		}
	}

	return sharedRMFClientPlugin;
}

#pragma mark - client list
/*
 * Makes network request to fetch client list with request parameters and returns the network response
 */

- (void)clientList:(NSDictionary *)requestParams response:(void (^)(NSError *error, NSArray *clientList))completion {
	// Call network controller to load client list from the server
	[self.systemManager.activeController clientList:requestParams response: ^(NSError *error, id data) {
	    if (data) {
	        // Response received from controller, send it for parsing
	        [self.systemManager.activeParser parseClientListWithInput:data output: ^(NSError *error, id clientList) {
	            if (completion) {
	                if (clientList && [clientList isKindOfClass:[NSArray class]])
						completion(error, (NSArray *)clientList);
	                NSString * logDescription = [NSString stringWithFormat:@"%@ : %@", kClientList, clientList];
	                RMF_LOG_DEBUG([self class], NSStringFromSelector(_cmd), logDescription);
				}
			}];
		}
	    else if (completion) {
	        completion(error, nil);
	        NSString *logDescription = [NSString stringWithFormat:@"%@ : %@", ERROR, error.description];
	        RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
		}
	}];
}

/*
 * Makes network request to fetch active/blocked client list  with request parameters and returns the network response
 */
- (void)clientList:(NSDictionary *)requestParams clientType:(kClientType)clientType response:(void (^)(NSError *error, NSArray *clientList))completion {
	// Call network controller to load client list from the server
	[self.systemManager.activeController clientList:requestParams clientType:clientType response: ^(NSError *error, id data) {
	    if (data) {
	        // Response received from controller, send it for parsing
	        [self.systemManager.activeParser parseClientListWithInput:data output: ^(NSError *error, id clientList) {
	            if (completion) {
	                NSString *logDescription = [NSString stringWithFormat:@"%@ : %@", kClientList, clientList];
	                RMF_LOG_DEBUG([self class], NSStringFromSelector(_cmd), logDescription);
	                if (clientList && [clientList isKindOfClass:[NSArray class]])
						completion(error, (NSArray *)clientList);
				}
			}];
		}
	    else if (completion) {
	        NSString *logDescription = [NSString stringWithFormat:@"%@ : %@", ERROR, error.description];
	        RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
	        completion(error, nil);
		}
	}];
}

#pragma mark - client details

/*
 * Makes network request to fetch Client details with request parameters and returns the network response
 */
- (void)clientDetails:(NSDictionary *)requestParams response:(void (^)(NSError *error,  RMFClientDetail *clientDetail))completion {
	// Call network controller to load Client details from the server
	[self.systemManager.activeController clientDetails:requestParams response: ^(NSError *error, id data) {
	    if (data) {
	        // Response received from controller, send it for parsing
	        [self.systemManager.activeParser parseClientDetailsWithInput:data output: ^(NSError *error, RMFClientDetail *clientDetails) {
	            if (completion) {
	                completion(error, clientDetails);
	                NSString *logDescription = [NSString stringWithFormat:@"%@ : %@", kClientDetails, clientDetails];
	                RMF_LOG_DEBUG([self class], NSStringFromSelector(_cmd), logDescription);
				}
			}];
		}
	    else if (completion) {
	        completion(error, nil);
	        NSString *logDescription = [NSString stringWithFormat:@"%@ : %@", ERROR, error.description];
	        RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
		}
	}];
}

#pragma mark - client filtering
/*
 * Requests network controller to fetch client list based on given filtering and sorting criteria,sends the
 * response to the parser and returns the parsed response
 */
- (void)clientListFilteredBy:(NSDictionary *)filteringParameters sortBy:(NSArray *)sortingParameters limit:(NSNumber *)limit offset:(NSNumber *)offset response:(void (^)(NSError *, id))completion {
	// Call network controller to fetch Wlan list
	[self.systemManager.activeController clientListFilteredBy:filteringParameters sortBy:sortingParameters limit:limit offset:offset response: ^(NSError *error, id data) {
	    if (data) {
	        // Response received from controller, send it for parsing
	        [self.systemManager.activeParser parseClientListFetchedUsingCustomParams:data output: ^(NSError *error, NSArray *wlanServiceList) {
	            if (completion) {
	                completion(error, wlanServiceList);
				}
			}];
		}
	    else {
	        if (completion) {
	            completion(error, nil);
			}
		}
	}];
}

@end
