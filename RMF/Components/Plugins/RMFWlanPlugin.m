/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFWlanPlugin.h"
#import "RMFMacro.h"
#import "RMFSystemManager.h"
#import "RMFUtils.h"
#import "RMFConfiguration.h"
#import "RMFDBAccessLayer.h"
#import "RMFErrorHandler.h"

@interface RMFWlanPlugin ()

// Stores RMFSystemManager instance
@property (nonatomic, strong) RMFSystemManager *systemManager;

@end

@implementation RMFWlanPlugin

static RMFWlanPlugin *sharedRMFWlanPlugin = nil;

#pragma mark - Class methods declaration

/*
 * Creates a singleton instance of RMFWlanPlugin
 */
+ (RMFWlanPlugin *)sharedRMFWlanPlugin {
	if (!OBJ_NOT_NIL(sharedRMFWlanPlugin)) {
		sharedRMFWlanPlugin = [[RMFWlanPlugin alloc] init];
	}

	return sharedRMFWlanPlugin;
}

#pragma mark - Instance methods implementation

/*
 * initialize the singleton instance of RMFLoginPlugin
 */
- (RMFWlanPlugin *)init {
	if (!OBJ_NOT_NIL(sharedRMFWlanPlugin)) {
		sharedRMFWlanPlugin = [super init];

		// Login plugin instance will be created successfully only when the system manager is configured properly
		if (sharedRMFWlanPlugin && [RMFUtils isSystemManagerConfigured:[RMFSystemManager sharedRMFSystemManager]]) {
			self.systemManager = [RMFSystemManager sharedRMFSystemManager];
		}
	}
	return sharedRMFWlanPlugin;
}

/*
 * Requests network controller to fetch wlan list, sends the response to the parser and
 * returns list of wlans after parsing
 */
- (void)wlanList:(NSDictionary *)requestParams response:(void (^)(NSError *error,  NSArray *wlanList))completion {
	// Call network controller to load wlan list from the server
	[self.systemManager.activeController wlanServiceList:requestParams response: ^(NSError *error, id data) {
	    if (data) {
	        // Response received from controller, send it for parsing
	        [self.systemManager.activeParser parseWlanServiceListWithInput:data output: ^(NSError *error, NSArray *wlanList) {
	            //Data received after parsing send it upper layer
	            if (completion) {
	                RMF_LOG_DEBUG([self class], NSStringFromSelector(_cmd), REQUEST_SUCCESS(kWlanServiceList));
	                completion(error, wlanList);
				}
                
                // If database is enabled store wlan list in the database
                if ([[RMFConfiguration sharedRMFConfiguration] isDatabaseEnabled]) {
                    for (RMFWlanService *wlan in wlanList) {
                        [[RMFDBAccessLayer sharedRMFDBAccessLayer] insertWlan:wlan];
                    }
                }

			}];
		}
	    else {
	        if (completion) {
                
                NSArray *wlanList = nil;
                if ([[RMFConfiguration sharedRMFConfiguration] isDatabaseEnabled]) {
                    wlanList = [[RMFDBAccessLayer sharedRMFDBAccessLayer] wlanList];
                }

	            completion(error, wlanList);
	            NSString *logDescription = [NSString stringWithFormat:@"%@ : %@", ERROR, error.description];
	            RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
			}
		}
	}];
}

/*
 * Requests network controller to fetch wlan list, sends the response to the parser and
 * returns list of wlans count after parsing
 */
- (void)wlanCount:(NSDictionary *)requestParams response:(void (^)(NSError *error,  NSInteger wlanCount))completion {
	// Call network controller to load wlan list from the server
	[self.systemManager.activeController wlanServiceList:requestParams response: ^(NSError *error, id data) {
	    if (data) {
	        // Response received from controller, send it for parsing
	        [self.systemManager.activeParser parseWlanServiceListWithInput:data output: ^(NSError *error, NSArray *wlanList) {
	            //Data received after parsing send it upper layer
	            if (completion) {
	                completion(error, [wlanList count]);
	                NSString *logDescription = [NSString stringWithFormat:@"%@ : %lu", REQUEST_SUCCESS(kWlanServiceList), (unsigned long)[wlanList count]];
	                RMF_LOG_DEBUG([self class], NSStringFromSelector(_cmd), logDescription);
				}
                
                // If database is enabled store wlan list in the database
                if ([[RMFConfiguration sharedRMFConfiguration] isDatabaseEnabled]) {
                    for (RMFWlanService *wlan in wlanList) {
                        [[RMFDBAccessLayer sharedRMFDBAccessLayer] insertWlan:wlan];
                    }
                }
			}];
		}
	    else {
	        if (completion) {
                
                NSArray *wlanList = nil;
                if ([[RMFConfiguration sharedRMFConfiguration] isDatabaseEnabled]) {
                    wlanList = [[RMFDBAccessLayer sharedRMFDBAccessLayer] wlanList];
                }
                
	            completion(error, [wlanList count]);
	            NSString *logDescription = [NSString stringWithFormat:@"%@ : %@", ERROR, error.description];
	            RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
			}
		}
	}];
}

/*
 * Requests network controller to fetch wlan service details, sends the response to the parser
 * and returns wlan model object after parsing
 */
- (void)wlanDetails:(NSDictionary *)requestParams response:(void (^)(NSError *error,  RMFWlanService *wlan))completion {
	// Call network controller to load wlan details from the server
	[self.systemManager.activeController wlanServiceDetails:requestParams response: ^(NSError *error, id data) {
	    if (data) {
	        // Response received from controller, send it for parsing
	        [self.systemManager.activeParser parseWlanServiceDetailsWithInput:data output: ^(NSError *error, RMFWlanService *wlan) {
	            //Data received after parsing send it upper layer

	            if (completion) {
	                completion(error, wlan);
	                RMF_LOG_DEBUG([self class], NSStringFromSelector(_cmd), REQUEST_SUCCESS(kWlanServiceDetails));
				}
                
                // If database is enabled update wlan entry in the database
                if ([[RMFConfiguration sharedRMFConfiguration] isDatabaseEnabled]) {
                    [[RMFDBAccessLayer sharedRMFDBAccessLayer] updateWlan:wlan];
                }
			}];
		}
	    else {
	        if (completion) {
                
                RMFWlanService *wlan = nil;
                if ([[RMFConfiguration sharedRMFConfiguration] isDatabaseEnabled]) {
                    wlan = [[RMFDBAccessLayer sharedRMFDBAccessLayer] wlanDetails:[requestParams objectForKey:kWlanSssidKey]];
                }
                
	            completion(error, wlan);
	            NSString *logDescription = [NSString stringWithFormat:@"%@ : %@", ERROR, error.description];
	            RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
			}
		}
	}];
}

/*
 * Requests network controller to fetch wlan status details, sends the response to the parser
 * and returns wlan status model object after parsing
 */
- (void)wlanStatus:(NSDictionary *)requestParams response:(void (^)(NSError *error,  RMFWlanServiceStatus *wlanStatus))completion {
	// Call network controller to load wlan details from the server
	[self.systemManager.activeController wlanStatus:requestParams response: ^(NSError *error, id data) {
	    if (data) {
	        // Response received from controller, send it for parsing
	        [self.systemManager.activeParser parseWlanServiceStatusWithInput:data output: ^(NSError *error, RMFWlanServiceStatus *wlanStatus) {
	            //Data received after parsing send it to the upper layer
	            if (completion) {
	                completion(error, wlanStatus);
	                RMF_LOG_DEBUG([self class], NSStringFromSelector(_cmd), REQUEST_SUCCESS(kWlanStatus));
				}
			}];
		}
	    else {
	        if (completion) {
	            completion(error, nil);
	            NSString *logDescription = [NSString stringWithFormat:@"%@ : %@", ERROR, error.description];
	            RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
			}
		}
	}];
}

/*
 * Requests network controller to create new Wlan, send the response to the parser and returns the
 * parsed response
 */
- (void)createWlan:(NSDictionary *)requestParams response:(void (^)(NSError *error, NSString *createWlanResponse))completion {
	// Call network controller to add new Wlan
	[self.systemManager.activeController createWlan:requestParams response: ^(NSError *error, id data) {
	    if (data) {
	        // Response received from controller, send it for parsing
	        [self.systemManager.activeParser parseCreateWlanResponseWithInput:data output: ^(NSError *error, NSString *createWlanResponse) {
	            //Data received after parsing send it to the upper layer
	            if (OBJ_NOT_NIL(completion)) {
	                completion(error, createWlanResponse);
	                RMF_LOG_VERBOSE([self class], NSStringFromSelector(_cmd), ADD_REQUEST_SUCCESS(kWlan));
				}
			}];
            
            // If database is enabled edit wlan entry in the database
            if ([[RMFConfiguration sharedRMFConfiguration] isDatabaseEnabled]) {
                
                NSMutableDictionary *wlanInfoDictionary = [requestParams objectForKey:kNewWlanInfoKey];
                if (wlanInfoDictionary) {
                    RMFWlanService *wlan = [[RMFWlanService alloc] init];
                    wlan.name = [wlanInfoDictionary objectForKey:kWlanNameKey];
                    wlan.ssid = [wlanInfoDictionary objectForKey:kWlanSssidKey];
                    wlan.pskPassPhrase = [wlanInfoDictionary objectForKey:kPskPassPhraseKey];
                    wlan.wlanType = [wlanInfoDictionary objectForKey:kWlanTypeKey];
                    wlan.wlanSecurity = [wlanInfoDictionary objectForKey:kWlanSecurityKey];
                    [[RMFDBAccessLayer sharedRMFDBAccessLayer] insertWlan:wlan];
                }
            }
		}
	    else {
	        if (OBJ_NOT_NIL(completion)) {
	            completion(error, nil);
	            NSString *logDescription = [NSString stringWithFormat:@"%@ : %@", ERROR, error.description];
	            RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
			}
		}
	}];
}

/*
 * Requests network controller to delete Wlan, send the response to the parser and returns the
 * parsed response
 */
- (void)deleteWlan:(NSDictionary *)requestParams response:(void (^)(NSError *error, NSString *deleteWlanResponse))completion {
	// Call network controller to delete Wlan
	[self.systemManager.activeController deleteWlan:requestParams response: ^(NSError *error, id data) {
	    if (data) {
	        // Response received from controller, send it for parsing
	        [self.systemManager.activeParser parseDeleteWlanResponseWithInput:data output: ^(NSError *error, NSString *createWlanResponse) {
                
	            //Data received after parsing send it to the upper layer
	            if (OBJ_NOT_NIL(completion)) {
	                completion(error, createWlanResponse);
	                RMF_LOG_VERBOSE([self class], NSStringFromSelector(_cmd), DELETE_REQUEST_SUCCESS(kWlan));
				}
			}];
            
            // If database is enabled delete wlan entry from the database
            if ([[RMFConfiguration sharedRMFConfiguration] isDatabaseEnabled]) {
                [[RMFDBAccessLayer sharedRMFDBAccessLayer] deleteWlan:[requestParams objectForKey:kWlanSssidKey]];
            }
		}
	    else {
	        if (OBJ_NOT_NIL(completion)) {
	            completion(error, nil);
	            NSString *logDescription = [NSString stringWithFormat:@"%@ : %@", ERROR, error.description];
	            RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
			}
		}
	}];
}

/*
 * Requests network controller to edit Wlan, sends the response to the parser and returns the
 * parsed response
 */
- (void)editWlan:(NSDictionary *)requestParams response:(void (^)(NSError *error, NSString *editWlanResponse))completion {
	// Call network controller to delete Wlan
	[self.systemManager.activeController editWlan:requestParams response: ^(NSError *error, id data) {
	    if (data) {
	        // Response received from controller, send it for parsing
	        [self.systemManager.activeParser parseEditWlanResponseWithInput:data output: ^(NSError *error, NSString *editWlanResponse) {
	            //Data received after parsing send it to the upper layer
	            if (OBJ_NOT_NIL(completion)) {
	                completion(error, editWlanResponse);
	                RMF_LOG_VERBOSE([self class], NSStringFromSelector(_cmd), EDIT_REQUEST_SUCCESS(kWlan));
				}
			}];
            
            // If database is enabled edit wlan entry in the database
            if ([[RMFConfiguration sharedRMFConfiguration] isDatabaseEnabled]) {
                RMFWlanService *wlan = [[RMFWlanService alloc] init];
                wlan.name = [requestParams objectForKey:kWlanNameKey];
                wlan.ssid = [requestParams objectForKey:kWlanSssidKey];
                wlan.pskPassPhrase = [requestParams objectForKey:kPskPassPhraseKey];
                wlan.wlanType = [requestParams objectForKey:kWlanTypeKey];
                wlan.wlanSecurity = [requestParams objectForKey:kWlanSecurityKey];
                [[RMFDBAccessLayer sharedRMFDBAccessLayer] updateWlan:wlan];
            }
            
		}
	    else {
	        if (OBJ_NOT_NIL(completion)) {
	            completion(error, nil);
	            NSString *logDescription = [NSString stringWithFormat:@"%@ : %@", ERROR, error.description];
	            RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
			}
		}
	}];
}

/*
 * Requests network controller to fetch Wlan list based on given filtering and sorting criteria,sends the
 * response to the parser and returns the parsed response
 */

- (void)wlanList:(NSDictionary *)requestParams sortBy:(kWlanSortingParameter)sortingParameter filterOn:(NSDictionary *)filter response:(void (^)(NSError *, id))completion {
	[self.systemManager.activeController wlanList:requestParams sortBy:sortingParameter filterOn:filter response: ^(NSError *error, id data) {
	    if (data) {
	        [self.systemManager.activeParser parseWlanListWithInput:data output: ^(NSError *error, NSArray *wlanServiceList) {
	            if (completion) {
	                completion(error, wlanServiceList);
	                RMF_LOG_VERBOSE([self class], NSStringFromSelector(_cmd), REQUEST_SUCCESS(kWlanServiceList));
				}
                
                // If database is enabled store wlan list in the database
                if ([[RMFConfiguration sharedRMFConfiguration] isDatabaseEnabled]) {
                    for (RMFWlanService *wlan in wlanServiceList) {
                        [[RMFDBAccessLayer sharedRMFDBAccessLayer] insertWlan:wlan];
                    }
                }
			}];
		}
	    else {
	        if (completion) {
                
                NSArray *wlanList = nil;
                
                if ([[RMFConfiguration sharedRMFConfiguration] isDatabaseEnabled]) {
                    wlanList = [[RMFDBAccessLayer sharedRMFDBAccessLayer] wlanList];
                }
                completion(error, wlanList);

	            NSString *logDescription = [NSString stringWithFormat:@"%@ : %@", ERROR, error.description];
	            RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
			}
		}
	}];
}

@end
