/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFBasePlugin.h"
#import "RMFAccessPoint.h"

// enum for AP sorting parameter

typedef enum {
	kSortingParameterName = 1,
	kSortingParameterMacAddress,
	kSortingParameterSerialNumber,
	kSortingParameterWlanCount,
	kSortingParameterNone
}kSortingParameter;


@interface RMFAPPlugin : RMFBasePlugin

// Returns the singleton instance of RMFAPPlugin
+ (RMFAPPlugin *)sharedRMFAPPlugin;

//Requests network controller to add new AP and returns RMFAccessPoint object
- (void)createAP:(NSDictionary *)requestParams response:(void (^)(NSError *error, RMFAccessPoint *rmfAccessPoint))completion;

//Requests network controller to get AP detail and returns RMFAccessPoint object
- (void)apDetail:(NSDictionary *)requestParams response:(void (^)(NSError *error, RMFAccessPoint *rmfAccessPoint))completion;

//Requests network controller to edit AP and returns RMFAccessPoint object
- (void)editAP:(NSDictionary *)requestParams response:(void (^)(NSError *error, RMFAccessPoint *rmfAccessPoint))completion;

//Requests network controller to delete AP and returns BOOL object
- (void)deleteAP:(NSDictionary *)requestParams response:(void (^)(NSError *error, BOOL success))completion;

//Requests network controller to get AP list and returns list of AP's
- (void)apList:(NSDictionary *)requestParams response:(void (^)(NSError *error,  NSArray *apList))completion;


// This method using QUERY API for get AP list
/**
 * Requests network controller to get AP list and returns filtered, sorted list of AP's

 * @param requestParams, dictionary for keys (tenant_name,venue_name,offset,limit) optional
 * @param sort, enum for sorting the list
 * @param filter, dictionary to filter the list for keys (extended-status/name,serial-number,status/mac-address,wlan-status/wlans/ssid,wlan-status/total-count)
 * @return error if any and aplist
 */
- (void)apList:(NSDictionary *)requestParams sortBy:(kSortingParameter)sort filterOn:(NSDictionary *)filter response:(void (^)(NSError *error,  NSArray *apList))completion;

// Return all possible keys use in RMFAPPlugin
- (NSArray *)allKeyForRequestParameter;

// Requests network controller to download image for an access point
- (void)downloadAPImage:(NSDictionary *)requestParams response:(void(^)(NSError *error, NSData *image))completion;

// Requests network controller to upload image for an access point
- (void)uploadAPImage:(NSDictionary *)requestParams response:(void(^)(NSError *error, id data))completion;

@end
