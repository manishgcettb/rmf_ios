/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFAPPlugin.h"
#import "RMFSystemManager.h"
#import "RMFMacro.h"
#import "RMFKUMOController.h"
#import "RMFUtils.h"
#import "RMFErrorHandler.h"
#import "RMFDBAccessLayer.h"
#import "RMFImageLoader.h"

@interface RMFAPPlugin ()
// Stores RMFSystemManager instance
@property (nonatomic, strong) RMFSystemManager *systemManager;

// Store and returns NSCache instance
@property (nonatomic, strong) NSCache *imageCache;

@end

@implementation RMFAPPlugin
static RMFAPPlugin *sharedRMFAPPlugin = nil;

/*
 * Creates a singleton instance of RMFAPPlugin
 */
+ (RMFAPPlugin *)sharedRMFAPPlugin {
	if (!OBJ_NOT_NIL(sharedRMFAPPlugin)) {
		sharedRMFAPPlugin = [[RMFAPPlugin alloc] init];
	}
	return sharedRMFAPPlugin;
}

/*
 * initialize the singleton instance of RMFAPPlugin
 */
- (id)init {
	if (!OBJ_NOT_NIL(sharedRMFAPPlugin)) {
		sharedRMFAPPlugin = [super init];
		// AP plugin instance will be created successfully only when the system manager is configured properly
		if (sharedRMFAPPlugin && [RMFUtils isSystemManagerConfigured:[RMFSystemManager sharedRMFSystemManager]]) {
			self.systemManager = [RMFSystemManager sharedRMFSystemManager];
		}
		// Allocate memory for image caching, set count and total cost limit
		self.imageCache = [[NSCache alloc] init];
		[self.imageCache setCountLimit:kCacheCountLimit];
		[self.imageCache setTotalCostLimit:kCacheTotalCostLimit];
	}
	return sharedRMFAPPlugin;
}

/*
 * Requests network controller to add new AP, send the response to the parser and returns RMFAccessPoint after parsing
 */
- (void)createAP:(NSDictionary *)requestParams response:(void (^)(NSError *error, RMFAccessPoint *rmfAccessPoint))completion {
	// Call network controller to add new AP
	[self.systemManager.activeController createAP:requestParams response: ^(NSError *error, id data) {
	    if (data) {
	        // Response received from controller, send it for parsing
	        [self.systemManager.activeParser parseCreateAPWithInput:data output: ^(NSError *error, RMFAccessPoint *rmfAccessPoint) {
	            //Data received after parsing send it to the upper layer
	            if (OBJ_NOT_NIL(completion)) {
	                RMF_LOG_VERBOSE([self class], NSStringFromSelector(_cmd), kAPCreatedSuccessfully);

	                completion(error, rmfAccessPoint);

	                // Check if database enabled insert value in database
	                if ([[RMFConfiguration sharedRMFConfiguration] isDatabaseEnabled]) {
	                    [[RMFDBAccessLayer sharedRMFDBAccessLayer] insertAP:rmfAccessPoint];
					}
				}
			}];
		}
	    else {
	        if (OBJ_NOT_NIL(completion)) {
	            NSString *logDescription = [NSString stringWithFormat:@"%@ : %@", ERROR, error.description];
	            RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
	            completion(error, nil);
			}
		}
	}];
}

/*
 * Requests network controller to get AP list, send the response to the parser and returns list of AP's after parsing
 */
- (void)apList:(NSDictionary *)requestParams response:(void (^)(NSError *error,  NSArray *apList))completion {
	// Call network controller to get AP list from the server
	[self.systemManager.activeController apList:requestParams response: ^(NSError *error, id data) {
	    if (data) {
	        // Response received from controller, send it for parsing
	        [self.systemManager.activeParser parseAPListWithInput:data output: ^(NSError *error, NSArray *apList) {
	            //Data received after parsing send it to the upper layer
	            if (OBJ_NOT_NIL(completion)) {
	                RMF_LOG_VERBOSE([self class], NSStringFromSelector(_cmd), kAPListReceivedSuccessfully);

	                completion(error, apList);

	                // Check if database enabled insert value in database
	                if ([[RMFConfiguration sharedRMFConfiguration] isDatabaseEnabled]) {
	                    for (RMFAccessPoint *accessPoint in apList) {
	                        [[RMFDBAccessLayer sharedRMFDBAccessLayer] insertAP:accessPoint];
						}
					}
				}
			}];
		}
	    else if (OBJ_NOT_NIL(completion)) {
	        NSString *logDescription = [NSString stringWithFormat:@"%@ : %@", ERROR, error.description];
	        RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);

	        // Check if database enabled return AP list from database
	        if ([[RMFConfiguration sharedRMFConfiguration] isDatabaseEnabled]) {
	            completion(error, [[RMFDBAccessLayer sharedRMFDBAccessLayer] apList]);
			}
	        else {
	            completion(error, nil);
			}
		}
	}];
}

/*
 * Requests network controller to get AP list, send the response to the parser and returns list of AP's after parsing, filtering and sorting
 */
- (void)apList:(NSDictionary *)requestParams sortBy:(kSortingParameter)sort filterOn:(NSDictionary *)filter response:(void (^)(NSError *error,  NSArray *apList))completion {
	// Call network controller to get AP list from the server

	[self.systemManager.activeController apList:requestParams sortBy:sort filterOn:filter response: ^(NSError *error, id data) {
	    if (data) {
	        // Response received from controller, send it for parsing
	        [self.systemManager.activeParser parseAPWithInput:data output: ^(NSError *error, NSArray *apList) {
	            //Data received after parsing send it to the upper layer
	            if (OBJ_NOT_NIL(completion)) {
	                RMF_LOG_VERBOSE([self class], NSStringFromSelector(_cmd), kAPListReceivedSuccessfully);

	                completion(error, apList);

	                // Check if database enabled insert value in database
	                if ([[RMFConfiguration sharedRMFConfiguration] isDatabaseEnabled]) {
	                    for (RMFAccessPoint *accessPoint in apList) {
	                        [[RMFDBAccessLayer sharedRMFDBAccessLayer] insertAP:accessPoint];
						}
					}
				}
			}];
		}
	    else if (OBJ_NOT_NIL(completion)) {
	        NSString *logDescription = [NSString stringWithFormat:@"%@ : %@", ERROR, error.description];
	        RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);

	        // Check if database enabled return AP list from database
	        if ([[RMFConfiguration sharedRMFConfiguration] isDatabaseEnabled]) {
	            completion(error, [[RMFDBAccessLayer sharedRMFDBAccessLayer] apList]);
			}
	        else {
	            completion(error, nil);
			}
		}
	}];
}

/*
 * Requests network controller to get AP detail, send the response to the parser and returns RMFAccessPoint after parsing
 */
- (void)apDetail:(NSDictionary *)requestParams response:(void (^)(NSError *error, RMFAccessPoint *rmfAccessPoint))completion {
	// Call network controller to get AP details from the server
	[self.systemManager.activeController apDetail:requestParams response: ^(NSError *error, id data) {
	    if (data) {
	        // Response received from controller, send it for parsing
	        [self.systemManager.activeParser parseAPDetailWithInput:data output: ^(NSError *error, RMFAccessPoint *rmfAccessPoint) {
	            //Data received after parsing send it to the upper layer
	            if (OBJ_NOT_NIL(completion)) {
	                RMF_LOG_VERBOSE([self class], NSStringFromSelector(_cmd), kAPDetailReceivedSuccessfully);

	                completion(error, rmfAccessPoint);

	                // Check if database enabled update value in database
	                if ([[RMFConfiguration sharedRMFConfiguration] isDatabaseEnabled]) {
	                    [[RMFDBAccessLayer sharedRMFDBAccessLayer] updateAP:rmfAccessPoint];
					}
				}
			}];
		}
	    else if (OBJ_NOT_NIL(completion)) {
	        NSString *logDescription = [NSString stringWithFormat:@"%@ : %@", ERROR, error.description];
	        RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);

	        // Check if database enabled return AP detail from database
	        if ([[RMFConfiguration sharedRMFConfiguration] isDatabaseEnabled]) {
	            NSString *serialNumber = [requestParams objectForKey:kSerialNumberKey];
	            completion(error, [[RMFDBAccessLayer sharedRMFDBAccessLayer] apDetail:serialNumber]);
			}
	        else {
	            completion(error, nil);
			}
		}
	}];
}

/*
 * Requests network controller to edit AP, send the response to the parser and returns RMFAccessPoint after parsing
 */
- (void)editAP:(NSDictionary *)requestParams response:(void (^)(NSError *error, RMFAccessPoint *rmfAccessPoint))completion {
	// Call network controller to edit AP
	[self.systemManager.activeController editAP:requestParams response: ^(NSError *error, id data) {
	    if (data) {
	        // Response received from controller, send it for parsing
	        [self.systemManager.activeParser parseEditAPWithInput:data output: ^(NSError *error, RMFAccessPoint *rmfAccessPoint) {
	            //Data received after parsing send it to the upper layer
	            if (OBJ_NOT_NIL(completion)) {
	                RMF_LOG_VERBOSE([self class], NSStringFromSelector(_cmd), kAPEditedSuccessfully);

	                completion(error, rmfAccessPoint);

	                // Check if database enabled update value in database
	                if ([[RMFConfiguration sharedRMFConfiguration] isDatabaseEnabled]) {
	                    [[RMFDBAccessLayer sharedRMFDBAccessLayer] updateAP:rmfAccessPoint];
					}
				}
			}];
		}
	    else {
	        if (OBJ_NOT_NIL(completion)) {
	            NSString *logDescription = [NSString stringWithFormat:@"%@ : %@", ERROR, error.description];
	            RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
	            completion(error, nil);
			}
		}
	}];
}

/*
 * Requests network controller to delete AP, send the response to the parser and returns delete status
 */
- (void)deleteAP:(NSDictionary *)requestParams response:(void (^)(NSError *error, BOOL success))completion {
	// Call network controller to delete AP
	[self.systemManager.activeController deleteAP:requestParams response: ^(NSError *error, id data) {
	    if (data) {
	        // Response received from controller, send it for parsing
	        [self.systemManager.activeParser parseDeleteAPWithInput:data output: ^(NSError *error, BOOL success) {
	            //Data received after parsing send it to the upper layer
	            if (OBJ_NOT_NIL(completion)) {
	                RMF_LOG_VERBOSE([self class], NSStringFromSelector(_cmd), kAPDeletedSuccessfully);

	                completion(error, success);

	                // Check if database enabled delete value in database
	                if ([[RMFConfiguration sharedRMFConfiguration] isDatabaseEnabled]) {
	                    NSString *serialNumber = [requestParams objectForKey:kSerialNumberKey];
	                    [[RMFDBAccessLayer sharedRMFDBAccessLayer] deleteAP:serialNumber];
					}
				}
			}];
		}
	    else {
	        if (OBJ_NOT_NIL(completion)) {
	            NSString *logDescription = [NSString stringWithFormat:@"%@ : %@", ERROR, error.description];
	            RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
	            completion(error, NO);
			}
		}
	}];
}

/*
 * Return all possible keys use in RMFAPPlugin
 */
- (NSArray *)allKeyForRequestParameter {
	return [NSArray arrayWithObjects:kTenantNameKey, kVenueNameKey, kSerialNumberKey, kLimit, kOffset, nil];
}

/*
 * Requests network controller to download image for an access point
 */

- (void)downloadAPImage:(NSDictionary *)requestParams response:(void (^)(NSError *error, NSData *image))completion {
	// TODO: create image URL from request parameters once the actual request is known
	[[RMFImageLoader sharedRMFImageLoader] downloadImage:nil response: ^(NSError *error, NSData *image) {
	    if (completion) {
	        completion(error, image);
		}
	}];
}

/*
 * Requests network controller to upload image for an access point
 */

- (void)uploadAPImage:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion {
	// TODO: create image URL from request parameters once the actual request is known
	[[RMFImageLoader sharedRMFImageLoader] uploadImage:nil imageUrl:nil response: ^(NSError *error, id data) {
	    if (completion) {
	        completion(error, data);
		}
	}];
}

@end
