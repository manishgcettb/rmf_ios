/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */


#import "RMFBasePlugin.h"

@interface RMFLoginPlugin : RMFBasePlugin

// Returns the singleton instance of RMFLoginPlugin
+ (RMFLoginPlugin *)sharedRMFLoginPlugin;

// Request network controller to make login request and returns the response received from the controller
- (void)loginRequestWithParameters:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion;

// Request network controller to make logout request and returns the response received from the controller
- (void)logoutRequestWithResponse:(void (^)(NSError *error, id data))completion;

@end
