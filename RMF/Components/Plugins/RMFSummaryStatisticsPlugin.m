/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFSummaryStatisticsPlugin.h"
#import "RMFMacro.h"
#import "RMFSummaryStatistics.h"
#import "RMFConfiguration.h"
#import "RMFUtils.h"
#import "RMFErrorHandler.h"

@interface RMFSummaryStatisticsPlugin ()
// Stores RMFSystemManager instance
@property (nonatomic, strong) RMFSystemManager *systemManager;
@end

@implementation RMFSummaryStatisticsPlugin


// Stores the singleton instance of RMFStatisticsPlugin
static RMFSummaryStatisticsPlugin *sharedRMFStatisticsPlugin = nil;

#pragma mark - Class methods declaration

/*
 * Creates a singleton instance of RMFStatisticsPlugin
 */

+ (RMFSummaryStatisticsPlugin *)sharedRMFStatisticsPlugin {
	if (!OBJ_NOT_NIL(sharedRMFStatisticsPlugin)) {
		sharedRMFStatisticsPlugin = [[RMFSummaryStatisticsPlugin alloc] init];
	}
	return sharedRMFStatisticsPlugin;
}

#pragma mark - Instance methods implementation

/*
 * initialize the singleton instance of RMFStatisticsPlugin
 */

- (RMFSummaryStatisticsPlugin *)init {
	if (!OBJ_NOT_NIL(sharedRMFStatisticsPlugin)) {
		sharedRMFStatisticsPlugin = [super init];

		// Event plugin instance will be created successfully only when the system manager is configured properly
		if (sharedRMFStatisticsPlugin && [RMFUtils isSystemManagerConfigured:[RMFSystemManager sharedRMFSystemManager]]) {
			self.systemManager = [RMFSystemManager sharedRMFSystemManager];
		}
	}

	return sharedRMFStatisticsPlugin;
}

/*
 * Requests network controller to fetch summary statistics, sends the response to the parser
 * and returns summaryStatistics model object after parsing
 */

- (void)summaryStatisticsWithResponse:(void (^)(NSError *error,  RMFSummaryStatistics *summaryStatistics))completion {
	RMFConfiguration *configuration = [RMFConfiguration sharedRMFConfiguration];

	if (configuration) {
		// Get summary statistics for var
		if (configuration.userType == kUserRoleSuper) {
			// Call network controller to load wlan details from the server
			[self.systemManager.activeController summaryStatisticsForVarWithResponse: ^(NSError *error, id data) {
			    if (data) {
			        // Response received from controller, send it for parsing
			        [self.systemManager.activeParser summaryStatistics:data output: ^(NSError *error, RMFSummaryStatistics *summaryStatistics) {
			            //Data received after parsing
			            if (completion) {
			                completion(error, summaryStatistics);
			                NSString *logDescription = [NSString stringWithFormat:@"%@ : %@", kSummaryStatistics, summaryStatistics];
			                RMF_LOG_DEBUG([self class], NSStringFromSelector(_cmd), logDescription);
						}
					}];
				}
			    else {
			        if (completion) {
			            completion(error, nil);
			            NSString *logDescription = [NSString stringWithFormat:@"%@ : %@", ERROR, error.description];
			            RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
					}
				}
			}];
		}

		// Get summary statistics for a tenant
		else if (configuration.userType == kUserRoleNormal) {
			if (configuration.userAssociation) {
				NSMutableDictionary *requestParam = [[NSMutableDictionary alloc] init];
				[requestParam setObject:configuration.userAssociation forKey:kTenantNameKey];
				[self.systemManager.activeController summaryStatisticsForTenant:requestParam response: ^(NSError *error, id data) {
				    // Response received from controller
				    if (data) {
				        [self.systemManager.activeParser summaryStatistics:data output: ^(NSError *error, RMFSummaryStatistics *summaryStatistics) {
				            // Data received after parsing send it the upper layer
				            if (completion) {
				                completion(error, summaryStatistics);
				                NSString *logDescription = [NSString stringWithFormat:@"%@ : %@", kSummaryStatistics, summaryStatistics];
				                RMF_LOG_DEBUG([self class], NSStringFromSelector(_cmd), logDescription);
							}
						}];
					}
				    else {
				        if (completion) {
				            completion(error, nil);
				            NSString *logDescription = [NSString stringWithFormat:@"%@ : %@", ERROR, error.description];
				            RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
						}
					}
				}];
			}
			else {
				// User association is not available
				if (completion) {
					completion([RMFUtils errorWithCode:RMFCodeForUserAssociationNotSet], nil);
				}
			}
		}
		else {
			// User type not available
			if (completion) {
				completion([RMFUtils errorWithCode:RMFCodeForUserTypeNotSet], nil);
			}
		}
	}
	else {
		// Configuration data not available
		if (completion) {
			completion([RMFUtils errorWithCode:RMFCodeForConfigDataNotFound], nil);
		}
	}
}

@end
