/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFBasePlugin.h"

// enum for WLAN sorting parameter
typedef enum {
	kWlanSortingParameterName = 1,
	kWlanSortingParameterSsid,
	kWlanSortingParameterNone
}kWlanSortingParameter;


@interface RMFWlanPlugin : RMFBasePlugin

// Returns the singleton instance of RMFWlanPlugin
+ (RMFWlanPlugin *)sharedRMFWlanPlugin;

// Requests network controller to fetch wlan list, sends the response to the parser and returns list of wlans after parsing
- (void)wlanList:(NSDictionary *)requestParams response:(void (^)(NSError *error,  NSArray *wlanServiceList))completion;

// Requests network controller to fetch wlan service details, sends the response to the parser and returns wlan model object after parsing
- (void)wlanDetails:(NSDictionary *)requestParams response:(void (^)(NSError *error,  RMFWlanService *wlanService))completion;

// Requests network controller to fetch wlan status details, sends the response to the parser and returns wlan status model object after parsing
- (void)wlanStatus:(NSDictionary *)requestParams response:(void (^)(NSError *error,  RMFWlanServiceStatus *wlanServiceStatus))completion;

//Requests network controller to fetch wlan list, sends the response to the parser and returns list of wlans count after parsing
- (void)wlanCount:(NSDictionary *)requestParams response:(void (^)(NSError *error,  NSInteger wlanCount))completion;

//Requests network controller to create new Wlan, send the response to the parser and returns the parsed response
- (void)createWlan:(NSDictionary *)requestParams response:(void (^)(NSError *error, NSString *createWlanResponse))completion;

//Requests network controller to delete Wlan, send the response to the parser and returns the parsed response
- (void)deleteWlan:(NSDictionary *)requestParams response:(void (^)(NSError *error, NSString *deleteWlanResponse))completion;

//Requests network controller to edit Wlan, sends the response to the parser and returns the parsed response
- (void)editWlan:(NSDictionary *)requestParams response:(void (^)(NSError *error, NSString *editWlanResponse))completion;

//Requests network controller to fetch Wlan list based on given filtering and sorting criteria,sends the  response to the parser and returns the parsed response
- (void)wlanList:(NSDictionary *)requestParams sortBy:(kWlanSortingParameter)sortingParameter filterOn:(NSDictionary *)filter response:(void (^)(NSError *, id))completion;

@end
