/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFBasePlugin.h"

// enum for event sorting parameter
typedef enum {
    kEventSortingParameterNone
}kEventSortingParameter;

@interface RMFEventPlugin : RMFBasePlugin

// Returns the singleton instance of RMFEventPlugin
+ (RMFEventPlugin *)sharedRMFEventPlugin;

// Makes network request to fetch Event details with request parameters and returns the network response
- (void)eventDetails:(NSDictionary *)requestParams response:(void (^)(NSError *error,  RMFEvent *event))completion;

// Makes network request to fetch Event list with request parameters and returns the network response
- (void)eventList:(NSDictionary *)requestParams response:(void (^)(NSError *error, NSArray *eventList))completion;

// Requests network controller to fetch event list based on given filtering and sorting criteria,sends the response to the parser and returns the parsed response
- (void)eventList:(NSDictionary *)requestParams sortBy:(kEventSortingParameter)sortingParameter filterOn:(NSDictionary *)filter response:(void (^)(NSError *, id))completion;

@end
