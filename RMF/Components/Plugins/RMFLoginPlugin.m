/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFLoginPlugin.h"
#import "RMFMacro.h"
#import "RMFSystemManager.h"
#import "RMFUtils.h"

@interface RMFLoginPlugin ()
// Stores RMFSystemManager instance
@property (nonatomic, strong) RMFSystemManager *systemManager;
@end

@implementation RMFLoginPlugin


// Stores the singleton instance of RMFLoginPlugin
static RMFLoginPlugin *sharedRMFLoginPlugin = nil;

#pragma mark - Class methods declaration

/*
 * Creates a singleton instance of RMFLoginPlugin
 */

+ (RMFLoginPlugin *)sharedRMFLoginPlugin {
    if (!OBJ_NOT_NIL(sharedRMFLoginPlugin)) {
        sharedRMFLoginPlugin = [[RMFLoginPlugin alloc] init];
    }
    return sharedRMFLoginPlugin;
}

#pragma mark - Instance methods implementation

/*
 * initialize the singleton instance of RMFLoginPlugin
 */

- (RMFLoginPlugin *)init {
    if (!OBJ_NOT_NIL(sharedRMFLoginPlugin)) {
        sharedRMFLoginPlugin = [super init];
        
        // Login plugin instance will be created successfully only when the system manager is configured properly
        if (sharedRMFLoginPlugin && [RMFUtils isSystemManagerConfigured:[RMFSystemManager sharedRMFSystemManager]]) {
            self.systemManager = [RMFSystemManager sharedRMFSystemManager];
        }
    }
    return sharedRMFLoginPlugin;
}

/*
 * Makes network request to perform login operation and returns back the response from KUMO
 */

- (void)loginRequestWithParameters:(NSDictionary *)requestParams response:(void (^)(NSError *error, id data))completion {
    [self.systemManager.activeController loginRequestWithParameters:requestParams response: ^(NSError *error, id data) {
        //TODO: return login response to the application layer
        // Save user data once it is available
    }];
}

/*
 * Makes network request to perform logout operation and returns back the response from KUMO
 */

- (void)logoutRequestWithResponse:(void (^)(NSError *error, id data))completion {
    [self.systemManager.activeController logoutRequestWithResponse: ^(NSError *error, id data) {
        //TODO: return logout response to the application layer
    }];
}

@end
