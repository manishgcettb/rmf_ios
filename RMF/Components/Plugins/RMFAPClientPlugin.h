/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFBasePlugin.h"
#import "RMFClientDetail.h"

typedef enum {
	kClientTypeActive,
	kClientTypeBlocked
}kClientType;


@interface RMFAPClientPlugin : RMFBasePlugin

// Returns the singleton instance of RMFClientPlugin
+ (RMFAPClientPlugin *)sharedRMFClientPlugin;

// Makes network request to fetch active/blocked client list  with request parameters and returns the network response
- (void)clientList:(NSDictionary *)requestParams clientType:(kClientType)clientType response:(void (^)(NSError *error, NSArray *clientList))completion;

// Makes network request to fetch Client list  with request parameters and returns the network response
- (void)clientList:(NSDictionary *)requestParams response:(void (^)(NSError *error, NSArray *clientList))completion;

// Makes network request to fetch Client details  with request parameters and returns the network response
- (void)clientDetails:(NSDictionary *)requestParams response:(void (^)(NSError *error,  RMFClientDetail *event))completion;

/*

   @brief Requests network controller to fetch Wlan list based on given filtering and sorting criteria,
   sends the response to the parser and returns the parsed response

   @param filteringParameters - The input dictionary used to filter the wlan list. filtering can be done
   based on the follwoing parameters:

   @"tenant_name"
   @"ssid"
   @"wlan-type"
   @"wlan-security"

   sortingParameters - The input array used to sort the wlan list. Sorting can be done
   based on the following parameters:

   @"name"
   @"ssid"

   limit - The start index of the list

   offset - Size of data chunk to be received

   completion - response block

   @return list of wlans.

 */
- (void)clientListFilteredBy:(NSDictionary *)filteringParameters sortBy:(NSArray *)sortingParameters limit:(NSNumber *)limit offset:(NSNumber *)offset response:(void (^)(NSError *, id))completion;

@end
