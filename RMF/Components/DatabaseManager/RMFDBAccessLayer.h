/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <Foundation/Foundation.h>
#import "RMFDBManager.h"
#import "RMFRootAssociation.h"
#import "RMFNotifications.h"
#import "RMFUser.h"
#import "NSData+AES.h"
#import "RMFAccessPoint.h"
#import "RMFWlanService.h"
#import "RMFEvent.h"

typedef enum {
	kAPConstraintsParameterSerialNumber,
	kAPConstraintsParameterMacAddress
}kAPConstraintsParameter;


@interface RMFDBAccessLayer : NSObject
{
	RMFDBManager *dbManager;
}

// Store and return parameter on which all database query and constraints work in AP default value is kConstraintsParameterSerialNumber
@property (nonatomic, assign) kAPConstraintsParameter apConstraintsParameter;

//To get instance of DBAccesslayer
+ (RMFDBAccessLayer *)sharedRMFDBAccessLayer;

//To get the instance of the Notification object to insert into database
- (RMFNotifications *)insertNotifications;

//To save the object into database
- (NSError *)saveNotificationObject:(RMFNotifications *)notificationObj;

//To get all notifications
- (NSArray *)getNotifications;

//To delete all notifications
- (NSError *)deleteAllNotifications;

//To delete a notification
- (NSError *)deleteNotifications:(RMFNotifications *)notification;

//To update a notification
- (NSBatchUpdateResult *)updateNotifications:(RMFNotifications *)notification;

//To insert an association
- (RMFRootAssociation *)insertRootAssociation;

//To get association
- (NSArray *)getRootAssociation:(NSString *)root;

//To save an association
- (NSError *)saveRootAssociationObject:(RMFRootAssociation *)associationObj;

//To delete all association
- (NSError *)deleteAllAssociation;

//To delete an association
- (NSError *)deleteRootAssociation:(RMFRootAssociation *)association;

#pragma mark - AP

//Insert list of AP in database
- (NSError *)insertAP:(RMFAccessPoint *)rmfAccessPoint;

//Update AP information in database
- (NSError *)updateAP:(RMFAccessPoint *)rmfAccessPoint;

//Return List of AP from database
- (NSArray *)apList;

//Return AP detail from database for given parameter
- (RMFAccessPoint *)apDetail:(NSString *)parameterValue;

//Clear all AP from database
- (NSError *)deleteAllAP;

//Delete AP from database for given parameter
- (NSError *)deleteAP:(NSString *)parameterValue;


#pragma mark - WLAN

// Return wlan details of a wlan from the database
- (RMFWlanService *)wlanDetails:(NSString *)ssid;

//Return List of WLANs from the database
- (NSArray *)wlanList;

// Insert WLAN in the database
- (NSError *)insertWlan:(RMFWlanService *)wlan;

// Delete all WLANs from the database
- (NSError *)deleteAllWlans;

// Delete a WLAN from the database
- (NSError *)deleteWlan:(NSString *)ssid;

// Update a WLAN in the database
- (NSError *)updateWlan:(RMFWlanService *)wlan;

#pragma mark - Event

// Return details of an event from the database
- (RMFEvent *)eventDetails:(NSInteger) eventTypeId;

// Returns List of events from the database
- (NSArray *)eventList;

// Inserts event in the database
- (NSError *)insertEvent:(RMFEvent *)event;

// Delete all events from the database
- (NSError *)deleteAllEvents;

// Delete an event from the database
- (NSError *)deleteEvent:(NSInteger)eventTypeId;

// Update a event in the database
- (NSError *)updateEvent:(RMFEvent *)event;


#pragma mark - USER
- (NSError *)saveUser:(RMFUser *)user userEntity:(NSManagedObject *)userInfo;
- (NSError *)updateUser:(RMFUser *)user;

- (NSError *)deleteUser ;
- (NSError *)insertUser:(RMFUser *)user;
- (RMFUser *)getUser;



@end
