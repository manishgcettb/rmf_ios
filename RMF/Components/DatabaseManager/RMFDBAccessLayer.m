/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFDBAccessLayer.h"
#import "RMFMacro.h"
#import "RMFNotifications.h"
#import "RMFConstants.h"
#import "RMFRootAssociation.h"
#import "RMFWlanService.h"
#import "RMFUtils.h"
#import "RMFErrorHandler.h"
#import "RMFArrayTransformer.h"
#import "RMFUtils.h"
#import "RMFUser.h"

@interface RMFDBAccessLayer (PrivateMethods)
- (void)saveChanges;
- (void)createDatabaseManager;
- (NSString *)getPathToDatabase;
- (NSString *)databaseAttribute;
- (NSString *)databaseAttributeValue:(RMFAccessPoint *)rmfAccessPoint;
- (NSManagedObject *)mapRFMAPModelToAPManagedObject:(RMFAccessPoint *)rmfAccessPoint managedObject:(NSManagedObject *)accessPoint;
- (NSMutableArray *)mapAPManagedObjectToRFMAPModel:(NSArray *)managedObjectArray;
@end

@implementation RMFDBAccessLayer

static RMFDBAccessLayer *sharedRMFDBAccess = nil;

#pragma mark - Class methods declaration

/*
 * Creates a singleton instance of RMFLoginPlugin
 */
+ (RMFDBAccessLayer *)sharedRMFDBAccessLayer {
	if (!OBJ_NOT_NIL(sharedRMFDBAccess)) {
		sharedRMFDBAccess = [[RMFDBAccessLayer alloc] init];
		RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), SHARED_INSTANCE_CREATED(kDBAccess));
	}
	return sharedRMFDBAccess;
}

#pragma mark - Instance methods implementation

/*
 * initialise the singleton instance of RMFLoginPlugin
 */
- (RMFDBAccessLayer *)init {
	if (!OBJ_NOT_NIL(sharedRMFDBAccess)) {
		sharedRMFDBAccess = [super init];
		self.apConstraintsParameter = kAPConstraintsParameterSerialNumber;
		[self createDatabaseManager];
	}
	return sharedRMFDBAccess;
}

#pragma mark - Private methods

- (void)saveChanges {
	[dbManager saveAll];
}

- (BOOL)deleteCachedData {
	NSString *databasePath = [self getPathToDatabase];
	NSError *error = nil;
	[[NSFileManager defaultManager] removeItemAtPath:databasePath error:&error];

	if (OBJ_NOT_NIL(error)) {
		dbManager = nil;
		[self createDatabaseManager];
		return true;
	}
	else {
		return false;
	}
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), CLEAN_CACHE_DATA);
}

- (void)createDatabaseManager {
	NSString *databasePath = [self getPathToDatabase];
	dbManager = [[RMFDBManager alloc] initWithPath:databasePath name:DATABASE_NAME];
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), DATABASE_MANAGER_CREATED);
}

- (NSString *)getPathToDatabase {
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *basePath = (paths.count > 0) ? paths[0] : nil;

	NSString *logDescription = [NSString stringWithFormat:@"%@ %@", DB_PATH, [basePath stringByAppendingPathComponent:DATABASE_FILE_NAME]];
	RMF_LOG_DEBUG([self class], NSStringFromSelector(_cmd), logDescription);

	return [basePath stringByAppendingPathComponent:DATABASE_FILE_NAME];
}

#pragma mark - Notifications data methods

- (RMFNotifications *)insertNotifications {
	RMFNotifications *notifications;
	notifications = (RMFNotifications *)[dbManager insertObjectWithName:NOTIFICATION_ENTITY];

	return notifications;
}

- (NSArray *)getNotifications {
	NSManagedObjectContext *managedObjectContext = nil;
	NSArray *notificationsArray = nil;
	managedObjectContext = [dbManager context];
	if (managedObjectContext) {
		notificationsArray = [dbManager objectsWithName:NOTIFICATION_ENTITY];
	}
	NSString *strDesc = [NSString stringWithFormat:@"%@ %@", ALL_NOTIFICATIONS, notificationsArray];
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), strDesc);

	return notificationsArray;
}

- (NSError *)saveNotificationObject:(RMFNotifications *)notificationObj {
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), SAVE_NOTIFICATION);
	return [dbManager saveObject:notificationObj];
}

- (NSError *)deleteAllNotifications {
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), DELETE_ALL_NOTIFICATION);
	return [dbManager deleteAllEntities:NOTIFICATION_ENTITY];
}

- (NSError *)deleteNotifications:(RMFNotifications *)notification {
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), DELETE_A_NOTIFICATION);
	return [dbManager deleteObject:notification];
}

//To update a notification
- (NSBatchUpdateResult *)updateNotifications:(RMFNotifications *)notification {
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), UPDATE_A_NOTIFICATION);
	return [dbManager updateValues:NOTIFICATION_ENTITY value:[NSString stringWithFormat:@"%d", kRead] attribute:READ_STATUS];
}

#pragma mark -
#pragma mark RootAssociation data methods

// get an object to be inserted in db
- (RMFRootAssociation *)insertRootAssociation {
	RMFRootAssociation *rootAssociation;
	rootAssociation = (RMFRootAssociation *)[dbManager insertObjectWithName:ASSOCIATION_ENTITY];

	return rootAssociation;
}

//Get association
- (NSArray *)getRootAssociation:(NSString *)root {
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	NSManagedObjectContext *managedObjectContext = nil;
	NSArray *rootAssociationArray = nil;

	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%@ = %@", CURRENT, root];
	[fetchRequest setPredicate:predicate];
	managedObjectContext = [dbManager context];
	if (managedObjectContext) {
		fetchRequest.entity = [NSEntityDescription entityForName:ASSOCIATION_ENTITY inManagedObjectContext:managedObjectContext];
		rootAssociationArray = [dbManager objectsWithName:ASSOCIATION_ENTITY];
	}
	NSString *strDesc = [NSString stringWithFormat:@"%@ %@", ASSOCIATION_ENTITY, rootAssociationArray];
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), strDesc);

	return rootAssociationArray;
}

// To save association object in DB
- (NSError *)saveRootAssociationObject:(RMFRootAssociation *)associationObj {
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), SAVE_ASSOCIATION);
	return [dbManager saveObject:associationObj];
}

//to delete all associations
- (NSError *)deleteAllAssociation {
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), DELETE_ASSOCIATION);
	return [dbManager deleteAllEntities:ASSOCIATION_ENTITY];
}

// delete a single association
- (NSError *)deleteRootAssociation:(RMFRootAssociation *)association {
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), DELETE_ASSOCIATION);
	return [dbManager deleteObject:association];
}

#pragma mark - AP

//Insert list of AP in database
- (NSError *)insertAP:(RMFAccessPoint *)rmfAccessPoint {
	NSString *strDesc = [NSString stringWithFormat:@"%@ %@", kLogInsertAPInDatabase, rmfAccessPoint];
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), strDesc);

	if (OBJ_NOT_NIL(rmfAccessPoint)) {
		// set value for entity RMFAccessPoint

		// Check if value already exist in database then update same object
		NSString *property = [self databaseAttribute];
		NSString *value = [self databaseAttributeValue:rmfAccessPoint];

		if (OBJ_NOT_NIL(property) && OBJ_NOT_NIL(value)) {
			NSArray *accessPoints = [dbManager objects:AP_ENTITY withProperty:property value:value];
			for (NSManagedObject *accessPoint in accessPoints) {
				return [dbManager saveObject:[self mapRFMAPModelToAPManagedObject:rmfAccessPoint managedObject:accessPoint]];
			}
		}
		// If value not exist then insert the entity
		NSManagedObject *accessPoint = [dbManager insertObjectWithName:AP_ENTITY];
		if (OBJ_NOT_NIL(accessPoint)) {
			// save object in database
			return [dbManager saveObject:[self mapRFMAPModelToAPManagedObject:rmfAccessPoint managedObject:accessPoint]];
		}
		else {
			return [RMFUtils errorWithCode:RMFCodeForDatabaseInstanceNotFound];
		}
	}
	else {
		return [RMFUtils errorWithCode:RMFCodeForDatabaseInputParameterNotFound];
	}
}

//Update AP information in database
- (NSError *)updateAP:(RMFAccessPoint *)rmfAccessPoint {
	NSString *strDesc = [NSString stringWithFormat:@"%@ %@", kLogUpdateAPInDatabase, rmfAccessPoint];
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), strDesc);

	NSError *error = nil;
	if (OBJ_NOT_NIL(rmfAccessPoint)) {
		NSString *property = [self databaseAttribute];
		NSString *value = [self databaseAttributeValue:rmfAccessPoint];

		if (OBJ_NOT_NIL(property) && OBJ_NOT_NIL(value)) {
			NSArray *accessPoints = [dbManager objects:AP_ENTITY withProperty:property value:value];
			for (NSManagedObject *accessPoint in accessPoints) {
				error = [dbManager saveObject:[self mapRFMAPModelToAPManagedObject:rmfAccessPoint managedObject:accessPoint]];
				if (OBJ_NOT_NIL(error)) {
					return error;
				}
			}
		}
	}
	else {
		error =  [RMFUtils errorWithCode:RMFCodeForDatabaseInputParameterNotFound];
	}
	return error;
}

//Return List of AP from database
- (NSArray *)apList {
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), kLogGetAPListFromDatabase);
	return [self mapAPManagedObjectToRFMAPModel:[dbManager objectsWithName:AP_ENTITY]];
}

//Return AP detail from database for given parameter
- (RMFAccessPoint *)apDetail:(NSString *)parameterValue {
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), kLogGetAPDetailsFromDatabase);

	RMFAccessPoint *rmfAccessPoint = nil;

	if (OBJ_NOT_NIL(parameterValue)) {
		NSString *property = [self databaseAttribute];

		if (OBJ_NOT_NIL(property)) {
			NSArray *accessPoints = [dbManager objects:AP_ENTITY withProperty:property value:parameterValue];
			NSArray *accessPointList =  [self mapAPManagedObjectToRFMAPModel:accessPoints];

			if (OBJ_NOT_NIL(accessPointList) && [accessPointList count]) {
				rmfAccessPoint = [accessPointList firstObject];
			}
		}
	}
	return rmfAccessPoint;
}

//Clear all AP from database
- (NSError *)deleteAllAP {
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), kLogDeleteAllAPFromDatabase);
	return [dbManager deleteAllEntities:AP_ENTITY];
}

//Delete AP from database for given parameter
- (NSError *)deleteAP:(NSString *)parameterValue {
	NSString *strDesc = [NSString stringWithFormat:@"%@ %@", kLogDeleteAPFromDatabase, parameterValue];
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), strDesc);

	NSError *error = nil;
	if (OBJ_NOT_NIL(parameterValue)) {
		NSString *property = [self databaseAttribute];

		if (OBJ_NOT_NIL(property)) {
			NSArray *accessPoints = [dbManager objects:AP_ENTITY withProperty:property value:parameterValue];
			for (NSManagedObject *accessPoint in accessPoints) {
				error = [dbManager deleteObject:accessPoint];
				if (!OBJ_NOT_NIL(error)) {
					return error;
				}
			}
		}
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForDatabaseInputParameterNotFound];
	}
	return error;
}

- (NSString *)databaseAttribute {
	NSString *attribute = nil;

	switch (self.apConstraintsParameter) {
		case kAPConstraintsParameterSerialNumber: {
			attribute = kAPKeySerialNumber;
		}
		break;

		case kAPConstraintsParameterMacAddress: {
			attribute = kAPKeyStatusMacAddress;
		}
		break;

		default:
			break;
	}
	return attribute;
}

- (NSString *)databaseAttributeValue:(RMFAccessPoint *)rmfAccessPoint {
	NSString *attributeValue = nil;

	switch (self.apConstraintsParameter) {
		case kAPConstraintsParameterSerialNumber: {
			attributeValue = rmfAccessPoint.serialNumber;
		}
		break;

		case kAPConstraintsParameterMacAddress: {
			attributeValue = rmfAccessPoint.rmfAPStatus.macAddress;
		}
		break;

		default:
			break;
	}
	return attributeValue;
}

- (NSMutableArray *)mapAPManagedObjectToRFMAPModel:(NSArray *)managedObjectArray {
	if (!OBJ_NOT_NIL(managedObjectArray)) {
		return nil;
	}
	NSMutableArray *aplist = [[NSMutableArray alloc] init];
	for (NSManagedObject *accessPoint in managedObjectArray) {
		// map RMFAccessPoint object
		RMFAccessPoint *rfmAccessPoint = [[RMFAccessPoint alloc] init];
		rfmAccessPoint.serialNumber = [accessPoint valueForKey:kAPKeySerialNumber];
		rfmAccessPoint.ssid = [accessPoint valueForKey:kAPKeySsid];
		rfmAccessPoint.rootAssociation = [accessPoint valueForKey:kAPKeyRootAssociation];
		rfmAccessPoint.imageUrl = [accessPoint valueForKey:kAPKeyImage];

		// map RMFAPWlanStatus object
		NSManagedObject *wlanStatus = [accessPoint valueForKey:kAPKeyWlanStatus];
		RMFAPWlanStatus *rmfAPWlanStatus = [[RMFAPWlanStatus alloc] init];
		rmfAPWlanStatus.totalCount = [[wlanStatus valueForKey:kAPKeyWlanStatusTotalCount] intValue];
		RMFArrayTransformer *rmfArrayTransformer = [[RMFArrayTransformer alloc] init];
		if (OBJ_NOT_NIL([wlanStatus valueForKey:kAPKeyWlanStatusWlans]))
			rmfAPWlanStatus.wlans = [rmfArrayTransformer reverseTransformedValue:[wlanStatus valueForKey:kAPKeyWlanStatusWlans]];
		rfmAccessPoint.rmfAPWlanStatus = rmfAPWlanStatus;

		// map RMFAPExtendedStatus object
		NSManagedObject *extendedStatus = [accessPoint valueForKey:kAPKeyExtendedStatus];
		RMFAPExtendedStatus *rmfAPExtendedStatus = [[RMFAPExtendedStatus alloc] init];
		rmfAPExtendedStatus.internalIp = [extendedStatus valueForKey:kAPKeyExtendedStatusInternalIp];
		rmfAPExtendedStatus.externalIp = [extendedStatus valueForKey:kAPKeyExtendedStatusExternalIp];
		rmfAPExtendedStatus.hops = [extendedStatus valueForKey:kAPKeyExtendedStatusHops];
		rmfAPExtendedStatus.tunnelType = [extendedStatus valueForKey:kAPKeyExtendedStatusTunnelType];
		rmfAPExtendedStatus.name = [extendedStatus valueForKey:kAPKeyExtendedStatusName];
		rmfAPExtendedStatus.uptime = [[extendedStatus valueForKey:kAPKeyExtendedStatusUptime] doubleValue];
		rfmAccessPoint.rmfAPExtendedStatus = rmfAPExtendedStatus;

		// map RMFAPRadioStatus object
		NSManagedObject *radioStatus = [accessPoint valueForKey:kAPKeyRadioStatus];
		RMFAPRadioStatus *rmfAPRadioStatus = [[RMFAPRadioStatus alloc] init];
		rmfAPRadioStatus.txPower = [radioStatus valueForKey:kAPKeyRadioStatusTxPower];
		rmfAPRadioStatus.channel = [[radioStatus valueForKey:kAPKeyRadioStatusChannel] intValue];
		rmfAPRadioStatus.channelWidth = [[radioStatus valueForKey:kAPKeyRadioStatusChannelWidth] intValue];
		rmfAPRadioStatus.wlanCount = [[radioStatus valueForKey:kAPKeyRadioStatusWlanCount] intValue];
		rmfAPRadioStatus.mode = [radioStatus valueForKey:kAPKeyRadioStatusChannel];
		rfmAccessPoint.rmfAPRadioStatus = rmfAPRadioStatus;

		// map RMFAPStatus object
		NSManagedObject *apStatus = [accessPoint valueForKey:kAPKeyStatus];
		RMFAPStatus *rmfAPStatus = [[RMFAPStatus alloc] init];
		rmfAPStatus.model = [apStatus valueForKey:kAPKeyStatusModel];
		rmfAPStatus.macAddress = [apStatus valueForKey:kAPKeyStatusMacAddress];
		rmfAPStatus.registrationStatus = [apStatus valueForKey:kAPKeyStatusRegistrationStatus];
		rmfAPStatus.connectionStatus = [apStatus valueForKey:kAPKeyStatusConnectionStatus];
		rmfAPStatus.configStatus = [apStatus valueForKey:kAPKeyStatusConfigStatus];
		rmfAPStatus.lastSeenByVscg = [apStatus valueForKey:kAPKeyStatusLastSeenByVscg];
		rmfAPStatus.swver = [apStatus valueForKey:kAPKeyStatusSwver];
		rmfAPStatus.location = [apStatus valueForKey:kAPKeyStatusLocation];
		rfmAccessPoint.rmfAPStatus = rmfAPStatus;

		[aplist addObject:rfmAccessPoint];
	}
	return aplist;
}

- (NSManagedObject *)mapRFMAPModelToAPManagedObject:(RMFAccessPoint *)rmfAccessPoint managedObject:(NSManagedObject *)accessPoint {
	if (OBJ_NOT_NIL(rmfAccessPoint.serialNumber))
		[accessPoint setValue:rmfAccessPoint.serialNumber forKey:kAPKeySerialNumber];
	if (OBJ_NOT_NIL(rmfAccessPoint.ssid))
		[accessPoint setValue:rmfAccessPoint.ssid forKey:kAPKeySsid];
	if (OBJ_NOT_NIL(rmfAccessPoint.rootAssociation))
		[accessPoint setValue:rmfAccessPoint.rootAssociation forKey:kAPKeyRootAssociation];
	if (OBJ_NOT_NIL(rmfAccessPoint.imageUrl))
		[accessPoint setValue:rmfAccessPoint.imageUrl forKey:kAPKeyImage];

	// set value for entity rmfAPWlanStatus
	NSManagedObject *apWlanStatus = [dbManager insertObjectWithName:AP_WLAN_STATUS_ENTITY];
	[apWlanStatus setValue:[NSNumber numberWithInt:rmfAccessPoint.rmfAPWlanStatus.totalCount] forKey:kAPKeyWlanStatusTotalCount];
	if (OBJ_NOT_NIL(rmfAccessPoint.rmfAPWlanStatus.wlans) && [rmfAccessPoint.rmfAPWlanStatus.wlans count]) {
		RMFArrayTransformer *rmfArrayTransformer = [[RMFArrayTransformer alloc] init];
		[apWlanStatus setValue:[rmfArrayTransformer transformedValue:rmfAccessPoint.rmfAPWlanStatus.wlans] forKey:kAPKeyWlanStatusWlans];
	}

	// set value for entity rmfAPExtendedStatus
	NSManagedObject *apExtendedStatus = [dbManager insertObjectWithName:AP_EXTENDED_STATUS_ENTITY];
	if (OBJ_NOT_NIL(rmfAccessPoint.rmfAPExtendedStatus.internalIp))
		[apExtendedStatus setValue:rmfAccessPoint.rmfAPExtendedStatus.internalIp forKey:kAPKeyExtendedStatusInternalIp];
	if (OBJ_NOT_NIL(rmfAccessPoint.rmfAPExtendedStatus.externalIp))
		[apExtendedStatus setValue:rmfAccessPoint.rmfAPExtendedStatus.externalIp forKey:kAPKeyExtendedStatusExternalIp];
	if (OBJ_NOT_NIL(rmfAccessPoint.rmfAPExtendedStatus.hops))
		[apExtendedStatus setValue:rmfAccessPoint.rmfAPExtendedStatus.hops forKey:kAPKeyExtendedStatusHops];
	if (OBJ_NOT_NIL(rmfAccessPoint.rmfAPExtendedStatus.tunnelType))
		[apExtendedStatus setValue:rmfAccessPoint.rmfAPExtendedStatus.tunnelType forKey:kAPKeyExtendedStatusTunnelType];
	if (OBJ_NOT_NIL(rmfAccessPoint.rmfAPExtendedStatus.name))
		[apExtendedStatus setValue:rmfAccessPoint.rmfAPExtendedStatus.name forKey:kAPKeyExtendedStatusName];
	[apExtendedStatus setValue:[NSNumber numberWithDouble:rmfAccessPoint.rmfAPExtendedStatus.uptime]  forKey:kAPKeyExtendedStatusUptime];

	// set value for entity rmfAPRadioStatus
	NSManagedObject *apRadioStatus = [dbManager insertObjectWithName:AP_RADIO_STATUS_ENTITY];
	if (OBJ_NOT_NIL(rmfAccessPoint.rmfAPRadioStatus.txPower))
		[apRadioStatus setValue:rmfAccessPoint.rmfAPRadioStatus.txPower forKey:kAPKeyRadioStatusTxPower];
	[apRadioStatus setValue:[NSNumber numberWithInt:rmfAccessPoint.rmfAPRadioStatus.channel]  forKey:kAPKeyRadioStatusChannel];
	[apRadioStatus setValue:[NSNumber numberWithInt:rmfAccessPoint.rmfAPRadioStatus.channelWidth]  forKey:kAPKeyRadioStatusChannelWidth];
	[apRadioStatus setValue:[NSNumber numberWithInt:rmfAccessPoint.rmfAPRadioStatus.wlanCount]  forKey:kAPKeyRadioStatusWlanCount];
	if (OBJ_NOT_NIL(rmfAccessPoint.rmfAPRadioStatus.mode))
		[apRadioStatus setValue:rmfAccessPoint.rmfAPRadioStatus.mode forKey:kAPKeyRadioStatusMode];

	// set value for entity rmfAPStatus
	NSManagedObject *apStatus = [dbManager insertObjectWithName:AP_STATUS_ENTITY];
	if (OBJ_NOT_NIL(rmfAccessPoint.rmfAPStatus.model))
		[apStatus setValue:rmfAccessPoint.rmfAPStatus.model forKey:kAPKeyStatusModel];
	if (OBJ_NOT_NIL(rmfAccessPoint.rmfAPStatus.macAddress))
		[apStatus setValue:rmfAccessPoint.rmfAPStatus.macAddress forKey:kAPKeyStatusMacAddress];
	if (OBJ_NOT_NIL(rmfAccessPoint.rmfAPStatus.registrationStatus))
		[apStatus setValue:rmfAccessPoint.rmfAPStatus.registrationStatus forKey:kAPKeyStatusRegistrationStatus];
	if (OBJ_NOT_NIL(rmfAccessPoint.rmfAPStatus.connectionStatus))
		[apStatus setValue:rmfAccessPoint.rmfAPStatus.connectionStatus forKey:kAPKeyStatusConnectionStatus];
	if (OBJ_NOT_NIL(rmfAccessPoint.rmfAPStatus.configStatus))
		[apStatus setValue:rmfAccessPoint.rmfAPStatus.configStatus forKey:kAPKeyStatusConfigStatus];
	if (OBJ_NOT_NIL(rmfAccessPoint.rmfAPStatus.lastSeenByVscg))
		[apStatus setValue:rmfAccessPoint.rmfAPStatus.lastSeenByVscg forKey:kAPKeyStatusLastSeenByVscg];
	if (OBJ_NOT_NIL(rmfAccessPoint.rmfAPStatus.swver))
		[apStatus setValue:rmfAccessPoint.rmfAPStatus.swver forKey:kAPKeyStatusSwver];
	if (OBJ_NOT_NIL(rmfAccessPoint.rmfAPStatus.location))
		[apStatus setValue:rmfAccessPoint.rmfAPStatus.location forKey:kAPKeyStatusLocation];

	if (OBJ_NOT_NIL(apWlanStatus))
		[accessPoint setValue:apWlanStatus forKey:kAPKeyWlanStatus];

	if (OBJ_NOT_NIL(apExtendedStatus))
		[accessPoint setValue:apExtendedStatus forKey:kAPKeyExtendedStatus];

	if (OBJ_NOT_NIL(apRadioStatus))
		[accessPoint setValue:apRadioStatus forKey:kAPKeyRadioStatus];

	if (OBJ_NOT_NIL(apStatus))
		[accessPoint setValue:apStatus forKey:kAPKeyStatus];

	return accessPoint;
}

#pragma mark - WLAN

/*
 * Return wlan details of a wlan from the database
 */

- (RMFWlanService *)wlanDetails:(NSString *)ssid {
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), kLogGetWLANDetailsFromDatabase);

	RMFWlanService *wlan = nil;

	if (ssid) {
		NSArray *wlanManagedObjects = [dbManager objects:WLAN_ENTITY withProperty:kWlanSsidAttribute value:ssid];

		if (wlanManagedObjects && [wlanManagedObjects count]) {
			// Get wlan object
			NSManagedObject *wlanManagedObject = [wlanManagedObjects firstObject];

			wlan  = [[RMFWlanService alloc] init];
			wlan.name = [wlanManagedObject valueForKey:kWlanNameAttribute];
			wlan.ssid = [wlanManagedObject valueForKey:kWlanSsidAttribute];
			wlan.wlanType = [wlanManagedObject valueForKey:kWlanTypeAttribute];
			wlan.wlanSecurity = [wlanManagedObject valueForKey:kWlanSecurityAttribute];
			wlan.pskPassPhrase = [wlanManagedObject valueForKey:kWlanPskPassPhraseAttribute];

			// Get wlan basic network customization object
			NSManagedObject *wlanBasicManagedObject = [wlanManagedObject valueForKey:kWlanBasicCustomizationAttribute];

			if (wlanBasicManagedObject) {
				RMFWlanBasicNwCustomization *wlanBasicNwCustomization = [[RMFWlanBasicNwCustomization alloc] init];
				wlanBasicNwCustomization.vlanId = [[wlanBasicManagedObject valueForKey:kVlanIdKey] integerValue];
				wlan.wlanBasicNwCustomization = wlanBasicNwCustomization;
			}

			// Get wlan advanced network customization object
			NSManagedObject *wlanAdvManagedObject = [wlanManagedObject valueForKey:kWlanAdvCustomizationAttribute];

			if (wlanAdvManagedObject) {
				RMFWlanAdvNwCustomization *wlanAdvCustmization = [[RMFWlanAdvNwCustomization alloc] init];
				wlanAdvCustmization.maxClientsOnWlanPerRadio = [[wlanAdvManagedObject valueForKey:kMaxClientOnWlanPerRadioAttribute] integerValue];
				wlanAdvCustmization.enableBandBalancing = [wlanAdvManagedObject valueForKey:kEnableBandBalancingAttribute];
				wlan.wlanAdvCustmization = wlanAdvCustmization;
			}
		}
	}
	return wlan;
}

/*
 * Return List of WLANs from the database
 */

- (NSArray *)wlanList {
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), kLogGetWLANListFromDatabase);
	return [dbManager objectsWithName:WLAN_ENTITY];
}

/*
 * Inserts WLAN in the database
 */

- (NSError *)insertWlan:(RMFWlanService *)wlan {
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), kLogInsertWLANInDatabase);
	NSError *error = nil;

	if (wlan) {
        
        if (wlan.ssid) {
            NSArray *wlanManagedObjects = [dbManager objects:WLAN_ENTITY withProperty:kWlanSsidAttribute value:wlan.ssid];
            
            // If entry already exists then update it
            if (wlanManagedObjects && [wlanManagedObjects count]) {
                return [self updateWlan:wlan];
            }
        }
        
		// Insert wlan entity in the database
		NSManagedObject *wlanEntity = [dbManager insertObjectWithName:WLAN_ENTITY];

		// Save wlan entity in the database
		error = [self saveWlan:wlan wlanEntity:wlanEntity];
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForDatabaseInputParameterNotFound];
	}

	return error;
}

/*
 * Delete all WLANs from the database
 */

- (NSError *)deleteAllWlans {
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), kLogDeleteAllWLANFromDatabase);
	return [dbManager deleteAllEntities:WLAN_ENTITY];
}

/*
 * Delete a WLAN from the database
 */

- (NSError *)deleteWlan:(NSString *)ssid {
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), kLogDeleteWLANFromDatabase);
	NSError *error = nil;
	if (ssid) {
		NSArray *wlanManagedObjects = [dbManager objects:WLAN_ENTITY withProperty:kWlanSsidAttribute value:ssid];
		for (NSManagedObject *wlanManagedObject in wlanManagedObjects) {
			error = [dbManager deleteObject:wlanManagedObject];
			if (error) {
				break;
			}
		}
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForWlanSsidNotFound];
	}

	return error;
}

/*
 * Update a WLAN in the database
 */

- (NSError *)updateWlan:(RMFWlanService *)wlan {
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), kLogUpdateWLANInDatabase);
	NSError *error = nil;

	if (wlan && wlan.ssid) {
		NSArray *wlanManagedObjects = [dbManager objects:WLAN_ENTITY withProperty:kWlanSsidAttribute value:wlan.ssid];

		if (wlanManagedObjects && [wlanManagedObjects count]) {
			for (NSManagedObject *wlanManagedObject in wlanManagedObjects) {
				// Update wlan managed object
				error = [self saveWlan:wlan wlanEntity:wlanManagedObject];
				if (error) {
					break;
				}
			}
			return error;
		}
	}
	return [RMFUtils errorWithCode:RMFCodeForWlanSsidNotFound];
}

/*
 * Save wlan entity in the database
 */

- (NSError *)saveWlan:(RMFWlanService *)wlan wlanEntity:(NSManagedObject *)wlanEntity {
	NSError *error = nil;

	if (wlan && wlanEntity) {
		// Get wlan advanced customization managed object
		NSManagedObject *wlanAdvEntity = [wlanEntity valueForKey:kWlanAdvCustomizationAttribute];

		// Get wlan basic customization managed object
		NSManagedObject *wlanBasicEntity = [wlanEntity valueForKey:kWlanBasicCustomizationAttribute];

		// Set wlan name
		if (wlan.name) {
			[wlanEntity setValue:wlan.name forKey:kWlanNameAttribute];
		}

		// Set wlan ssid
		if (wlan.ssid) {
			[wlanEntity setValue:wlan.ssid forKey:kWlanSsidAttribute];
		}

		// Set wlan type
		if (wlan.wlanType) {
			[wlanEntity setValue:wlan.wlanType forKey:kWlanTypeAttribute];
		}

		// Set wlan security
		if (wlan.wlanSecurity) {
			[wlanEntity setValue:wlan.wlanSecurity forKey:kWlanSecurityAttribute];
		}

		// Set wlan passphrase
		if (wlan.pskPassPhrase) {
			[wlanEntity setValue:wlan.pskPassPhrase forKey:kWlanPskPassPhraseAttribute];
		}

		// Set WLAN advanced entity
		if (wlanAdvEntity && wlan.wlanAdvCustmization) {
			if (wlan.wlanAdvCustmization.maxClientsOnWlanPerRadio) {
				// Set maximum clinet on wlan per radio
				[wlanAdvEntity setValue:[NSNumber numberWithInteger:wlan.wlanAdvCustmization.maxClientsOnWlanPerRadio] forKey:kMaxClientOnWlanPerRadioAttribute];
			}

			if (wlan.wlanAdvCustmization.enableBandBalancing) {
				// Set enableBandBalancing
				[wlanAdvEntity setValue:[NSNumber numberWithInteger:wlan.wlanAdvCustmization.enableBandBalancing] forKey:kEnableBandBalancingAttribute];
			}
			[wlanEntity setValue:wlanAdvEntity forKey:kWlanAdvCustomizationAttribute];
		}

		// Set WLAN basic entity
		if (wlanBasicEntity && wlan.wlanBasicNwCustomization) {
			if (wlan.wlanBasicNwCustomization.vlanId) {
				// Set wlan vlanID
				[wlanBasicEntity setValue:[NSNumber numberWithInteger:wlan.wlanBasicNwCustomization.vlanId] forKey:kVlanIdKey];
			}
			[wlanEntity setValue:wlanBasicEntity forKey:kWlanBasicCustomizationAttribute];
		}

		// Save wlan entity in the database
		error = [dbManager saveObject:wlanEntity];
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForDatabaseInputParameterNotFound];
	}

	return error;
}

#pragma mark - Events

/*
 * Return details of an event from the database
 */

- (RMFEvent *)eventDetails:(NSInteger)eventTypeId {
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), kLogGetEventDetailsFromDatabase);

	RMFEvent *event = nil;
	NSArray *eventManagedObjects = [dbManager objects:EVENT_ENTITY withProperty:kEventTypeIdAttribute value:[NSNumber numberWithInteger:eventTypeId]];

	if (eventManagedObjects && [eventManagedObjects count]) {
		NSManagedObject *eventManagedObject = [eventManagedObjects firstObject];
		event  = [[RMFEvent alloc] init];
		event.time = [eventManagedObject valueForKey:kEventTimeAttribute];
		event.type = [eventManagedObject valueForKey:kEventTypeAttribute];
		event.severity = [eventManagedObject valueForKey:kEventSeverityAttribute];
		event.eventDescription = [eventManagedObject valueForKey:kEventDescriptionAttribute];
		event.eventTypeId = [[eventManagedObject valueForKey:kEventTypeIdAttribute] integerValue];
	}

	return event;
}

/*
 * Return List of events from the database
 */

- (NSArray *)eventList {
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), kLogGetEventListFromDatabase);
	return [dbManager objectsWithName:EVENT_ENTITY];
}

/*
 * Inserts event in the database
 */


// Insert User in the database
- (NSError *)insertUser:(RMFUser *)user {
	NSError *error = nil;
	NSManagedObject *userInfo = [dbManager insertObjectWithName:USER_ENTITY];
	error = [self saveUser:user userEntity:userInfo];

	if (!error) {
		RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), SAVE_USER);
	}
	else
		RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), SAVE_USER_ERROR);
	return error;
}

// Delete User from the database
- (NSError *)deleteUser {
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), DELETE_USER);
	return [dbManager deleteAllEntities:USER_ENTITY];
}

- (NSError *)insertEvent:(RMFEvent *)event {
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), kLogInsertEventInDatabase);
	NSError *error = nil;

	if (event) {
		// Insert event entity in the database
		NSManagedObject *eventEntity = [dbManager insertObjectWithName:EVENT_ENTITY];

		// Save event entity in the database
		error = [self saveEvent:event eventEntity:eventEntity];
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForDatabaseInputParameterNotFound];
	}

	return error;
}

/*
 * Delete all events from the database
 */

- (NSError *)deleteAllEvents {
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), kLogDeleteAllEventFromDatabase);
	return [dbManager deleteAllEntities:EVENT_ENTITY];
}

/*
 * Delete an event from the database
 */

- (NSError *)deleteEvent:(NSInteger)eventTypeId {
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), kLogDeleteEventFromDatabase);
	NSError *error = nil;

	NSArray *eventManagedObjects = [dbManager objects:EVENT_ENTITY withProperty:kEventTypeIdAttribute value:[NSNumber numberWithInteger:eventTypeId]];

	for (NSManagedObject *eventManagedObject in eventManagedObjects) {
		error = [dbManager deleteObject:eventManagedObject];
		if (error) {
			break;
		}
	}

	return error;
}

// Save USER entity in the database
- (NSError *)saveUser:(RMFUser *)user userEntity:(NSManagedObject *)userInfo {
	NSError *error = nil;
	if (user) {
		if (user.userName) {
			[userInfo setValue:user.userName forKey:USER_NAME];
		}
		if (user.fullName) {
			[userInfo setValue:user.fullName forKey:USER_FULL_NAME];
		}
		if (user.password) {
			NSData *cipherData = [[user.password dataUsingEncoding:NSUTF8StringEncoding] AES128EncryptedDataWithKey:ENCRYPTION_KEY];
			user.password = [[NSString alloc] initWithData:cipherData encoding:NSUTF8StringEncoding];
			[userInfo setValue:user.password forKey:USER_PASSWORD];
		}
		if (user.address) {
			[userInfo setValue:user.address forKey:USER_ADDRESS];
		}
		if (user.roleType) {
			[userInfo setValue:[NSString stringWithFormat:@"%d", user.roleType] forKey:USER_ROLE];
		}
		if (user.email) {
			[userInfo setValue:user.email forKey:USER_EMAIL];
		}
		if (user.phoneNumber) {
			[userInfo setValue:user.phoneNumber forKey:USER_PH_NO];
		}
		error = [dbManager saveObject:userInfo];
	}
	if (!error) {
		RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), SAVE_USER);
	}
	else
		RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), SAVE_USER_ERROR);
	return error;
}

/*
 * Update a event in the database
 */

- (NSError *)updateEvent:(RMFEvent *)event {
	RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), kLogUpdateEventInDatabase);
	NSError *error = nil;

	if (event && event.eventTypeId) {
		NSArray *eventManagedObjects = [dbManager objects:EVENT_ENTITY withProperty:kEventTypeIdAttribute value:[NSNumber numberWithInteger:event.eventTypeId]];

		if (eventManagedObjects && [eventManagedObjects count]) {
			for (NSManagedObject *eventManagedObject in eventManagedObjects) {
				// Update event managed object
				error = [self saveEvent:event eventEntity:eventManagedObject];

				if (error) {
					break;
				}
			}
			return error;
		}
	}
	return [RMFUtils errorWithCode:RMFCodeForWlanSsidNotFound];
}

/*
 * Save event entity in the database
 */

- (NSError *)saveEvent:(RMFEvent *)event eventEntity:(NSManagedObject *)eventEntity {
	NSError *error = nil;

	if (event && eventEntity) {
		// Set event time
		if (event.time) {
			[eventEntity setValue:event.time forKey:kEventTimeAttribute];
		}

		// Set event type
		if (event.type) {
			[eventEntity setValue:event.type forKey:kEventTypeAttribute];
		}

		// Set event type
		if (event.severity) {
			[eventEntity setValue:event.severity forKey:kEventSeverityAttribute];
		}

		// Set event security
		if (event.eventDescription) {
			[eventEntity setValue:event.eventDescription forKey:kEventDescriptionAttribute];
		}

		// Set event id
		if (event.eventTypeId) {
			[eventEntity setValue:[NSNumber numberWithInteger:event.eventTypeId] forKey:kEventTypeIdKey];
		}

		// Save event entity in the database
		error = [dbManager saveObject:eventEntity];
	}
	else {
		error = [RMFUtils errorWithCode:RMFCodeForDatabaseInputParameterNotFound];
	}

	return error;
}

#pragma mark - User Profile

// Insert User in the database
- (NSError *)insertUserProfile:(RMFUser *)user {
	NSError *error = nil;
	NSManagedObject *userInfo = [dbManager insertObjectWithName:USER_ENTITY];
	error = [self saveUser:user userEntity:userInfo];
	return error;
}

// Delete User from the database
- (NSError *)deleteUserProfile {
	return [dbManager deleteAllEntities:USER_ENTITY];
}

// Update a user in the database
- (NSError *)updateUser:(RMFUser *)user {
	NSError *error = nil;
	if (user) {
		NSArray *userManagedObjects = [dbManager objects:USER_ENTITY withProperty:USER_NAME value:user.userName];
		for (NSManagedObject *userInfo in userManagedObjects) {
			error = [self saveUser:user userEntity:userInfo];
		}
	}
	return error;
}

//Returns user data
- (RMFUser *)getUser {
	NSManagedObjectContext *managedObjectContext = nil;
	NSArray *users = nil;
	RMFUser *user;
	managedObjectContext = [dbManager context];
	if (managedObjectContext) {
		users = [dbManager objectsWithName:USER_ENTITY];
	}
	if (users && [users count] > 0) {
		user = [[RMFUser alloc] init];
		user.userName = [[users objectAtIndex:0] valueForKey:USER_NAME];
		user.email = [[users objectAtIndex:0] valueForKey:USER_EMAIL];
		NSData *cipherData = [[[users objectAtIndex:0] valueForKey:USER_PASSWORD] dataUsingEncoding:NSUTF8StringEncoding];
		user.password  = [[NSString alloc] initWithData:[cipherData AES128DecryptedDataWithKey:ENCRYPTION_KEY]
		                                       encoding:NSUTF8StringEncoding];
		user.address = [[users objectAtIndex:0] valueForKey:USER_ADDRESS];
		user.fullName = [[users objectAtIndex:0] valueForKey:USER_FULL_NAME];
		user.roleType = [[[users objectAtIndex:0] valueForKey:USER_ROLE] intValue];
		user.phoneNumber = [[users objectAtIndex:0] valueForKey:USER_PH_NO];
		NSString *logDescription = [NSString stringWithFormat:@"%@ %@", FETCH_USER_LOG, user];
		RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), logDescription);
	}

	else {
		NSString *logDescription = [NSString stringWithFormat:@"%@ , %@", ERROR, USER_INFO_NOT_FETCHED];
		RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
	}

	return user;
}

@end
