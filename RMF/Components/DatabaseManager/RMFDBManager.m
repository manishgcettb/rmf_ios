/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFDBManager.h"
#import "RMFMacro.h"


@interface RMFDBManager ()

- (NSManagedObjectModel *)managedObjectModel;
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator;
- (NSManagedObjectContext *)managedObjectContext;
- (BOOL)isEntitiesHashesValid;

@end

@implementation RMFDBManager
static RMFDBManager *sharedRMFDBManager = nil;


#pragma mark - Class methods declaration

/*
 * Creates a singleton instance
 */
+ (RMFDBManager *)sharedInstance {
    if (!OBJ_NOT_NIL(sharedRMFDBManager)) {
        sharedRMFDBManager = [[RMFDBManager alloc] init];
        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), SHARED_INSTANCE_CREATED(kDBManager));
    }
    return sharedRMFDBManager;
}

#pragma mark - Instance methods implementation

/*
 * initialise the singleton instance
 */
- (RMFDBManager *)init {
    if (!OBJ_NOT_NIL(sharedRMFDBManager)) {
        sharedRMFDBManager = [super init];
    }
    
    return sharedRMFDBManager;
}

#pragma mark - Private methods

- (NSManagedObjectModel *)managedObjectModel {
    if (OBJ_NOT_NIL(_managedObjectModel)) {
        return _managedObjectModel;
    }
    NSBundle *rmfLibBundle = RMFBUNDLE;
    if (OBJ_NOT_NIL(rmfLibBundle)) {
        NSString *dbFilePath =  [[rmfLibBundle resourcePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.momd", _name]];
        NSURL *staticLibraryMOMURL = [NSURL URLWithString:dbFilePath];
        _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:staticLibraryMOMURL];
    }
    RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), OBJECT_MODEL_CREATED);
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (OBJ_NOT_NIL(_persistentStoreCoordinator)) {
        return _persistentStoreCoordinator;
    }
    
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    NSURL *storeUrl = [NSURL fileURLWithPath:_path];
    NSError *error;
    
    if ([self managedObjectModel]) {
        _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
        [_persistentStoreCoordinator addPersistentStoreWithType:NSBinaryStoreType configuration:nil URL:storeUrl options:options error:&error];
    }
    RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), PERSISTANCE_CREATED);
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext {
    if (OBJ_NOT_NIL(_managedObjectContext)) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (OBJ_NOT_NIL(coordinator)) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), MODEL_CONTEXT_CREATED);
    return _managedObjectContext;
}

- (BOOL)isEntitiesHashesValid {
    NSError *error = nil;
    NSDictionary *metadata = [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:NSSQLiteStoreType
                                                                                        URL:[NSURL fileURLWithPath:_path]
                                                                                      error:&error];
    
    if (OBJ_NOT_NIL(metadata)) {
        NSMutableDictionary *storeEntityHashes = [NSMutableDictionary dictionaryWithDictionary:[metadata objectForKey:STORE_MODE_VERSION]];
        NSDictionary *modelEntityHashes = [[self managedObjectModel] entityVersionHashesByName];
        NSArray *entities = [modelEntityHashes allKeys];
        for (NSString *entity in entities) {
            NSData *modelEntityHash = [modelEntityHashes objectForKey:entity];
            NSData *storeEntityHash = [storeEntityHashes objectForKey:entity];
            
            if (OBJ_NOT_NIL(storeEntityHash) && [modelEntityHash isEqualToData:storeEntityHash]) {
                [storeEntityHashes removeObjectForKey:entity];
            }
            else {
                return NO;
            }
        }
        if ([[storeEntityHashes allKeys] count] > 0) {
            return NO;
        }
        return YES;
    }
    return NO;
}

#pragma mark - Public methods

- (NSManagedObjectContext *)context {
    return [self managedObjectContext];
}

- (NSPersistentStoreCoordinator *)coordinator {
    return [self persistentStoreCoordinator];
}

- (NSManagedObjectModel *)model {
    return [self managedObjectModel];
}

// init db with name
- (id)initWithName:(NSString *)name {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    NSString *defaultPath = [basePath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite", name]];
    if ((self = [self initWithPath:defaultPath name:name])) {
    }
    RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), DATABASE_INIT);
    return self;
}

- (id)initWithPath:(NSString *)path name:(NSString *)name {
    if ((self = [super init])) {
        _name = [name copy];
        _path = [path copy];
    }
    RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), DATABASE_INIT_PATH);
    return self;
}

- (NSManagedObject *)insertObjectWithName:(NSString *)name {
    NSManagedObject *managedObject = nil;
    if ([self managedObjectContext]) {
        managedObject = [NSEntityDescription insertNewObjectForEntityForName:name inManagedObjectContext:[self managedObjectContext]];
    }
    return managedObject;
}

- (void)removeObject:(NSManagedObject *)object {
    [object.managedObjectContext deleteObject:object];
}

- (NSError *)saveObject:(NSManagedObject *)object {
    NSError *error = nil;
    if(object)
        [object.managedObjectContext save:&error];
    else
        error = [[NSError alloc] initWithDomain:OBJECT_NIL_ERROR code:OBJECT_NIL_CODE userInfo:nil];
    return error;
}

- (NSError *)saveAll {
    NSError *error = nil;
    [[self managedObjectContext] save:&error];
    return error;
}

- (NSArray *)objects:(NSString *)objectName withProperty:(NSString *)propertyName value:(id)value {
    NSArray *results = nil;
    NSEntityDescription *entity = nil;
    if ([self managedObjectContext]) {
         entity = [NSEntityDescription entityForName:objectName inManagedObjectContext:[self managedObjectContext]];
        return nil;
    }
    if (!OBJ_NOT_NIL(entity)) {
        return nil;
    }
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K = %@", propertyName, value];
    [fetchRequest setPredicate:predicate];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:propertyName ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSFetchedResultsController *fetchController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                      managedObjectContext:[self managedObjectContext]
                                                                                        sectionNameKeyPath:nil
                                                                                                 cacheName:nil];
    NSError *error;
    BOOL success = [fetchController performFetch:&error];
    if (success) {
        results = fetchController.fetchedObjects;
    }
    return results;
}

- (NSError *)deleteObject:(NSManagedObject *)object {
    [object.managedObjectContext deleteObject:object];
    NSError *error = nil;
    [object.managedObjectContext save:&error];
    
    return error;
}

- (NSError *)deleteAllEntities:(NSString *)nameEntity {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:nameEntity];
    [fetchRequest setIncludesPropertyValues:NO];
    NSError *error;
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    if (managedObjectContext) {
        NSArray *fetchedObjects = [managedObjectContext executeFetchRequest:fetchRequest error:&error];
        for (NSManagedObject *object in fetchedObjects) {
            [managedObjectContext deleteObject:object];
        }
        
        error = nil;
        [managedObjectContext save:&error];
    }
    return error;
}

- (NSArray *)objectsWithName:(NSString *)objectName {
    NSArray *results = nil;
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    if (managedObjectContext) {
        NSEntityDescription *entity = [NSEntityDescription entityForName:objectName inManagedObjectContext:managedObjectContext];
        
        if (entity == nil) {
            return results;
        }
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:entity];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:nil];
        [fetchRequest setSortDescriptors:sortDescriptors];
        NSFetchedResultsController *fetchController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                          managedObjectContext:[self managedObjectContext]
                                                                                            sectionNameKeyPath:nil
                                                                                                     cacheName:nil];
        
        
        NSError *error;
        BOOL success = [fetchController performFetch:&error];
        if (success) {
            results = fetchController.fetchedObjects;
        }
    }
    return results;
}

- (NSArray *)objects:(NSString *)objectName withPredicateString:(NSString *)string {
    return [self objects:objectName withPredicateString:string arguments:nil sortDescriptors:nil fetchLimit:0];
}

- (NSArray *)objects:(NSString *)objectName withPredicateString:(NSString *)string arguments:(NSArray *)arguments
     sortDescriptors:(NSArray *)sortDescriptors
          fetchLimit:(int)fetchLimit {
    NSArray *results = nil;
    NSEntityDescription *entity = [NSEntityDescription entityForName:objectName inManagedObjectContext:[self managedObjectContext]];
    if (entity == nil) {
        return nil;
    }
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:string argumentArray:arguments];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setSortDescriptors:sortDescriptors ? sortDescriptors : [NSArray array]];
    if (fetchLimit) {
        [fetchRequest setFetchLimit:fetchLimit];
    }
    NSFetchedResultsController *fetchController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                      managedObjectContext:[self managedObjectContext]
                                                                                        sectionNameKeyPath:nil
                                                                                                 cacheName:nil];
    
    NSError *error;
    BOOL success = [fetchController performFetch:&error];
    if (success) {
        results = fetchController.fetchedObjects;
    }
    return results;
}

- (NSBatchUpdateResult *)updateValues:(NSString *)entityName value:(NSString *)value attribute:(NSString *)attribute {
    NSBatchUpdateRequest *req = [[NSBatchUpdateRequest alloc] initWithEntityName:entityName];
    req.predicate = [NSPredicate predicateWithFormat:@"%@ == %@", attribute, value];
    req.propertiesToUpdate = @{
                               attribute : value
                               };
    req.resultType = NSUpdatedObjectsCountResultType;
    NSError *error;
    NSBatchUpdateResult *result = (NSBatchUpdateResult *)[[self managedObjectContext] executeRequest:req error:&error];
    return result;
}

- (void)clearCoreData {
    NSPersistentStore *store = [[self.persistentStoreCoordinator persistentStores] lastObject];
    NSError *error;
    [self.persistentStoreCoordinator removePersistentStore:store error:&error];
    [[NSFileManager defaultManager] removeItemAtURL:[store URL] error:&error];
    _managedObjectContext = nil;
    _persistentStoreCoordinator = nil;
    
    RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), DATA_CLEANED);
}

@end
