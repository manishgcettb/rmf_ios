/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface RMFDBManager : NSObject
{
	NSString *_path;
	NSString *_name;

	NSManagedObjectModel *_managedObjectModel;
	NSManagedObjectContext *_managedObjectContext;
	NSPersistentStoreCoordinator *_persistentStoreCoordinator;
}

//Returns shared instance of DBManager class
+ (RMFDBManager *)sharedInstance;

//Returns the ManagedObject Context
- (NSManagedObjectContext *)context;

//Returns the Persistent Store
- (NSPersistentStoreCoordinator *)coordinator;

//Returns the Managed Object Model
- (NSManagedObjectModel *)model;

//To initialise it with the name of Database
- (id)initWithName:(NSString *)name;

//To initialise it with the name and path of Database
- (id)initWithPath:(NSString *)path name:(NSString *)name;

//To delete a data object from the database
- (NSError *)deleteObject:(NSManagedObject *)object;

//To insert the object into database
- (NSManagedObject *)insertObjectWithName:(NSString *)name;

//To remove object from the Database
- (void)removeObject:(NSManagedObject *)object;

//To save object
- (NSError *)saveObject:(NSManagedObject *)object;

//To save all the objects into database
- (NSError *)saveAll;

//Fetch the data with property
- (NSArray *)objects:(NSString *)objectName withProperty:(NSString *)propertyName value:(id)value;

//Fetch the data with Predicate String
- (NSArray *)objects:(NSString *)objectName withPredicateString:(NSString *)string;

//Fetch the data with Predicate String and argument
- (NSArray *)objects:(NSString *)objectName withPredicateString:(NSString *)string arguments:(NSArray *)arguments
     sortDescriptors:(NSArray *)sortDescriptors
          fetchLimit:(int)fetchLimit;

//Fetch all the objects with object name
- (NSArray *)objectsWithName:(NSString *)objectName;

//To update values
- (NSBatchUpdateResult *)updateValues:(NSString *)entityName value:(NSString *)value attribute:(NSString *)attribute;

//To delete all enteries
- (NSError *)deleteAllEntities:(NSString *)nameEntity;

//To clean core data from the device.
- (void)clearCoreData;
@end
