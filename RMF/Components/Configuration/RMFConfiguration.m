/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFConfiguration.h"
#import "RMFMacro.h"
#import "RMFNavigationFlow.h"
#import "RMFUtils.h"

#import "RMFLogger.h"
#import "RMFLog.h"


@interface RMFConfiguration ()
@property (nonatomic, strong) NSDictionary *screenNavigationFlow;

- (NSString *)findValueForKey:(NSString *)key;
- (NSArray *)findArrayForKey:(NSString *)key;
- (NSDictionary *)findDictionaryForKey:(NSString *)key;

@end


@implementation RMFConfiguration

static RMFConfiguration *sharedRMFConfiguration = nil;


@synthesize rmfConfig = _rmfConfig;
@synthesize portInfo = _portInfo;

@synthesize mimeType = _mimeType;
@synthesize rmfVersion = _rmfVersion;
@synthesize rmfName = _rmfName;
@synthesize rmfDescription = _rmfDescription;
@synthesize defaultZome = _defaultZome;
@synthesize termOfUseURL = _termOfUseURL;
@synthesize privacyPolicyURL = _privacyPolicyURL;
@synthesize aboutUs = _aboutUs;
@synthesize privacyPolicy = _privacyPolicy;
@synthesize help = _help;


+ (RMFConfiguration *)sharedRMFConfiguration {
	if (!OBJ_NOT_NIL(sharedRMFConfiguration)) {
		sharedRMFConfiguration = [[RMFConfiguration alloc] init];
	}
	return sharedRMFConfiguration;
}

- (id)init {
	if (!OBJ_NOT_NIL(sharedRMFConfiguration)) {
		sharedRMFConfiguration = [super init];
		self.system = kSystemTypeNone;
		// Set configuration value
		[self initRmfConfigValues];
	}
	return sharedRMFConfiguration;
}

- (void)initRmfConfigValues {
	NSData *configFileData = nil;
	configFileData = RMFBUNDLE_DATA_WITH_FILE_NAME(CONFIG_FILE_NAME);

	if (configFileData != nil) {
		NSError *error = nil;
		id configObj = [NSJSONSerialization JSONObjectWithData:configFileData options:NSJSONReadingMutableContainers error:&error];
		if (error) {
			NSString *logDescription = [NSString stringWithFormat:@" %@ %@", ERROR, error.description];
			RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
		}
		else if ([configObj isKindOfClass:[NSDictionary class]]) {
			_rmfConfig = (NSDictionary *)configObj;

			if (![[self findValueForKey:SYSTEM] isEqualToString:@""]) {
				self.system = [[self findValueForKey:SYSTEM] intValue];
			}
			_keepMeLogin = [[self findValueForKey:KEEP_ME_LOGIN] boolValue];
			_notificationEnabled = [[self findValueForKey:NOTIFICATION_ENABLED] boolValue];
			self.isAnalyticsEnabled = [[self findValueForKey:ANALYTICS_ENABLED] boolValue];

			_isSandboxEnabled = [[self findValueForKey:IS_SANDBOX_ENABLED] boolValue];

			_supportedSocialLinks = [self findDictionaryForKey:SUPPORTED_SOCIAL_LINKS];

			_configUrl = [self findValueForKey:CONFIG_URL];
			_systemIP = [self findValueForKey:SYSTEM_IP];

			_companyName = [self findValueForKey:COMPANY_NAME];
			_companyUrl = [self findValueForKey:COMPANY_URL];
			_promoURL = [self findValueForKey:PROMO_URL];
			_appVersion = [self findValueForKey:APP_VERSION];
			_supportEmail = [self findValueForKey:SUPPORT_EMAIL];
			_supportPhone = [self findValueForKey:SUPPORT_PHONE];
			_systemVersion = [self findValueForKey:SYSTEM_VERSION];

			_sponsoredURL = [self findDictionaryForKey:SPONSORED_URL];
			_portInfo = [self findDictionaryForKey:PORT_INFO];
			_port = [self findValueForKey:PORT];

			_mimeType = [self findValueForKey:MIME_TYPE];
			_rmfVersion = [self findValueForKey:PROFILE_VERSION];
			_rmfName = [self findValueForKey:PROFILE_NAME];
			_rmfDescription = [self findValueForKey:PROFILE_DESC];

			_defaultZome = [self findValueForKey:DEFAULT_ZONE];
			_termOfUseURL = [self findValueForKey:TERMS_OF_USE_URL];
			_privacyPolicyURL = [self findValueForKey:PRIVACY_POLICY_URL];
			_aboutUs = [self findValueForKey:ABOUT_US];
			_privacyPolicy = [self findValueForKey:PRIVACY_POLICY];
			_help = [self findValueForKey:HELP];

			_notificationTypes = [self findDictionaryForKey:NOTIFICATION_TYPE];

			self.screenNavigationFlow = [self findDictionaryForKey:SCREEN_NAV_FLOW];
			self.isDatabaseEnabled = [self findValueForKey:IS_DATABASE_ENABLED];
		}
	}
}

- (NSString *)findValueForKey:(NSString *)key {
	if (OBJ_NOT_NIL(_rmfConfig) && OBJ_NOT_NIL(key) && OBJ_NOT_NIL([_rmfConfig objectForKey:key]) && [[_rmfConfig objectForKey:key] isKindOfClass:[NSString class]]) {
		return [_rmfConfig objectForKey:key];
	}
	else {
		return @"";
	}
}

- (NSArray *)findArrayForKey:(NSString *)key {
	if (OBJ_NOT_NIL(_rmfConfig) && OBJ_NOT_NIL(key) && OBJ_NOT_NIL([_rmfConfig objectForKey:key]) && [[_rmfConfig objectForKey:key] isKindOfClass:[NSArray class]]) {
		return [_rmfConfig objectForKey:key];
	}
	else {
		return nil;
	}
}

- (NSDictionary *)findDictionaryForKey:(NSString *)key {
	if (OBJ_NOT_NIL(_rmfConfig) && OBJ_NOT_NIL(key) && OBJ_NOT_NIL([_rmfConfig objectForKey:key]) && [[_rmfConfig objectForKey:key] isKindOfClass:[NSDictionary class]]) {
		return [_rmfConfig objectForKey:key];
	}
	else {
		return nil;
	}
}

// Return screen navigation flow on the basis of user role (i.e. Admin , Normal)
- (NSString *)screenNavigationFlowForRole:(int)userRole {
	if (self.system == kSystemTypeNone) {
		NSString *logDescription = [NSString stringWithFormat:@" %@ %@", WARNING, kSystemTypeNotDefined];
		RMF_LOG_WARNING([self class], NSStringFromSelector(_cmd), logDescription);
		[RMFUtils raiseException:NSInvalidArgumentException WithDescription:kSystemTypeNotDefined];
	}

	if (!OBJ_NOT_NIL(self.screenNavigationFlow))
		return @"";

	RMFNavigationFlow *rmfNavigationFlow = [[RMFNavigationFlow alloc] init];
	if (OBJ_NOT_NIL(rmfNavigationFlow)) {
		return [rmfNavigationFlow screenNavigationFlowForRole:userRole navigationFlowDic:self.screenNavigationFlow systemType:self.system];
	}
	return @"";
}

// Return screen navigation flow for the first time
- (NSString *)screenNavigationFlowForFirstTime {
	if (self.system == kSystemTypeNone) {
		NSString *logDescription = [NSString stringWithFormat:@" %@ %@", WARNING, kSystemTypeNotDefined];
		RMF_LOG_WARNING([self class], NSStringFromSelector(_cmd), logDescription);
		[RMFUtils raiseException:NSInvalidArgumentException WithDescription:kSystemTypeNotDefined];
	}

	if (!OBJ_NOT_NIL(self.screenNavigationFlow))
		return @"";

	RMFNavigationFlow *rmfNavigationFlow = [[RMFNavigationFlow alloc] init];
	if (OBJ_NOT_NIL(rmfNavigationFlow)) {
		return [rmfNavigationFlow screenNavigationFlowForFirstTimeFromNavigationDic:self.screenNavigationFlow systemType:self.system];
	}
	return @"";
}

// Return sponsored URL for orientation
- (NSString *)sponsoredUrlForOrientation:(int)orientation {
	if (OBJ_NOT_NIL(_sponsoredURL)) {
		switch (orientation) {
			case kScreenOrientationPortrait:
			{
				if (OBJ_NOT_NIL([_sponsoredURL objectForKey:PORTRAIT])) {
					return [_sponsoredURL objectForKey:PORTRAIT];
				}
			}
			break;

			case kScreenOrientationLandscape:
			{
				if (OBJ_NOT_NIL([_sponsoredURL objectForKey:LANDSCAPE])) {
					return [_sponsoredURL objectForKey:LANDSCAPE];
				}
			}
		}
	}
	return @"";
}

- (NSString *)contentTypeForActiveController {
	NSString *contentType = @"";
	switch (self.system) {
		case kSystemTypeKUMO:
		{
			contentType = kKUMOContentTypeKey;
			break;
		}

		case kSystemTypeSCG:
		{
			contentType = kSCGContentTypeKey;
			break;
		}

		case kSystemTypeZD:
		{
			contentType = kZDContentTypeKey;
			break;
		}

		default:
			break;
	}
	return contentType;
}

@end
