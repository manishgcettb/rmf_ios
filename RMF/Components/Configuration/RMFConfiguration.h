/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <Foundation/Foundation.h>


// enum for system type (i.e. KUMO = 1, ZD = 2, SCG = 3)

enum {
	kSystemTypeKUMO = 1,
	kSystemTypeSCG,
	kSystemTypeZD,
	kSystemTypeNone
};

// enum for user role (i.e. Admin = 1, Normal = 2)
enum {
	kUserRoleSuper = 1,
	kUserRoleNormal
};

// screen orientation (i.e. Portrait = 1, Landscape =2)
enum {
	kScreenOrientationPortrait = 1,
	kScreenOrientationLandscape
};

// enum for analytics type
enum {
	GoogleAnalytics = 1,
	FlurryAnalytics,
	CrittercismAnalytics,
	CrashlyticsAnalytics
};

typedef enum {
	kLogFlagFatal     = (1 << 0), // 0...00001
	kLogFlagError      = (1 << 1), // 0...000010
	kLogFlagWarning    = (1 << 2), // 0...000100
	kLogFlagNotice    = (1 << 3), // 0...0001000
	kLogFlagInfo       = (1 << 4), // 0...0010000
	kLogFlagDebug      = (1 << 5), // 0...0100000
	kLogFlagVerbose    = (1 << 6)  // 0...1000000
}RMFLogLevel;


@interface RMFConfiguration : NSObject


#pragma mark - APP can set these property

// Define system type (i.e. KUMO = 1, SCG = 2, ZD = 3)
@property (nonatomic, assign) int system;

@property (nonatomic, assign) BOOL keepMeLogin;

// Enable and disable notification
@property (nonatomic, assign) BOOL notificationEnabled;

// A dictionary contains bool(i.e. true, false) value for key(i.e. fb, tw, linkedin, gplus)
@property (nonatomic, strong) NSDictionary *supportedSocialLinks;

// Enable and disable database
@property (nonatomic, assign) BOOL isDatabaseEnabled;

// Enable and disable analytics
@property (nonatomic, assign) BOOL isAnalyticsEnabled;

// Debug Environment - Stores and returns mode of library (i.e. sandbox or production)
@property (nonatomic, assign) BOOL isSandboxEnabled;

// Stores and returns user type
@property (nonatomic, assign) NSInteger userType;

// Stores and returns user association information
@property (nonatomic, strong) NSString *userAssociation;

// URL for API request
@property (nonatomic, strong) NSString *configUrl;

// IP Address for API request without port number
@property (nonatomic, strong) NSString *systemIP;

@property (nonatomic, strong) NSString *companyName;
@property (nonatomic, strong) NSString *companyUrl;
@property (nonatomic, strong) NSString *promoURL;
@property (nonatomic, strong) NSString *appVersion;
@property (nonatomic, strong) NSString *supportEmail;
@property (nonatomic, strong) NSString *supportPhone;
@property (nonatomic, strong) NSString *systemVersion;
@property (nonatomic, strong) NSString *deviceToken;
@property (nonatomic, strong) NSDictionary *sponsoredURL;

// enable notification type (i.e. sound, alert, badge)
@property (readonly, strong) NSDictionary *notificationTypes;

// Port used for API's request
@property (nonatomic, strong) NSString *port;

#pragma mark - set log level
@property (nonatomic, assign) RMFLogLevel logLevelRMF;


#pragma mark - Ready Only Property

// RMF configuration dictionary
@property (readonly, strong) NSDictionary *rmfConfig;

// Dictionary with port for zd, scg, kumo
@property (readonly, strong) NSDictionary *portInfo;

@property (readonly, strong) NSString *mimeType;
@property (readonly, strong) NSString *rmfVersion;
@property (readonly, strong) NSString *rmfName;
@property (readonly, strong) NSString *rmfDescription;
@property (readonly, strong) NSString *defaultZome;
@property (readonly, strong) NSString *termOfUseURL;
@property (readonly, strong) NSString *privacyPolicyURL;

// Privacy Policy
@property (readonly, strong) NSString *privacyPolicy;
// About us
@property (readonly, strong) NSString *aboutUs;
// Help
@property (readonly, strong) NSString *help;

// Return singleton instance of RMFConfiguration
+ (RMFConfiguration *)sharedRMFConfiguration;

/**
 * Get screen navigation flow for different user role

 * @param userRole (i.e. Admin = 1 , Normal = 2)
 * @return screen navigation flow string
 */
- (NSString *)screenNavigationFlowForRole:(int)userRole;

/**
 * Get screen navigation flow for first time

 * @return screen navigation flow string
 */
- (NSString *)screenNavigationFlowForFirstTime;

/**
 * Get sponsored URL for different orientation

 * @param orientation (i.e. Portrait = 1 , Landscape =2)
 * @return sponsored URL string
 */
- (NSString *)sponsoredUrlForOrientation:(int)orientation;
- (NSString *)contentTypeForActiveController;


@end
