/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <Foundation/Foundation.h>
#import "RMFConfiguration.h"

@interface RMFNavigationFlow : NSObject

// Return screen navigation flow on the basis of user role (i.e. Admin = 1 ,Normal = 2) controller type (KUMO = 1, SCG = 2, ZD = 3)

/**
 * Get screen navigation flow for different user role
 
 * @param userRole (i.e. Admin = 1 , Normal = 2)
 * @param navFlowDic navigation flow dictionary
 * @param systemType system type (i.e. KUMO = 1, ZD = 2, SCG = 3)
 * @return screen navigation flow string
 */
- (NSString *)screenNavigationFlowForRole:(int)userRole navigationFlowDic:(NSDictionary *)navFlowDic systemType:(int)systemType;


/**
 * Get screen navigation flow for first time
 
 * @param navFlowDic navigation flow dictionary
 * @param systemType system type (i.e. KUMO = 1, ZD = 2, SCG = 3)
 * @return screen navigation flow string
 */
- (NSString *)screenNavigationFlowForFirstTimeFromNavigationDic:(NSDictionary *)navFlowDic systemType:(int)systemType;

@end
