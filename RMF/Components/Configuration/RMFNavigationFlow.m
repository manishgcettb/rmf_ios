/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFNavigationFlow.h"
#import "RMFMacro.h"


@implementation RMFNavigationFlow


// Return screen navigation flow on the basis of user role (i.e. Admin = 1 ,Normal = 2) controller type (KUMO = 1, SCG = 2, ZD = 3)
- (NSString *)screenNavigationFlowForRole:(int)userRole navigationFlowDic:(NSDictionary *)navFlowDic systemType:(int)systemType;
{
	if (OBJ_NOT_NIL(navFlowDic)) {
		switch (systemType) {
			case kSystemTypeKUMO:
			{
				if (OBJ_NOT_NIL([navFlowDic objectForKey:KUMO])) {
					NSDictionary *kumoNavFlow = [navFlowDic objectForKey:KUMO];
					if (OBJ_NOT_NIL(kumoNavFlow)) {
						return [self navigationFlowForRole:userRole navigationFlowDic:kumoNavFlow];
					}
				}
			}
			break;

			case kSystemTypeSCG:
			{
				if (OBJ_NOT_NIL([navFlowDic objectForKey:SCG])) {
					NSDictionary *scgNavFlow = [navFlowDic objectForKey:SCG];

					if (OBJ_NOT_NIL(scgNavFlow)) {
						return [self navigationFlowForRole:userRole navigationFlowDic:scgNavFlow];
					}
				}
			}
			break;

			case kSystemTypeZD:
			{
				if (OBJ_NOT_NIL([navFlowDic objectForKey:ZD])) {
					NSDictionary *zdNavFlow = [navFlowDic objectForKey:ZD];
					if (OBJ_NOT_NIL(zdNavFlow)) {
						return [self navigationFlowForRole:userRole navigationFlowDic:zdNavFlow];
					}
				}
			}
		}
	}
	return @"";
}

- (NSString *)navigationFlowForRole:(int)userRole navigationFlowDic:(NSDictionary *)navigationFlowDic {
	if (userRole == kUserRoleSuper) {
		RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), USER_ROLE_ADMIN);

		if (OBJ_NOT_NIL(navigationFlowDic) && OBJ_NOT_NIL([navigationFlowDic objectForKey:NAVIGATION_FLOW_FOR_ADMIN])) {
			return [navigationFlowDic objectForKey:NAVIGATION_FLOW_FOR_ADMIN];
		}
	}
	else if (userRole == kUserRoleNormal) {
		RMFLogInfo(USER_ROLE_NORMAL);
		if (OBJ_NOT_NIL(navigationFlowDic) && OBJ_NOT_NIL([navigationFlowDic objectForKey:NAVIGATION_FLOW_FOR_NORMAL])) {
			return [navigationFlowDic objectForKey:NAVIGATION_FLOW_FOR_NORMAL];
		}
	}
	return @"";
}

// Return screen navigation flow for the first time on the basis of controller type (KUMO = 1, SCG = 2, ZD = 3)

- (NSString *)screenNavigationFlowForFirstTimeFromNavigationDic:(NSDictionary *)navFlowDic systemType:(int)systemType {
	if (OBJ_NOT_NIL(navFlowDic)) {
		switch (systemType) {
			case kSystemTypeKUMO:
			{
				if (OBJ_NOT_NIL([navFlowDic objectForKey:KUMO])) {
					NSDictionary *kumoNavFlow = [navFlowDic objectForKey:KUMO];
					if (OBJ_NOT_NIL(kumoNavFlow) && OBJ_NOT_NIL([kumoNavFlow objectForKey:NAVIGATION_FLOW_FIRST_TIME])) {
						return [kumoNavFlow objectForKey:NAVIGATION_FLOW_FIRST_TIME];
					}
				}
			}
			break;

			case kSystemTypeSCG:
			{
				if (OBJ_NOT_NIL([navFlowDic objectForKey:SCG])) {
					NSDictionary *kumoNavFlow = [navFlowDic objectForKey:SCG];
					if (OBJ_NOT_NIL(kumoNavFlow) && OBJ_NOT_NIL([kumoNavFlow objectForKey:NAVIGATION_FLOW_FIRST_TIME])) {
						return [kumoNavFlow objectForKey:NAVIGATION_FLOW_FIRST_TIME];
					}
				}
			}
			break;

			case kSystemTypeZD:
			{
				if (OBJ_NOT_NIL([navFlowDic objectForKey:ZD])) {
					NSDictionary *kumoNavFlow = [navFlowDic objectForKey:ZD];
					if (OBJ_NOT_NIL(kumoNavFlow) && OBJ_NOT_NIL([kumoNavFlow objectForKey:NAVIGATION_FLOW_FIRST_TIME])) {
						return [kumoNavFlow objectForKey:NAVIGATION_FLOW_FIRST_TIME];
					}
				}
			}
		}
	}
	return @"";
}

@end
