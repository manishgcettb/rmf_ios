/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <Foundation/Foundation.h>
#import "RMFConfiguration.h"

@interface RMFErrorHandler : NSObject

	typedef NS_ENUM (NSUInteger, HTTPCode) {
	//HTTP code for depecting informational status codes
	HTTPCodeFor1XXInformationalUnknown = 1,
	HTTPCodeForContinue = 100,
	HTTPCodeForSwitchingProtocols = 101,
	HTTPCodeForProcessing = 102,

	//HTTP code for depecting success status codes
	HTTPCodeFor2XXSuccessUnknown = 2,
	HTTPCodeForOK = 200,
	HTTPCodeForCreated = 201,
	HTTPCodeForAccepted = 202,
	HTTPCodeForNonAuthoritativeInformation = 203,
	HTTPCodeForNoContent = 204,
	HTTPCodeForResetContent = 205,
	HTTPCodeForPartialContent = 206,
	HTTPCodeForMultiStatus = 207,
	HTTPCodeForAlreadyReported = 208,
	HTTPCodeForIMUsed = 226,

	//HTTP code for depecting redirectional status codes
	HTTPCodeFor3XXSuccessUnknown = 3,
	HTTPCodeForMultipleChoices = 300,
	HTTPCodeForMovedPermanently = 301,
	HTTPCodeForFound = 302,
	HTTPCodeForSeeOther = 303,
	HTTPCodeForNotModified = 304,
	HTTPCodeForUseProxy = 305,
	HTTPCodeForSwitchProxy = 306,
	HTTPCodeForTemporaryRedirect = 307,
	HTTPCodeForPermanentRedirect = 308,

	//HTTP code for depecting client error status codes
	HTTPCodeFor4XXSuccessUnknown = 4,
	HTTPCodeForBadRequest = 400,
	HTTPCodeForUnauthorised = 401,
	HTTPCodeForPaymentRequired = 402,
	HTTPCodeForForbidden = 403,
	HTTPCodeForNotFound = 404,
	HTTPCodeForMethodNotAllowed = 405,
	HTTPCodeForNotAcceptable = 406,
	HTTPCodeForProxyAuthenticationRequired = 407,
	HTTPCodeForRequestTimeout = 408,
	HTTPCodeForConflict = 409,
	HTTPCodeForGone = 410,
	HTTPCodeForLengthRequired = 411,
	HTTPCodeForPreconditionFailed = 412,
	HTTPCodeForRequestEntityTooLarge = 413,
	HTTPCodeForRequestURITooLong = 414,
	HTTPCodeForUnsupportedMediaType = 415,
	HTTPCodeForRequestedRangeNotSatisfiable = 416,
	HTTPCodeForExpectationFailed = 417,
	HTTPCodeForIamATeapot = 418,
	HTTPCodeForAuthenticationTimeout = 419,
	HTTPCodeForMethodFailureSpringFramework = 420,
	HTTPCodeForMisdirectedRequest = 421,
	HTTPCodeForEnhanceYourCalmTwitter = 4200,
	HTTPCodeForUnprocessableEntity = 422,
	HTTPCodeForLocked = 423,
	HTTPCodeForFailedDependency = 424,
	HTTPCodeForMethodFailureWebDaw = 4240,
	HTTPCodeForUpgradeRequired = 426,
	HTTPCodeForPreconditionRequired = 428,
	HTTPCodeForTooManyRequests = 429,
	HTTPCodeForRequestHeaderFieldsTooLarge = 431,
	HTTPCodeForLoginTimeout = 440,
	HTTPCodeForNoResponseNginx = 444,
	HTTPCodeForRetryWithMicrosoft = 449,
	HTTPCodeForBlockedByWindowsParentalControls = 450,
	HTTPCodeForRedirectMicrosoft = 451,
	HTTPCodeForUnavailableForLegalReasons = 4510,
	HTTPCodeForRequestHeaderTooLargeNginx = 494,
	HTTPCodeForCertErrorNginx = 495,
	HTTPCodeForNoCertNginx = 496,
	HTTPCodeForHTTPToHTTPSNginx = 497,
	HTTPCodeForTokenExpiredOrInvalid = 498,
	HTTPCodeForClientClosedRequestNginx = 499,


	//HTTP code for depecting server error status codes
	HTTPCodeFor5XXSuccessUnknown = 5,
	HTTPCodeForInternalServerError = 500,
	HTTPCodeForNotImplemented = 501,
	HTTPCodeForBadGateway = 502,
	HTTPCodeForServiceUnavailable = 503,
	HTTPCodeForGatewayTimeout = 504,
	HTTPCodeForHTTPVersionNotSupported = 505,
	HTTPCodeForVariantAlsoNegotiates = 506,
	HTTPCodeForInsufficientStorage = 507,
	HTTPCodeForLoopDetected = 508,
	HTTPCodeForBandwidthLimitExceeded = 509,
	HTTPCodeForNotExtended = 510,
	HTTPCodeForNetworkAuthenticationRequired = 511,
	HTTPCodeForConnectionTimedOut = 522,
	HTTPCodeForNetworkReadTimeoutErrorUnknown = 598,
	HTTPCodeForNetworkConnectTimeoutErrorUnknown = 599,

	//RMF specific error
	RMFCodeForUrlNotFound = 1000,
	RMFCodeForRequestParamNotFound = 1001,
	RMFCodeForAPIResponseIsNil = 1002,
	RMFCodeForParsingFailedError = 1003,
	RMFCodeForNetworkUnreachableError = 1004,
	RMFCodeForBaseUrlNotFound = 1005,
	RMFCodeForPortNotFound = 1006,
	RMFCodeForConfigDataNotFound = 1007,
	RMFCodeForSystemManagerNotFound = 1008,
	RMFCodeForActiveControllerNotFound = 1009,
	RMFCodeForActiveParserNotFound = 1010,
	RMFCodeForSystemTypeNotDefined = 1011,
	RMFCodeForLocalizedDescriptionKey = 1013,
	RMFCodeForTenantNameNotFound = 2000,
	RMFCodeForTenantAndVenueNameNotFound = 2001,
	RMFCodeForTenantAndVenueInfoNotFound = 2002,
	RMFCodeForTenantAndVenueNameAndSerialNumberNotFound = 2003,
	RMFCodeForTenantAndVenueNameAndAPInfoNotFound = 2004,
	RMFCodeForTenantAndWlanNameNotFound = 2005,
	RMFCodeForTenantAndWlanInfoNotFound   = 2006,
	RMFCodeForTenantAndMacAddressNotFound   = 2007,
	RMFCodeForAPIndexNotSet = 2008,
	RMFCodeForUserAssociationNotSet = 2009,
	RMFCodeForUserTypeNotSet = 2010,
	RMFCodeForWrongAPFilter = 2011,
	RMFCodeForWlanNameNotFound   = 2012,
	RMFCodeForImageUrlNotFound   = 2013,
	RMFCodeForWrongRootType = 2014,
	RMFCodeForWlanSsidNotFound   = 2015,
	//RMF database specific error
	RMFCodeForDatabaseAPImageUrlNotFound = 2016,
	RMFCodeForDatabaseAPParameterNotFound = 2017,
	RMFCodeForImageAndUrlNotFound = 2018,
	RMFCodeForDatabaseInputParameterNotFound = 2019,
	RMFCodeForDatabaseInstanceNotFound = 2020
};

/*
   Retruns HTTP error code desc based on code
 */
+ (NSString *)getErrorMessageForHTTP:(HTTPCode)code;

/*
   Retruns controller error code desc based on code
 */
+ (NSString *)getErrorMessageForSystem:(HTTPCode)code;

/*
   Retruns domain name for code specified
 */
+ (NSString *)domainForCode:(HTTPCode)code;

@end
