/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFErrorHandler.h"
#import "RMFMacro.h"




@implementation RMFErrorHandler

/*
   Function to return description for HTTP 1XX codes
 */
+ (NSString *)getDescriptionFor1XXCode:(HTTPCode)code {
	NSString *desc = [NSString stringWithFormat:@"%lu status code is unknown", (unsigned long)code];

	switch (code) {
		//HTTP code for depecting informational status codes
		case HTTPCodeFor1XXInformationalUnknown:
			desc = kHTTPCodeFor1XXInformationalUnknownDesc;
			break;

		case HTTPCodeForContinue:
			desc = kHTTPCodeForContinueDesc;
			break;

		case HTTPCodeForSwitchingProtocols:
			desc = kHTTPCodeForSwitchingProtocolsDesc;
			break;

		case HTTPCodeForProcessing:
			desc = kHTTPCodeForProcessingDesc;
			break;

		default:
			break;
	}
	return desc;
}

/*
   Function to return description for HTTP 2XX codes
 */
+ (NSString *)getDescriptionFor2XXCode:(HTTPCode)code {
	NSString *desc = [NSString stringWithFormat:@"%lu status code is unknown", (unsigned long)code];

	switch (code) {
		//HTTP code for depecting success status codes
		case HTTPCodeFor2XXSuccessUnknown:
			desc = kHTTPCodeFor2XXSuccessUnknownDesc;
			break;

		case HTTPCodeForOK:
			desc = kHTTPCodeForOKDesc;
			break;


		case HTTPCodeForCreated:
			desc = kHTTPCodeForCreatedDesc;
			break;


		case HTTPCodeForAccepted:
			desc = kHTTPCodeForAcceptedDesc;
			break;


		case HTTPCodeForNonAuthoritativeInformation:
			desc = kHTTPCodeForNonAuthoritativeInformationDesc;
			break;


		case HTTPCodeForNoContent:
			desc = kHTTPCodeForNoContentDesc;
			break;


		case HTTPCodeForResetContent:
			desc = kHTTPCodeForResetContentDesc;
			break;


		case HTTPCodeForPartialContent:
			desc = kHTTPCodeForPartialContentDesc;
			break;


		case HTTPCodeForMultiStatus:
			desc = kHTTPCodeForMultiStatusDesc;
			break;


		case HTTPCodeForAlreadyReported:
			desc = kHTTPCodeForAlreadyReportedDesc;
			break;

		case HTTPCodeForIMUsed:
			desc = kHTTPCodeForIMUsedDesc;
			break;

		default:
			break;
	}
	return desc;
}

/*
   Function to return description for HTTP 3XX codes
 */
+ (NSString *)getDescriptionFor3XXCode:(HTTPCode)code {
	NSString *desc = [NSString stringWithFormat:@"%lu status code is unknown", (unsigned long)code];
	switch (code) {
		//HTTP code for depecting redirectional status codes
		case HTTPCodeFor3XXSuccessUnknown:
			desc = kHTTPCodeFor3XXSuccessUnknownDesc;
			break;

		case HTTPCodeForMultipleChoices:
			desc = kHTTPCodeForMultipleChoicesDesc;
			break;


		case HTTPCodeForMovedPermanently:
			desc = kHTTPCodeForMovedPermanentlyDesc;
			break;


		case HTTPCodeForFound:
			desc = kHTTPCodeForFoundDesc;
			break;


		case HTTPCodeForSeeOther:
			desc = kHTTPCodeForSeeOtherDesc;
			break;


		case HTTPCodeForNotModified:
			desc = kHTTPCodeForNotModifiedDesc;
			break;


		case HTTPCodeForUseProxy:
			desc = kHTTPCodeForUseProxyDesc;
			break;


		case HTTPCodeForSwitchProxy:
			desc = kHTTPCodeForSwitchProxyDesc;
			break;


		case HTTPCodeForTemporaryRedirect:
			desc = kHTTPCodeForTemporaryRedirectDesc;
			break;


		case HTTPCodeForPermanentRedirect:
			desc = kHTTPCodeForPermanentRedirect;
			break;



		default:
			break;
	}
	return desc;
}

/*
   Function to return description for HTTP 4XX code
 */
+ (NSString *)getDescriptionFor4XXCode:(HTTPCode)code {
	NSString *desc = [NSString stringWithFormat:@"%lu status code is unknown", (unsigned long)code];
	switch (code) {
		//HTTP code for depecting client error status codes
		case HTTPCodeFor4XXSuccessUnknown:
			desc = kHTTPCodeFor4XXSuccessUnknownDesc;
			break;

		case HTTPCodeForBadRequest:
			desc = kHTTPCodeForBadRequestDesc;
			break;

		case HTTPCodeForUnauthorised:
			desc = kHTTPCodeForUnauthorisedDesc;
			break;

		case HTTPCodeForPaymentRequired:
			desc = kHTTPCodeForPaymentRequired;
			break;

		case HTTPCodeForForbidden:
			desc = kHTTPCodeForForbiddenDesc;
			break;

		case HTTPCodeForNotFound:
			desc = kHTTPCodeForNotFoundDesc;
			break;

		case HTTPCodeForMethodNotAllowed:
			desc = kHTTPCodeForMethodNotAllowedDesc;
			break;

		case HTTPCodeForNotAcceptable:
			desc = kHTTPCodeForNotAcceptableDesc;
			break;

		case HTTPCodeForProxyAuthenticationRequired:
			desc = kHTTPCodeForProxyAuthenticationRequiredDesc;
			break;

		case HTTPCodeForRequestTimeout:
			desc = kHTTPCodeForRequestTimeoutDesc;
			break;

		case HTTPCodeForConflict:
			desc = kHTTPCodeForConflictDesc;
			break;

		case HTTPCodeForGone:
			desc = kHTTPCodeForGoneDesc;
			break;

		case HTTPCodeForLengthRequired:
			desc = kHTTPCodeForLengthRequiredDesc;
			break;

		case HTTPCodeForPreconditionFailed:
			desc = kHTTPCodeForPreconditionFailedDesc;
			break;

		case HTTPCodeForRequestEntityTooLarge:
			desc = kHTTPCodeForRequestEntityTooLargeDesc;
			break;

		case HTTPCodeForRequestURITooLong:
			desc = kHTTPCodeForRequestURITooLongDesc;
			break;

		case HTTPCodeForUnsupportedMediaType:
			desc = kHTTPCodeForUnsupportedMediaTypeDesc;
			break;

		case HTTPCodeForRequestedRangeNotSatisfiable:
			desc = kHTTPCodeForRequestedRangeNotSatisfiableDesc;
			break;

		case HTTPCodeForExpectationFailed:
			desc = kHTTPCodeForExpectationFailedDesc;
			break;

		case HTTPCodeForIamATeapot:
			desc = kHTTPCodeForIamATeapotDesc;
			break;

		case HTTPCodeForAuthenticationTimeout:
			desc = kHTTPCodeForAuthenticationTimeoutDesc;
			break;

		case HTTPCodeForMethodFailureSpringFramework:
			desc = kHTTPCodeForMethodFailureSpringFramework;
			break;

		case HTTPCodeForMisdirectedRequest:
			desc = kHTTPCodeForMisdirectedRequestDesc;
			break;

		case HTTPCodeForEnhanceYourCalmTwitter:
			desc = kHTTPCodeForEnhanceYourCalmTwitterDesc;
			break;

		case HTTPCodeForUnprocessableEntity:
			desc = kHTTPCodeForUnprocessableEntityDesc;
			break;

		case HTTPCodeForLocked:
			desc = kHTTPCodeForLockedDesc;
			break;

		case HTTPCodeForFailedDependency:
			desc = kHTTPCodeForFailedDependencyDesc;
			break;

		case HTTPCodeForMethodFailureWebDaw:
			desc = kHTTPCodeForMethodFailureWebDawDesc;
			break;

		case HTTPCodeForUpgradeRequired:
			desc = kHTTPCodeForUpgradeRequiredDesc;
			break;

		case HTTPCodeForPreconditionRequired:
			desc = kHTTPCodeForPreconditionRequiredDesc;
			break;

		case HTTPCodeForTooManyRequests:
			desc = kHTTPCodeForTooManyRequestsDesc;
			break;

		case HTTPCodeForRequestHeaderFieldsTooLarge:
			desc = kHTTPCodeForRequestHeaderFieldsTooLargeDesc;
			break;

		case HTTPCodeForLoginTimeout:
			desc = kHTTPCodeForLoginTimeoutDesc;
			break;


		case HTTPCodeForNoResponseNginx:
			desc = kHTTPCodeForNoResponseNginxDesc;
			break;

		case HTTPCodeForRetryWithMicrosoft:
			desc = kHTTPCodeForRetryWithMicrosoftDesc;
			break;

		case HTTPCodeForBlockedByWindowsParentalControls:
			desc = kHTTPCodeForBlockedByWindowsParentalControlsDesc;
			break;

		case HTTPCodeForRedirectMicrosoft:
			desc = kHTTPCodeForRedirectMicrosoftDesc;
			break;

		case HTTPCodeForUnavailableForLegalReasons:
			desc = kHTTPCodeForUnavailableForLegalReasonsDesc;
			break;

		case HTTPCodeForRequestHeaderTooLargeNginx:
			desc = kHTTPCodeForRequestHeaderTooLargeNginxDesc;
			break;

		case HTTPCodeForCertErrorNginx:
			desc = kHTTPCodeForCertErrorNginxDesc;
			break;

		case HTTPCodeForNoCertNginx:
			desc = kHTTPCodeForNoCertNginxDesc;
			break;

		case HTTPCodeForHTTPToHTTPSNginx:
			desc = kHTTPCodeForHTTPToHTTPSNginxDesc;
			break;

		case HTTPCodeForTokenExpiredOrInvalid:
			desc = kHTTPCodeForTokenExpiredOrInvalidDesc;
			break;

		case HTTPCodeForClientClosedRequestNginx:
			desc = kHTTPCodeForClientClosedRequestNginxDesc;
			break;

		default:
			break;
	}
	return desc;
}

/*
   Function to return description for HTTP 5XX code
 */
+ (NSString *)getDescriptionFor5XXCode:(HTTPCode)code {
	NSString *desc = [NSString stringWithFormat:@"%lu status code is unknown", (unsigned long)code];
	switch (code) {
		//HTTP code for depecting server error status codes
		case HTTPCodeFor5XXSuccessUnknown:
			desc = kHTTPCodeFor5XXSuccessUnknownDesc;
			break;

		case HTTPCodeForInternalServerError:
			desc = kHTTPCodeForInternalServerErrorDesc;
			break;

		case HTTPCodeForNotImplemented:
			desc = kHTTPCodeForNotImplementedDesc;
			break;

		case HTTPCodeForBadGateway:
			desc = kHTTPCodeForBadGatewayDesc;
			break;

		case HTTPCodeForServiceUnavailable:
			desc = kHTTPCodeForServiceUnavailableDesc;
			break;

		case HTTPCodeForGatewayTimeout:
			desc = kHTTPCodeForGatewayTimeoutDesc;
			break;

		case HTTPCodeForHTTPVersionNotSupported:
			desc = kHTTPCodeForHTTPVersionNotSupportedDesc;
			break;

		case HTTPCodeForVariantAlsoNegotiates:
			desc = kHTTPCodeForVariantAlsoNegotiatesDesc;
			break;

		case HTTPCodeForInsufficientStorage:
			desc = kHTTPCodeForInsufficientStorageDesc;
			break;

		case HTTPCodeForLoopDetected:
			desc = kHTTPCodeForLoopDetectedDesc;
			break;

		case HTTPCodeForBandwidthLimitExceeded:
			desc = kHTTPCodeForBandwidthLimitExceededDesc;
			break;

		case HTTPCodeForNotExtended:
			desc = kHTTPCodeForNotExtendedDesc;
			break;

		case HTTPCodeForNetworkAuthenticationRequired:
			desc = kHTTPCodeForNetworkAuthenticationRequiredDesc;
			break;

		case HTTPCodeForConnectionTimedOut:
			desc = kHTTPCodeForConnectionTimedOutDesc;
			break;

		case HTTPCodeForNetworkReadTimeoutErrorUnknown:
			desc = kHTTPCodeForNetworkReadTimeoutErrorUnknownDesc;
			break;

		case HTTPCodeForNetworkConnectTimeoutErrorUnknown:
			desc = kHTTPCodeForNetworkConnectTimeoutErrorUnknownDesc;
			break;

		default:
			break;
	}
	return desc;
}

/*
   Function to return description for KUMO controller error code
 */
+ (NSString *)getDescriptionForKumoErrorCode:(HTTPCode)code {
	NSString *desc = [NSString stringWithFormat:@"%lu status code is unknown", (unsigned long)code];

	switch (code) {
		case RMFCodeForUrlNotFound:
			desc = kKUMOCodeForUrlNotFoundDesc;
			break;

		case RMFCodeForAPIResponseIsNil:
			desc = kKUMOCodeForAPIResponseIsNilDesc;
			break;

		case RMFCodeForParsingFailedError:
			desc = kKUMOCodeForParsingFailedErrorDesc;
			break;

		case RMFCodeForNetworkUnreachableError:
			desc = kKUMOCodeForNetworkUnreachableErrorDesc;
			break;

		case RMFCodeForBaseUrlNotFound:
			desc = kKUMOCodeForBaseUrlNotFoundDesc;
			break;

		case RMFCodeForPortNotFound:
			desc = kKUMOCodeForPortNotFoundDesc;
			break;

		case RMFCodeForConfigDataNotFound:
			desc = kKUMOCodeForConfigDataNotFoundDesc;
			break;

		case RMFCodeForSystemManagerNotFound:
			desc = kKUMOCodeForSystemManagerNotFoundDesc;
			break;

		case RMFCodeForActiveControllerNotFound:
			desc = kKUMOCodeForActiveControllerNotFoundDesc;
			break;

		case RMFCodeForActiveParserNotFound:
			desc = kKUMOCodeForActiveParserNotFoundDesc;
			break;

		case RMFCodeForSystemTypeNotDefined:
			desc = kKUMOCodeForSystemTypeNotDefinedDesc;
			break;

		case RMFCodeForRequestParamNotFound:
			desc = kKUMOCodeForRequestParamNotFoundKeyDesc;
			break;

		case RMFCodeForTenantNameNotFound:
			desc = kKUMOCodeForTenantNameNotFoundDesc;
			break;

		case RMFCodeForTenantAndVenueNameNotFound:
			desc = kKUMOCodeForTenantAndVenueNameNotFoundDesc;
			break;

		case RMFCodeForTenantAndVenueInfoNotFound:
			desc = kKUMOCodeForTenantAndVenueInfoNotFoundDesc;
			break;

		case RMFCodeForTenantAndVenueNameAndSerialNumberNotFound:
			desc = kKUMOCodeForTenantAndVenueNameAndSerialNumberNotFoundDesc;
			break;

		case RMFCodeForTenantAndVenueNameAndAPInfoNotFound:
			desc = kKUMOCodeForTenantAndVenueNameAndAPInfoNotFoundDesc;
			break;

		case RMFCodeForTenantAndWlanNameNotFound:
			desc = kKUMOCodeForTenantAndWlanNameNotFoundDesc;
			break;

		case RMFCodeForTenantAndWlanInfoNotFound:
			desc = kKUMOCodeForTenantAndWlanInfoNotFoundDesc;
			break;

		case RMFCodeForTenantAndMacAddressNotFound:
			desc = kKUMOCodeForTenantAndMacAddressNotFoundDesc;
			break;

		case RMFCodeForAPIndexNotSet:
			desc = kKUMOCodeForAPIndexNotSetDesc;
			break;

		case RMFCodeForUserAssociationNotSet:
			desc = kKUMOCodeForAPIndexNotSetDesc;
			break;

		case RMFCodeForUserTypeNotSet:
			desc = kKUMOCodeForAPIndexNotSetDesc;
			break;

		case RMFCodeForWrongAPFilter:
			desc = kKUMOCodeForWrongAPFiler;
			break;

		case RMFCodeForWlanNameNotFound:
			desc = kKUMOCodeForWlanNameNotFoundDesc;
			break;

		case RMFCodeForImageUrlNotFound:
			desc = kKUMOCodeForImageUrlNotFoundDesc;
			break;

		case RMFCodeForWrongRootType:
			desc = kKUMOCodeForWrongRootType;
			break;

		case RMFCodeForWlanSsidNotFound:
			desc = kKUMOCodeForWlanSsidNotFoundDesc;
			break;

		case RMFCodeForDatabaseAPImageUrlNotFound:
			desc = kKUMOCodeForDatabaseImageUrlNotFound;
			break;

		case RMFCodeForImageAndUrlNotFound:
			desc = kKUMOCodeForImageAndUrlNotFoundDesc;
			break;

		case RMFCodeForDatabaseInputParameterNotFound:
			desc = kKUMOCodeForDatabaseInputParameterNotFound;
			break;

		case RMFCodeForDatabaseInstanceNotFound:
			desc = kKUMOCodeForDatabaseInstanceNotFound;
			break;



		default:
			break;
	}
	return desc;
}

/*
   Function to return description for SCG controller error code
 */
+ (NSString *)getDescriptionForSCGErrorCode:(HTTPCode)code {
	NSString *desc = [NSString stringWithFormat:@"%lu status code is unknown", (unsigned long)code];

	switch (code) {
		case RMFCodeForUrlNotFound:
			desc = kSCGCodeForUrlNotFoundDesc;
			break;

		case RMFCodeForAPIResponseIsNil:
			desc = kSCGCodeForAPIResponseIsNilDesc;
			break;

		case RMFCodeForParsingFailedError:
			desc = kSCGCodeForParsingFailedErrorDesc;
			break;

		case RMFCodeForNetworkUnreachableError:
			desc = kSCGCodeForNetworkUnreachableErrorDesc;
			break;

		case RMFCodeForBaseUrlNotFound:
			desc = kSCGCodeForBaseUrlNotFoundDesc;
			break;

		case RMFCodeForPortNotFound:
			desc = kSCGCodeForPortNotFoundDesc;
			break;

		case RMFCodeForConfigDataNotFound:
			desc = kSCGCodeForConfigDataNotFoundDesc;
			break;

		case RMFCodeForSystemManagerNotFound:
			desc = kSCGCodeForSystemManagerNotFoundDesc;
			break;

		case RMFCodeForActiveControllerNotFound:
			desc = kSCGCodeForActiveControllerNotFoundDesc;
			break;

		case RMFCodeForActiveParserNotFound:
			desc = kSCGCodeForActiveParserNotFoundDesc;
			break;

		case RMFCodeForSystemTypeNotDefined:
			desc = kSCGCodeForSystemTypeNotDefinedDesc;
			break;

		case RMFCodeForRequestParamNotFound:
			desc = kSCGCodeForRequestParamNotFoundKeyDesc;
			break;

		case RMFCodeForTenantNameNotFound:
			desc = kSCGCodeForTenantNameNotFoundDesc;
			break;

		case RMFCodeForTenantAndVenueNameNotFound:
			desc = kSCGCodeForTenantAndVenueNameNotFoundDesc;
			break;

		case RMFCodeForTenantAndVenueInfoNotFound:
			desc = kSCGCodeForTenantAndVenueInfoNotFoundDesc;
			break;

		case RMFCodeForTenantAndVenueNameAndSerialNumberNotFound:
			desc = kSCGCodeForTenantAndVenueNameAndSerialNumberNotFoundDesc;
			break;

		case RMFCodeForTenantAndVenueNameAndAPInfoNotFound:
			desc = kSCGCodeForTenantAndVenueNameAndAPInfoNotFoundDesc;
			break;

		case RMFCodeForTenantAndWlanNameNotFound:
			desc = kSCGCodeForTenantAndWlanNameNotFoundDesc;
			break;

		case RMFCodeForTenantAndWlanInfoNotFound:
			desc = kSCGCodeForTenantAndWlanInfoNotFoundDesc;
			break;

		case RMFCodeForTenantAndMacAddressNotFound:
			desc = kSCGCodeForTenantAndMacAddressNotFoundDesc;
			break;

		case RMFCodeForAPIndexNotSet:
			desc = kSCGCodeForAPIndexNotSetDesc;
			break;

		case RMFCodeForWrongAPFilter:
			desc = kSCGCodeForWrongAPFiler;
			break;

		default:
			break;
	}

	return desc;
}

/*
   Function to return description for ZD controller error code
 */
+ (NSString *)getDescriptionForZDErrorCode:(HTTPCode)code {
	NSString *desc = [NSString stringWithFormat:@"%lu status code is unknown", (unsigned long)code];

	switch (code) {
		case RMFCodeForUrlNotFound:
			desc = kZDCodeForUrlNotFoundDesc;
			break;

		case RMFCodeForAPIResponseIsNil:
			desc = kZDCodeForAPIResponseIsNilDesc;
			break;

		case RMFCodeForParsingFailedError:
			desc = kZDCodeForParsingFailedErrorDesc;
			break;

		case RMFCodeForNetworkUnreachableError:
			desc = kZDCodeForNetworkUnreachableErrorDesc;
			break;

		case RMFCodeForBaseUrlNotFound:
			desc = kZDCodeForBaseUrlNotFoundDesc;
			break;

		case RMFCodeForPortNotFound:
			desc = kZDCodeForPortNotFoundDesc;
			break;

		case RMFCodeForConfigDataNotFound:
			desc = kZDCodeForConfigDataNotFoundDesc;
			break;

		case RMFCodeForSystemManagerNotFound:
			desc = kZDCodeForSystemManagerNotFoundDesc;
			break;

		case RMFCodeForActiveControllerNotFound:
			desc = kZDCodeForActiveControllerNotFoundDesc;
			break;

		case RMFCodeForActiveParserNotFound:
			desc = kZDCodeForActiveParserNotFoundDesc;
			break;

		case RMFCodeForSystemTypeNotDefined:
			desc = kZDCodeForSystemTypeNotDefinedDesc;
			break;

		case RMFCodeForRequestParamNotFound:
			desc = kZDCodeForRequestParamNotFoundKeyDesc;
			break;

		case RMFCodeForTenantNameNotFound:
			desc = kZDCodeForTenantNameNotFoundDesc;
			break;

		case RMFCodeForTenantAndVenueNameNotFound:
			desc = kZDCodeForTenantAndVenueNameNotFoundDesc;
			break;

		case RMFCodeForTenantAndVenueInfoNotFound:
			desc = kZDCodeForTenantAndVenueInfoNotFoundDesc;
			break;

		case RMFCodeForTenantAndVenueNameAndSerialNumberNotFound:
			desc = kZDCodeForTenantAndVenueNameAndSerialNumberNotFoundDesc;
			break;

		case RMFCodeForTenantAndVenueNameAndAPInfoNotFound:
			desc = kZDCodeForTenantAndVenueNameAndAPInfoNotFoundDesc;
			break;

		case RMFCodeForTenantAndWlanNameNotFound:
			desc = kZDCodeForTenantAndWlanNameNotFoundDesc;
			break;

		case RMFCodeForTenantAndWlanInfoNotFound:
			desc = kZDCodeForTenantAndWlanInfoNotFoundDesc;
			break;

		case RMFCodeForTenantAndMacAddressNotFound:
			desc = kZDCodeForTenantAndMacAddressNotFoundDesc;
			break;

		case RMFCodeForAPIndexNotSet:
			desc = kZDCodeForAPIndexNotSetDesc;
			break;

		case RMFCodeForWrongAPFilter:
			desc = kZDCodeForWrongAPFiler;
			break;

		default:
			break;
	}

	return desc;
}

/*
   Retruns HTTP error code desc based on code
 */
+ (NSString *)getErrorMessageForHTTP:(HTTPCode)code {
	NSString *desc = [NSString stringWithFormat:@"%lu status code is unknown", (unsigned long)code];

	if (BETWEEN(code, HTTPCodeForContinue, HTTPCodeForProcessing)) {
		desc = [self getDescriptionFor1XXCode:code];
	}

	else if (BETWEEN(code, HTTPCodeForOK, HTTPCodeForIMUsed)) {
		desc = [self getDescriptionFor2XXCode:code];
	}

	else if (BETWEEN(code, HTTPCodeForMultipleChoices, HTTPCodeForPermanentRedirect)) {
		desc = [self getDescriptionFor3XXCode:code];
	}

	else if (BETWEEN(code, HTTPCodeForBadRequest, HTTPCodeForClientClosedRequestNginx)) {
		desc = [self getDescriptionFor4XXCode:code];
	}

	else if (BETWEEN(code, HTTPCodeForInternalServerError, HTTPCodeForNetworkConnectTimeoutErrorUnknown)) {
		desc = [self getDescriptionFor5XXCode:code];
	}

	return desc;
}

/*
   Retruns controller error code desc based on code
 */
+ (NSString *)getErrorMessageForSystem:(HTTPCode)code {
	NSString *desc = [NSString stringWithFormat:@"%lu status code is unknown", (unsigned long)code];
	RMFConfiguration *config = [RMFConfiguration sharedRMFConfiguration];
	switch (config.system) {
		case 1: //for KUMO
			desc = [self getDescriptionForKumoErrorCode:code];
			break;

		case 2: //for SCG
			desc = [self getDescriptionForSCGErrorCode:code];
			break;

		case 3: //for ZD
			desc = [self getDescriptionForZDErrorCode:code];
			break;

		default:
			break;
	}

	return desc;
}

/*
   Retruns domain name for code specified
 */

+ (NSString *)domainForCode:(HTTPCode)code {
	NSString *domain = kErrorDomainName;
	switch (code) {
		// TODO: more error domain will be added in future. Currently a single error domain is used
		default: domain = kErrorDomainApplication;
			break;
	}
	return domain;
}

@end
