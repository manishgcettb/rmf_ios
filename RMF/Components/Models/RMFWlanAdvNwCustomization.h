/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <Foundation/Foundation.h>

@interface RMFWlanAdvNwCustomization : NSObject

// Stores and returns maximum client count on a Wlan per radio
@property (nonatomic, assign) NSInteger maxClientsOnWlanPerRadio;

// Stores and returns the band balancing status
@property (nonatomic, assign) BOOL enableBandBalancing;

@end
