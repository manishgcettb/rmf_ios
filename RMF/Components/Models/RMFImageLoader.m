/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFImageLoader.h"
#import "RMFMacro.h"
#import "RMFSystemManager.h"
#import "RMFUtils.h"
#import "RMFErrorHandler.h"

@interface RMFImageLoader ()

// Store and returns NSCache instance
@property (nonatomic, strong) NSCache *imageCache;

// Stores RMFSystemManager instance
@property (nonatomic, strong) RMFSystemManager *systemManager;

@end

@implementation RMFImageLoader

static RMFImageLoader *sharedRMFImageLoader = nil;

/*
 * Creates a singleton instance of RMFImageLoader
 */

+ (RMFImageLoader *)sharedRMFImageLoader {
	if (!OBJ_NOT_NIL(sharedRMFImageLoader)) {
		sharedRMFImageLoader = [[RMFImageLoader alloc] init];
	}
	return sharedRMFImageLoader;
}

/*
 * Initialize the singleton instance of RMFImageLoader
 */

- (id)init {
	if (!OBJ_NOT_NIL(sharedRMFImageLoader)) {
		sharedRMFImageLoader = [super init];

		// RMFImageLoader instance will be created successfully only when the system manager is configured properly
		if (sharedRMFImageLoader && [RMFUtils isSystemManagerConfigured:[RMFSystemManager sharedRMFSystemManager]]) {
			self.systemManager = [RMFSystemManager sharedRMFSystemManager];
		}

		// Allocate memory for image caching, set count and total cost limit
		self.imageCache = [[NSCache alloc] init];
		[self.imageCache setCountLimit:kCacheCountLimit];
		[self.imageCache setTotalCostLimit:kCacheTotalCostLimit];
	}
	return sharedRMFImageLoader;
}

/*
 * Requests network controller to download image
 */

- (NSData *)downloadImage:(NSString *)imageUrl response:(void (^)(NSError *error, NSData *image))completion {
	NSError *error = nil;
	__block NSData *image = nil;

	if (imageUrl) {
		image = [self.imageCache objectForKey:imageUrl];

		// Image is not available in the cache, make network request to fetch
		[self.systemManager.activeController downloadImage:imageUrl response: ^(NSError *error, id data) {
		    if (completion) {
		        if (data) {
		            RMF_LOG_VERBOSE([self class], NSStringFromSelector(_cmd), IMAGE_DOWNLOAD_SUCCESS(kEmptyText));

		            // Store image in the cache
		            [self.imageCache setObject:data forKey:imageUrl];
		            completion(nil, data);
				}
		        else {
		            completion(error, nil);
				}
			}
		}];
	}
	else {
		if (completion) {
			RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), IMAGE_DOWNLOAD_FAILURE(kEmptyText, error.description));
			error = [RMFUtils errorWithCode:RMFCodeForImageUrlNotFound];
			completion(error, nil);
		}
	}
	return image;
}

/*
 * Requests network controller to upload image for an access point
 */

- (void)uploadImage:(NSData *)image imageUrl:(NSString *)imageUrl response:(void (^)(NSError *error, id data))completion {
	NSError *error = nil;

	if (image && imageUrl) {
		// Call network controller to upload image
		[self.systemManager.activeController uploadImage:image imageUrl:(NSString *)imageUrl response: ^(NSError *error, id data) {
		    if (data) {
		        // TODO: implement parsing once image upload response is known

		        if (completion) {
		            completion(nil, data);
		            RMF_LOG_VERBOSE([self class], NSStringFromSelector(_cmd), IMAGE_UPLOAD_SUCCESS(kEmptyText));
				}
			}
		    else {
		        if (completion) {
		            completion(error, nil);
		            RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), IMAGE_UPLOAD_FAILURE(kEmptyText, error.description));
				}
			}
		}];
	}
	else {
		if (completion) {
			RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), IMAGE_UPLOAD_FAILURE(kEmptyText, error.description));
			error = [RMFUtils errorWithCode:RMFCodeForImageAndUrlNotFound];
			completion(error, nil);
		}
	}
}

/*
 * Clears image cache
 */

- (void)clearImageCache {
	[self.imageCache removeAllObjects];
}

@end
