/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <Foundation/Foundation.h>

@interface RMFEvent : NSObject

//Stores and returns event generation time
@property (nonatomic, strong) NSString *time;

//Stores and returns type of event
@property (nonatomic, strong) NSString *type;

// Stores and returns severity
@property (nonatomic, strong) NSString *severity;

// Stores and returns description of event
@property (nonatomic, strong) NSString *eventDescription;

// Stores and returns event id
@property (nonatomic, assign) NSInteger eventTypeId;

@end
