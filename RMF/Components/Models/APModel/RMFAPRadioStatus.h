/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <Foundation/Foundation.h>

@interface RMFAPRadioStatus : NSObject

// define tx Power of an AP
@property (nonatomic, strong) NSString *txPower;

// define channel of an AP
@property (nonatomic, assign) int channel;

// define channel width of an AP
@property (nonatomic, assign) int channelWidth;

// define number of wlans in an AP
@property (nonatomic, assign) int wlanCount;

// define mode of an AP
@property (nonatomic, strong) NSString *mode;


@end
