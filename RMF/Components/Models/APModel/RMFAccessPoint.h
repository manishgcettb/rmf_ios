/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <Foundation/Foundation.h>
#import "RMFAPWlanStatus.h"
#import "RMFAPExtendedStatus.h"
#import "RMFAPRadioStatus.h"
#import "RMFAPStatus.h"


@interface RMFAccessPoint : NSObject

// define serial number of an AP
@property (nonatomic, strong) NSString *serialNumber;

// define ssid  of an AP
@property (nonatomic, strong) NSString *ssid;

// define wlans status of an AP
@property (nonatomic, strong) RMFAPWlanStatus *rmfAPWlanStatus;

// define extended status of an AP
@property (nonatomic, strong) RMFAPExtendedStatus *rmfAPExtendedStatus;

// define radio status of an AP
@property (nonatomic, strong) RMFAPRadioStatus *rmfAPRadioStatus;

// define status of an AP
@property (nonatomic, strong) RMFAPStatus *rmfAPStatus;

// define all root association with -> separated
@property (nonatomic, strong) NSString *rootAssociation;

// define image url  of an AP
@property (nonatomic, strong) NSString *imageUrl;

@end
