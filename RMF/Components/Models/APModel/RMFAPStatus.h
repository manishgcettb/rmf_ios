/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <Foundation/Foundation.h>

@interface RMFAPStatus : NSObject

// define model of an AP
@property (nonatomic, strong) NSString *model;

// define mac address of an AP
@property (nonatomic, strong) NSString *macAddress;

// define registration status of an AP
@property (nonatomic, strong) NSString *registrationStatus;

// define connection status of an AP
@property (nonatomic, strong) NSString *connectionStatus;

// define config status of an AP
@property (nonatomic, strong) NSString *configStatus;

// define last seen By Vscg
@property (nonatomic, strong) NSString *lastSeenByVscg;
@property (nonatomic, strong) NSString *swver;

// define location of an AP
@property (nonatomic, strong) NSString *location;

@end
