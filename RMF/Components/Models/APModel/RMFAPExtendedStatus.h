/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <Foundation/Foundation.h>

@interface RMFAPExtendedStatus : NSObject

// define internal IP of an AP
@property (nonatomic, strong) NSString *internalIp;

// define external IP of an AP
@property (nonatomic, strong) NSString *externalIp;

// define hops of an AP
@property (nonatomic, strong) NSString *hops;

// define tunnel type of an AP
@property (nonatomic, strong) NSString *tunnelType;

// define name of an AP
@property (nonatomic, strong) NSString *name;

// define uptime of an AP
@property (nonatomic, assign) long uptime;


@end
