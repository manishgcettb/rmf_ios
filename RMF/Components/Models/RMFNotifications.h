/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface RMFNotifications : NSManagedObject

@property (nonatomic, retain) NSString * apMacAddress;
@property (nonatomic, retain) NSString * message;
@property (nonatomic, retain) NSDate * notificationTime;
@property (nonatomic, retain) NSNumber * readStatus;
@property (nonatomic, retain) NSString * type;

@end
