/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */


#import <Foundation/Foundation.h>

@interface RMFImageLoader : NSObject

// Returns the singleton instance of RMFAPPlugin
+ (RMFImageLoader *)sharedRMFImageLoader;

// Requests network controller to download image from the server
- (NSData *)downloadImage:(NSString *)imageUrl response:(void (^)(NSError *error, NSData *image))completion;

// Requests network controller to upload image
- (void)uploadImage:(NSData *)image imageUrl:(NSString *)imageUrl response:(void (^)(NSError *error, id data))completion;

// Clears image cache
- (void)clearImageCache;

@end
