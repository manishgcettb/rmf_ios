/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <Foundation/Foundation.h>

@interface RMFSummaryStatistics : NSObject

// Stores and returns wlan count
@property (nonatomic, assign) NSInteger wlanCount;

// Stores and returns APs count
@property (nonatomic, assign) NSInteger apCount;

// Stores and returns total alarm count
@property (nonatomic, assign) NSInteger totalAlarmCount;

// Stores and returns critical alarm count
@property (nonatomic, assign) NSInteger criticalAlarmCount;

// Stores and returns major alarm count
@property (nonatomic, assign) NSInteger majorAlarmCount;

// Stores and returns minor alarm count
@property (nonatomic, assign) NSInteger minorAlarmCount;

// Stores and returns client count
@property (nonatomic, assign) NSInteger clientCount;

@end
