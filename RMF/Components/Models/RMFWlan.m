/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFWlan.h"

@implementation RMFWlan

- (void)encodeWithCoder:(NSCoder *)coder {
	[coder encodeObject:self.bssid forKey:@"bssid"];
}

- (id)initWithCoder:(NSCoder *)coder {
	self = [super init];
	if (self != nil) {
		self.bssid = [coder decodeObjectForKey:@"bssid"];
	}
	return self;
}

@end
