/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <Foundation/Foundation.h>
#import "RMFWlanAdvNwCustomization.h"
#import "RMFWlanBasicNwCustomization.h"

@interface RMFWlanService : NSObject

// Stores and returns Wlan name
@property (nonatomic, copy) NSString *name;

// Stroes and returns Wlan ssid information
@property (nonatomic, copy) NSString *ssid;

// Store and returns Wlan type
@property (nonatomic, copy) NSString *wlanType;

// Stores and returns Wlan security type
@property (nonatomic, copy) NSString *wlanSecurity;

// Stores and returns Wlan passphrase
@property (nonatomic, copy) NSString *pskPassPhrase;

// Stores and returns Wlan advanced customization information
@property (nonatomic, strong) RMFWlanAdvNwCustomization *wlanAdvCustmization;

// Stores and returns Wlan basic network customi  customization information
@property (nonatomic, strong) RMFWlanBasicNwCustomization *wlanBasicNwCustomization;

@end
