/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */


#import <Foundation/Foundation.h>

@interface RMFClientDetail : NSObject

// Stores and returns auth method
@property (nonatomic, copy) NSString *authMethod;

// Stroes and returns connection duration
@property (nonatomic, copy) NSNumber * connectionDuration;

// Store and returns ip
@property (nonatomic, copy) NSString *ip;

// Stores and returns mac address
@property (nonatomic, copy) NSString *macAddress;

// Stores and returns os type
@property (nonatomic, copy) NSString *osType;

// Stores and returns status
@property (nonatomic, copy) NSString *status;

// Stores and returns Wlan
@property (nonatomic, copy) NSString *wlan;

@end
