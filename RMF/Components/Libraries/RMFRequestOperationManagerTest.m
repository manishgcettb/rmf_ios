/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFRequestOperationManager.h"

#define REQUEST_TIMEOUT 50
#define REQUEST_URL     @"www.ruckuswireless.com"

@interface RMFRequestOperationManagerTest : XCTestCase

// Stores and returns the instance of RMFRequestOperationManager
@property (nonatomic, strong) RMFRequestOperationManager *requestOperationManager;

@end

@implementation RMFRequestOperationManagerTest

- (void)setUp {
	[super setUp];

	// Create the instance of RMFRequestOperationManager
	self.requestOperationManager = [[RMFRequestOperationManager alloc] init];
	XCTAssertNotNil(self.requestOperationManager);
}

- (void)tearDown {
	[super tearDown];
}

/*
 * Checks whether the RMFRequestOperationManager instance is avaialable or not
 */
- (void)testRMFRequestOperationManagerinstanceScenario1 {
	XCTAssertNotNil(self.requestOperationManager);
}

/*
 * GET method should throw NSInternalInconsistencyException If called without the request
 * URL, parameters and the response block
 */
- (void)testGETScenario1 {
	XCTAssertThrowsSpecificNamed([self.requestOperationManager get:nil withParameters:nil andResponse:nil], NSException, NSInternalInconsistencyException);
}

/*
 * GET method should not throw any exception If called with the request URL and without the
 * request parameters and the response block
 */
- (void)testGETScenario2 {
	XCTAssertNoThrow([self.requestOperationManager get:REQUEST_URL withParameters:nil andResponse:nil]);
}

/*
 * GET method should throw NSInternalInconsistencyException exception If called with the
 * request parameters and without the request URL and the response block
 */
- (void)testGETScenario3 {
	NSDictionary *loginData = @{
		@"userName":@"admin",
		@"password":@"password"
	};

	XCTAssertThrowsSpecificNamed([self.requestOperationManager get:nil withParameters:loginData andResponse:nil], NSException, NSInternalInconsistencyException);
}

/*
 * GET method should throw NSInternalInconsistencyException exception If called with
 * the response block  and without the request URL and the request parameters
 */
- (void)testGETScenario4 {
	XCTAssertThrowsSpecificNamed([self.requestOperationManager get:nil withParameters:nil andResponse: ^(NSError *error, id data) {
	}], NSException, NSInternalInconsistencyException);
}

/*
 * GET method should not throw any exception If called with the request parameters and
 * the request URL and without the response block
 */
- (void)testGETScenario5 {
	NSDictionary *loginData = @{
		@"userName":@"admin",
		@"password":@"password"
	};

	XCTAssertNoThrow([self.requestOperationManager get:REQUEST_URL withParameters:loginData andResponse:nil]);
}

/*
 * GET method should return network response If called with the response block  and the
 * request URL and without the request parameters
 */
- (void)testGETScenario6 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:@"Network response should be received"];

	XCTAssertNoThrow([self.requestOperationManager get:REQUEST_URL withParameters:nil andResponse: ^(NSError *error, id data) {
	    [expectation fulfill];
	}]);

	[self waitForExpectationsWithTimeout:REQUEST_TIMEOUT handler: ^(NSError *error) {
	    XCTAssertNil(error);
	}];
}

/*
 * GET method should return the network response If called with the response block,
 * request URL and the request parameters
 */
- (void)testGETScenario7 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:@"Network response should be received"];

	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:@"tenant_name"];

	[self.requestOperationManager get:REQUEST_URL withParameters:requestParameters andResponse: ^(NSError *error, id data) {
	    [expectation fulfill];
	}];
	[self waitForExpectationsWithTimeout:REQUEST_TIMEOUT handler: ^(NSError *error) {
	    XCTAssertNil(error);
	}];
}

/*
 * POST method should throw NSInternalInconsistencyException If called without the request
 * URL, parameters and the response block
 */
- (void)testPOSTScenario1 {
	XCTAssertThrowsSpecificNamed([self.requestOperationManager post:nil withParameters:nil andResponse:nil], NSException, NSInternalInconsistencyException);
}

/*
 * POST method should not throw any exception If called with the request URL and without the
 * request parameters and the response block
 */
- (void)testPOSTScenario2 {
	XCTAssertNoThrow([self.requestOperationManager post:REQUEST_URL withParameters:nil andResponse:nil]);
}

/*
 * POST method should throw NSInternalInconsistencyException exception If called with the
 * request parameters and without the request URL and the response block
 */
- (void)testPOSTScenario3 {
	NSDictionary *loginData = @{
		@"userName":@"admin",
		@"password":@"password"
	};

	XCTAssertThrowsSpecificNamed([self.requestOperationManager post:nil withParameters:loginData andResponse:nil], NSException, NSInternalInconsistencyException);
}

/*
 * POST method should throw NSInternalInconsistencyException exception If called with
 * the response block  and without the request URL and the request parameters
 */
- (void)testPOSTScenario4 {
	XCTAssertThrowsSpecificNamed([self.requestOperationManager post:nil withParameters:nil andResponse: ^(NSError *error, id data) {
	}], NSException, NSInternalInconsistencyException);
}

/*
 * POST method should not throw any exception If called with the request parameters and
 * the request URL and without the response block
 */
- (void)testPOSTScenario5 {
	NSDictionary *loginData = @{
		@"userName":@"admin",
		@"password":@"password"
	};

	XCTAssertNoThrow([self.requestOperationManager post:REQUEST_URL withParameters:loginData andResponse:nil]);
}

/*
 * POST method should return network response If called with the response block  and the
 * request URL and without the request parameters
 */
- (void)testPOSTScenario6 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:@"Network response should be received"];

	XCTAssertNoThrow([self.requestOperationManager post:REQUEST_URL withParameters:nil andResponse: ^(NSError *error, id data) {
	    [expectation fulfill];
	}]);

	[self waitForExpectationsWithTimeout:REQUEST_TIMEOUT handler: ^(NSError *error) {
	    XCTAssertNil(error);
	}];
}

/*
 * POST method should return the network response If called with the response block,
 * request URL and the request parameters
 */
- (void)testPOSTScenario7 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:@"Network response should be received"];

	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:@"tenant_name"];

	[self.requestOperationManager post:REQUEST_URL withParameters:requestParameters andResponse: ^(NSError *error, id data) {
	    [expectation fulfill];
	}];
	[self waitForExpectationsWithTimeout:REQUEST_TIMEOUT handler: ^(NSError *error) {
	    XCTAssertNil(error);
	}];
}

/*
 * PUT method should throw NSInternalInconsistencyException If called without the request
 * URL, parameters and the response block
 */
- (void)testPUTScenario1 {
	XCTAssertThrowsSpecificNamed([self.requestOperationManager put:nil withParameters:nil andResponse:nil], NSException, NSInternalInconsistencyException);
}

/*
 * PUT method should not throw any exception If called with the request URL and without the
 * request parameters and the response block
 */
- (void)testPUTScenario2 {
	XCTAssertNoThrow([self.requestOperationManager put:REQUEST_URL withParameters:nil andResponse:nil]);
}

/*
 * PUT method should throw NSInternalInconsistencyException exception If called with the
 * request parameters and without the request URL and the response block
 */
- (void)testPUTScenario3 {
	NSDictionary *loginData = @{
		@"userName":@"admin",
		@"password":@"password"
	};

	XCTAssertThrowsSpecificNamed([self.requestOperationManager put:nil withParameters:loginData andResponse:nil], NSException, NSInternalInconsistencyException);
}

/*
 * PUT method should throw NSInternalInconsistencyException exception If called with
 * the response block  and without the request URL and the request parameters
 */
- (void)testPUTScenario4 {
	XCTAssertThrowsSpecificNamed([self.requestOperationManager put:nil withParameters:nil andResponse: ^(NSError *error, id data) {
	}], NSException, NSInternalInconsistencyException);
}

/*
 * PUT method should not throw any exception If called with the request parameters and
 * the request URL and without the response block
 */
- (void)testPUTScenario5 {
	NSDictionary *loginData = @{
		@"userName":@"admin",
		@"password":@"password"
	};

	XCTAssertNoThrow([self.requestOperationManager put:REQUEST_URL withParameters:loginData andResponse:nil]);
}

/*
 * PUT method should return network response If called with the response block  and the
 * request URL and without the request parameters
 */
- (void)testPUTScenario6 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:@"Network response should be received"];

	XCTAssertNoThrow([self.requestOperationManager put:REQUEST_URL withParameters:nil andResponse: ^(NSError *error, id data) {
	    [expectation fulfill];
	}]);

	[self waitForExpectationsWithTimeout:REQUEST_TIMEOUT handler: ^(NSError *error) {
	    XCTAssertNil(error);
	}];
}

/*
 * PUT method should return the network response If called with the response block,
 * request URL and the request parameters
 */
- (void)testPUTScenario7 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:@"Network response should be received"];

	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:@"tenant_name"];

	[self.requestOperationManager put:REQUEST_URL withParameters:requestParameters andResponse: ^(NSError *error, id data) {
	    [expectation fulfill];
	}];
	[self waitForExpectationsWithTimeout:REQUEST_TIMEOUT handler: ^(NSError *error) {
	    XCTAssertNil(error);
	}];
}

/*
 * PATCH method should throw NSInternalInconsistencyException If called without the request
 * URL, parameters and the response block
 */
- (void)testPATCHScenario1 {
	XCTAssertThrowsSpecificNamed([self.requestOperationManager patch:nil withParameters:nil andResponse:nil], NSException, NSInternalInconsistencyException);
}

/*
 * PATCH method should not throw any exception If called with the request URL and without the
 * request parameters and the response block
 */
- (void)testPATCHScenario2 {
	XCTAssertNoThrow([self.requestOperationManager patch:REQUEST_URL withParameters:nil andResponse:nil]);
}

/*
 * PATCH method should throw NSInternalInconsistencyException exception If called with the
 * request parameters and without the request URL and the response block
 */
- (void)testPATCHScenario3 {
	NSDictionary *loginData = @{
		@"userName":@"admin",
		@"password":@"password"
	};

	XCTAssertThrowsSpecificNamed([self.requestOperationManager patch:nil withParameters:loginData andResponse:nil], NSException, NSInternalInconsistencyException);
}

/*
 * PATCH method should throw NSInternalInconsistencyException exception If called with
 * the response block  and without the request URL and the request parameters
 */
- (void)testPATCHScenario4 {
	XCTAssertThrowsSpecificNamed([self.requestOperationManager patch:nil withParameters:nil andResponse: ^(NSError *error, id data) {
	}], NSException, NSInternalInconsistencyException);
}

/*
 * PATCH method should not throw any exception If called with the request parameters and
 * the request URL and without the response block
 */
- (void)testPATCHScenario5 {
	NSDictionary *loginData = @{
		@"userName":@"admin",
		@"password":@"password"
	};

	XCTAssertNoThrow([self.requestOperationManager patch:REQUEST_URL withParameters:loginData andResponse:nil]);
}

/*
 * PATCH method should return network response If called with the response block  and the
 * request URL and without the request parameters
 */
- (void)testPATCHScenario6 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:@"Network response should be received"];

	XCTAssertNoThrow([self.requestOperationManager patch:REQUEST_URL withParameters:nil andResponse: ^(NSError *error, id data) {
	    [expectation fulfill];
	}]);

	[self waitForExpectationsWithTimeout:REQUEST_TIMEOUT handler: ^(NSError *error) {
	    XCTAssertNil(error);
	}];
}

/*
 * PATCH method should return the network response If called with the response block,
 * request URL and the request parameters
 */
- (void)testPATCHScenario7 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:@"Network response should be received"];

	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:@"tenant_name"];

	[self.requestOperationManager patch:REQUEST_URL withParameters:requestParameters andResponse: ^(NSError *error, id data) {
	    [expectation fulfill];
	}];
	[self waitForExpectationsWithTimeout:REQUEST_TIMEOUT handler: ^(NSError *error) {
	    XCTAssertNil(error);
	}];
}

/*
 * DELETE method should throw NSInternalInconsistencyException If called without the request
 * URL, parameters and the response block
 */
- (void)testDELETEScenario1 {
	XCTAssertThrowsSpecificNamed([self.requestOperationManager deleteRequest:nil withParameters:nil andResponse:nil], NSException, NSInternalInconsistencyException);
}

/*
 * DELETE method should not throw any exception If called with the request URL and without
 * the request parameters and the response block
 */
- (void)testDELETEScenario2 {
	XCTAssertNoThrow([self.requestOperationManager deleteRequest:REQUEST_URL withParameters:nil andResponse:nil]);
}

/*
 * DELETE method should throw NSInternalInconsistencyException exception If called with the
 * request parameters and without the request URL and the response block
 */
- (void)testDELETEScenario3 {
	NSDictionary *loginData = @{
		@"userName":@"admin",
		@"password":@"password"
	};

	XCTAssertThrowsSpecificNamed([self.requestOperationManager deleteRequest:nil withParameters:loginData andResponse:nil], NSException, NSInternalInconsistencyException);
}

/*
 * DELETE method should throw NSInternalInconsistencyException exception If called with
 * the response block  and without the request URL and the request parameters
 */
- (void)testDELETEScenario4 {
	XCTAssertThrowsSpecificNamed([self.requestOperationManager deleteRequest:nil withParameters:nil andResponse: ^(NSError *error, id data) {
	}], NSException, NSInternalInconsistencyException);
}

/*
 * DELETE method should not throw any exception If called with the request parameters and
 * the request URL and without the response block
 */
- (void)testDELETEScenario5 {
	NSDictionary *loginData = @{
		@"userName":@"admin",
		@"password":@"password"
	};

	XCTAssertNoThrow([self.requestOperationManager deleteRequest:REQUEST_URL withParameters:loginData andResponse:nil]);
}

/*
 * DELETE method should return network response If called with the response block  and the
 * request URL and without the request parameters
 */
- (void)testDELETEScenario6 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:@"Network response should be received"];

	XCTAssertNoThrow([self.requestOperationManager deleteRequest:REQUEST_URL withParameters:nil andResponse: ^(NSError *error, id data) {
	    [expectation fulfill];
	}]);

	[self waitForExpectationsWithTimeout:REQUEST_TIMEOUT handler: ^(NSError *error) {
	    XCTAssertNil(error);
	}];
}

/*
 * DELETE method should return the network response If called with the response block,
 * request URL and the request parameters
 */
- (void)testDELETEScenario7 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:@"Network response should be received"];

	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:@"tenant_name"];

	[self.requestOperationManager deleteRequest:REQUEST_URL withParameters:requestParameters andResponse: ^(NSError *error, id data) {
	    [expectation fulfill];
	}];
	[self waitForExpectationsWithTimeout:REQUEST_TIMEOUT handler: ^(NSError *error) {
	    XCTAssertNil(error);
	}];
}

@end
