/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFMacro.h"
#import "RMFUtils.h"
#import "RMFRequestOperationManager.h"

@implementation RMFRequestOperationManager

/*
 * Makes HTTP GET request with request parameters and returns error/data in the response block
 */

- (void)get:(NSString *)url withParameters:(id)params andResponse:(void (^)(NSError *error, id data))completion {
    if (OBJ_NOT_NIL(url)) {
        [self GET:url parameters:params success: ^(AFHTTPRequestOperation *operation, id responseObject) {
            if (completion) {
                completion(nil, responseObject);
                RMF_LOG_VERBOSE([self class], NSStringFromSelector(_cmd), RESPONSE_SUCCESSFUL);
            }
        } failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
            if (completion) {
                completion(error, nil);
                NSString *logDescription = [NSString stringWithFormat: @"%@ %@",ERROR, error.description];
                RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
            }
        }];
    }
    else {
        [RMFUtils raiseException:NSInternalInconsistencyException WithDescription:kUrlNotFound];
    }
}

/*
 * Makes HTTP POST request with request parameters and returns error/data in the response block
 */
- (void)post:(NSString *)url withParameters:(id)params andResponse:(void (^)(NSError *error, id data))completion {
    if (OBJ_NOT_NIL(url)) {
        [self POST:url parameters:params success: ^(AFHTTPRequestOperation *operation, id responseObject) {
            if (completion) {
                completion(nil, responseObject);
                RMF_LOG_VERBOSE([self class], NSStringFromSelector(_cmd), RESPONSE_SUCCESSFUL);
            }
        } failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
            if (completion) {
                completion(error, nil);
                
                NSString *logDescription = [NSString stringWithFormat: @"%@ %@",ERROR,error.description];
                RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
            }
        }];
    }
    else {
        [RMFUtils raiseException:NSInternalInconsistencyException WithDescription:kUrlNotFound];
    }
}

/*
 * Makes HTTP PUT request with request parameters and returns error/data in the response block
 */
- (void)put:(NSString *)url withParameters:(id)params andResponse:(void (^)(NSError *error, id data))completion {
    if (OBJ_NOT_NIL(url)) {
        [self PUT:url parameters:params success: ^(AFHTTPRequestOperation *operation, id responseObject) {
            if (completion) {
                completion(nil, responseObject);
                RMF_LOG_VERBOSE([self class], NSStringFromSelector(_cmd), RESPONSE_SUCCESSFUL);
                
            }
        } failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
            if (completion) {
                completion(error, nil);
                
                NSString *logDescription = [NSString stringWithFormat: @"%@ %@",ERROR,error.description];
                RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
            }
        }];
    }
    else {
        [RMFUtils raiseException:NSInternalInconsistencyException WithDescription:kUrlNotFound];
    }
}

/*
 * Makes HTTP PATCH request with request parameters and returns error/data in the response block
 */
- (void)patch:(NSString *)url withParameters:(id)params andResponse:(void (^)(NSError *error, id data))completion {
    if (OBJ_NOT_NIL(url)) {
        [self PATCH:url parameters:params success: ^(AFHTTPRequestOperation *operation, id responseObject) {
            if (completion) {
                completion(nil, responseObject);
                RMF_LOG_VERBOSE([self class], NSStringFromSelector(_cmd), RESPONSE_SUCCESSFUL);
                
            }
        } failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
            if (completion) {
                completion(error, nil);
                
                NSString *logDescription = [NSString stringWithFormat: @"%@ %@",ERROR, error.description];
                RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
            }
        }];
    }
    else {
        [RMFUtils raiseException:NSInternalInconsistencyException WithDescription:kUrlNotFound];
    }
}

/*
 * Makes HTTP DELETE request with request parameters and returns error/data in the response block
 */

- (void)deleteRequest:(NSString *)url withParameters:(id)params andResponse:(void (^)(NSError *error, id data))completion {
    if (OBJ_NOT_NIL(url)) {
        [self DELETE:url parameters:params success: ^(AFHTTPRequestOperation *operation, id responseObject) {
            if (completion) {
                completion(nil, responseObject);
                RMF_LOG_VERBOSE([self class], NSStringFromSelector(_cmd), RESPONSE_SUCCESSFUL);
            }
        } failure: ^(AFHTTPRequestOperation *operation, NSError *error) {
            if (completion) {
                completion(error, nil);
                
                NSString *logDescription = [NSString stringWithFormat: @"%@ %@",ERROR, error.description];
                RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
            }
        }];
    }
    else {
        [RMFUtils raiseException:NSInternalInconsistencyException WithDescription:kUrlNotFound];
    }
}

@end
