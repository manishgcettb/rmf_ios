/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "AFHTTPRequestOperationManager.h"

@interface RMFRequestOperationManager : AFHTTPRequestOperationManager

// Makes HTTP GET request with request parameters and returns error/data in the response block
- (void)get:(NSString *)url withParameters:(id)params andResponse:(void (^)(NSError *error, id data))completion;

// Makes HTTP POST request with request parameters and returns error/data in the response block
- (void)post:(NSString *)url withParameters:(id)params andResponse:(void (^)(NSError *error, id data))completion;

// Makes HTTP PUT request with request parameters and returns error/data in the response block
- (void)put:(NSString *)url withParameters:(id)params andResponse:(void (^)(NSError *error, id data))completion;

// Makes HTTP PATCH request with request parameters and returns error/data in the response block
- (void)patch:(NSString *)url withParameters:(id)params andResponse:(void (^)(NSError *error, id data))completion;

// Makes HTTP DELETE request with request parameters and returns error/data in the response block
- (void)deleteRequest:(NSString *)url withParameters:(id)params andResponse:(void (^)(NSError *error, id data))completion;

@end
