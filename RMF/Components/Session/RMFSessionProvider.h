/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <Foundation/Foundation.h>

@interface RMFSessionProvider : NSObject

// User name used for login
@property (nonatomic, strong) NSString *userName;

// Password used for login
@property (nonatomic, strong) NSString *password;

// Session token
@property (nonatomic, strong) NSString *token;

// last login time
@property (nonatomic, strong) NSString *lastLoginDateTime;

// define the connection is secure or not
@property (nonatomic, assign) BOOL isSecureConnection;

// define session exists or not
@property (nonatomic, assign) BOOL isSessionExists;

+ (RMFSessionProvider *)sharedSessionProvider;


@end
