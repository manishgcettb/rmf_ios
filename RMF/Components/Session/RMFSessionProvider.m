/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFSessionProvider.h"
#import "RMFMacro.h"

#import "RMFLog.h"
#import "RMFLogger.h"

@implementation RMFSessionProvider

static RMFSessionProvider *sharedSessionProvider = nil;


+ (RMFSessionProvider *)sharedSessionProvider {
    if (!OBJ_NOT_NIL(sharedSessionProvider)) {
        sharedSessionProvider = [[RMFSessionProvider alloc] init];
        RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), SHARED_INSTANCE_CREATED(kSessionProvider));
    }
    return sharedSessionProvider;
}

- (id)init {
    if (!OBJ_NOT_NIL(sharedSessionProvider)) {
        sharedSessionProvider = [super init];
        // Setting up temporary username, password
        self.userName = @"kumo@mobile.com";
        self.password = @"Password";
    }
    return sharedSessionProvider;
}

@end
