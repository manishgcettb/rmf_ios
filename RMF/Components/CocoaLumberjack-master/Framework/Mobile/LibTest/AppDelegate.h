#import <UIKit/UIKit.h>

@class DemoViewController;


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) DemoViewController *viewController;

@end
