/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <Foundation/Foundation.h>
#import "RMFAccessPoint.h"
#import "RMFWlanService.h"
#import "RMFWlanServiceStatus.h"
#import "RMFEvent.h"
#import "RMFSummaryStatistics.h"

#import "RMFClientDetail.h"


@protocol RMFParsingProtocol <NSObject>

@optional

#pragma mark - AP

// Returns access point information after parsing the network output
- (void)parseCreateAPWithInput:(id)input output:(void (^)(NSError *error, RMFAccessPoint *rmfAccessPoint))completion;

// Returns array of access point after parsing the network output
- (void)parseAPListWithInput:(id)input output:(void (^)(NSError *error, NSArray *apList))completion;

// Returns access point information after parsing the network output
- (void)parseAPDetailWithInput:(id)input output:(void (^)(NSError *error, RMFAccessPoint *rmfAccessPoint))completion;

// Returns array of access point after parsing the network output
- (void)parseAPWithInput:(id)input output:(void (^)(NSError *error, NSArray *apList))completion;

// Returns access point information after parsing the network output
- (void)parseEditAPWithInput:(id)input output:(void (^)(NSError *error, RMFAccessPoint *rmfAccessPoint))completion;

// Returns delete AP status information after parsing the network output
- (void)parseDeleteAPWithInput:(id)input output:(void (^)(NSError *error, BOOL status))completion;


#pragma mark - Wlan

+ (NSMutableArray *)parseWlanServiceListWithInput:(id)input;

// Returns the list of wlan services after parsing the network output
- (void)parseWlanServiceListWithInput:(id)input output:(void (^)(NSError *error,  NSArray *wlanServiceList))completion;

// Returns the wlan service details after parsing the network output
- (void)parseWlanServiceDetailsWithInput:(id)input output:(void (^)(NSError *error,  RMFWlanService *wlanService))completion;

// Returns the wlan service status after parsing the network output
- (void)parseWlanServiceStatusWithInput:(id)input output:(void (^)(NSError *error,  RMFWlanServiceStatus *wlanServiceStatus))completion;

// Returns the list of wlan services after parsing the network output
- (void)parseWlanFrameWithInput:(id)input output:(void (^)(NSError *error,  NSArray *wlanServiceList))completion;

// Returns create wlan result after parsing the network response
- (void)parseCreateWlanResponseWithInput:(id)input output:(void (^)(NSError *error,  NSString *createWlanResponse))completion;

// Returns delete wlan result after parsing the network response
- (void)parseDeleteWlanResponseWithInput:(id)input output:(void (^)(NSError *error,  NSString *createWlanResponse))completion;

// Returns edit wlan result after parsing the network response
- (void)parseEditWlanResponseWithInput:(id)input output:(void (^)(NSError *error,  NSString *editWlanResponse))completion;

// Returns the list of wlan services fetched using custom parameters after parsing the network response
- (void)parseWlanListWithInput:(id)input output:(void (^)(NSError *error,  NSArray *wlanServiceList))completion;

#pragma mark - clients

// client list fetching using yang model and custom parameters ..
- (void)parseClientListFetchedUsingCustomParams:(id)input output:(void (^)(NSError *error,  NSArray *wlanServiceList))completion;

// Returns clients list after parsing the network output
- (void)parseClientListWithInput:(id)input output:(void (^)(NSError *error,  id clientList))completion;

// parse client details and get parsed response ..
- (void)parseClientDetailsWithInput:(id)input output:(void (^)(NSError *error,  RMFClientDetail *eventDetails))completion;

#pragma - Events

// Returns Event list after parsing the network output
- (void)parseEventList:(id)input output:(void (^)(NSError *error,  NSArray *eventList))completion;

// Returns Event list after parsing the network output
- (void)parseEventListWithInput:(id)input output:(void (^)(NSError *error,  NSArray *eventList))completion;

// Returns Event details after parsing
- (void)parseEventDetailsWithInput:(id)input output:(void (^)(NSError *error,  RMFEvent *eventDetails))completion;

// Returns summary statistics after parsing
- (void)summaryStatistics:(id)input output:(void (^)(NSError *error,   RMFSummaryStatistics *summaryStatistics))completion;

@end
