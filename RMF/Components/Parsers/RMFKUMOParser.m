/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFKUMOParser.h"
#import "RMFMacro.h"
#import "RMFAccessPoint.h"
#import "RMFUtils.h"
#import "RMFWlanService.h"
#import "RMFWlanServiceStatus.h"
#import "RMFWlanBasicNwCustomization.h"
#import "RMFWlanAdvNwCustomization.h"
#import "RMFAccessPoint.h"
#import "RMFEvent.h"
#import "RMFWlan.h"
#import "RMFEvent.h"
#import "RMFSummaryStatistics.h"
#import "RMFErrorHandler.h"
#import "RMFClients.h"
#import "RMFClientDetail.h"

@implementation RMFKUMOParser

/*
 * Stores the singleton instance of RMFKumoParser
 */
static RMFKUMOParser *sharedRMFKUMOParser = nil;

#pragma mark - Class methods declaration

/*
 * Creates a singleton instance of RMFScgParser
 */

+ (RMFKUMOParser *)sharedRMFKUMOParser {
	if (!OBJ_NOT_NIL(sharedRMFKUMOParser)) {
		sharedRMFKUMOParser = [[RMFKUMOParser alloc] init];
		RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), SHARED_INSTANCE_CREATED(kKumoParser));
	}
	return sharedRMFKUMOParser;
}

#pragma mark - Instance methods implementation

/*
 * initialize the singleton instance of RMFScgParser
 */

- (RMFKUMOParser *)init {
	if (!OBJ_NOT_NIL(sharedRMFKUMOParser)) {
		sharedRMFKUMOParser = [super init];
	}
	return sharedRMFKUMOParser;
}

#pragma mark - AP

/*
 * Returns access point information after parsing the network response
 */
- (void)parseCreateAPWithInput:(id)input output:(void (^)(NSError *error, RMFAccessPoint *accessPoint))completion {
	NSError *error = nil;

	if (OBJ_NOT_NIL(input)) {
		if ([input isKindOfClass:[NSDictionary class]]) {
			// TODO right now no response in case of createAP
			RMFAccessPoint *rmfAccessPoint = [[RMFAccessPoint alloc] init];
			if (OBJ_NOT_NIL(completion)) {
				completion(error, rmfAccessPoint);
				NSString *logDescription = [NSString stringWithFormat:@"%@ %@", ACCESSPOINT_List, rmfAccessPoint];
				RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), logDescription);
			}
			return;
		}
	}
	if (OBJ_NOT_NIL(completion)) {
		error = [RMFUtils errorWithCode:RMFCodeForParsingFailedError];
		completion(error, nil);
		NSString *logDescription = [NSString stringWithFormat:@"%@ %@", ERROR, error.description];
		RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
	}
}

/*
 * Returns array of access point after parsing the network response
 */
- (void)parseAPListWithInput:(id)input output:(void (^)(NSError *error, NSArray *apList))completion {
	NSError *error = nil;
	id typeChecker = nil;
	NSMutableArray *apList = [[NSMutableArray alloc] init];

	if (input && [input isKindOfClass:[NSDictionary class]]) {
		// Parse aps dictionary
		NSDictionary *responseDict = (NSDictionary *)input;
		typeChecker = [responseDict objectForKey:kApsKey];

		if (typeChecker && [typeChecker isKindOfClass:[NSDictionary class]]) {
			// Parse ap list
			NSDictionary *apsDict = (NSDictionary *)typeChecker;
			typeChecker = [apsDict objectForKey:kAPKey];

			if (typeChecker && [typeChecker isKindOfClass:[NSArray class]]) {
				NSArray *serialNumberArray = (NSArray *)typeChecker;

				[serialNumberArray enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop) {
				    if ([obj isKindOfClass:[NSDictionary class]]) {
				        // Parse serial number
				        NSDictionary *aps = (NSDictionary *)obj;
				        id typeChecker = [aps objectForKey:kSerialNumberKey];

				        if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
				            RMFAccessPoint *rmfAccessPoint = [[RMFAccessPoint alloc] init];
				            rmfAccessPoint.serialNumber = (NSString *)typeChecker;
				            [apList addObject:rmfAccessPoint];
						}
					}
				}];

				if ([serialNumberArray count] == [apList count]) {
					if (OBJ_NOT_NIL(completion)) {
						completion(nil, apList);
						NSString *logDescription = [NSString stringWithFormat:@"%@ %@", ACCESSPOINT_List, apList];
						RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), logDescription);
					}
					return;
				}
			}
		}
	}
	if (OBJ_NOT_NIL(completion)) {
		error = [RMFUtils errorWithCode:RMFCodeForParsingFailedError];
		completion(error, nil);
		NSString *logDescription = [NSString stringWithFormat:@"%@ %@", ERROR, error.description];
		RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
	}
}

/*
 * Returns access point information after parsing the network response
 */
- (void)parseAPDetailWithInput:(id)input output:(void (^)(NSError *error, RMFAccessPoint *rmfAccessPoint))completion {
	NSError *error = nil;
	id typeChecker = nil;

	if (input && [input isKindOfClass:[NSDictionary class]]) {
		// Parse ap dictionary
		NSDictionary *responseDict = (NSDictionary *)input;
		typeChecker = [responseDict objectForKey:kAPKey];
		if (typeChecker && [typeChecker isKindOfClass:[NSDictionary class]]) {
			RMFAccessPoint *rmfAccessPoint = [[RMFAccessPoint alloc] init];

			NSDictionary *apDetailsDict = (NSDictionary *)typeChecker;
			// Parse serial number
			typeChecker = [apDetailsDict objectForKey:kSerialNumberKey];
			if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
				rmfAccessPoint.serialNumber = (NSString *)typeChecker;
			}

			// Parse provisioning-data
			typeChecker = [apDetailsDict objectForKey:kAPProvisioningData];
			if (typeChecker && [typeChecker isKindOfClass:[NSDictionary class]]) {
				// Parse AP name
				NSDictionary *provisioningData = (NSDictionary *)typeChecker;
				id typeChecker = [provisioningData objectForKey:kAPName];

				if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
					// TODO
				}
			}

			// Parse wlan-status
			typeChecker = [apDetailsDict objectForKey:kAPWlanStatus];
			if (typeChecker && [typeChecker isKindOfClass:[NSDictionary class]]) {
				NSDictionary *wlansData = (NSDictionary *)typeChecker;
				RMFAPWlanStatus *rmfAPWlanStatus = [[RMFAPWlanStatus alloc] init];

				id typeChecker = [wlansData objectForKey:kAPWlanTotalCount];
				if (typeChecker) {
					rmfAPWlanStatus.totalCount = [typeChecker intValue];
				}
				typeChecker = [wlansData objectForKey:kAPWlans];
				if (typeChecker && [typeChecker isKindOfClass:[NSArray class]]) {
					NSMutableArray *wlansList = [[NSMutableArray alloc] init];
					NSArray *wlans = (NSArray *)typeChecker;

					[wlans enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop) {
					    if ([obj isKindOfClass:[NSDictionary class]]) {
					        // Parse serial number
					        NSDictionary *bssid = (NSDictionary *)obj;
					        id typeChecker = [bssid objectForKey:kBssidKey];

					        if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
					            RMFWlan *rmfWlan = [[RMFWlan alloc] init];
					            rmfWlan.bssid = (NSString *)typeChecker;
					            [wlansList addObject:rmfWlan];
							}
						}
					}];
					rmfAPWlanStatus.wlans = wlansList;
				}
				rmfAccessPoint.rmfAPWlanStatus = rmfAPWlanStatus;
			}

			// Parse extended-status
			typeChecker = [apDetailsDict objectForKey:kAPExtendedStatus];
			if (typeChecker && [typeChecker isKindOfClass:[NSDictionary class]]) {
				NSDictionary *extendedStatusData = (NSDictionary *)typeChecker;
				RMFAPExtendedStatus *rmfAPExtendedStatus = [[RMFAPExtendedStatus alloc] init];

				id typeChecker = [extendedStatusData objectForKey:kAPInternalIP];
				if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
					rmfAPExtendedStatus.internalIp = (NSString *)typeChecker;
				}
				typeChecker = [extendedStatusData objectForKey:kAPExternalIP];
				if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
					rmfAPExtendedStatus.externalIp = (NSString *)typeChecker;
				}
				typeChecker = [extendedStatusData objectForKey:kAPHops];
				if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
					rmfAPExtendedStatus.hops = (NSString *)typeChecker;
				}
				typeChecker = [extendedStatusData objectForKey:kAPTunnelType];
				if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
					rmfAPExtendedStatus.tunnelType = (NSString *)typeChecker;
				}
				typeChecker = [extendedStatusData objectForKey:kAPName];
				if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
					rmfAPExtendedStatus.name = (NSString *)typeChecker;
				}
				typeChecker = [extendedStatusData objectForKey:kAPUptime];
				if (typeChecker) {
					rmfAPExtendedStatus.uptime = [typeChecker longValue];
				}

				rmfAccessPoint.rmfAPExtendedStatus = rmfAPExtendedStatus;
			}

			// Parse radio-status
			typeChecker = [apDetailsDict objectForKey:kAPRadioStatus];
			if (typeChecker && [typeChecker isKindOfClass:[NSDictionary class]]) {
				NSDictionary *rmfRadioStatusData = (NSDictionary *)typeChecker;
				RMFAPRadioStatus *rmfAPRadioStatus = [[RMFAPRadioStatus alloc] init];

				id typeChecker = [rmfRadioStatusData objectForKey:kAPTxPower];
				if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
					rmfAPRadioStatus.txPower = (NSString *)typeChecker;
				}
				typeChecker = [rmfRadioStatusData objectForKey:kAPChannel];
				if (typeChecker) {
					rmfAPRadioStatus.channel = [typeChecker intValue];
				}
				typeChecker = [rmfRadioStatusData objectForKey:kAPChannelWidth];
				if (typeChecker) {
					rmfAPRadioStatus.channelWidth = [typeChecker intValue];
				}
				typeChecker = [rmfRadioStatusData objectForKey:kAPWlanCount];
				if (typeChecker) {
					rmfAPRadioStatus.wlanCount = [typeChecker intValue];
				}
				typeChecker = [rmfRadioStatusData objectForKey:kAPMode];
				if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
					rmfAPRadioStatus.mode = (NSString *)typeChecker;
				}
				rmfAccessPoint.rmfAPRadioStatus = rmfAPRadioStatus;
			}

			// Parse status
			typeChecker = [apDetailsDict objectForKey:kAPStatus];
			if (typeChecker && [typeChecker isKindOfClass:[NSDictionary class]]) {
				NSDictionary *rmfAPStatusData = (NSDictionary *)typeChecker;

				RMFAPStatus *rmfAPStatus = [[RMFAPStatus alloc] init];

				id typeChecker = [rmfAPStatusData objectForKey:kAPModel];
				if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
					rmfAPStatus.model = (NSString *)typeChecker;
				}
				typeChecker = [rmfAPStatusData objectForKey:kAPMacAddress];
				if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
					rmfAPStatus.macAddress = (NSString *)typeChecker;
				}
				typeChecker = [rmfAPStatusData objectForKey:kAPRegistrationStatus];
				if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
					rmfAPStatus.registrationStatus = (NSString *)typeChecker;
				}
				typeChecker = [rmfAPStatusData objectForKey:kAPConnectionStatus];
				if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
					rmfAPStatus.connectionStatus = (NSString *)typeChecker;
				}
				typeChecker = [rmfAPStatusData objectForKey:kAPConfigStatus];
				if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
					rmfAPStatus.configStatus = (NSString *)typeChecker;
				}
				typeChecker = [rmfAPStatusData objectForKey:kAPLastSeenByVscg];
				if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
					rmfAPStatus.lastSeenByVscg = (NSString *)typeChecker;
				}
				typeChecker = [rmfAPStatusData objectForKey:kAPSWver];
				if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
					rmfAPStatus.swver = (NSString *)typeChecker;
				}
				typeChecker = [rmfAPStatusData objectForKey:kAPLocation];
				if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
					rmfAPStatus.location = (NSString *)typeChecker;
				}
				rmfAccessPoint.rmfAPStatus = rmfAPStatus;
			}
			if (OBJ_NOT_NIL(completion)) {
				completion(error, rmfAccessPoint);
				NSString *logDescription = [NSString stringWithFormat:@"%@ %@", ACCESSPOINT_List, rmfAccessPoint];
				RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), logDescription);
			}
			return;
		}
	}

	if (OBJ_NOT_NIL(completion)) {
		error = [RMFUtils errorWithCode:RMFCodeForParsingFailedError];
		completion(error, nil);
		NSString *logDescription = [NSString stringWithFormat:@"%@ %@", ERROR, error.description];
		RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
	}
}

/*
 * Returns array of access point after parsing the network response
 */
- (void)parseAPWithInput:(id)input output:(void (^)(NSError *error, NSArray *apList))completion {
	NSError *error = nil;
	id typeChecker = nil;

	if (input && [input isKindOfClass:[NSDictionary class]]) {
		// Parse aps dictionary
		NSDictionary *inputDict = (NSDictionary *)input;
		typeChecker = [inputDict objectForKey:kQueryResult];

		if (typeChecker && [typeChecker isKindOfClass:[NSDictionary class]]) {
			NSMutableArray *apList = [[NSMutableArray alloc] init];

			// Parse ap list
			NSDictionary *queryResult = (NSDictionary *)typeChecker;
			typeChecker = [queryResult objectForKey:kResult];

			if (typeChecker && [typeChecker isKindOfClass:[NSArray class]]) {
				NSArray *results = (NSArray *)typeChecker;

				[results enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop) {
				    if ([obj isKindOfClass:[NSDictionary class]]) {
				        RMFAccessPoint *rmfAccessPoint = [[RMFAccessPoint alloc] init];

				        // Parse select
				        NSDictionary *result = (NSDictionary *)obj;
				        id typeChecker = [result objectForKey:kSelect];

				        if (typeChecker && [typeChecker isKindOfClass:[NSArray class]]) {
				            NSArray *selectedObjs = (NSArray *)typeChecker;

				            [selectedObjs enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop) {
				                if ([obj isKindOfClass:[NSDictionary class]]) {
				                    // Parse select
				                    NSDictionary *selectedObj = (NSDictionary *)obj;
				                    id typeChecker = [selectedObj objectForKey:kSelectParametersLabel];
				                    if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
				                        if ([(NSString *)typeChecker isEqualToString:kSelectLabelName]) {
				                            RMFAPExtendedStatus *rmfAPExtendedStatus = [[RMFAPExtendedStatus alloc] init];

				                            id value = [selectedObj objectForKey:kSelectResultParametersValue];
				                            if (value && [value isKindOfClass:[NSString class]]) {
				                                rmfAPExtendedStatus.name = (NSString *)value;
				                                rmfAccessPoint.rmfAPExtendedStatus = rmfAPExtendedStatus;
											}
										}
				                        else if ([(NSString *)typeChecker isEqualToString:kSelectLabelSerialNumber]) {
				                            id value = [selectedObj objectForKey:kSelectResultParametersValue];
				                            if (value && [value isKindOfClass:[NSString class]]) {
				                                rmfAccessPoint.serialNumber = (NSString *)value;
											}
										}
				                        else if ([(NSString *)typeChecker isEqualToString:kSelectLabelMacAddress]) {
				                            RMFAPStatus *rmfAPStatus = [[RMFAPStatus alloc] init];

				                            id value = [selectedObj objectForKey:kSelectResultParametersValue];
				                            if (value && [value isKindOfClass:[NSString class]]) {
				                                rmfAPStatus.macAddress = (NSString *)value;
				                                rmfAccessPoint.rmfAPStatus = rmfAPStatus;
											}
										}
				                        if ([(NSString *)typeChecker isEqualToString:kSelectLabelSSID]) {
				                            id value = [selectedObj objectForKey:kSelectResultParametersValue];
				                            if (value && [value isKindOfClass:[NSString class]]) {
				                                rmfAccessPoint.ssid = (NSString *)value;
											}
										}
				                        if ([(NSString *)typeChecker isEqualToString:kSelectLabelWLANCount]) {
				                            RMFAPWlanStatus *rmfAPWlanStatus = [[RMFAPWlanStatus alloc] init];

				                            id value = [selectedObj objectForKey:kSelectResultParametersValue];
				                            if (value && [value isKindOfClass:[NSString class]]) {
				                                rmfAPWlanStatus.totalCount = [(NSString *)value intValue];
				                                rmfAccessPoint.rmfAPWlanStatus = rmfAPWlanStatus;
											}
										}
									}
								}
							}];
						}
				        if (OBJ_NOT_NIL(rmfAccessPoint)) {
				            [apList addObject:rmfAccessPoint];
						}
					}
				}];
			}

			if (OBJ_NOT_NIL(completion) && OBJ_NOT_NIL(apList)) {
				completion(nil, apList);
				NSString *logDescription = [NSString stringWithFormat:@"%@ %@", ACCESSPOINT_List, apList];
				RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), logDescription);
				return;
			}
		}
	}
	if (OBJ_NOT_NIL(completion)) {
		error = [RMFUtils errorWithCode:RMFCodeForParsingFailedError];
		completion(error, nil);
		NSString *logDescription = [NSString stringWithFormat:@"%@ %@", ERROR, error.description];
		RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
	}
}

/*
 * Returns access point information after parsing the network response
 */
- (void)parseEditAPWithInput:(id)input output:(void (^)(NSError *error, RMFAccessPoint *rmfAccessPoint))completion {
	NSError *error = nil;
	if (OBJ_NOT_NIL(input)) {
		if ([input isKindOfClass:[NSDictionary class]]) {
			// TODO right now no response in case of edit AP
			RMFAccessPoint *rmfAccessPoint = [[RMFAccessPoint alloc] init];
			if (OBJ_NOT_NIL(completion)) {
				completion(error, rmfAccessPoint);
				NSString *logDescription = [NSString stringWithFormat:@"%@ %@", ACCESSPOINT_List, rmfAccessPoint];
				RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), logDescription);
			}
			return;
		}
	}
	if (OBJ_NOT_NIL(completion)) {
		error = [RMFUtils errorWithCode:RMFCodeForParsingFailedError];
		completion(error, nil);
		NSString *logDescription = [NSString stringWithFormat:@"%@ %@", ERROR, error.description];
		RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
	}
}

/*
 * Returns delete AP status information after parsing the network output
 */
- (void)parseDeleteAPWithInput:(id)input output:(void (^)(NSError *error, BOOL status))completion {
	NSError *error = nil;

	if (OBJ_NOT_NIL(input)) {
		if ([input isKindOfClass:[NSDictionary class]]) {
			// TODO right now no response in case of delete AP
			BOOL status  = YES;
			if (OBJ_NOT_NIL(completion)) {
				completion(error, status);
				NSString *logDescription = [NSString stringWithFormat:@"%@ %d", DELETE_ACCESSPOINT, status];
				RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), logDescription);
			}
			return;
		}
	}
	if (OBJ_NOT_NIL(completion)) {
		error = [RMFUtils errorWithCode:RMFCodeForParsingFailedError];
		completion(error, false);
		NSString *logDescription = [NSString stringWithFormat:@"%@ %@", ERROR, error.description];
		RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
	}
}

#pragma mark - WLAN
/*
 * Returns list of wlan services after parsing the network response
 */

- (void)parseWlanServiceListWithInput:(id)input output:(void (^)(NSError *error,  NSArray *wlanServiceList))completion {
	NSError *error = nil;
	id typeChecker = nil;
	NSMutableArray *wlanServiceList = [[NSMutableArray alloc] init];

	if (input && [input isKindOfClass:[NSDictionary class]]) {
		// Parse service dictionary
		NSDictionary *responseDict = (NSDictionary *)input;
		typeChecker = [responseDict objectForKey:kServicesKey];

		if (typeChecker && [typeChecker isKindOfClass:[NSDictionary class]]) {
			// Parse wlan service list
			NSDictionary *servicesDict = (NSDictionary *)typeChecker;
			typeChecker = [servicesDict objectForKey:kWlanServiceKey];

			if (typeChecker && [typeChecker isKindOfClass:[NSArray class]]) {
				NSArray *wlanServiceArray = (NSArray *)typeChecker;

				[wlanServiceArray enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop) {
				    if ([obj isKindOfClass:[NSDictionary class]]) {
				        // Parse wlan name
				        NSDictionary *wlanService = (NSDictionary *)obj;
				        id typeChecker = [wlanService objectForKey:kWlanNameKey];

				        if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
				            NSString *wlanName = (NSString *)typeChecker;
                            RMFWlanService *wlan = [[RMFWlanService alloc] init];
                            wlan.name = wlanName;
				            [wlanServiceList addObject:wlan];
						}
					}
				}];

				if ([wlanServiceArray count] == [wlanServiceList count]) {
					if (OBJ_NOT_NIL(completion)) {
						completion(nil, wlanServiceList);
						NSString *logDescription = [NSString stringWithFormat:@"%@ %@", WLAN_List, wlanServiceList];
						RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), logDescription);
					}
					return;
				}
			}
		}
	}

	if (OBJ_NOT_NIL(completion)) {
		error = [RMFUtils errorWithCode:RMFCodeForParsingFailedError];
		completion(error, nil);
		NSString *logDescription = [NSString stringWithFormat:@"%@ %@", ERROR, error.description];
		RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
	}
}

/*
 * Returns list of wlan services after parsing the network response
 */

- (void)parseWlanListWithInput:(id)input output:(void (^)(NSError *error,  NSArray *wlanServiceList))completion {
	NSError *error = nil;
	id typeChecker = nil;

	if (input && [input isKindOfClass:[NSDictionary class]]) {
		// Parse wlan dictionary
		NSDictionary *inputDict = (NSDictionary *)input;
		typeChecker = [inputDict objectForKey:kQueryResult];

		if (typeChecker && [typeChecker isKindOfClass:[NSDictionary class]]) {
			NSMutableArray *wlanList = [[NSMutableArray alloc] init];

			// Parse wlan list
			NSDictionary *queryResult = (NSDictionary *)typeChecker;
			typeChecker = [queryResult objectForKey:kResult];

			if (typeChecker && [typeChecker isKindOfClass:[NSArray class]]) {
				NSArray *results = (NSArray *)typeChecker;

				[results enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop) {
				    if ([obj isKindOfClass:[NSDictionary class]]) {
				        RMFWlanService *wlanService = [[RMFWlanService alloc] init];

				        // Parse select
				        NSDictionary *result = (NSDictionary *)obj;
				        id typeChecker = [result objectForKey:kSelect];

				        if (typeChecker && [typeChecker isKindOfClass:[NSArray class]]) {
				            NSArray *selectedObjs = (NSArray *)typeChecker;

				            [selectedObjs enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop) {
				                if ([obj isKindOfClass:[NSDictionary class]]) {
				                    // Parse select
				                    NSDictionary *selectedObj = (NSDictionary *)obj;
				                    id typeChecker = [selectedObj objectForKey:kSelectParametersLabel];
				                    if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
				                        if ([(NSString *)typeChecker isEqualToString:kWlanNameKey]) {
				                            id value = [selectedObj objectForKey:kSelectResultParametersValue];
				                            if (value && [value isKindOfClass:[NSString class]]) {
				                                wlanService.name = (NSString *)value;
											}
										}
				                        else if ([(NSString *)typeChecker isEqualToString:kWlanSssidKey]) {
				                            id value = [selectedObj objectForKey:kSelectResultParametersValue];
				                            if (value && [value isKindOfClass:[NSString class]]) {
				                                wlanService.ssid = (NSString *)value;
											}
										}
				                        if ([(NSString *)typeChecker isEqualToString:kWlanTypeKey]) {
				                            id value = [selectedObj objectForKey:kSelectResultParametersValue];
				                            if (value && [value isKindOfClass:[NSString class]]) {
				                                wlanService.wlanType = (NSString *)value;
											}
										}
				                        if ([(NSString *)typeChecker isEqualToString:kPskPassPhraseKey]) {
				                            id value = [selectedObj objectForKey:kSelectResultParametersValue];
				                            if (value && [value isKindOfClass:[NSString class]]) {
				                                wlanService.pskPassPhrase = (NSString *)value;
											}
										}
				                        if ([(NSString *)typeChecker isEqualToString:kWlanSecurityKey]) {
				                            id value = [selectedObj objectForKey:kSelectResultParametersValue];
				                            if (value && [value isKindOfClass:[NSString class]]) {
				                                wlanService.wlanSecurity = (NSString *)value;
											}
										}
									}
								}
							}];
						}
				        if (OBJ_NOT_NIL(wlanService)) {
				            [wlanList addObject:wlanService];
						}
					}
				}];
			}

			if (OBJ_NOT_NIL(completion) && OBJ_NOT_NIL(wlanList)) {
				completion(nil, wlanList);
				NSString *logDescription = [NSString stringWithFormat:@"%@ %@", WLAN_List, wlanList];
				RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), logDescription);
				return;
			}
		}
	}
	if (OBJ_NOT_NIL(completion)) {
		error = [RMFUtils errorWithCode:RMFCodeForParsingFailedError];
		completion(error, nil);
		NSString *logDescription = [NSString stringWithFormat:@"%@ %@", ERROR, error.description];
		RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
	}
}

/*
 * Returns the wlan service details after parsing the network response
 */

- (void)parseWlanServiceDetailsWithInput:(id)input output:(void (^)(NSError *error,  RMFWlanService *wlanService))completion {
	id typeChecker = nil;

	RMFWlanService *wlanServiceDetails = [[RMFWlanService alloc] init];

	// Check if response data is of dictionary type
	if (input && [input isKindOfClass:[NSDictionary class]]) {
		// Parse wlan service dictionay
		NSDictionary *responseDict = (NSDictionary *)input;
		typeChecker = [responseDict objectForKey:kWlanServiceKey];

		if (typeChecker && [typeChecker isKindOfClass:[NSDictionary class]]) {
			NSDictionary *wlanServicesDict = (NSDictionary *)typeChecker;

			// Parse wlan name
			typeChecker = [wlanServicesDict objectForKey:kWlanNameKey];
			if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
				NSString *wlanName = (NSString *)typeChecker;
				wlanServiceDetails.name = wlanName;

				// Parse wlan ssid
				typeChecker = [wlanServicesDict objectForKey:kWlanSssidKey];
				if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
					NSString *wlanSsid = (NSString *)typeChecker;
					wlanServiceDetails.ssid = wlanSsid;

					// Parse wlan type
					typeChecker = [wlanServicesDict objectForKey:kWlanTypeKey];
					if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
						NSString *wlanType = (NSString *)typeChecker;
						wlanServiceDetails.wlanType = wlanType;

						// Parse wlan security type
						typeChecker = [wlanServicesDict objectForKey:kWlanSecurityKey];
						if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
							NSString *wlanSecurity = (NSString *)typeChecker;
							wlanServiceDetails.wlanSecurity = wlanSecurity;

							// Parse basic network customization value
							typeChecker = [wlanServicesDict objectForKey:kBasicNwCustomizationKey];
							if (typeChecker && [typeChecker isKindOfClass:[NSDictionary class]]) {
								// Parse vlan id
								NSDictionary *basicNwCustomizationDict = (NSDictionary *)typeChecker;
								NSInteger vlanId = [[basicNwCustomizationDict objectForKey:kVlanIdKey] integerValue];

								RMFWlanBasicNwCustomization *wlanBasicNwCustomization = [[RMFWlanBasicNwCustomization alloc] init];

								if (wlanBasicNwCustomization) {
									wlanBasicNwCustomization.vlanId = vlanId;
									wlanServiceDetails.wlanBasicNwCustomization = wlanBasicNwCustomization;


									// Parse advanced customization value
									typeChecker = [wlanServicesDict objectForKey:kAdvCustomizationKey];
									if (typeChecker && [typeChecker isKindOfClass:[NSDictionary class]]) {
										NSDictionary *advancedCustomizationKey = (NSDictionary *)typeChecker;

										// Parse maximum client on wlan per radio value
										NSInteger maxClientOnWlanPerRadio = [[advancedCustomizationKey objectForKey:kMaxClientOnWlanPerRadio] integerValue];

										// Parse band balancing status value
										BOOL enableBandBalancing = [[advancedCustomizationKey objectForKey:kEnableBandBalancingKey] boolValue];

										RMFWlanAdvNwCustomization *wlanAdvNwCustomization = [[RMFWlanAdvNwCustomization alloc] init];

										if (wlanAdvNwCustomization) {
											wlanAdvNwCustomization.maxClientsOnWlanPerRadio = maxClientOnWlanPerRadio;
											wlanAdvNwCustomization.enableBandBalancing = enableBandBalancing;
										}
										wlanServiceDetails.wlanAdvCustmization = wlanAdvNwCustomization;


										// Parse passphrase value
										typeChecker = [wlanServicesDict objectForKey:kPskPassPhraseKey];
										if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
											NSString *pskPassPhrase = (NSString *)typeChecker;
											wlanServiceDetails.pskPassPhrase = pskPassPhrase;
											if (OBJ_NOT_NIL(completion)) {
												completion(nil, wlanServiceDetails);
												NSString *logDescription = [NSString stringWithFormat:@"%@ %@", WLAN_List, wlanServiceDetails];
												RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), logDescription);
											}
											return;
										}
									}
								}
							}
						}
					}
				}
			}

			if (OBJ_NOT_NIL(completion)) {
				NSError *error = [RMFUtils errorWithCode:RMFCodeForParsingFailedError];
				completion(error, nil);
				NSString *logDescription = [NSString stringWithFormat:@"%@ %@", ERROR, error.description];
				RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
			}
		}
	}
}

/*
 * Returns the wlan service status after parsing the network response
 */

- (void)parseWlanServiceStatusWithInput:(id)input output:(void (^)(NSError *error,  RMFWlanServiceStatus *wlanServiceStatus))completion {
	id typeChecker = nil;
	NSError *error = nil;

	NSMutableArray *bssidList = [[NSMutableArray alloc] init];
	RMFWlanServiceStatus *wlanServiceStatus = [[RMFWlanServiceStatus alloc] init];

	//Check if response data is of dictionary type
	if (input && [input isKindOfClass:[NSDictionary class]]) {
		// Parse wlan status value
		NSDictionary *responseDict = (NSDictionary *)input;
		typeChecker = [responseDict objectForKey:kWlanStatusKey];

		if (typeChecker && [typeChecker isKindOfClass:[NSDictionary class]]) {
			NSDictionary *wlanStatusDict = (NSDictionary *)typeChecker;

			// Parse total count value
			wlanServiceStatus.totalCount = [[wlanStatusDict objectForKey:kTotalCountKey] integerValue];

			// Parse wlans list value
			typeChecker = [wlanStatusDict objectForKey:kWlansKey];

			if (typeChecker && [typeChecker isKindOfClass:[NSArray class]]) {
				NSArray *bssidArray = (NSArray *)typeChecker;

				[bssidArray enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop) {
				    if ([obj isKindOfClass:[NSDictionary class]]) {
				        // Parse bssid value
				        NSDictionary *bssidDict = (NSDictionary *)obj;
				        id typeChecker = [bssidDict objectForKey:kBssidKey];

				        if ([typeChecker isKindOfClass:[NSString class]]) {
				            NSString *bssid = (NSString *)typeChecker;
				            [bssidList addObject:bssid];
						}
					}
				}];

				if ([bssidList count] == [bssidArray count]) {
					wlanServiceStatus.bssidArray = bssidList;
					if (OBJ_NOT_NIL(completion)) {
						completion(nil, wlanServiceStatus);
						NSString *logDescription = [NSString stringWithFormat:@"%@ %@", WLAN_List, wlanServiceStatus];
						RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), logDescription);
					}
					return;
				}
			}
		}
	}

	if (OBJ_NOT_NIL(completion)) {
		error = [RMFUtils errorWithCode:RMFCodeForParsingFailedError];
		completion(error, nil);
		NSString *logDescription = [NSString stringWithFormat:@"%@ %@", ERROR, error.description];
		RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
	}
}

/*
 * Returns list of wlan services after parsing the network response
 */
- (void)parseWlanFrameWithInput:(id)input output:(void (^)(NSError *error,  NSArray *wlanServiceList))completion {
	if (OBJ_NOT_NIL(completion)) {
		completion(nil, [NSArray arrayWithObject:@""]);
	}
}

/*
 * Returns create wlan result after parsing the network response
 */

- (void)parseCreateWlanResponseWithInput:(id)input output:(void (^)(NSError *error,  NSString *createWlanResponse))completion {
	if (OBJ_NOT_NIL(completion)) {
		//TODO: parse wlan create response
	}
}

/*
 * Returns delete wlan result after parsing the network response
 */

- (void)parseDeleteWlanResponseWithInput:(id)input output:(void (^)(NSError *error,  NSString *createWlanResponse))completion {
	if (OBJ_NOT_NIL(completion)) {
		//TODO: parse wlan delete response
	}
}

/*
 * Returns edit wlan result after parsing the network response
 */

- (void)parseEditWlanResponseWithInput:(id)input output:(void (^)(NSError *error,  NSString *editWlanResponse))completion {
	if (OBJ_NOT_NIL(completion)) {
		//TODO: parse wlan edit response
	}
}

#pragma mark -
#pragma mark - PARSING METHODS FOR CLIENTS

/*
 * Returns list of clients after parsing the network response
 */

- (void)parseClientListFetchedUsingCustomParams:(id)input output:(void (^)(NSError *error,  NSArray *wlanServiceList))completion {
	NSError *error = nil;
	id typeChecker = nil;
	//NSMutableArray *clientList = [[NSMutableArray alloc] init];

	if (input && [input isKindOfClass:[NSArray class]]) {
		NSArray *responseArray = (NSArray *)input;

		for (id input in responseArray) {
			if (input && [input isKindOfClass:[NSArray class]]) {
				NSArray *wlanDetailsArray = (NSArray *)input;

				//RMFClientDetail *clientDetail = [[RMFClientDetail alloc] init];

				for (id input in wlanDetailsArray) {
					if (input && [input isKindOfClass:[NSDictionary class]]) {
						//TODO:
					}
				}
			}
		}
	}
}

/*
 * Returns the client list after parsing the network response
 */
- (void)parseClientListWithInput:(id)input output:(void (^)(NSError *error,  id clientList))completion {
	NSError *error = nil;
	id typeChecker = nil;
	NSMutableArray *clientsList = [[NSMutableArray alloc] init];

	if (input && [input isKindOfClass:[NSDictionary class]]) {
		// Parse client list response dictionary
		NSDictionary *responseDict = (NSDictionary *)input;
		typeChecker = [responseDict objectForKey:kClientsKey]; // clients parent dict

		if (typeChecker && [typeChecker isKindOfClass:[NSDictionary class]]) {
			// Parse client service list
			NSDictionary *clientsDict = (NSDictionary *)typeChecker;
			typeChecker = [clientsDict objectForKey:kClientsDataKey]; // clients data

			if (typeChecker && [typeChecker isKindOfClass:[NSArray class]]) {
				NSArray *clientsDataArray = (NSArray *)typeChecker; // clients data arrray

				[clientsDataArray enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop)
				{
				    if ([obj isKindOfClass:[NSDictionary class]]) { // one obj is in form of dictionary
				        // Parse mac address of client name
				        NSDictionary *clientsDict = (NSDictionary *)obj;
				        id typeChecker = [clientsDict objectForKey:kClientMacAddressKey];

				        if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
				            NSString *clientMacAddress = (NSString *)typeChecker;
				            [clientsList addObject:clientMacAddress];
						}
					}
				}];

				if ([clientsDataArray count] == [clientsList count]) {
					if (OBJ_NOT_NIL(completion)) {
						completion(nil, clientsList);
						NSString *logDescription = [NSString stringWithFormat:@"%@ %@", CLIENT_List, clientsList];
						RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), logDescription);
					}
					return;
				}
			}
		}
	}

	if (OBJ_NOT_NIL(completion)) {
		error = [RMFUtils errorWithCode:RMFCodeForParsingFailedError];
		completion(error, nil);
		NSString *logDescription = [NSString stringWithFormat:@"%@ %@", ERROR, error.description];
		RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
	}
}

/*
 * Returns the client details after parsing the network response
 */
- (void)parseClientDetailsWithInput:(id)input output:(void (^)(NSError *error, RMFClientDetail *clientDetails))completion {
	id typeChecker = nil;

	RMFClientDetail *clientDetails = [[RMFClientDetail alloc] init];

	// Check if response data is of dictionary type
	if (input && [input isKindOfClass:[NSDictionary class]]) {
		// Parse client detail  dictionay
		NSDictionary *responseDict = (NSDictionary *)input;
		typeChecker = [responseDict objectForKey:kClientsDataKey]; // client data for one client..

		if (typeChecker && [typeChecker isKindOfClass:[NSDictionary class]]) {
			NSDictionary *clientDetailDict = (NSDictionary *)typeChecker;

			// Parse auth name
			typeChecker = [clientDetailDict objectForKey:kClientAuthMethodKey];
			if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
				NSString *authMethod = (NSString *)typeChecker;
				clientDetails.authMethod = authMethod;
			}

			// Parse connetion duration ..
			typeChecker = [clientDetailDict objectForKey:kClientConnecitonDurationKey];
			if (typeChecker) { // long value
				clientDetails.connectionDuration = (NSNumber *)typeChecker;
			}

			// Parse ip
			typeChecker = [clientDetailDict objectForKey:kClientIPKey];
			if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
				NSString *ip = (NSString *)typeChecker;
				clientDetails.ip = ip;
			}

			// Parse mac address of client
			typeChecker = [clientDetailDict objectForKey:kClientMacAddressKey];
			if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
				NSString *macAddress = (NSString *)typeChecker;
				clientDetails.macAddress = macAddress;
			}

			// os type
			typeChecker = [clientDetailDict objectForKey:kClientOSTypeKey];
			if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
				NSString *osType = (NSString *)typeChecker;
				clientDetails.osType = osType;
			}

			// Parse status
			typeChecker = [clientDetailDict objectForKey:kClientStatusKey];
			if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
				NSString *status = (NSString *)typeChecker;
				clientDetails.status = status;
			}

			// Parse wlan
			typeChecker = [clientDetailDict objectForKey:kClientWlanKey];
			if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
				NSString *wlan = (NSString *)typeChecker;
				clientDetails.wlan = wlan;
			}

			if (OBJ_NOT_NIL(completion)) {
				completion(nil, clientDetails); //send back rmf client detail..
				NSString *logDescription = [NSString stringWithFormat:@"%@ %@", CLIENT_Details, clientDetails];
				RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), logDescription);
			}
			return;
		}
	}
	if (OBJ_NOT_NIL(completion)) {
		NSError *error = [RMFUtils errorWithCode:RMFCodeForParsingFailedError];
		completion(error, nil);
		NSString *logDescription = [NSString stringWithFormat:@"%@ %@", ERROR, error.description];
		RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
	}
}

#pragma mark - Event

/*
 * Returns the Event list after parsing the network response
 */

- (void)parseEventListWithInput:(id)input output:(void (^)(NSError *error,  NSArray *eventList))completion {
	//TODO: write logic to parse Events list response
}

/*
 * Returns the Event details after parsing the network response
 */

- (void)parseEventDetailsWithInput:(id)input output:(void (^)(NSError *error,  RMFEvent *eventDetails))completion {
	//TODO: write logic to parse Events details response
}

/*
 * Returns summary summaryStatistics after parsing the network response
 */

- (void)summaryStatistics:(id)input output:(void (^)(NSError *error,   RMFSummaryStatistics *summaryStatistics))completion {
	//TODO: write logic to parse Events details response
}

/*
 * Returns list of events after parsing the network response
 */

- (void)parseEventList:(id)input output:(void (^)(NSError *error,  NSArray *eventList))completion {
    NSError *error = nil;
    id typeChecker = nil;
    
    if (input && [input isKindOfClass:[NSDictionary class]]) {
        // Parse wlan dictionary
        NSDictionary *inputDict = (NSDictionary *)input;
        typeChecker = [inputDict objectForKey:kQueryResult];
        
        if (typeChecker && [typeChecker isKindOfClass:[NSDictionary class]]) {
            NSMutableArray *eventList = [[NSMutableArray alloc] init];
            
            // Parse event list
            NSDictionary *queryResult = (NSDictionary *)typeChecker;
            typeChecker = [queryResult objectForKey:kResult];
            
            if (typeChecker && [typeChecker isKindOfClass:[NSArray class]]) {
                NSArray *results = (NSArray *)typeChecker;
                
                [results enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop) {
                    if ([obj isKindOfClass:[NSDictionary class]]) {
                        RMFEvent *event = [[RMFEvent alloc] init];
                        
                        // Parse select
                        NSDictionary *result = (NSDictionary *)obj;
                        id typeChecker = [result objectForKey:kSelect];
                        
                        if (typeChecker && [typeChecker isKindOfClass:[NSArray class]]) {
                            NSArray *selectedObjs = (NSArray *)typeChecker;
                            
                            [selectedObjs enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop) {
                                if ([obj isKindOfClass:[NSDictionary class]]) {
                                    // Parse select
                                    NSDictionary *selectedObj = (NSDictionary *)obj;
                                    id typeChecker = [selectedObj objectForKey:kSelectParametersLabel];
                                    if (typeChecker && [typeChecker isKindOfClass:[NSString class]]) {
                                        
                                        if ([(NSString *)typeChecker isEqualToString:kEventTimeKey]) {
                                            id value = [selectedObj objectForKey:kSelectResultParametersValue];
                                            if (value && [value isKindOfClass:[NSString class]]) {
                                                event.time = (NSString *)value;
                                            }
                                        }
                                        else if ([(NSString *)typeChecker isEqualToString:kEventTypeKey]) {
                                            id value = [selectedObj objectForKey:kSelectResultParametersValue];
                                            if (value && [value isKindOfClass:[NSString class]]) {
                                                event.type = (NSString *)value;
                                            }
                                        }
                                        if ([(NSString *)typeChecker isEqualToString:kEventSeverityKey]) {
                                            id value = [selectedObj objectForKey:kSelectResultParametersValue];
                                            if (value && [value isKindOfClass:[NSString class]]) {
                                                event.severity = (NSString *)value;
                                            }
                                        }
                                        if ([(NSString *)typeChecker isEqualToString:kEventDescriptionKey]) {
                                            id value = [selectedObj objectForKey:kSelectResultParametersValue];
                                            if (value && [value isKindOfClass:[NSString class]]) {
                                                event.eventDescription = (NSString *)value;
                                            }
                                        }
                                    }
                                }
                            }];
                        }
                        if (OBJ_NOT_NIL(event)) {
                            [eventList addObject:event];
                        }
                    }
                }];
            }
            
            if (OBJ_NOT_NIL(completion) && OBJ_NOT_NIL(eventList)) {
                completion(nil, eventList);
                NSString *logDescription = [NSString stringWithFormat:@"%@ %@", EVENT_List, eventList];
                RMF_LOG_INFO([self class], NSStringFromSelector(_cmd), logDescription);
                return;
            }
        }
    }
    if (OBJ_NOT_NIL(completion)) {
        error = [RMFUtils errorWithCode:RMFCodeForParsingFailedError];
        completion(error, nil);
        NSString *logDescription = [NSString stringWithFormat:@"%@ %@", ERROR, error.description];
        RMF_LOG_ERROR([self class], NSStringFromSelector(_cmd), logDescription);
    }
}

@end
