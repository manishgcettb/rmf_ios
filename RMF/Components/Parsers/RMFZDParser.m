/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFZDParser.h"
#import "RMFMacro.h"

@implementation RMFZDParser

// Stores the singleton instance of RMFZDParser
static RMFZDParser *sharedRMFZDParser = nil;

#pragma mark - Class methods declaration

/*
 * Creates a singleton instance of RMFScgParser
 */

+ (RMFZDParser *)sharedRMFZDParser {
    if (!OBJ_NOT_NIL(sharedRMFZDParser)) {
        sharedRMFZDParser = [[RMFZDParser alloc] init];
    }
    return sharedRMFZDParser;
}

#pragma mark - Instance methods implementation

/*
 * initialize the singleton instance of RMFScgParser
 */

- (RMFZDParser *)init {
    if (!OBJ_NOT_NIL(sharedRMFZDParser)) {
        sharedRMFZDParser = [super init];
    }
    
    return sharedRMFZDParser;
}

@end
