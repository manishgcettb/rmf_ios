/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFSCGParser.h"
#import "RMFMacro.h"

@implementation RMFSCGParser

// Stores the singleton instance of RMFScgParser
static RMFSCGParser *sharedRMFSCGParser = nil;

#pragma mark - Class methods declaration

/*
 * Creates a singleton instance of RMFScgParser
 */

+ (RMFSCGParser *)sharedRMFSCGParser {
    if (!OBJ_NOT_NIL(sharedRMFSCGParser)) {
        sharedRMFSCGParser = [[RMFSCGParser alloc] init];
    }
    return sharedRMFSCGParser;
}

#pragma mark - Instance methods implementation

/*
 * initialize the singleton instance of RMFScgParser
 */

- (RMFSCGParser *)init {
    if (!OBJ_NOT_NIL(sharedRMFSCGParser)) {
        sharedRMFSCGParser = [super init];
    }
    
    return sharedRMFSCGParser;
}

@end
