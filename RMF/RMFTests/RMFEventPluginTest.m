/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFEventPlugin.h"
#import "RMFConfiguration.h"
#import "RMFMacro.h"

#define TEST_PORT                   @"8080"
#define TEST_IP                     @"10.199.4.106"

#define  kEventDetailsLoaded         @"Event details should be loaded"
#define  kEventListLoaded            @"Event list should be loaded"

@interface RMFEventPluginTest : XCTestCase

// Stores the instance of RMFEventPlugin
@property (nonatomic, strong) RMFEventPlugin *eventPlugin;

// Stores the instance of RMFConfiguration
@property (nonatomic, strong) RMFConfiguration *configuration;

// Stores the request parameters
@property (nonatomic, strong) NSMutableDictionary *requestParameters;

@end

@implementation RMFEventPluginTest

- (void)setUp {
	[super setUp];
	[self initialConfiguration];
	self.eventPlugin = [RMFEventPlugin sharedRMFEventPlugin];
	[self initRequestParameters];
}

- (void)tearDown {
	[super tearDown];
}

/*
 * initialise RMFConfiguration instance with system type, system IP and system port
 */
- (void)initialConfiguration {
	self.configuration = [RMFConfiguration sharedRMFConfiguration];
	self.configuration.system = kSystemTypeKUMO;
	self.configuration.systemIP = TEST_IP;
	self.configuration.port = TEST_PORT;
}

/*
 * initialise request parameter dictionary
 */
- (void)initRequestParameters {
	self.requestParameters = [[NSMutableDictionary alloc] init];
	if (self.requestParameters) {
		[self.requestParameters setObject:@"default" forKey:kVenueNameKey];
		[self.requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];
		[self.requestParameters setObject:@"161205002059" forKey:kSerialNumberKey];
		[self.requestParameters setObject:@"kumomobile" forKey:kWlanNameKey];
		[self.requestParameters setObject:@"20:C9:D0:8C:68:9F" forKey:kMacAddressInfoKey];
	}
}

/*
 *  sharedRMFEventPlugin should return a valid instance
 */
- (void)testSharedRMFEventPlugin {
	XCTAssertNotNil(self.eventPlugin);
}

/*
 * [[RMFEventPlugin alloc] init] should return a valid instance
 */
- (void)testRMFEventPluginUsingAlloc {
	XCTAssertNotNil([[RMFEventPlugin alloc] init]);
}

/*
 * [[RMFEventPlugin alloc] init] and sharedRMFEventPlugin should return same instance
 */
- (void)testIsSharedRMFEventPluginAndAllocReturnsSameObject {
	XCTAssertEqual([[RMFEventPlugin alloc] init], self.eventPlugin);
}

/*
 * If request parameters and response block are nil then EventList should not throw any
 * exception
 */

- (void)testEventListScenario1 {
	XCTAssertNoThrow([self.eventPlugin eventList:nil response:nil]);
}

/*
 * If request parameter dictionary is available but the response block is nil then EventList
 * should not throw any exception
 */

- (void)testEventListScenario2 {
	XCTAssertNoThrow([self.eventPlugin eventList:self.requestParameters response:nil]);
}

/*
 * If request parameter dictionary is nil but the response block is available then EventList should
 * return NSError object with description "Request parameters not found"
 */

- (void)testEventListScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kEventListLoaded];

	[self.eventPlugin eventList:nil response: ^(NSError *error, NSArray *wlanServiceList) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(wlanServiceList);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are nil then EventDetails should not throw any
 * exception
 */
- (void)testEventDetailsScenario1 {
	XCTAssertNoThrow([self.eventPlugin eventDetails:nil response:nil]);
}

/*
 * If request parameter dictionary is available but the response block is nil then EventDetails
 * should not throw any exception
 */

- (void)testEventDetailsScenario2 {
	XCTAssertNoThrow([self.eventPlugin eventDetails:self.requestParameters response:nil]);
}

/*
 * If request parameter dictionary is nil but the response block is available then EventDetails should
 * return NSError object with description "Request parameters not found"
 */

- (void)testEventDetailsScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kEventDetailsLoaded];

	[self.eventPlugin eventDetails:nil response: ^(NSError *error, RMFEvent *Event) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(Event);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameters and response block are available then EventDetails should
 * return the instance of RMFEvent initialized with parsed data
 */
- (void)testEventDetailsScenario4 {
	//TODO: Write assertion once the Event details request is implemented
}

/*
 * If event list, filtering parameter and response block are nil and sortby parameter is not a valid input
 * then eventList should not throw any exception
 *
 */

- (void)testEventsListScenario1 {
	XCTAssertNoThrow([self.eventPlugin eventList:nil sortBy:3 filterOn:nil response:nil]);
}

/*
 * If request parameter and filtering parameter are nil, response block is available and sortby parameter is not a valid input
 * then eventList should return error
 *
 */
- (void)testEventsListScenario2 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kEventListLoaded];
	[self.eventPlugin eventList:nil sortBy:3 filterOn:nil response: ^(NSError *error, id data) {
	    // TODO: Update this test case once the request url is available
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    if (error) {
	        XCTAssertNotNil(error);
		}
	    else {
	        XCTAssertNil(error);
		}
	    expectation = nil;
	}];
}

/*
 * If filtering parameter is nil, response block and request parameter are available and sortby parameter is not a valid input
 * then eventList should return error
 *
 */
- (void)testEventsListScenario3 {
	NSMutableDictionary *requestParams = [[NSMutableDictionary alloc] init];

	[requestParams setObject:[NSNumber numberWithInt:2] forKey:kLimit];
	[requestParams setObject:[NSNumber numberWithInt:1] forKey:kOffset];

	__block XCTestExpectation *expectation = [self expectationWithDescription:kEventListLoaded];
	[self.eventPlugin eventList:requestParams sortBy:2 filterOn:nil response: ^(NSError *error, id data) {
	    // TODO: Update this test case once the request url is available
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    if (error) {
	        XCTAssertNotNil(error);
		}
	    else {
	        XCTAssertNil(error);
		}
	    expectation = nil;
	}];
}

/*
 * If request parameter is nil, response block and  filtering parameter are available and sortby parameter is a valid input
 * then eventList should return error
 *
 */
- (void)testEventsListScenario4 {
	NSMutableDictionary *filter = [[NSMutableDictionary alloc] init];
	// TODO: Update this test case once the request url is available
}

/*
 * If request parameter, response block and  filtering parameter are available and sortby parameter is a valid input
 * then eventList should return event list
 *
 */
- (void)testEventsListScenario5 {
	// Create request parameter dictionary
	NSMutableDictionary *requestParams = [[NSMutableDictionary alloc] init];
	[requestParams setObject:[NSNumber numberWithInt:2] forKey:kLimit];
	[requestParams setObject:[NSNumber numberWithInt:1] forKey:kOffset];

	// Create filter dictionary
	NSMutableDictionary *filter = [[NSMutableDictionary alloc] init];
	// TODO: update this test case once the date formate is available
}

@end
