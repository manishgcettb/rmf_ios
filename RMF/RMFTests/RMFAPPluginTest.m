/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFAPPlugin.h"
#import "RMFMacro.h"
#import "RMFConfiguration.h"


#define TEST_IP      @"10.199.4.106"
#define TEST_PORT @"8080"

#define  kAPImageDownloaded          @"AP image should be downloaded"
#define  kAPImageUploaded            @"AP should be uploaded"
#define  kAPListLoaded     @"AP list should be loaded"
#define  kAPDetailsLoaded  @"AP details should be loaded"
#define  kAPCreated     @"AP should be created"
#define  kAPEdited     @"AP should be edited"
#define  kAPDeleted     @"AP should be deleted"
#define  kAPListLoadedSortAndFilter     @"AP List should be loaded"
#define  kAPFrameLoadedWithSortAndFilter     @"AP frame should be loaded"

@interface RMFAPPluginTest : XCTestCase
// Stores the instance of RMFConfiguration
@property (nonatomic, strong) RMFConfiguration *configuration;
// Stores the instance of RMFAPPlugin
@property (nonatomic, strong) RMFAPPlugin *rmfAPPlugin;

@end


@implementation RMFAPPluginTest

- (void)setUp {
	[super setUp];
	[self intialConfiguration];
	self.rmfAPPlugin = [RMFAPPlugin sharedRMFAPPlugin];
}

- (void)tearDown {
	[super tearDown];
}

/*
 * initialise RMFConfiguration and RMFSystemManager instance with valid information for
 * testing network APIs
 */
- (void)intialConfiguration {
	self.configuration = [RMFConfiguration sharedRMFConfiguration];
	self.configuration.systemIP = TEST_IP;
	self.configuration.port = TEST_PORT;
	self.configuration.system = kSystemTypeKUMO;
}

/*
 * testSharedRMFAPPlugin method should throw exception if RMFConfiguration
 * instance is not available
 */
- (void)testSharedRMFAPPluginScenario {
	if (![RMFConfiguration sharedRMFConfiguration]) {
		XCTAssertThrows([RMFAPPlugin sharedRMFAPPlugin]);
	}
}

/*
 * init method should return the instance of RMFAPPlugin instance if called with alloc
 */
- (void)testInitScenario1 {
	XCTAssertNotNil([[RMFAPPlugin alloc] init]);
}

/*
 * When called with alloc init method should return the same instance of RMFAPPlugin
 * which is created by using sharedRMFAPPlugin
 */
- (void)testInitScenario2 {
	RMFAPPlugin *rmfAPPlugin = [RMFAPPlugin sharedRMFAPPlugin];
	if (rmfAPPlugin) {
		XCTAssertEqualObjects(rmfAPPlugin, [[RMFAPPlugin alloc] init]);
	}
}

#pragma mark - AP list
/*
 * If response block is nil then apList should not throw any exception
 */
- (void)testAPListScenario1 {
	if ([RMFSystemManager sharedRMFSystemManager]) {
		NSDictionary *requestParameters = [[NSDictionary alloc] initWithObjectsAndKeys:@"kumomobile", kTenantNameKey, @"default", kVenueNameKey, nil];
		XCTAssertNoThrow([self.rmfAPPlugin apList:requestParameters response:nil]);
	}
}

/*
 * If request parameter is not nil then apList should return the response object
 */
- (void)testAPListScenario2 {
	if ([RMFSystemManager sharedRMFSystemManager]) {
		__block XCTestExpectation *expectation = [self expectationWithDescription:kAPListLoaded];
		NSDictionary *requestParameters = [[NSDictionary alloc] initWithObjectsAndKeys:@"kumomobile", kTenantNameKey, @"default", kVenueNameKey, nil];

		[self.rmfAPPlugin apList:requestParameters response: ^(NSError *error, id data) {
		    if ([self isInternetConnectionError:error]) {
		        XCTAssertNotNil(error);
			}
		    else {
		        XCTAssertNotNil(data);
		        XCTAssertNil(error);
			}
		    [expectation fulfill];
		}];

		[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
		    XCTAssertNil(error);
		    expectation = nil;
		}];
	}
}

#pragma mark - AP list with sorting and filtring
/*
 * If response block is nil then apList should not throw any exception
 */
- (void)testAPListWithSortAndFilterScenario1 {
	if ([RMFSystemManager sharedRMFSystemManager]) {
		NSDictionary *requestParameters = [[NSDictionary alloc] initWithObjectsAndKeys:@"kumomobile", kTenantNameKey, @"default", kVenueNameKey, nil];
		XCTAssertNoThrow([self.rmfAPPlugin apList:requestParameters sortBy:kSortingParameterNone filterOn:[NSDictionary dictionary] response:nil]);
	}
}

/*
 * If request parameter is not nil then apList should return the response object
 */
- (void)testAPListWithSortAndFilterScenario2 {
	if ([RMFSystemManager sharedRMFSystemManager]) {
		__block XCTestExpectation *expectation = [self expectationWithDescription:kAPListLoadedSortAndFilter];
		NSDictionary *requestParameters = [[NSDictionary alloc] initWithObjectsAndKeys:@"kumomobile", kTenantNameKey, @"default", kVenueNameKey, nil];

		[self.rmfAPPlugin apList:requestParameters sortBy:kSortingParameterNone filterOn:[NSDictionary dictionary] response: ^(NSError *error, id data) {
		    if ([self isInternetConnectionError:error]) {
		        XCTAssertNotNil(error);
			}
		    else {
		        XCTAssertNotNil(data);
		        XCTAssertNil(error);
			}
		    [expectation fulfill];
		}];
		[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
		    XCTAssertNil(error);
		    expectation = nil;
		}];
	}
}

#pragma mark - AP details

/*
 * If response block is nil then apDetails should not throw any exception
 */
- (void)testAPDetailsScenario1 {
	if ([RMFSystemManager sharedRMFSystemManager]) {
		NSDictionary *requestParameters = [[NSDictionary alloc] initWithObjectsAndKeys:@"kumomobile", kTenantNameKey, @"default", kVenueNameKey, nil];
		XCTAssertNoThrow([self.rmfAPPlugin apDetail:requestParameters response:nil]);
	}
}

/*
 * If request parameter is not nil then apDetails should return the response object
 */
- (void)testAPDetailsScenario2 {
	if ([RMFSystemManager sharedRMFSystemManager]) {
		__block XCTestExpectation *expectation = [self expectationWithDescription:kAPDetailsLoaded];
		NSDictionary *requestParameters = [[NSDictionary alloc] initWithObjectsAndKeys:@"kumomobile", kTenantNameKey, @"default", kVenueNameKey, @"161205002059", kSerialNumberKey, nil];

		[self.rmfAPPlugin apDetail:requestParameters response: ^(NSError *error, id data) {
		    if ([self isInternetConnectionError:error]) {
		        XCTAssertNotNil(error);
			}
		    else if (data) {
		        XCTAssertNotNil(data);
			}
		    [expectation fulfill];
		}];

		[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
		    expectation = nil;
		}];
	}
}

#pragma mark - create AP

/*
 * If response block is nil then createAP should not throw any exception
 */
- (void)testcreateAPScenario1 {
	if ([RMFSystemManager sharedRMFSystemManager]) {
		NSDictionary *requestParameters = [[NSDictionary alloc] initWithObjectsAndKeys:@"kumomobile", kTenantNameKey, @"default", kVenueNameKey, nil];
		XCTAssertNoThrow([self.rmfAPPlugin createAP:requestParameters response:nil]);
	}
}

/*
 * If request parameter is not nil then createAP should return the response object
 */
- (void)testcreateAPScenario2 {
	if ([RMFSystemManager sharedRMFSystemManager]) {
		__block XCTestExpectation *expectation = [self expectationWithDescription:kAPCreated];
		NSDictionary *requestParameters = [[NSDictionary alloc] initWithObjectsAndKeys:@"kumomobile", kTenantNameKey, @"default", kVenueNameKey, @"111111111111", kSerialNumberKey, nil];

		[self.rmfAPPlugin createAP:requestParameters response: ^(NSError *error, id data) {
		    if ([self isInternetConnectionError:error]) {
		        XCTAssertNotNil(error);
			}
		    else if (data) {
		        XCTAssertNotNil(data);
			}
		    [expectation fulfill];
		}];

		[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
		    XCTAssertNil(error);
		    expectation = nil;
		}];
	}
}

#pragma mark - edit AP
/*
 * If response block is nil then editAP should not throw any exception
 */
- (void)testEditAPScenario1 {
	if ([RMFSystemManager sharedRMFSystemManager]) {
		NSDictionary *requestParameters = [[NSDictionary alloc] initWithObjectsAndKeys:@"kumomobile", kTenantNameKey, @"default", kVenueNameKey, nil];
		XCTAssertNoThrow([self.rmfAPPlugin editAP:requestParameters response:nil]);
	}
}

/*
 * If request parameter is not nil then editAP should return the response object
 */
- (void)testEditAPScenario2 {
	if ([RMFSystemManager sharedRMFSystemManager]) {
		__block XCTestExpectation *expectation = [self expectationWithDescription:kAPEdited];
		NSDictionary *requestParameters = [[NSDictionary alloc] initWithObjectsAndKeys:@"kumomobile", kTenantNameKey, @"default", kVenueNameKey, @"111111111111", kSerialNumberKey, nil];

		[self.rmfAPPlugin editAP:requestParameters response: ^(NSError *error, id data) {
		    if ([self isInternetConnectionError:error]) {
		        XCTAssertNotNil(error);
			}
		    else if (data) {
		        XCTAssertNotNil(data);
			}
		    [expectation fulfill];
		}];
		[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
		    XCTAssertNil(error);
		    expectation = nil;
		}];
	}
}

#pragma mark - delete AP
/*
 * If response block is nil then deleteAP should not throw any exception
 */
- (void)testDeleteAPScenario1 {
	if ([RMFSystemManager sharedRMFSystemManager]) {
		NSDictionary *requestParameters = [[NSDictionary alloc] initWithObjectsAndKeys:@"kumomobile", kTenantNameKey, @"default", kVenueNameKey, nil];
		XCTAssertNoThrow([self.rmfAPPlugin deleteAP:requestParameters response:nil]);
	}
}

/*
 * If request parameter is not nil then deleteAP should return the response object
 */
- (void)testDeleteAPScenario2 {
	if ([RMFSystemManager sharedRMFSystemManager]) {
		__block XCTestExpectation *expectation = [self expectationWithDescription:kAPDeleted];
		NSDictionary *requestParameters = [[NSDictionary alloc] initWithObjectsAndKeys:@"kumomobile", kTenantNameKey, @"default", kVenueNameKey, @"111111111111", kSerialNumberKey, nil];

		[self.rmfAPPlugin deleteAP:requestParameters response: ^(NSError *error, BOOL success) {
		    if ([self isInternetConnectionError:error]) {
		        XCTAssertNotNil(error);
			}
		    else {
		        XCTAssertNotNil(error);
			}
		    [expectation fulfill];
		}];
		[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
		    XCTAssertNil(error);
		    expectation = nil;
		}];
	}
}

#pragma mark - allKeyForRequestParameter

/*
 *  allKeyForRequestParameter should not be nil
 */
- (void)testAllKeyForRequestParameterScenario {
	if ([RMFSystemManager sharedRMFSystemManager]) {
		XCTAssertNotNil([self.rmfAPPlugin allKeyForRequestParameter]);
	}
}

- (BOOL)isInternetConnectionError:(NSError *)error {
	if (OBJ_NOT_NIL(error) && OBJ_NOT_NIL(error.userInfo) && [[error.userInfo objectForKey:kLocalizedDescription] containsString:kCouldNotConnectToServer]) {
		return YES;
	}
	return NO;
}

#pragma mark - Download AP image

/*
 * If request parameter and response block are available then downloadAPImage should return
 * AP image
 */

- (void)testDownloadAPImageScenario1 {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:kImageUrlkey forKey:@"ap/image"];
	// TODO: update this test case once the network request to fetch AP image is implementetd
}

/*
 * If request parameter and response block are not available then downloadAPImage should not throw
 * any exception
 */

- (void)testDownloadAPImageScenario2 {
	XCTAssertNoThrow([self.rmfAPPlugin downloadAPImage:nil response:nil]);
}

/*
 * If request parameter is available and response block is not available then downloadAPImage should
 * not throw any exception
 */

- (void)testDownloadAPImageScenario3 {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:kImageUrlkey forKey:@"ap/image"];
	XCTAssertNoThrow([self.rmfAPPlugin downloadAPImage:requestParameters response:nil]);
}

/*
 * If request parameter is not available but response block is available then downloadAPImage should
 * return NSError object with description "Request parameters not found"
 */

- (void)testDownloadAPImageScenario4 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPImageDownloaded];
	[self.rmfAPPlugin downloadAPImage:nil response: ^(NSError *error, id data) {
	    XCTAssertEqual(kKUMOCodeForImageUrlNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter and response block are available but the request parameter does not
 * contain imageUrl then downloadAPImage should return NSError object with description
 * "KUMO Error: Image url is required"
 */

- (void)testDownloadAPImageScenario5 {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];

	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPImageDownloaded];
	[self.rmfAPPlugin downloadAPImage:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertEqual(kKUMOCodeForImageUrlNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter and response block are available then uploadAPImage should upload
 * AP image
 */

- (void)testUploadAPImageScenario1 {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:kImageUrlkey forKey:@"ap/image"];
	// TODO: update this test case once the network request to upload AP image is implementetd
}

/*
 * If request parameter and response block are not available then uploadAPImage should not throw
 * any exception
 */

- (void)testUploadAPImageScenario2 {
	XCTAssertNoThrow([self.rmfAPPlugin uploadAPImage:nil response:nil]);
}

/*
 * If request parameter is available and response block is not available then uploadAPImage should
 * not throw any exception
 */

- (void)testUploadAPImageScenario3 {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:kImageUrlkey forKey:@"ap/image"];
	XCTAssertNoThrow([self.rmfAPPlugin uploadAPImage:requestParameters response:nil]);
}

/*
 * If request parameter is not available but response block is available then uploadAPImage should
 * return NSError object with description "Request parameters not found"
 */

- (void)testUploadAPImageScenario4 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPImageDownloaded];
	[self.rmfAPPlugin uploadAPImage:nil response: ^(NSError *error, id data) {
	    XCTAssertEqual(kKUMOCodeForImageAndUrlNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter and response block are available but the request parameter does not
 * contain imageUrl then uploadAPImage should return NSError object with description
 * "KUMO Error: Image url is required"
 */

- (void)testUploadAPImageScenario5 {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];

	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPImageDownloaded];
	[self.rmfAPPlugin uploadAPImage:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertEqual(kKUMOCodeForImageAndUrlNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

@end
