/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFWlanService.h"

@interface RMFWlanServiceTest : XCTestCase
@property (nonatomic, strong) RMFWlanService *wlanService;
@end

@implementation RMFWlanServiceTest

- (void)setUp {
	[super setUp];

	// Create an instance of RMFWlanService
	self.wlanService = [[RMFWlanService alloc] init];
}

- (void)tearDown {
	[super tearDown];
}

// Test if RMFWlanService instance is not nil
- (void)testRMFWlanServiceObject {
	XCTAssertNotNil(self.wlanService);
}

// Property name should store/return a string value
- (void)testName {
	NSString *testName = @"test";

	if (self.wlanService) {
		self.wlanService.name = testName;
		XCTAssertEqualObjects(self.wlanService.name, testName);
	}
}

// Property ssid should store/return a string value
- (void)testSssid {
	NSString *testSsid = @"ssid";

	if (self.wlanService) {
		self.wlanService.ssid = testSsid;
		XCTAssertEqualObjects(self.wlanService.ssid, testSsid);
	}
}

// Property wlanType should store/return a string value
- (void)testWlanType {
	NSString *testWlanType = @"wlantype";

	if (self.wlanService) {
		self.wlanService.wlanType = testWlanType;
		XCTAssertEqualObjects(self.wlanService.wlanType, testWlanType);
	}
}

// Property wlanSecurity should store/return a string value
- (void)testWlanSecurity {
	NSString *testWlanSecurity = @"wlansecurity";

	if (self.wlanService) {
		self.wlanService.wlanSecurity = testWlanSecurity;
		XCTAssertEqualObjects(self.wlanService.wlanSecurity, testWlanSecurity);
	}
}

// Property pskPassPhrase should store/return a string value
- (void)testPskPassPhrase {
	NSString *testPskPassPhrase = @"pskPassPhrase";

	if (self.wlanService) {
		self.wlanService.pskPassPhrase = testPskPassPhrase;
		XCTAssertEqualObjects(self.wlanService.pskPassPhrase, testPskPassPhrase);
	}
}

// Property wlanAdvCustmization should store/return an object of type RMFWlanAdvNwCustomization
- (void)testWlanAdvCustmization {
	RMFWlanAdvNwCustomization *testWlanAdvCustmization = [[RMFWlanAdvNwCustomization alloc] init];

	if (self.wlanService) {
		self.wlanService.wlanAdvCustmization = testWlanAdvCustmization;
		XCTAssertEqualObjects(self.wlanService.wlanAdvCustmization, testWlanAdvCustmization);
	}
}

// Property wlanBasicNwCustomization should store/return an object of type RMFWlanBasicNwCustomization
- (void)testWlanBasicNwCustomization {
	RMFWlanBasicNwCustomization *testWlanBasicNwCustomization = [[RMFWlanBasicNwCustomization alloc] init];

	if (self.wlanService) {
		self.wlanService.wlanBasicNwCustomization = testWlanBasicNwCustomization;
		XCTAssertEqualObjects(self.wlanService.wlanBasicNwCustomization, testWlanBasicNwCustomization);
	}
}

@end
