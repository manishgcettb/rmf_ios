/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */


#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "RMFMacro.h"
#import "RMFConfiguration.h"
#import "RMFSystemManager.h"

#import "RMFAPClientPlugin.h"

#define TEST_PORT                   @"8080"
#define TEST_IP                     @"10.199.4.106"

#define  kClientListLoaded     @"Client list should be loaded"
#define  kClientDetailsLoaded  @"Client details should be loaded"


@interface RMFClientPluginTest : XCTestCase

// Stores the instance of RMFConfiguration
@property (nonatomic, strong) RMFConfiguration *configuration;

// Stores the instance of RMFclientPlugin
@property (nonatomic, strong) RMFAPClientPlugin *clientPlugin;

// Stores the request parameters
@property (nonatomic, strong) NSMutableDictionary *requestParameters;

@end

@implementation RMFClientPluginTest

- (void)setUp {
	[super setUp];
	// Put setup code here. This method is called before the invocation of each test method in the class.
	[self initialConfiguration];
	self.clientPlugin = [RMFAPClientPlugin sharedRMFClientPlugin];
}

- (void)tearDown {
	// Put teardown code here. This method is called after the invocation of each test method in the class.
	[super tearDown];
}

/*
 * initialise RMFConfiguration instance with system type, system IP and system port
 */
- (void)initialConfiguration {
	self.configuration = [RMFConfiguration sharedRMFConfiguration];
	self.configuration.system = kSystemTypeKUMO;
	self.configuration.systemIP = TEST_IP;
	self.configuration.port = TEST_PORT;
}

/*
 * initialise request parameter dictionary
 */
- (void)initRequestParameters {
	self.requestParameters = [[NSMutableDictionary alloc] init];
	if (self.requestParameters) {
		[self.requestParameters setObject:@"default" forKey:kVenueNameKey];
		[self.requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];
		[self.requestParameters setObject:@"161205002059" forKey:kSerialNumberKey];
		[self.requestParameters setObject:@"kumomobile" forKey:kWlanNameKey];
		[self.requestParameters setObject:@"20:C9:D0:8C:68:9F" forKey:kMacAddressInfoKey];
	}
}

/*
 * testSharedRMFClientPlugin method should throw exception if RMFConfiguration
 * instance is not available
 */
- (void)testSharedRMFAPPluginScenario {
	if (![RMFConfiguration sharedRMFConfiguration]) {
		XCTAssertThrows([RMFAPClientPlugin sharedRMFClientPlugin]);
	}
}

/*
 * init method should return the instance of RMFClientPlugin instance if called with alloc
 */
- (void)testInitScenario1 {
	XCTAssertNotNil([[RMFAPClientPlugin alloc] init]);
}

/*
 * When called with alloc init method should return the same instance of RMFClientPlugin
 * which is created by using sharedRMFClientPlugin
 */
- (void)testInitScenario2 {
	RMFAPClientPlugin *rmfClientPlugin = [RMFAPClientPlugin sharedRMFClientPlugin];
	if (rmfClientPlugin) {
		XCTAssertEqualObjects(rmfClientPlugin, [[RMFAPClientPlugin alloc] init]);
	}
}

#pragma mark - Client list
/*
 * If request parameter dictionary and response block are nil then client list should not throw any
 * exception
 */

- (void)testClientListScenario1 {
	XCTAssertNoThrow([self.clientPlugin clientList:nil response:nil]);
}

/*
 * If request parameter dictionary is available but the response block is nil then client
 * should not throw any exception
 */

- (void)testClientListScenario2 {
	XCTAssertNoThrow([self.clientPlugin clientList:self.requestParameters response:nil]);
}

/*
 * If request parameter dictionary is nil but the response block is available then client list should
 * return NSError object with description "Request parameters not found"
 */

- (void)testClientListScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kClientListLoaded];

	[self.clientPlugin clientList:nil response: ^(NSError *error, NSArray *clientList) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(clientList);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If correct request parameter dictionary and response block are available then clientList should
 * return list of wlan
 */
- (void)testClientListScenario4 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kClientListLoaded];

	[self.clientPlugin clientList:self.requestParameters response: ^(NSError *error, NSArray *clientList) {
	    if (OBJ_NOT_NIL(error)) {
	        XCTAssertNotNil(error);
		}
	    else {
	        XCTAssertNil(clientList);
		}
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    if (error) {
	        XCTAssertNotNil(error);
		}
	    expectation = nil;
	}];
}

#pragma mark - active/blocked Client
/*
 * If request parameter dictionary and response block are nil then client list should not throw any
 * exception
 */
- (void)testClientListScenario5 {
	XCTAssertNoThrow([self.clientPlugin clientList:nil clientType:kClientTypeActive response:nil]);
}

/*
 * If request parameter dictionary is available but the response block is nil then client
 * should not throw any exception
 */
- (void)testClientListScenario6 {
	XCTAssertNoThrow([self.clientPlugin clientList:self.requestParameters clientType:kClientTypeActive response:nil]);
}

/*
 * If request parameter dictionary is nil, clientType is kClientTypeActive but the response block is available then client list should return NSError object with description "Request parameters not found"
 */
- (void)testClientListScenario7 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kClientListLoaded];

	[self.clientPlugin clientList:nil clientType:kClientTypeActive response: ^(NSError *error, NSArray *clientList) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(clientList);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary is nil, clientType is kClientTypeBlocked but the response block is available then client list should return NSError object with description "Request parameters not found"
 */
- (void)testClientListScenario8 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kClientListLoaded];

	[self.clientPlugin clientList:nil clientType:kClientTypeBlocked response: ^(NSError *error, NSArray *clientList) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(clientList);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

#pragma mark - Client details

/*
 * If request parameter dictionary and response block are nil then clientDetails should not throw any
 * exception
 */
- (void)testclientDetailsScenario1 {
	XCTAssertNoThrow([self.clientPlugin clientDetails:nil response:nil]);
}

/*
 * If request parameter dictionary is available but the response block is nil then clientDetails
 * should not throw any exception
 */

- (void)testclientDetailsScenario2 {
	XCTAssertNoThrow([self.clientPlugin clientDetails:self.requestParameters response:nil]);
}

/*
 * If request parameter dictionary is nil but the response block is available then clientDetails should
 * return NSError object with description "Request parameters not found"
 */

- (void)testclientDetailsScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kClientDetailsLoaded];

	[self.clientPlugin clientDetails:nil response: ^(NSError *error, RMFClientDetail *clientDetail) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(clientDetail);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If correct request parameters and response block are available then clientDetails should
 * return the instance of RMFClientDetail initialise with parsed data
 */
- (void)testclientDetailsScenario4 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kClientDetailsLoaded];

	[self.clientPlugin clientDetails:self.requestParameters response: ^(NSError *error, RMFClientDetail *clientDetail) {
	    if (OBJ_NOT_NIL(error)) {
	        XCTAssertNotNil(error);
		}
	    else {
	        XCTAssertNil(clientDetail);
		}
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

#pragma mark - AP list with sorting and filtring
/*
 * If response block is nil then apList should not throw any exception
 */
- (void)testClientListWithSortAndFilterScenario1 {
	if ([RMFSystemManager sharedRMFSystemManager]) {
		XCTAssertNoThrow([self.clientPlugin clientListFilteredBy:self.requestParameters sortBy:[NSArray array] limit:nil offset:nil response:nil]);
	}
}

/*
 * If request parameter is not nil then apList should return the response object
 */
- (void)testClientListWithSortAndFilterScenario2 {
	if ([RMFSystemManager sharedRMFSystemManager]) {
		__block XCTestExpectation *expectation = [self expectationWithDescription:kClientListLoaded];

		[self.clientPlugin clientListFilteredBy:self.requestParameters sortBy:nil limit:[NSNumber numberWithInt:5] offset:[NSNumber numberWithInt:1] response: ^(NSError *error, id data)
		{
		    if (OBJ_NOT_NIL(error)) {
		        XCTAssertNotNil(error);
			}
		    else {
		        XCTAssertNotNil(data);
		        XCTAssertNil(error);
			}
		    [expectation fulfill];
		}];
		[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
		    XCTAssertNil(error);
		    expectation = nil;
		}];
	}
}

@end
