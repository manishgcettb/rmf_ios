/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "RMFRootAssociationPlugin.h"
#import "RMFConfiguration.h"
#import "RMFMacro.h"

@interface RMFRootAssociationPluginTest : XCTestCase {
	RMFRootAssociationPlugin *rootPlugin;
}
@end

@implementation RMFRootAssociationPluginTest

- (void)setUp {
	[super setUp];
	// Put setup code here. This method is called before the invocation of each test method in the class.
	[[RMFConfiguration sharedRMFConfiguration] setSystem:kSystemTypeKUMO];
	[[RMFConfiguration sharedRMFConfiguration] setSystemIP:@"10.199.4.106"];
	[[RMFConfiguration sharedRMFConfiguration] setPort:@"8080"];
	rootPlugin = [RMFRootAssociationPlugin sharedRMFRootAssociationPlugin];
}

- (void)tearDown {
	// Put teardown code here. This method is called after the invocation of each test method in the class.
	[super tearDown];
}

- (void)testfetchParentAssoicationTenant {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];
	[rootPlugin fetchParentAssoication:(KUMOSystem)kTypeTenant root:requestParameters response: ^(NSError *error, NSDictionary *associationList) {
	    XCTAssertNil(error);
	}];
}

- (void)testfetchParentAssoicationNilTentant {
	[rootPlugin fetchParentAssoication:(KUMOSystem)kTypeTenant root:nil response: ^(NSError *error, NSDictionary *associationList) {
	    XCTAssertNotNil(error);
	}];
}

- (void)testfetchParentAssoicationVar {
	[rootPlugin fetchParentAssoication:(KUMOSystem)kTypeVAR root:nil response: ^(NSError *error, NSDictionary *associationList) {
	    XCTAssertNil(error);
	}];
}

- (void)testfetchParentAssoicationNilVar {
	[rootPlugin fetchParentAssoication:(KUMOSystem)kTypeVAR root:nil response: ^(NSError *error, NSDictionary *associationList) {
	    XCTAssertNotNil(error);
	}];
}

- (void)testfetchParentAssoicationVenueWLAN {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];
	[requestParameters setObject:@"kumomobile" forKey:WLAN];
	[rootPlugin fetchParentAssoication:(KUMOSystem)kTypeVenue root:requestParameters response: ^(NSError *error, NSDictionary *associationList) {
	    if (OBJ_NOT_NIL(error)) {
	        XCTAssertNotNil(error);
		}
	    else {
	        XCTAssertNil(error);
	        XCTAssertNotNil(associationList);
		}
	}];
}

- (void)testfetchParentAssoicationVenueNilWLAN {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:WLAN];
	[rootPlugin fetchParentAssoication:(KUMOSystem)kTypeVenue root:requestParameters response: ^(NSError *error, NSDictionary *associationList) {
	    XCTAssertNotNil(error);
	}];
}

- (void)testfetchParentAssoicationVenueAP {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];
	[requestParameters setObject:@"kumomobile" forKey:kVenueNameKey];

	[requestParameters setObject:@"kumomobile" forKey:kAPKey];
	[rootPlugin fetchParentAssoication:(KUMOSystem)kTypeVenue root:requestParameters response: ^(NSError *error, NSDictionary *associationList) {
	    if (OBJ_NOT_NIL(error)) {
	        XCTAssertNotNil(error);
		}
	    else {
	        XCTAssertNil(error);
	        XCTAssertNotNil(associationList);
		}
	}];
}

- (void)testfetchParentAssoicationVenueNilAP {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:kAPKey];
	[rootPlugin fetchParentAssoication:(KUMOSystem)kTypeVenue root:requestParameters response: ^(NSError *error, NSDictionary *associationList) {
	    XCTAssertNotNil(error);
	}];
}

- (void)testRefreshAssociation {
	[rootPlugin refreshforRoot:@"kumomobile"];
}

- (void)testClearAssociation {
	[rootPlugin clear];
}

@end
