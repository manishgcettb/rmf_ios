/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFBasePlugin.h"
#import "RMFSystemManager.h"
#import "RMFConfiguration.h"
#import "RMFKUMOController.h"
#import "RMFSCGController.h"
#import "RMFZDController.h"
#import "RMFKUMOParser.h"
#import "RMFZDParser.h"
#import "RMFSCGParser.h"

#define TEST_PORT                   @"8080"
#define TEST_IP                     @"10.199.4.106"

@interface RMFBasePluginTest : XCTestCase

// Stores/returns the instance of RMFBasePlugin
@property (nonatomic, strong) RMFBasePlugin *basePlugin;

// Stores/returns the instance of RMFSystemManager
@property (nonatomic, strong) RMFSystemManager *systemManager;

// Stores/returns the instance of RMFConfiguration
@property (nonatomic, strong) RMFConfiguration *configuration;

@end

@implementation RMFBasePluginTest

- (void)setUp {
	[super setUp];
	[self initialConfiguration];
	self.systemManager = [RMFSystemManager sharedRMFSystemManager];
	self.basePlugin = [[RMFBasePlugin alloc] init];
}

- (void)tearDown {
	[super tearDown];
}

/*
 * initialise RMFConfiguration instance with system ip and system type and port number
 */
- (void)initialConfiguration {
	self.configuration = [RMFConfiguration sharedRMFConfiguration];
	self.configuration.systemIP = TEST_IP;
	self.configuration.port = TEST_PORT;
	self.configuration.system = kSystemTypeKUMO;
}

/*
 * Checks if base plugin instance is available or not
 */

- (void)testRMFBasePlugin {
	XCTAssertNotNil(self.basePlugin);
}

@end
