/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "RMFWifiManager.h"
@interface RMFWifiManagerTest : XCTestCase
@end


@implementation RMFWifiManagerTest
RMFWifiManager * wifiManager;


- (void)setUp {
	[super setUp];
	// Put setup code here. This method is called before the invocation of each test method in the class.
	wifiManager = [RMFWifiManager sharedInstance];
}

- (void)tearDown {
	// Put teardown code here. This method is called after the invocation of each test method in the class.
	[super tearDown];
}

- (void)testSimCardAvailable {
	XCTAssertFalse([wifiManager isSimCardAvailable]);
}



-(void)testisInetnetConnectedViaWifi
{
    XCTAssertNil([wifiManager internetConnectedViaWifi:ReachableViaWiFi]);
}

-(void)testisInetnetConnectedViaWWAN
{
    XCTAssertNil([wifiManager internetConnectedViaWWAN:ReachableViaWWAN]);
}

- (void)testisWiFiEnabled {
    // just checking the enabling of wi-fi
    if ([wifiManager isWiFiEnabled]) {
        XCTAssertTrue([wifiManager isWiFiEnabled]);
    } else {
        XCTAssertFalse([wifiManager isWiFiEnabled]);
    }
}



@end
