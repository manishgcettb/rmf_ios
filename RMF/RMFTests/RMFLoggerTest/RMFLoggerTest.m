/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */


#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

#import "RMFlogger.h"
#import "RMFMacro.h"

@interface RMFLoggerTest : XCTestCase

@property (nonatomic, strong) RMFLogger *rmfLogger;

@end

@implementation RMFLoggerTest

- (void)setUp {
	[super setUp];
	// Put setup code here. This method is called before the invocation of each test method in the class.

	self.rmfLogger = [RMFLogger sharedRMFLogger];
}

- (void)tearDown {
	// Put teardown code here. This method is called after the invocation of each test method in the class.
	[super tearDown];
}

// Test RMFLogger object for nil and validate it
- (void)testSessionProviderObjectForNil {
	XCTAssertNotNil(self.rmfLogger);
	XCTAssertTrue([self.rmfLogger isKindOfClass:[RMFLogger class]]);
}

// Test property isRecording
- (void)testIsRecordingLogs {
	if (OBJ_NOT_NIL(self.rmfLogger)) {
		self.rmfLogger.isRecordingLogs = false;
		XCTAssertFalse(self.rmfLogger.isRecordingLogs);
	}
	else {
		self.rmfLogger.isRecordingLogs = true;
		XCTAssertTrue(self.rmfLogger.isRecordingLogs);
	}
}

- (void)testDisableRMFLogger {
	XCTAssertNoThrow([self.rmfLogger disableRMFLogger]);
}

- (void)testEnableRMFLogger {
	XCTAssertNoThrow([self.rmfLogger disableRMFLogger]);
}

- (void)testRetrieveLogFilePaths {
	XCTAssertNoThrow([self.rmfLogger retrieveLogFilePaths]);
}

@end
