/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */


#import <XCTest/XCTest.h>
#import "RMFWlan.h"

@interface RMFWlanTest : XCTestCase
@property (nonatomic, strong) RMFWlan *Wlan;
@end



@implementation RMFWlanTest

- (void)setUp {
	[super setUp];
	self.Wlan = [[RMFWlan alloc] init];
}

- (void)tearDown {
	[super tearDown];
}

// Test if RMFWlan instance is not nil
- (void)testRMFWlanObject {
	XCTAssertNotNil(self.Wlan);
}

// Property bssid should store/return the string
- (void)testBssid {
	NSString *testString = @"abcdfffff";

	if (self.Wlan) {
		self.Wlan.bssid = testString;
		XCTAssertEqualObjects(self.Wlan.bssid, testString);
	}
}

@end
