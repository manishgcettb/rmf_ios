/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFWlanPlugin.h"
#import "RMFMacro.h"
#import "RMFConfiguration.h"
#import "RMFSystemManager.h"
#import "RMFWlanServiceStatus.h"
#import "RMFWlanService.h"

#define TEST_PORT                   @"8080"
#define TEST_IP                     @"10.199.4.106"

#define  kWlanDeleted                @"WLAN should be deleted"
#define  kWlanUpdated                @"WLAN information should be updated"
#define  kNewWlanCreated             @"New WLAN should be created"
#define  kWlanListLoaded     @"Wlan service list should be loaded"
#define  kWlanStatusLoaded   @"Wlan service status should be loaded"
#define  kWlanDetailsLoaded  @"Wlan service details should be loaded"

@interface RMFWlanPluginTest : XCTestCase

// Stores the instance of RMFConfiguration
@property (nonatomic, strong) RMFConfiguration *configuration;

// Stores the instance of RMFWlanPlugin
@property (nonatomic, strong) RMFWlanPlugin *wlanPlugin;

// Stores the request parameters
@property (nonatomic, strong) NSMutableDictionary *requestParameters;

@end

@implementation RMFWlanPluginTest

- (void)setUp {
	[super setUp];
	[self initialConfiguration];
	self.wlanPlugin = [RMFWlanPlugin sharedRMFWlanPlugin];
	[self initRequestParameters];
}

- (void)tearDown {
	[super tearDown];
}

/*
 * initialise RMFConfiguration instance with system type, system IP and system port
 */
- (void)initialConfiguration {
	self.configuration = [RMFConfiguration sharedRMFConfiguration];
	self.configuration.system = kSystemTypeKUMO;
	self.configuration.systemIP = TEST_IP;
	self.configuration.port = TEST_PORT;
}

/*
 * initialise request parameter dictionary
 */
- (void)initRequestParameters {
	self.requestParameters = [[NSMutableDictionary alloc] init];
	if (self.requestParameters) {
		[self.requestParameters setObject:@"default" forKey:kVenueNameKey];
		[self.requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];
		[self.requestParameters setObject:@"161205002059" forKey:kSerialNumberKey];
		[self.requestParameters setObject:@"kumomobile" forKey:kWlanNameKey];
		[self.requestParameters setObject:@"20:C9:D0:8C:68:9F" forKey:kMacAddressInfoKey];
	}
}

/*
 *  sharedRMFWlanPlugin should return a valid instance
 */
- (void)testSharedRMFWlanPlugin {
	XCTAssertNotNil(self.wlanPlugin);
}

/*
 * [[RMFWlanPlugin alloc] init] should return a valid instance
 */
- (void)testSharedRMFWlanPluginUsingAlloc {
	XCTAssertNotNil([[RMFWlanPlugin alloc] init]);
}

/*
 * [[RMFWlanPlugin alloc] init] and sharedRMFWlanPlugin should return same instance
 */
- (void)testIsSharedRMFScgParserAndAllocReturnsSameObject {
	XCTAssertEqual([[RMFWlanPlugin alloc] init], self.wlanPlugin);
}

#pragma mark - Wlan list
/*
 * If request parameter dictionary and response block are nil then wlanServiceList should not throw any
 * exception
 */

- (void)testwlanListScenario1 {
	XCTAssertNoThrow([self.wlanPlugin wlanList:nil response:nil]);
}

/*
 * If request parameter dictionary is available but the response block is nil then wlanList
 * should not throw any exception
 */

- (void)testwlanListScenario2 {
	XCTAssertNoThrow([self.wlanPlugin wlanList:self.requestParameters response:nil]);
}

/*
 * If request parameter dictionary is nil but the response block is available then wlanList should
 * return NSError object with description "Request parameters not found"
 */

- (void)testwlanListScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanListLoaded];

	[self.wlanPlugin wlanList:nil response: ^(NSError *error, NSArray *wlanList) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(wlanList);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If correct request parameter dictionary and response block are available then wlanList should
 * return list of wlan
 */
- (void)testwlanListScenario4 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanListLoaded];

	[self.wlanPlugin wlanList:self.requestParameters response: ^(NSError *error, NSArray *wlanList) {
	    if (error) {
	        XCTAssertNotNil(error);
		}
	    else {
	        XCTAssertNil(error);
	        XCTAssertNotNil(wlanList);
		}
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    if (error) {
	        XCTAssertNotNil(error);
		}
	    expectation = nil;
	}];
}

#pragma mark - Wlan count
/*
 * If request parameter dictionary and response block are nil then wlanCount should not throw any
 * exception
 */
- (void)testWlanCountScenario1 {
	XCTAssertNoThrow([self.wlanPlugin wlanCount:nil response:nil]);
}

/*
 * If request parameter dictionary is available but the response block is nil then wlanCount
 * should not throw any exception
 */

- (void)testWlanCountScenario2 {
	XCTAssertNoThrow([self.wlanPlugin wlanCount:self.requestParameters response:nil]);
}

/*
 * If request parameter dictionary is nil but the response block is available then wlanCount should
 * return NSError object with description "Request parameters not found"
 */

- (void)testWlanCountScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanListLoaded];

	[self.wlanPlugin wlanCount:nil response: ^(NSError *error, NSInteger wlanCount) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertEqual(wlanCount, 0);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If correct request parameter dictionary and response block are available then wlanCount should
 * return list of wlan
 */
- (void)testWlanCountScenario4 {
	[self initialConfiguration];
	[self initRequestParameters];
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanListLoaded];

	[self.wlanPlugin wlanCount:self.requestParameters response: ^(NSError *error, NSInteger wlanCount) {
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

#pragma mark - wlan status
/*
 * If request parameter dictionary and response block are nil then wlanStatus should not throw any
 * exception
 */
- (void)testwlanStatusScenario1 {
	XCTAssertNoThrow([self.wlanPlugin wlanStatus:nil response:nil]);
}

/*
 * If request parameter dictionary is available but the response block is nil then wlanStatus
 * should not throw any exception
 */

- (void)testwlanStatusScenario2 {
	XCTAssertNoThrow([self.wlanPlugin wlanStatus:self.requestParameters response:nil]);
}

/*
 * If request parameter dictionary is nil but the response block is available then wlanStatus should
 * return NSError object with description "Request parameters not found"
 */

- (void)testwlanStatusScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanStatusLoaded];

	[self.wlanPlugin wlanStatus:nil response: ^(NSError *error, RMFWlanServiceStatus *wlanStatus) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(wlanStatus);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If correct request parameter dictionary and response block are available then wlanStatus should
 * return list of wlan
 */
- (void)testwlanStatusScenario4 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanStatusLoaded];

	[self.wlanPlugin wlanStatus:self.requestParameters response: ^(NSError *error, RMFWlanServiceStatus *wlanStatus) {
	    if (OBJ_NOT_NIL(error)) {
	        XCTAssertNotNil(error);
		}
	    else {
	        XCTAssertNil(error);
		}
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

#pragma mark - wlan details
/*
 * If request parameter dictionary and response block are nil then wlanDetails should not throw any
 * exception
 */
- (void)testwlanDetailsScenario1 {
	XCTAssertNoThrow([self.wlanPlugin wlanDetails:nil response:nil]);
}

/*
 * If request parameter dictionary is available but the response block is nil then wlanDetails
 * should not throw any exception
 */

- (void)testwlanDetailsScenario2 {
	XCTAssertNoThrow([self.wlanPlugin wlanDetails:self.requestParameters response:nil]);
}

/*
 * If request parameter dictionary is nil but the response block is available then wlanDetails should
 * return NSError object with description "Request parameters not found"
 */

- (void)testwlanDetailsScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanDetailsLoaded];

	[self.wlanPlugin wlanDetails:nil response: ^(NSError *error, RMFWlanService *wlan) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(wlan);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If correct request parameters and response block are available then wlanDetails should
 * return the instance of RMFwlan initialise with parsed data
 */
- (void)testwlanDetailsScenario4 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanDetailsLoaded];

	[self.wlanPlugin wlanDetails:self.requestParameters response: ^(NSError *error, RMFWlanService *wlan) {
	    if (OBJ_NOT_NIL(error)) {
	        XCTAssertNotNil(error);
		}
	    else {
	        XCTAssertNil(error);
		}
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

#pragma mark - wlan create

/*
 * If request parameter and response block are not available then createWlan should handle this
 * scenanrio without throwing any exception
 */

- (void)testCreateWlanScenario1 {
	XCTAssertNoThrow([self.wlanPlugin createWlan:nil response:nil]);
}

/*
 * If request parameter is available but response block is not available then createWlan should
 * handle this scenario without throwing any exception
 */

- (void)testCreateWlanScenario2 {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	XCTAssertNoThrow([self.wlanPlugin createWlan:requestParameters response:nil]);
}

/*
 * If request parameter is not available but response block is available then createWlan
 * should return NSError object with description "Request parameters not found"
 */

- (void)testCreateWlanScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kNewWlanCreated];

	[self.wlanPlugin createWlan:nil response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available but the request parameter dictionary
 * does not contain tenant name and new wlan information dictionary then createWlan should return
 * NSError object with description "Tenant name and new wlan information are required"
 */

- (void)testCreateWlanScenario4 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kNewWlanCreated];
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];

	[self.wlanPlugin createWlan:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndWlanInfoNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter dictionary
 * contains tenant name but does not contain new venue information dictionary then createWlan should
 * return NSError object with description "Tenant name and new wlan information are required"
 */

- (void)testCreateWlanScenario5 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kNewWlanCreated];
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];

	[self.wlanPlugin createWlan:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndWlanInfoNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

#pragma mark - wlan delete
/*
 * If the wlan name is available in the request parameter then WLAN entry should be deleted
 */
- (void)testDeleteWlanScenario1 {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"test" forKey:kWlanNameKey];
	//TODO: update this test case once delete wlan request is implemented
}

/*
 * If the wlan name is not available in the request parameter then deleteWlan method should return an error with description
 * "KUMO Error: Wlan name is required"
 */

- (void)testDeleteWlanScenario2 {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanDeleted];
	[self.wlanPlugin deleteWlan:requestParameters response: ^(NSError *error, id data) {
	    [expectation fulfill];
	    XCTAssertEqual(kKUMOCodeForWlanNameNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If the request parameter dictionary is not available in the request parameter then deleteWlan method should return an error
 * with description "KUMO Error: Request parameters not found"
 */

- (void)testDeleteWlanScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanDeleted];
	[self.wlanPlugin deleteWlan:nil response: ^(NSError *error, id data) {
	    [expectation fulfill];
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If the request parameter dictionary and response block are not available then deleteWlan should not throw any exception
 */
- (void)testDeleteWlanScenario4 {
	XCTAssertNoThrow([self.wlanPlugin deleteWlan:nil response:nil]);
}

#pragma mark - wlan edit
/*
 * If all the required parameters are available then wlan should be updated
 */

- (void)testEditWlanScenario1 {
	NSMutableDictionary *requestParams = [[NSMutableDictionary alloc] init];
	[requestParams setObject:kEmptyText forKey:kWlanNameKey];
	[requestParams setObject:kEmptyText forKey:kTenantNameKey];
	[requestParams setObject:kEmptyText forKey:kWlanSssidKey];
	[requestParams setObject:kEmptyText forKey:kWlanTypeKey];
	[requestParams setObject:kEmptyText forKey:kWlanSecurityKey];
	[requestParams setObject:kEmptyText forKey:kPskPassPhraseKey];

	//TODO: update this test case once edit wlan request is implemented
}

/*
 * If all the required parameters are not available then edit wlan method should return error with description
 * "KUMO Error: Tenant name and new wlan information are required"
 */

- (void)testEditWlanScenario2 {
	NSMutableDictionary *requestParams = [[NSMutableDictionary alloc] init];
	[requestParams setObject:kEmptyText forKey:kWlanNameKey];
	[requestParams setObject:kEmptyText forKey:kTenantNameKey];
	[requestParams setObject:kEmptyText forKey:kWlanSssidKey];
	[requestParams setObject:kEmptyText forKey:kWlanTypeKey];

	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanUpdated];
	[self.wlanPlugin editWlan:requestParams response: ^(NSError *error, id data) {
	    [expectation fulfill];
	    XCTAssertEqual(kKUMOCodeForTenantAndWlanInfoNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary is not available then edit wlan method should return error with description
 * "KUMO Error: Request parameters not found"
 */
- (void)testEditWlanScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanUpdated];
	[self.wlanPlugin editWlan:nil response: ^(NSError *error, id data) {
	    [expectation fulfill];
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If the request parameter dictionary and response block are not available then editWlan should not throw any exception
 */
- (void)testEditWlanScenario4 {
	XCTAssertNoThrow([self.wlanPlugin editWlan:nil response:nil]);
}

- (void)testWlanListScenario1 {
	XCTAssertNoThrow([self.wlanPlugin wlanList:nil sortBy:kWlanSortingParameterNone filterOn:nil response:nil]);
}

- (void)testWlanListScenario2 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanListLoaded];
	[self.wlanPlugin wlanList:nil sortBy:kWlanSortingParameterSsid filterOn:nil response: ^(NSError *error, id data) {
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

- (void)testWlanListScenario3 {
	NSMutableDictionary *requestParams = [[NSMutableDictionary alloc] init];

	[requestParams setObject:[NSNumber numberWithInt:2] forKey:kLimit];
	[requestParams setObject:[NSNumber numberWithInt:1] forKey:kOffset];

	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanListLoaded];
	[self.wlanPlugin wlanList:requestParams sortBy:kWlanSortingParameterSsid filterOn:nil response: ^(NSError *error, id data) {
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

- (void)testWlanListScenario4 {
	NSMutableDictionary *filter = [[NSMutableDictionary alloc] init];
	[filter setObject:@"kumomobile" forKey:kWlanNameKey];

	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanListLoaded];
	[self.wlanPlugin wlanList:nil sortBy:kWlanSortingParameterSsid filterOn:filter response: ^(NSError *error, id data) {
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

- (void)testWlanListScenario5 {
	// Create request parameter dictionary
	NSMutableDictionary *requestParams = [[NSMutableDictionary alloc] init];
	[requestParams setObject:[NSNumber numberWithInt:2] forKey:kLimit];
	[requestParams setObject:[NSNumber numberWithInt:1] forKey:kOffset];

	// Create filter dictionary
	NSMutableDictionary *filter = [[NSMutableDictionary alloc] init];
	[filter setObject:@"kumomobile" forKey:kWlanNameKey];

	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanListLoaded];

	// Make network request to fetch wlan list
	[self.wlanPlugin wlanList:requestParams sortBy:kWlanSortingParameterSsid filterOn:nil response: ^(NSError *error, id data) {
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

@end
