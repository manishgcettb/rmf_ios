/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFSessionProvider.h"
#import "RMFMacro.h"


@interface RMFSessionProviderTest : XCTestCase
@property (nonatomic, strong) RMFSessionProvider *rmfSessionProvider;

@end

@implementation RMFSessionProviderTest

- (void)setUp {
	[super setUp];
	// Put setup code here. This method is called before the invocation of each test method in the class.
	self.rmfSessionProvider = [RMFSessionProvider sharedSessionProvider];
}

- (void)tearDown {
	// Put teardown code here. This method is called after the invocation of each test method in the class.
	[super tearDown];
}

// Test RMFSessionProvider object for nil and validate it
- (void)testSessionProviderObjectForNil {
	XCTAssertNotNil(self.rmfSessionProvider);
	XCTAssertTrue([self.rmfSessionProvider isKindOfClass:[RMFSessionProvider class]]);
}

// Test property userName
- (void)testUserName {
	if (OBJ_NOT_NIL(self.rmfSessionProvider)) {
		self.rmfSessionProvider.userName = @"test";
		XCTAssertEqualObjects(@"test", self.rmfSessionProvider.userName);
	}
	else {
		XCTAssertNil(self.rmfSessionProvider.userName);
	}
}

// Test property token
- (void)testToken {
	if (OBJ_NOT_NIL(self.rmfSessionProvider)) {
		self.rmfSessionProvider.token = @"test";
		XCTAssertEqualObjects(@"test", self.rmfSessionProvider.token);
	}
	else {
		XCTAssertNil(self.rmfSessionProvider.token);
	}
}

// Test property lastLoginDateTime
- (void)testLastLoginDateTime {
	if (OBJ_NOT_NIL(self.rmfSessionProvider)) {
		self.rmfSessionProvider.lastLoginDateTime = @"test";
		XCTAssertEqualObjects(@"test", self.rmfSessionProvider.lastLoginDateTime);
	}
	else {
		XCTAssertNil(self.rmfSessionProvider.lastLoginDateTime);
	}
}

// Test property isSecureConnection
- (void)testIsSecureConnection {
	if (OBJ_NOT_NIL(self.rmfSessionProvider)) {
		XCTAssertFalse(self.rmfSessionProvider.isSecureConnection);
	}
	else {
		self.rmfSessionProvider.isSecureConnection = true;
		XCTAssertTrue(self.rmfSessionProvider.isSecureConnection);
	}
}

// Test property isSessionExists
- (void)testIsSessionExists {
	if (OBJ_NOT_NIL(self.rmfSessionProvider)) {
		XCTAssertFalse(self.rmfSessionProvider.isSessionExists);
	}
	else {
		self.rmfSessionProvider.isSessionExists = true;
		XCTAssertTrue(self.rmfSessionProvider.isSessionExists);
	}
}

@end
