/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFSummaryStatisticsPlugin.h"
#import "RMFConfiguration.h"
#import "RMFMacro.h"

#define TEST_PORT                   @"8080"
#define TEST_IP                     @"10.199.4.106"

@interface RMFSummaryStatisticsPluginTest : XCTestCase

// Stores the instance of RMFStatisticsPlugin
@property (nonatomic, strong) RMFSummaryStatisticsPlugin *summaryStatisticsPlugin;

// Stores the instance of RMFConfiguration
@property (nonatomic, strong) RMFConfiguration *configuration;

// Stores the request parameters
@property (nonatomic, strong) NSMutableDictionary *requestParameters;


@end

@implementation RMFSummaryStatisticsPluginTest

- (void)setUp {
	[super setUp];
	[self initialConfiguration];
	self.summaryStatisticsPlugin = [RMFSummaryStatisticsPlugin sharedRMFStatisticsPlugin];
	[self initRequestParameters];
}

- (void)tearDown {
	[super tearDown];
}

/*
 * initialise RMFConfiguration instance with system type, system IP and system port
 */
- (void)initialConfiguration {
	self.configuration = [RMFConfiguration sharedRMFConfiguration];
	self.configuration.system = kSystemTypeKUMO;
	self.configuration.systemIP = TEST_IP;
	self.configuration.port = TEST_PORT;
}

/*
 * initialise request parameter dictionary
 */
- (void)initRequestParameters {
	self.requestParameters = [[NSMutableDictionary alloc] init];
	if (self.requestParameters) {
		[self.requestParameters setObject:@"default" forKey:kVenueNameKey];
		[self.requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];
		[self.requestParameters setObject:@"161205002059" forKey:kSerialNumberKey];
		[self.requestParameters setObject:@"kumomobile" forKey:kWlanNameKey];
		[self.requestParameters setObject:@"20:C9:D0:8C:68:9F" forKey:kMacAddressInfoKey];
	}
}

/*
 *  sharedRMFStatisticsPlugin should return a valid instance
 */
- (void)testSharedRMFStatisticsPlugin {
	XCTAssertNotNil(self.summaryStatisticsPlugin);
}

/*
 * [[RMFStatisticsPlugin alloc] init] should return a valid instance
 */
- (void)testRMFStatisticsPluginUsingAlloc {
	XCTAssertNotNil([[RMFSummaryStatisticsPlugin alloc] init]);
}

/*
 * [[RMFStatisticsPlugin alloc] init] and sharedRMFStatisticsPlugin should return same instance
 */
- (void)testIsSharedRMFStatisticsPluginAndAllocReturnsSameObject {
	XCTAssertEqual([[RMFSummaryStatisticsPlugin alloc] init], self.summaryStatisticsPlugin);
}

/*
 * If response block is nil then summaryStatisticsWithResponse should not throw any
 * exception
 */

- (void)testSummaryStatisticsScenario1 {
	XCTAssertNoThrow([self.summaryStatisticsPlugin summaryStatisticsWithResponse:nil]);
}

/*
 * If response block is available then summaryStatisticsWithResponse should
 * return response data
 */
- (void)testSummaryStatisticsScenario2 {
	//TODO: write this case once summary statistics request is implemented
}

@end
