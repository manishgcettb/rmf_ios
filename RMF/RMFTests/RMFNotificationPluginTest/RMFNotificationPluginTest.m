/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "RMFNotificationPlugin.h"
#import "RMFMacro.h"
#import "RMFNotifications.h"
#import "RMFConstants.h"

@interface RMFNotificationPluginTest : XCTestCase {
	RMFNotificationPlugin *notificationPlugin;
}

@end

@implementation RMFNotificationPluginTest

- (void)setUp {
	[super setUp];
	// Put setup code here. This method is called before the invocation of each test method in the class.
	notificationPlugin = [RMFNotificationPlugin sharedRMFNotificationPlugin];
	XCTAssertNotNil(notificationPlugin);
}

- (void)tearDown {
	// Put teardown code here. This method is called after the invocation of each test method in the class.
	[super tearDown];
}

- (void)testRecievedNotification {
	NSDictionary *notificationDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"TestMessage", NOTIFICATION_KEY, nil];
	XCTAssertNotNil([notificationPlugin receivedNotification:notificationDictionary atDate:[NSDate date] key:NOTIFICATION_KEY]);
}

- (void)testGetAllNotifications {
	XCTAssertNil([notificationPlugin getAllNotifications]);
}

- (void)testDeleteAllNotification {
	XCTAssertNil([notificationPlugin deleteAllNotification]);
}

- (void)testDeleteNotification {
	RMFDBAccessLayer *databaseManager   = [RMFDBAccessLayer sharedRMFDBAccessLayer];
	RMFConfiguration *config            = [RMFConfiguration sharedRMFConfiguration];
	RMFNotifications *notification         = [databaseManager insertNotifications];
	notification.message                = @"TestMessage";
	notification.notificationTime   = [NSDate date];
	notification.readStatus            = [NSNumber numberWithInt:kUnRead];
	notification.type                   = [NSString stringWithFormat:@"%d", config.system];

	XCTAssertNil([notificationPlugin deleteNotification:notification]);
}

@end
