/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFAPStatus.h"
#import "RMFMacro.h"


@interface RMFAPStatusTest : XCTestCase
@property (nonatomic, strong) RMFAPStatus *rmfAPStatus;
@end

@implementation RMFAPStatusTest

- (void)setUp {
	[super setUp];
	// Put setup code here. This method is called before the invocation of each test method in the class.

	self.rmfAPStatus = [[RMFAPStatus alloc] init];
}

- (void)tearDown {
	// Put teardown code here. This method is called after the invocation of each test method in the class.
	[super tearDown];
}

// Test RMFAPStatus object for nil and validate it
- (void)testRMFAPStatusObjectForNil {
	XCTAssertNotNil(self.rmfAPStatus);
	XCTAssertTrue([self.rmfAPStatus isKindOfClass:[RMFAPStatus class]]);
}

// Test property model
- (void)testModel {
	if (OBJ_NOT_NIL(self.rmfAPStatus)) {
		self.rmfAPStatus.model = @"test";
		XCTAssertEqualObjects(@"test", self.rmfAPStatus.model);
	}
	else {
		XCTAssertNil(self.rmfAPStatus.model);
	}
}

// Test property macAddress
- (void)testMacAddress {
	if (OBJ_NOT_NIL(self.rmfAPStatus)) {
		self.rmfAPStatus.macAddress = @"test";
		XCTAssertEqualObjects(@"test", self.rmfAPStatus.macAddress);
	}
	else {
		XCTAssertNil(self.rmfAPStatus.macAddress);
	}
}

// Test property registrationStatus
- (void)testRegistrationStatus {
	if (OBJ_NOT_NIL(self.rmfAPStatus)) {
		self.rmfAPStatus.registrationStatus = @"test";
		XCTAssertEqualObjects(@"test", self.rmfAPStatus.registrationStatus);
	}
	else {
		XCTAssertNil(self.rmfAPStatus.registrationStatus);
	}
}

// Test property connectionStatus
- (void)testConnectionStatus {
	if (OBJ_NOT_NIL(self.rmfAPStatus)) {
		self.rmfAPStatus.connectionStatus = @"test";
		XCTAssertEqualObjects(@"test", self.rmfAPStatus.connectionStatus);
	}
	else {
		XCTAssertNil(self.rmfAPStatus.connectionStatus);
	}
}

// Test property configStatus
- (void)testConfigStatus {
	if (OBJ_NOT_NIL(self.rmfAPStatus)) {
		self.rmfAPStatus.configStatus = @"test";
		XCTAssertEqualObjects(@"test", self.rmfAPStatus.configStatus);
	}
	else {
		XCTAssertNil(self.rmfAPStatus.configStatus);
	}
}

// Test property
- (void)testLastSeenByVscg {
	if (OBJ_NOT_NIL(self.rmfAPStatus)) {
		self.rmfAPStatus.lastSeenByVscg = @"test";
		XCTAssertEqualObjects(@"test", self.rmfAPStatus.lastSeenByVscg);
	}
	else {
		XCTAssertNil(self.rmfAPStatus.lastSeenByVscg);
	}
}

// Test property swver
- (void)testSwver {
	if (OBJ_NOT_NIL(self.rmfAPStatus)) {
		self.rmfAPStatus.swver = @"test";
		XCTAssertEqualObjects(@"test", self.rmfAPStatus.swver);
	}
	else {
		XCTAssertNil(self.rmfAPStatus.swver);
	}
}

// Test property location
- (void)testLocation {
	if (OBJ_NOT_NIL(self.rmfAPStatus)) {
		self.rmfAPStatus.location = @"test";
		XCTAssertEqualObjects(@"test", self.rmfAPStatus.location);
	}
	else {
		XCTAssertNil(self.rmfAPStatus.location);
	}
}

@end
