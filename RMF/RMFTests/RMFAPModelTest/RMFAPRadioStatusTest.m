/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFAPRadioStatus.h"
#import "RMFMacro.h"


@interface RMFAPRadioStatusTest : XCTestCase
@property (nonatomic, strong) RMFAPRadioStatus *rmfAPRadioStatus;

@end

@implementation RMFAPRadioStatusTest

- (void)setUp {
	[super setUp];
	// Put setup code here. This method is called before the invocation of each test method in the class.
	self.rmfAPRadioStatus = [[RMFAPRadioStatus alloc] init];
}

- (void)tearDown {
	// Put teardown code here. This method is called after the invocation of each test method in the class.
	[super tearDown];
}

// Test RMFAPRadioStatus object for nil and validate it
- (void)testRMFAPRadioStatusObjectForNil {
	XCTAssertNotNil(self.rmfAPRadioStatus);
	XCTAssertTrue([self.rmfAPRadioStatus isKindOfClass:[RMFAPRadioStatus class]]);
}

// Test property txPower
- (void)testTxPower {
	if (OBJ_NOT_NIL(self.rmfAPRadioStatus)) {
		self.rmfAPRadioStatus.txPower = @"test";
		XCTAssertEqualObjects(@"test", self.rmfAPRadioStatus.txPower);
	}
	else {
		XCTAssertNil(self.rmfAPRadioStatus.txPower);
	}
}

// Test property channel
- (void)testChannel {
	if (OBJ_NOT_NIL(self.rmfAPRadioStatus)) {
		self.rmfAPRadioStatus.channel = 1;
		XCTAssertEqual(1, self.rmfAPRadioStatus.channel);
	}
}

// Test property channelWidth
- (void)testChannelWidth {
	if (OBJ_NOT_NIL(self.rmfAPRadioStatus)) {
		self.rmfAPRadioStatus.channelWidth = 1;
		XCTAssertEqual(1, self.rmfAPRadioStatus.channelWidth);
	}
}

// Test property wlanCount
- (void)testWlanCount {
	if (OBJ_NOT_NIL(self.rmfAPRadioStatus)) {
		self.rmfAPRadioStatus.wlanCount = 1;
		XCTAssertEqual(1, self.rmfAPRadioStatus.wlanCount);
	}
}

// Test property mode
- (void)testMode {
	if (OBJ_NOT_NIL(self.rmfAPRadioStatus)) {
		self.rmfAPRadioStatus.mode = @"test";
		XCTAssertEqualObjects(@"test", self.rmfAPRadioStatus.mode);
	}
	else {
		XCTAssertNil(self.rmfAPRadioStatus.mode);
	}
}

@end
