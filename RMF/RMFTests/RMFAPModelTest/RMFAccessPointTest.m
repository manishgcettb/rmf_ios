/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFAccessPoint.h"
#import "RMFMacro.h"


@interface RMFAccessPointTest : XCTestCase
@property (nonatomic, strong) RMFAccessPoint *rmfAccessPoint;
@end

@implementation RMFAccessPointTest

- (void)setUp {
	[super setUp];
	// Put setup code here. This method is called before the invocation of each test method in the class.
	self.rmfAccessPoint = [[RMFAccessPoint alloc] init];
}

- (void)tearDown {
	// Put teardown code here. This method is called after the invocation of each test method in the class.
	[super tearDown];
}

// Test RMFAccessPoint object for nil and validate it
- (void)testRMFAccessPointObjectForNil {
	XCTAssertNotNil(self.rmfAccessPoint);
	XCTAssertTrue([self.rmfAccessPoint isKindOfClass:[RMFAccessPoint class]]);
}

// Test property serialNumber
- (void)testSerialNumber {
	if (OBJ_NOT_NIL(self.rmfAccessPoint)) {
		self.rmfAccessPoint.serialNumber = @"test";
		XCTAssertEqualObjects(@"test", self.rmfAccessPoint.serialNumber);
	}
	else {
		XCTAssertNil(self.rmfAccessPoint.serialNumber);
	}
}

// Test property ssid
- (void)testSSID {
	if (OBJ_NOT_NIL(self.rmfAccessPoint)) {
		self.rmfAccessPoint.ssid = @"test";
		XCTAssertEqualObjects(@"test", self.rmfAccessPoint.ssid);
	}
	else {
		XCTAssertNil(self.rmfAccessPoint.ssid);
	}
}

// Test property image
- (void)testImageUrl {
	if (OBJ_NOT_NIL(self.rmfAccessPoint)) {
		self.rmfAccessPoint.imageUrl = @"test";
		XCTAssertEqualObjects(@"test", self.rmfAccessPoint.imageUrl);
	}
	else {
		XCTAssertNil(self.rmfAccessPoint.imageUrl);
	}
}

// Test property rmfAPWlanStatus
- (void)testRmfAPWlanStatus {
	if (OBJ_NOT_NIL(self.rmfAccessPoint)) {
		self.rmfAccessPoint.rmfAPWlanStatus = [[RMFAPWlanStatus alloc] init];
		XCTAssertNotNil(self.rmfAccessPoint.rmfAPWlanStatus);
	}
	else {
		XCTAssertNil(self.rmfAccessPoint.rmfAPWlanStatus);
	}
}

// Test property rmfAPExtendedStatus
- (void)testRmfAPExtendedStatus {
	if (OBJ_NOT_NIL(self.rmfAccessPoint)) {
		self.rmfAccessPoint.rmfAPExtendedStatus = [[RMFAPExtendedStatus alloc] init];
		XCTAssertNotNil(self.rmfAccessPoint.rmfAPExtendedStatus);
	}
	else {
		XCTAssertNil(self.rmfAccessPoint.rmfAPExtendedStatus);
	}
}

// Test property rmfAPRadioStatus
- (void)testRmfAPRadioStatus {
	if (OBJ_NOT_NIL(self.rmfAccessPoint)) {
		self.rmfAccessPoint.rmfAPRadioStatus = [[RMFAPRadioStatus alloc] init];
		XCTAssertNotNil(self.rmfAccessPoint.rmfAPRadioStatus);
	}
	else {
		XCTAssertNil(self.rmfAccessPoint.rmfAPRadioStatus);
	}
}

// Test property rmfAPStatus
- (void)testRmfAPStatus {
	if (OBJ_NOT_NIL(self.rmfAccessPoint)) {
		self.rmfAccessPoint.rmfAPStatus = [[RMFAPStatus alloc] init];
		XCTAssertNotNil(self.rmfAccessPoint.rmfAPStatus);
	}
	else {
		XCTAssertNil(self.rmfAccessPoint.rmfAPStatus);
	}
}

// Test property rootAssociation
- (void)testRootAssociation {
	if (OBJ_NOT_NIL(self.rmfAccessPoint)) {
		self.rmfAccessPoint.rootAssociation = @"test";
		XCTAssertEqualObjects(@"test", self.rmfAccessPoint.rootAssociation);
	}
	else {
		XCTAssertNil(self.rmfAccessPoint.rootAssociation);
	}
}

@end
