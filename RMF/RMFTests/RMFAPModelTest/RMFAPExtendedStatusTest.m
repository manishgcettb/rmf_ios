/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFAPExtendedStatus.h"
#import "RMFMacro.h"

@interface RMFAPExtendedStatusTest : XCTestCase
@property (nonatomic, strong) RMFAPExtendedStatus *rmfAPExtendedStatus;

@end

@implementation RMFAPExtendedStatusTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.rmfAPExtendedStatus = [[RMFAPExtendedStatus alloc] init];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

// Test RMFAPExtendedStatus object for nil and validate it
- (void)testRMFAPExtendedStatusObjectForNil {
    XCTAssertNotNil(self.rmfAPExtendedStatus);
    XCTAssertTrue([self.rmfAPExtendedStatus isKindOfClass:[RMFAPExtendedStatus class]]);
}

// Test property internalIp
- (void)testInternalIp {
    if (OBJ_NOT_NIL(self.rmfAPExtendedStatus)) {
        self.rmfAPExtendedStatus.internalIp = @"test";
        XCTAssertEqualObjects(@"test", self.rmfAPExtendedStatus.internalIp);
    }
    else {
        XCTAssertNil(self.rmfAPExtendedStatus.internalIp);
    }
}

// Test property externalIp
- (void)testExternalIp {
    if (OBJ_NOT_NIL(self.rmfAPExtendedStatus)) {
        self.rmfAPExtendedStatus.externalIp = @"test";
        XCTAssertEqualObjects(@"test", self.rmfAPExtendedStatus.externalIp);
    }
    else {
        XCTAssertNil(self.rmfAPExtendedStatus.externalIp);
    }
}

// Test property hops
- (void)testHops {
    if (OBJ_NOT_NIL(self.rmfAPExtendedStatus)) {
        self.rmfAPExtendedStatus.hops = @"test";
        XCTAssertEqualObjects(@"test", self.rmfAPExtendedStatus.hops);
    }
    else {
        XCTAssertNil(self.rmfAPExtendedStatus.hops);
    }
}

// Test property tunnelType
- (void)testTunnelType {
    if (OBJ_NOT_NIL(self.rmfAPExtendedStatus)) {
        self.rmfAPExtendedStatus.tunnelType = @"test";
        XCTAssertEqualObjects(@"test", self.rmfAPExtendedStatus.tunnelType);
    }
    else {
        XCTAssertNil(self.rmfAPExtendedStatus.tunnelType);
    }
}

// Test property name
- (void)testName {
    if (OBJ_NOT_NIL(self.rmfAPExtendedStatus)) {
        self.rmfAPExtendedStatus.name = @"test";
        XCTAssertEqualObjects(@"test", self.rmfAPExtendedStatus.name);
    }
    else {
        XCTAssertNil(self.rmfAPExtendedStatus.name);
    }
}

// Test property uptime
- (void)testUptime {
    if (OBJ_NOT_NIL(self.rmfAPExtendedStatus)) {
        self.rmfAPExtendedStatus.uptime = 234234234;
        XCTAssertEqual(234234234, self.rmfAPExtendedStatus.uptime);
    }
}

@end
