/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFEvent.h"

@interface RMFEventTest : XCTestCase

@property (nonatomic, strong) RMFEvent *rmfEvent;

@end

@implementation RMFEventTest

- (void)setUp {
	[super setUp];
	self.rmfEvent = [[RMFEvent alloc] init];
}

- (void)tearDown {
	[super tearDown];
}

// Test if RMFEvent instance is not nil
- (void)testRMFWlanObject {
	XCTAssertNotNil(self.rmfEvent);
}

@end
