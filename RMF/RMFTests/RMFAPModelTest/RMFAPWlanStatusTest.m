/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFAPWlanStatus.h"
#import "RMFMacro.h"


@interface RMFAPWlanStatusTest : XCTestCase
@property (nonatomic, strong) RMFAPWlanStatus *rmfAPWlanStatus;

@end

@implementation RMFAPWlanStatusTest

- (void)setUp {
	[super setUp];
	// Put setup code here. This method is called before the invocation of each test method in the class.
	self.rmfAPWlanStatus = [[RMFAPWlanStatus alloc] init];
}

- (void)tearDown {
	// Put teardown code here. This method is called after the invocation of each test method in the class.
	[super tearDown];
}

// Test RMFAPWlanStatus object for nil and validate it
- (void)testRMFAPWlanStatusObjectForNil {
	XCTAssertNotNil(self.rmfAPWlanStatus);
	XCTAssertTrue([self.rmfAPWlanStatus isKindOfClass:[RMFAPWlanStatus class]]);
}

// Test property totalCount
- (void)testTotalCount {
	if (OBJ_NOT_NIL(self.rmfAPWlanStatus)) {
		self.rmfAPWlanStatus.totalCount = 1;
		XCTAssertEqual(1, self.rmfAPWlanStatus.totalCount);
	}
}

// Test property wlans
- (void)testWlans {
	if (OBJ_NOT_NIL(self.rmfAPWlanStatus)) {
		self.rmfAPWlanStatus.wlans = [NSArray arrayWithObject:@""];
		XCTAssertNotNil(self.rmfAPWlanStatus.wlans);

		self.rmfAPWlanStatus.wlans = nil;
		XCTAssertNil(self.rmfAPWlanStatus.wlans);
	}
	else {
		XCTAssertNil(self.rmfAPWlanStatus.wlans);
	}
}

@end
