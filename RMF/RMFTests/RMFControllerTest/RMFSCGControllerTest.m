/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFSCGController.h"
#import "RMFConfiguration.h"
#import "RMFMacro.h"
#import "RMFRequestOperationManager.h"

#define SCG_TEST_IP      @"10.199.4.106"
#define SCG_TEST_PORT    @"8080"

@interface RMFSCGControllerTest : XCTestCase

// Stores/returns the instance of RMFConfiguration
@property (nonatomic, strong) RMFConfiguration *configuration;

// Stores and returns the instance of RMFScgController
@property (nonatomic, strong) RMFSCGController *scgController;

@end

@implementation RMFSCGControllerTest

- (void)setUp {
	[super setUp];
	[self initConfiguration];
	self.scgController = [RMFSCGController sharedRMFSCGController];
}

- (void)tearDown {
	[super tearDown];
}

/*
 * initialise RMFConfiguration instance with valid information for testing SCG APIs
 */
- (void)initConfiguration {
	self.configuration = [RMFConfiguration sharedRMFConfiguration];
	self.configuration.systemIP = SCG_TEST_IP;
	self.configuration.port = SCG_TEST_PORT;
}

/*
 * Checks whether the instance of RMFScgController is available or not
 */
- (void)testRMFScgControllerScenario1 {
	XCTAssertNotNil(self.scgController);
}

/*
 * Checks whether the instance created with [RMFScgController alloc] init] is available
 * or not
 */
- (void)testInitScenario1 {
	XCTAssertNotNil([[RMFSCGController alloc] init]);
}

/*
 * Checks whether the instance created with [RMFScgController alloc] init] is same as the
 * one created with sharedRMFSCGController
 */
- (void)testRMFScgControllerScenario2 {
	XCTAssertEqualObjects(self.scgController, [[RMFSCGController alloc] init]);
}

/*
 * initRequestOperationManager method should not create a new instance of
 * requestOperationManager if there is no change in the base url
 */
- (void)testInitRequestOperationManagerScenario1 {
	RMFRequestOperationManager *currentRequestOperationManager = self.scgController.requestOperationManager;

	[self.scgController initRequestOperationManager];
	XCTAssertEqualObjects(self.scgController.requestOperationManager, currentRequestOperationManager);
}

/*
 * initRequestOperationManager method should create a new instance of
 * requestOperationManager if there is a change in the base url
 */
- (void)testInitRequestOperationManagerScenario2 {
	RMFRequestOperationManager *currentRequestOperationManager = self.scgController.requestOperationManager;

	// Change IP and port
	self.configuration.systemIP = @"162.222.182.50";
	self.configuration.port = @"8980";
	[self.scgController initRequestOperationManager];

	XCTAssertNotEqualObjects(self.scgController.requestOperationManager, currentRequestOperationManager);

	// Reset IP and port so that other UT cases use the correct instance
	self.configuration.systemIP = SCG_TEST_IP;
	self.configuration.port = SCG_TEST_PORT;
	[self.scgController initRequestOperationManager];
}

@end
