/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "RMFZDController.h"
#import "RMFConfiguration.h"
#import "RMFSystemManager.h"
#import "RMFMacro.h"

#define ZD_TEST_IP      @"10.199.4.106"
#define ZD_TEST_PORT    @"8080"

@interface RMFZDControllerTest : XCTestCase

// Stores/returns the instance of RMFConfiguration
@property (nonatomic, strong) RMFConfiguration *configuration;

// Stores and returns the instance of RMFZDController
@property (nonatomic, strong) RMFZDController *zdController;

@end

@implementation RMFZDControllerTest

- (void)setUp {
	[super setUp];
	[self initConfiguration];
	self.zdController = [RMFZDController sharedRMFZDController];
}

- (void)tearDown {
	[super tearDown];
}

/*
 * initialise RMFConfiguration instance with valid information for testing ZD APIs
 */
- (void)initConfiguration {
	self.configuration = [RMFConfiguration sharedRMFConfiguration];
	self.configuration.systemIP = ZD_TEST_IP;
	self.configuration.port = ZD_TEST_PORT;
}

/*
 * Checks whether the instance of RMFZDController is available or not
 */
- (void)testRMFZDControllerScenario1 {
	XCTAssertNotNil(self.zdController);
}

/*
 * Checks whether the instance created with [RMFZDController alloc] init] is available
 * or not
 */
- (void)testInitScenario1 {
	XCTAssertNotNil([[RMFZDController alloc] init]);
}

/*
 * Checks whether the instance created with [RMFZDController alloc] init] is same as the
 * one created with sharedRMFZDController
 */
- (void)testRMFZDControllerScenario2 {
	XCTAssertEqualObjects(self.zdController, [[RMFZDController alloc] init]);
}

/*
 * initRequestOperationManager method should not create a new instance of
 * requestOperationManager if there is no change in the base url
 */
- (void)testInitRequestOperationManagerScenario1 {
	RMFRequestOperationManager *currentRequestOperationManager = self.zdController.requestOperationManager;

	[self.zdController initRequestOperationManager];

	XCTAssertEqualObjects(self.zdController.requestOperationManager, currentRequestOperationManager);
}

/*
 * initRequestOperationManager method should create a new instance of
 * requestOperationManager if there is a change in the base url
 */
- (void)testInitRequestOperationManagerScenario2 {
	RMFRequestOperationManager *currentRequestOperationManager = self.zdController.requestOperationManager;

	// Change IP and port
	self.configuration.systemIP = @"162.222.182.50";
	self.configuration.port = @"8980";
	[self.zdController initRequestOperationManager];

	XCTAssertNotEqualObjects(self.zdController.requestOperationManager, currentRequestOperationManager);

	// Reset IP and port so that other UT cases use the correct instance
	self.configuration.systemIP = ZD_TEST_IP;
	self.configuration.port = ZD_TEST_PORT;
	[self.zdController initRequestOperationManager];
}

@end
