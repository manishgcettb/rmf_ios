/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFKUMOController.h"
#import "RMFUtils.h"
#import "RMFConfiguration.h"
#import "RMFSystemManager.h"
#import "RMFMacro.h"
#import "RMFAPPlugin.h"

#define KUMO_TEST_PORT    @"8080"
#define KUMO_TEST_IP      @"10.199.4.106"

#define  kClientDetailsLoaded        @"Client details should be loaded"
#define  kClientListLoaded           @"Client list should be loaded"

#define  kImageDownloaded            @"Image should be downloaded"
#define  kImageUploaded            @"Image should be uploaded"
#define  kAPDetailsLoaded            @"AP details should be loaded"
#define  kAPListLoaded               @"AP list should be loaded"
#define  kAPFrameLoaded               @"AP frame should be loaded"
#define  kAPCreated              @"AP should be created"
#define  kAPEdited               @"AP should be edited"
#define  kAPDeleted               @"AP should be deleted"
#define  kAPListLoadedSortAndFilter     @"AP List should be loaded"
#define  kAPFrameLoadedWithSortAndFilter     @"AP frame should be loaded"

#define  kWlanDeleted                @"WLAN should be deleted"
#define  kWlanUpdated                @"WLAN information should be updated"
#define  kNewWlanCreated             @"New WLAN should be created"
#define  kNewVenueCreated            @"New Venue should be created"
#define  kVenueDetailsLoaded         @"Venue details should be loaded"
#define  kVenueListLoaded            @"Venue list should be loaded"
#define  kTenantDetailsLoaded        @"Tenant details should be loaded"
#define  kTenantListLoaded           @"Tenant list should be loaded"
#define  kEventDetailsLoaded         @"Event details should be loaded"
#define  kEventListLoaded            @"Event list should be loaded"
#define  kWlanServiceListLoaded      @"Wlan service list should be loaded"
#define  kWlanServiceStatusLoaded    @"Wlan service status should be loaded"
#define  kWlanServiceDetailsLoaded   @"Wlan service details should be loaded"

@interface RMFKUMOControllerTest : XCTestCase

// Stores/returns the instance of RMFConfiguration
@property (nonatomic, strong) RMFConfiguration *configuration;

// Stores and returns the instance of RMFKumoController
@property (nonatomic, strong) RMFKUMOController *kumoController;

@end

@implementation RMFKUMOControllerTest

- (void)setUp {
	[super setUp];
	[self initConfiguration];
	self.kumoController = [RMFKUMOController sharedRMFKUMOController];
}

- (void)tearDown {
	[super tearDown];
}

/*
 * initialise RMFConfiguration instance with valid information for testing ZD APIs
 */
- (void)initConfiguration {
	self.configuration = [RMFConfiguration sharedRMFConfiguration];
	self.configuration.systemIP = KUMO_TEST_IP;
	self.configuration.port = KUMO_TEST_PORT;
	self.configuration.system = kSystemTypeKUMO;
}

/*
 * Checks whether the instance of RMFKumoController is available or not
 */
- (void)testRMFKumoControllerScenario1 {
	XCTAssertNotNil(self.kumoController);
}

/*
 * Checks whether the instance created with [RMFKumoController alloc] init] is available
 * or not
 */
- (void)testInitScenario1 {
	XCTAssertNotNil([[RMFKUMOController alloc] init]);
}

/*
 * Checks whether the instance created with [RMFKumoController alloc] init] is same as the
 * one created with sharedRMFKUMOController
 */
- (void)testRMFKumoControllerScenario2 {
	XCTAssertEqualObjects(self.kumoController, [[RMFKUMOController alloc] init]);
}

/*
 * initRequestOperationManager method should not create a new instance of
 * requestOperationManager if there is no change in the base url
 */
- (void)testInitRequestOperationManagerScenario1 {
	RMFRequestOperationManager *currentRequestOperationManager = self.kumoController.requestOperationManager;

	[self.kumoController initRequestOperationManager];

	XCTAssertEqualObjects(self.kumoController.requestOperationManager, currentRequestOperationManager);
}

/*
 * initRequestOperationManager method should create a new instance of
 * requestOperationManager if there is a change in the base url
 */
- (void)testInitRequestOperationManagerScenario2 {
	RMFRequestOperationManager *currentRequestOperationManager = self.kumoController.requestOperationManager;

	// Change IP and port
	self.configuration.systemIP = @"162.222.182.50";
	self.configuration.port = @"8980";
	[self.kumoController initRequestOperationManager];

	XCTAssertNotEqualObjects(self.kumoController.requestOperationManager, currentRequestOperationManager);

	// Reset IP and port so that other UT cases use the correct instance
	self.configuration.systemIP = KUMO_TEST_IP;
	self.configuration.port = KUMO_TEST_PORT;
	[self.kumoController initRequestOperationManager];
}

#pragma mark - WLAN

/*
 * If request parameter and response block are not available then wlanServiceList should handle this
 * scenanrio without throwing any exception
 */
- (void)testWlanServiceListScenario1 {
	XCTAssertNoThrow([self.kumoController wlanServiceList:nil response:nil]);
}

/*
 * If request parameter is available but response block is not available then wlanServiceList should
 * handle this scenario without throwing any exception
 */
- (void)testWlanServiceListScenario2 {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	XCTAssertNoThrow([self.kumoController wlanServiceList:requestParameters response:nil]);
}

/*
 * If request parameter is not available but response block is available then wlanServiceList should return
 * NSError object with description "Request parameters not found"
 */

- (void)testWlanServiceListScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceListLoaded];

	[self.kumoController wlanServiceList:nil response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available but the request parameter dictionary
 * does not contain tenant name then wlanServiceList should return NSError object with description
 * "Tenant name is required"
 */

- (void)testWlanServiceListScenario4 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceListLoaded];

	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];

	[self.kumoController wlanServiceList:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantNameNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter dictionary
 * contains tenant name then wlanServiceList should return the response data
 */

- (void)testWlanServiceListScenario5 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceListLoaded];

	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];

	[self.kumoController wlanServiceList:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter and response block are not available then wlanStatus should handle this
 * scenanrio without throwing any exception
 */
- (void)testWlanStatusScenario1 {
	XCTAssertNoThrow([self.kumoController wlanStatus:nil response:nil]);
}

/*
 * If request parameter is available but response block is not available then wlanStatus should
 * handle this scenario without throwing any exception
 */
- (void)testWlanStatusScenario2 {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	XCTAssertNoThrow([self.kumoController wlanStatus:requestParameters response:nil]);
}

/*
 * If request parameter is not available but response block is available then wlanStatus should return
 * NSError object with description "Request parameters not found"
 */

- (void)testWlanStatusScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceStatusLoaded];

	[self.kumoController wlanStatus:nil response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available but the request parameter
 * dictionary does not contain  tenant name, venue name and serial number then wlanStatus should return
 * NSError object with description "Tenant name venue name and serial number are required"
 */

- (void)testWlanStatusScenario4 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceStatusLoaded];
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];

	[self.kumoController wlanStatus:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndVenueNameAndSerialNumberNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter
 * dictionary contains tenant name but does not contain venue name and serial number then wlanStatus
 * should return NSError object with description "Tenant name venue name and serial number are required"
 */

- (void)testWlanStatusScenario5 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceStatusLoaded];

	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];

	[self.kumoController wlanStatus:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndVenueNameAndSerialNumberNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter
 * dictionary contains venue name but does not contain tenant name and serial number then wlanStatus
 * should return NSError object with description "Tenant name venue name and serial number are required"
 */

- (void)testWlanStatusScenario6 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceStatusLoaded];

	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"default" forKey:kVenueNameKey];

	[self.kumoController wlanStatus:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndVenueNameAndSerialNumberNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter
 * dictionary contains serial number but does not contain tenant name and venue name then wlanStatus
 * should return NSError object with description "Tenant name venue name and serial number are required"
 */

- (void)testWlanStatusScenario7 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceStatusLoaded];

	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"default" forKey:kSerialNumberKey];

	[self.kumoController wlanStatus:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndVenueNameAndSerialNumberNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter
 * dictionary contains venue name and tenant name but does not contain serial number then wlanStatus
 * should return NSError object with description "Tenant name venue name and serial number are required"
 */

- (void)testWlanStatusScenario8 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceStatusLoaded];

	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];
	[requestParameters setObject:@"default" forKey:kVenueNameKey];

	[self.kumoController wlanStatus:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndVenueNameAndSerialNumberNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter
 * dictionary contains venue name and serial number but does not contain tenant name then wlanStatus
 * should return NSError object with description "Tenant name venue name and serial number are required"
 */

- (void)testWlanStatusScenario9 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceStatusLoaded];

	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"1111111" forKey:kSerialNumberKey];
	[requestParameters setObject:@"default" forKey:kVenueNameKey];

	[self.kumoController wlanStatus:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndVenueNameAndSerialNumberNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter
 * dictionary contains tenant name and serial number but does not contain venue name then wlanStatus
 * should return NSError object with description "Tenant name venue name and serial number are required"
 */

- (void)testWlanStatusScenario10 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceStatusLoaded];

	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"1111111" forKey:kSerialNumberKey];
	[requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];

	[self.kumoController wlanStatus:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndVenueNameAndSerialNumberNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter dictionary
 * contains tenant name, venue name and serial number then wlanStatus should return the response data
 */

- (void)testWlanStatusScenario11 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceStatusLoaded];

	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"161205002059" forKey:kSerialNumberKey];
	[requestParameters setObject:@"default" forKey:kVenueNameKey];
	[requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];

	[self.kumoController wlanStatus:requestParameters response: ^(NSError *error, id data) {
	    if (OBJ_NOT_NIL(error)) {
	        XCTAssertNotNil(error);
		}
	    else {
	        XCTAssertNil(error);
		}
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter and response block are not available then wlanServiceDetails should handle this
 * scenanrio without throwing any exception
 */

- (void)testWlanServiceDetailsScenario1 {
	XCTAssertNoThrow([self.kumoController wlanServiceDetails:nil response:nil]);
}

/*
 * If request parameter is available but response block is not available then wlanServiceDetails should
 * handle this scenario without throwing any exception
 */

- (void)testWlanServiceDetailsScenario2 {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	XCTAssertNoThrow([self.kumoController wlanServiceList:requestParameters response:nil]);
}

/*
 * If request parameter is not available but response block is available then wlanServiceDetails
 * should return NSError object with description "Request parameters not found"
 */

- (void)testWlanServiceDetailsScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceDetailsLoaded];

	[self.kumoController wlanServiceDetails:nil response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available but the request parameter dictionary
 * does not contain tenantName and wlanServiceName then wlanServiceList should return NSError object
 * with description "Tenant name and wlan service name are required"
 */

- (void)testWlanServiceDetailsScenario4 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceDetailsLoaded];
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];

	[self.kumoController wlanServiceDetails:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndWlanNameNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter dictionary
 * contain tenant name but does not contain wlan service name then wlanServiceList should return NSError
 * object with description "Tenant name and wlan service name are required"
 */

- (void)testWlanServiceDetailsScenario5 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceDetailsLoaded];

	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];

	[self.kumoController wlanServiceDetails:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndWlanNameNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter dictionary
 * contain wlan service name  but does not contain tenant name then wlanServiceList should return NSError
 * object with description "Tenant name and wlan service name are required"
 */

- (void)testWlanServiceDetailsScenario6 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceDetailsLoaded];

	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:kWlanNameKey];

	[self.kumoController wlanServiceDetails:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndWlanNameNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter dictionary
 * contain wlan service name and tenant name then wlanServiceList should return response data
 */

- (void)testWlanServiceDetailsScenario7 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceDetailsLoaded];

	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];
	[requestParameters setObject:@"kumomobile" forKey:kWlanNameKey];

	[self.kumoController wlanServiceDetails:requestParameters response: ^(NSError *error, id data) {
	    if (OBJ_NOT_NIL(error)) {
	        XCTAssertNotNil(error);
		}
	    else {
	        XCTAssertNil(error);
	        XCTAssertNotNil(data);
		}
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter and response block are not available then createWlan should handle this
 * scenanrio without throwing any exception
 */

- (void)testCreateWlanScenario1 {
	XCTAssertNoThrow([self.kumoController createWlan:nil response:nil]);
}

/*
 * If request parameter is available but response block is not available then createWlan should
 * handle this scenario without throwing any exception
 */

- (void)testCreateWlanScenario2 {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	XCTAssertNoThrow([self.kumoController createWlan:requestParameters response:nil]);
}

/*
 * If request parameter is not available but response block is available then createWlan
 * should return NSError object with description "Request parameters not found"
 */

- (void)testCreateWlanScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kNewWlanCreated];

	[self.kumoController createWlan:nil response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available but the request parameter dictionary
 * does not contain tenant name and new wlan information dictionary then venueDetails should return
 * NSError object with description "Tenant name and new wlan information are required"
 */

- (void)testCreateWlanScenario4 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kNewWlanCreated];
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];

	[self.kumoController createWlan:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndWlanInfoNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter dictionary
 * contains tenant name but does not contain new venue information dictionary then createVenue should
 * return NSError object with description "Tenant name and new wlan information are required"
 */

- (void)testCreateWlanScenario5 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kNewWlanCreated];
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];

	[self.kumoController createWlan:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndWlanInfoNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter dictionary
 * contains new wlan information dictionary but does not contain tenant name then createVenue should
 * return NSError object with description "Tenant name and new wlan information are required"
 */

- (void)testCreateWlanScenario6 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kNewWlanCreated];
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	NSMutableDictionary *newWlanInfoDictionary = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:newWlanInfoDictionary forKey:kNewWlanInfoKey];

	[self.kumoController createWlan:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndWlanInfoNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter dictionary
 * contain tenant name name and new wlan information dictionary then wlanServiceList should return
 * response data
 */

- (void)testCreateWlanScenario7 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kNewWlanCreated];
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];
	NSMutableDictionary *newWlanInfoDictionary = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:newWlanInfoDictionary forKey:kNewWlanInfoKey];

	[self.kumoController createWlan:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}


/*
 * If event list, filtering parameter and response block are nil and sortby parameter is not a valid input
 * then wlanList should not throw any exception
 *
 */
- (void)testWlanListScenario1 {
	XCTAssertNoThrow([self.kumoController wlanList:nil sortBy:3 filterOn:nil response:nil]);
}

/*
 * If request parameter and filtering parameter are nil, response block is available and sortby parameter is not a valid input
 * then wlanList should return error
 *
 */
- (void)testWlanListScenario2 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceListLoaded];
	[self.kumoController wlanList:nil sortBy:3 filterOn:nil response: ^(NSError *error, id data) {
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If filtering parameter is nil, response block and request parameter are available and sortby parameter is not a valid input
 * then wlanList should return error
 *
 */
- (void)testWlanListScenario3 {
	NSMutableDictionary *requestParams = [[NSMutableDictionary alloc] init];

	[requestParams setObject:[NSNumber numberWithInt:2] forKey:kLimit];
	[requestParams setObject:[NSNumber numberWithInt:1] forKey:kOffset];

	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceListLoaded];
	[self.kumoController wlanList:requestParams sortBy:2 filterOn:nil response: ^(NSError *error, id data) {
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}


/*
 * If request parameter is nil, response block and  filtering parameter are available and sortby parameter is a valid input
 * then wlanList should return error
 *
 */
- (void)testWlanListScenario4 {
	NSMutableDictionary *filter = [[NSMutableDictionary alloc] init];
	[filter setObject:@"kumomobile" forKey:kWlanNameKey];

	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceListLoaded];
	[self.kumoController wlanList:nil sortBy:1 filterOn:filter response: ^(NSError *error, id data) {
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter, response block and  filtering parameter are available and sortby parameter is a valid input
 * then wlanList should return event list
 *
 */

- (void)testWlanListScenario5 {
	// Create request parameter dictionary
	NSMutableDictionary *requestParams = [[NSMutableDictionary alloc] init];
	[requestParams setObject:[NSNumber numberWithInt:2] forKey:kLimit];
	[requestParams setObject:[NSNumber numberWithInt:1] forKey:kOffset];

	// Create filter dictionary
	NSMutableDictionary *filter = [[NSMutableDictionary alloc] init];
	[filter setObject:@"kumomobile" forKey:kWlanNameKey];

	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceListLoaded];

	// Make network request to fetch wlan list
	[self.kumoController wlanList:requestParams sortBy:1 filterOn:nil response: ^(NSError *error, id data) {
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If all the required parameters are available then wlan should be updated
 */

- (void)testEditWlanScenario1 {
	NSMutableDictionary *requestParams = [[NSMutableDictionary alloc] init];
	[requestParams setObject:kEmptyText forKey:kWlanNameKey];
	[requestParams setObject:kEmptyText forKey:kTenantNameKey];
	[requestParams setObject:kEmptyText forKey:kWlanSssidKey];
	[requestParams setObject:kEmptyText forKey:kWlanTypeKey];
	[requestParams setObject:kEmptyText forKey:kWlanSecurityKey];
	[requestParams setObject:kEmptyText forKey:kPskPassPhraseKey];

	//TODO: update this test case once edit wlan request is implemented
}

/*
 * If all the required parameters are not available then edit wlan method should return error with description
 * "KUMO Error: Tenant name and new wlan information are required"
 */

- (void)testEditWlanScenario2 {
	NSMutableDictionary *requestParams = [[NSMutableDictionary alloc] init];
	[requestParams setObject:kEmptyText forKey:kWlanNameKey];
	[requestParams setObject:kEmptyText forKey:kTenantNameKey];
	[requestParams setObject:kEmptyText forKey:kWlanSssidKey];
	[requestParams setObject:kEmptyText forKey:kWlanTypeKey];

	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanUpdated];
	[self.kumoController editWlan:requestParams response: ^(NSError *error, id data) {
	    [expectation fulfill];
	    XCTAssertEqual(kKUMOCodeForTenantAndWlanInfoNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary is not available then edit wlan method should return error with description
 * "KUMO Error: Request parameters not found"
 */
- (void)testEditWlanScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanUpdated];
	[self.kumoController editWlan:nil response: ^(NSError *error, id data) {
	    [expectation fulfill];
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If the request parameter dictionary and response block are not available then editWlan should not throw any exception
 */
- (void)testEditWlanScenario4 {
	XCTAssertNoThrow([self.kumoController editWlan:nil response:nil]);
}

/*
 * If the wlan name is available in the request parameter then WLAN entry should be deleted
 */
- (void)testDeleteWlanScenario1 {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"test" forKey:kWlanNameKey];
	//TODO: update this test case once delete wlan request is implemented
}

/*
 * If the wlan name is not available in the request parameter then deleteWlan method should return an error with description
 * "KUMO Error: Wlan name is required"
 */

- (void)testDeleteWlanScenario2 {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanDeleted];
	[self.kumoController deleteWlan:requestParameters response: ^(NSError *error, id data) {
	    [expectation fulfill];
	    XCTAssertEqual(kKUMOCodeForWlanNameNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If the request parameter dictionary is not available in the request parameter then deleteWlan method should return an error
 * with description "KUMO Error: Request parameters not found"
 */

- (void)testDeleteWlanScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanDeleted];
	[self.kumoController deleteWlan:nil response: ^(NSError *error, id data) {
	    [expectation fulfill];
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If the request parameter dictionary and response block are not available then deleteWlan should not throw any exception
 */
- (void)testDeleteWlanScenario4 {
	XCTAssertNoThrow([self.kumoController deleteWlan:nil response:nil]);
}

#pragma mark - Tenants

/*
 * If response block is not available then tenantListWithresponse should handle this scenanrio
 * without throwing any exception
 */

- (void)testTenantListScenario1 {
	XCTAssertNoThrow([self.kumoController tenantListWithresponse:nil]);
}

/*
 * If response block is available then tenantListWithresponse should return the response data
 */

- (void)testTenantListScenario2 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kTenantListLoaded];

	[self.kumoController tenantListWithresponse: ^(NSError *error, id data) {
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	}];
}

/*
 * If request parameter and response block are not available then tenantDetails should
 * handle this scenanrio without throwing any exception
 */
- (void)testTenantDetailsScenario1 {
	XCTAssertNoThrow([self.kumoController tenantDetails:nil response:nil]);
}

/*
 * If request parameter is available but response block is not available then tenantDetails should
 * handle this scenario without throwing any exception
 */
- (void)testTenantDetailsScenario2 {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	XCTAssertNoThrow([self.kumoController tenantDetails:requestParameters response:nil]);
}

/*
 * If request parameter is not available but response block is available then tenantDetails should return
 * NSError object with description "Request parameters not found"
 */

- (void)testTenantDetailsScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kTenantDetailsLoaded];

	[self.kumoController tenantDetails:nil response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available but the request parameter dictionary
 * does not contain tenant name then tenantDetails should return NSError object with description
 * "Tenant name is required"
 */

- (void)testTenantDetailsScenario4 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kTenantDetailsLoaded];
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];

	[self.kumoController tenantDetails:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantNameNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter dictionary
 * contains tenant name then tenantDetails should return the response data
 */

- (void)testTenantDetailsScenario5 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceListLoaded];
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];

	[self.kumoController tenantDetails:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

#pragma mark - venue

/*
 * If request parameter and response block are not available then venueList should
 * handle this scenanrio without throwing any exception
 */
- (void)testVenueListScenario1 {
	XCTAssertNoThrow([self.kumoController venueList:nil response:nil]);
}

/*
 * If request parameter is available but response block is not available then venueList should
 * handle this scenario without throwing any exception
 */
- (void)testVenueListScenario2 {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	XCTAssertNoThrow([self.kumoController venueList:requestParameters response:nil]);
}

/*
 * If request parameter is not available but response block is available then venueList should return
 * NSError object with description "Request parameters not found"
 */

- (void)testVenueListScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kVenueListLoaded];

	[self.kumoController venueList:nil response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available but the request parameter dictionary
 * does not contain tenant name then venueList should return NSError object with description
 * "Tenant name is required"
 */

- (void)testVenueListScenario4 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kVenueListLoaded];
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];

	[self.kumoController venueList:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantNameNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter dictionary
 * contains tenant name then venueList should return the response data
 */

- (void)testVenueListScenario5 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kVenueListLoaded];
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];

	[self.kumoController venueList:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter and response block are not available then venueDetails should handle this
 * scenanrio without throwing any exception
 */

- (void)testVenueDetailsScenario1 {
	XCTAssertNoThrow([self.kumoController venueDetails:nil response:nil]);
}

/*
 * If request parameter is available but response block is not available then venueDetails should
 * handle this scenario without throwing any exception
 */

- (void)testVenueDetailsScenario2 {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	XCTAssertNoThrow([self.kumoController venueDetails:requestParameters response:nil]);
}

/*
 * If request parameter is not available but response block is available then venueDetails
 * should return NSError object with description "Request parameters not found"
 */

- (void)testVenueDetailsScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kVenueDetailsLoaded];

	[self.kumoController venueDetails:nil response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available but the request parameter dictionary
 * does not contain tenant name and venue name then venueDetails should return NSError object
 * with description "Tenant name and venue name are required"
 */

- (void)testVenueDetailsScenario4 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kVenueDetailsLoaded];
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];

	[self.kumoController venueDetails:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndVenueNameNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter dictionary
 * contain tenant name but does not contain venue name then venueDetails should return NSError
 * object with description "Tenant name and venue name are required"
 */

- (void)testVenueDetailsScenario5 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kVenueDetailsLoaded];
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];

	[self.kumoController venueDetails:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndVenueNameNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter dictionary
 * contain venue name  but does not contain tenant name then venueDetails should return NSError
 * object with description "Tenant name and venue name are required"
 */

- (void)testVenueDetailsScenario6 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kVenueDetailsLoaded];
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"default" forKey:kVenueNameKey];

	[self.kumoController venueDetails:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndVenueNameNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter dictionary
 * contain venue name and tenant name then wlanServiceList should return response data
 */

- (void)testVenueDetailsScenario7 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kVenueDetailsLoaded];
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];
	[requestParameters setObject:@"default" forKey:kVenueNameKey];

	[self.kumoController venueDetails:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter and response block are not available then createVenue should handle this
 * scenanrio without throwing any exception
 */

- (void)testCreateVenueScenario1 {
	XCTAssertNoThrow([self.kumoController createVenue:nil response:nil]);
}

/*
 * If request parameter is available but response block is not available then createVenue should
 * handle this scenario without throwing any exception
 */

- (void)testCreateVenueScenario2 {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	XCTAssertNoThrow([self.kumoController createVenue:requestParameters response:nil]);
}

/*
 * If request parameter is not available but response block is available then createVenue
 * should return NSError object with description "Request parameters not found"
 */

- (void)testCreateVenueScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kNewVenueCreated];

	[self.kumoController createVenue:nil response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available but the request parameter dictionary
 * does not contain tenant name and new venue information dictionary then venueDetails should return
 * NSError object with description "Tenant name and new venue information are required"
 */

- (void)testCreateVenueScenario4 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kNewVenueCreated];
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];

	[self.kumoController createVenue:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndVenueInfoNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter dictionary
 * contains tenant name but does not contain new venue information dictionary then createVenue should
 * return NSError object with description "Tenant name and new venue information are required"
 */

- (void)testCreateVenueScenario5 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kNewVenueCreated];
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];

	[self.kumoController createVenue:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndVenueInfoNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter dictionary
 * contains new venue information dictionary but does not contain tenant name then createVenue should
 * return NSError object with description "Tenant name and new venue information are required"
 */

- (void)testCreateVenueScenario6 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kNewVenueCreated];
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	NSMutableDictionary *newVenueInfoDictionary = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:newVenueInfoDictionary forKey:kNewVenueInfoKey];

	[self.kumoController createVenue:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndVenueInfoNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter dictionary
 * contain tenant name name and new venue information dictionary then wlanServiceList should return
 * response data
 */

- (void)testCreateVenueScenario7 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kNewVenueCreated];
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];
	NSMutableDictionary *newVenueInfoDictionary = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:newVenueInfoDictionary forKey:kNewVenueInfoKey];

	[self.kumoController createVenue:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

#pragma mark - AP


#pragma mark - AP List

/*
 * If request parameter is nil then apList should return NSError object with
 * description "Request parameters not found"
 */

- (void)testAPListScenario1 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPListLoaded];
	[self.kumoController apList:nil response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter is not nil but tenant name is not available then apList
 * should return NSError object with description "Tenant name and venue name are required"
 */

- (void)testAPListScenario2 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPListLoaded];
	NSDictionary *requestParameters = [[NSDictionary alloc] init];

	[self.kumoController apList:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndVenueNameNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter is not nil, tenant name available but venue is not available then apList
 * should return NSError object with description "Tenant name and venue name are required"
 */

- (void)testAPListScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPListLoaded];
	NSDictionary *requestParameters = [[NSDictionary alloc] initWithObjectsAndKeys:@"kumomobile", kTenantNameKey, nil];

	[self.kumoController apList:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndVenueNameNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If tenant name and venue name is available apList should return the response object
 */
- (void)testAPListScenario4 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPListLoaded];
	NSDictionary *requestParameters = [[NSDictionary alloc] initWithObjectsAndKeys:@"kumomobile", kTenantNameKey, @"default", kVenueNameKey, nil];

	[self.kumoController apList:requestParameters response: ^(NSError *error, id data) {
	    if ([self isInternetConnectionError:error]) {
	        XCTAssertNotNil(error);
		}
	    else {
	        XCTAssertNotNil(data);
	        XCTAssertNil(error);
		}
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

#pragma mark - AP List with sorting and filtring

/*
 * If request parameter is nil then apList should return the response object
 */
- (void)testAPListWithSortAndFilterScenario1 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPListLoaded];
	[self.kumoController apList:nil sortBy:kSortingParameterNone filterOn:[NSDictionary dictionary] response: ^(NSError *error, id data) {
	    if ([self isInternetConnectionError:error]) {
	        XCTAssertNotNil(error);
		}
	    else {
	        XCTAssertNotNil(data);
	        XCTAssertNil(error);
		}
	    [expectation fulfill];
	}];
	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter is empty then apList then apList should return the response object
 */
- (void)testAPListWithSortAndFilterScenario2 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPListLoaded];
	NSDictionary *requestParameters = [[NSDictionary alloc] init];
	[self.kumoController apList:requestParameters sortBy:kSortingParameterNone filterOn:[NSDictionary dictionary] response: ^(NSError *error, id data) {
	    if ([self isInternetConnectionError:error]) {
	        XCTAssertNotNil(error);
		}
	    else {
	        XCTAssertNotNil(data);
	        XCTAssertNil(error);
		}
	    [expectation fulfill];
	}];
	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request and filter parameter available but sort parameter is none, apList should return the response object
 */
- (void)testAPListWithSortAndFilterScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPListLoaded];
	NSDictionary *requestParameters = [[NSDictionary alloc] initWithObjectsAndKeys:@"kumomobile", kTenantNameKey, @"default", kVenueNameKey, nil];
	[self.kumoController apList:requestParameters sortBy:kSortingParameterNone filterOn:[NSDictionary dictionary] response: ^(NSError *error, id data) {
	    if ([self isInternetConnectionError:error]) {
	        XCTAssertNotNil(error);
		}
	    else {
	        XCTAssertNotNil(data);
	        XCTAssertNil(error);
		}
	    [expectation fulfill];
	}];
	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request,sort and filter parameter available, apList should return the response object
 */
- (void)testAPListWithSortAndFilterScenario4 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPListLoaded];
	NSDictionary *requestParameters = [[NSDictionary alloc] initWithObjectsAndKeys:@"kumomobile", kTenantNameKey, @"default", kVenueNameKey, nil];
	[self.kumoController apList:requestParameters sortBy:kSortingParameterWlanCount filterOn:[NSDictionary dictionary] response: ^(NSError *error, id data) {
	    if ([self isInternetConnectionError:error]) {
	        XCTAssertNotNil(error);
		}
	    else {
	        XCTAssertNotNil(data);
	        XCTAssertNil(error);
		}
	    [expectation fulfill];
	}];
	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request,sort and filter parameter available, apList should return the response object
 */
- (void)testAPListWithSortAndFilterScenario5 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPListLoaded];
	NSDictionary *requestParameters = [[NSDictionary alloc] initWithObjectsAndKeys:@"kumomobile", kTenantNameKey, @"default", kVenueNameKey, nil];
	[self.kumoController apList:requestParameters sortBy:kSortingParameterName filterOn:[NSDictionary dictionary] response: ^(NSError *error, id data) {
	    if ([self isInternetConnectionError:error]) {
	        XCTAssertNotNil(error);
		}
	    else {
	        XCTAssertNotNil(data);
	        XCTAssertNil(error);
		}
	    [expectation fulfill];
	}];
	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request,sort and filter parameter available, apList should return the response object
 */
- (void)testAPListWithSortAndFilterScenario6 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPListLoaded];
	NSDictionary *requestParameters = [[NSDictionary alloc] initWithObjectsAndKeys:@"kumomobile", kTenantNameKey, @"default", kVenueNameKey, nil];
	[self.kumoController apList:requestParameters sortBy:kSortingParameterMacAddress filterOn:[NSDictionary dictionary] response: ^(NSError *error, id data) {
	    if ([self isInternetConnectionError:error]) {
	        XCTAssertNotNil(error);
		}
	    else {
	        XCTAssertNotNil(data);
	        XCTAssertNil(error);
		}
	    [expectation fulfill];
	}];
	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

#pragma mark - AP Detail

/*
 * If request parameter is nil then apDetails should return NSError object with
 * description "Request parameters not found"
 */

- (void)testAPDetailScenario1 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPDetailsLoaded];
	[self.kumoController apDetail:nil response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter is not nil but tenant name is not available then apDetails
 * should return NSError object with description "Tenant name venue name and serial number are required"
 */

- (void)testAPDetailScenario2 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPDetailsLoaded];
	NSDictionary *requestParameters = [[NSDictionary alloc] init];

	[self.kumoController apDetail:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndVenueNameAndSerialNumberNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter is not nil, tenant name available but venue is not available then apDetails
 * should return NSError object with description "Tenant name venue name and serial number are required"
 */

- (void)testAPDetailScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPDetailsLoaded];
	NSDictionary *requestParameters = [[NSDictionary alloc] initWithObjectsAndKeys:@"kumomobile", kTenantNameKey, nil];

	[self.kumoController apDetail:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndVenueNameAndSerialNumberNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter is not nil, tenant name and venue name available but serial number is not available then apDetails should return NSError object with description "Tenant name venue name and serial number are required"
 */

- (void)testAPDetailScenario4 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPDetailsLoaded];
	NSDictionary *requestParameters = [[NSDictionary alloc] initWithObjectsAndKeys:@"kumomobile", kTenantNameKey, @"default", kVenueNameKey, nil];

	[self.kumoController apDetail:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndVenueNameAndSerialNumberNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If tenant name, venue name and serial number is available apDetails should return the response object
 */
- (void)testAPDetailScenario5 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPDetailsLoaded];
	NSDictionary *requestParameters = [[NSDictionary alloc] initWithObjectsAndKeys:@"kumomobile", kTenantNameKey, @"default", kVenueNameKey, @"161205002059", kSerialNumberKey, nil];

	[self.kumoController apDetail:requestParameters response: ^(NSError *error, id data) {
	    if ([self isInternetConnectionError:error]) {
	        XCTAssertNotNil(error);
		}
	    else if (OBJ_NOT_NIL(error)) {
	        XCTAssertNil(data);
	        XCTAssertNotNil(error);
		}
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    if (error) {
	        XCTAssertNotNil(error);
		}
	    expectation = nil;
	}];
}

#pragma mark - AP Create


/*
 * If request parameter is nil then createAP should return NSError object with
 * description "Request parameters not found"
 */

- (void)testCreateAPScenario1 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPCreated];
	[self.kumoController createAP:nil response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter is empty then createAP
 * should return NSError object with description "Tenant name, venue name and AP information are required"
 */

- (void)testCreateAPScenario2 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPCreated];
	NSDictionary *requestParameters = [[NSDictionary alloc] init];

	[self.kumoController createAP:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndVenueNameAndAPInfoNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request is available then createAP should return the response object
 */
- (void)testCreateAPScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPCreated];
	NSDictionary *requestParameters = [[NSDictionary alloc] initWithObjectsAndKeys:@"kumomobile", kTenantNameKey, @"default", kVenueNameKey, @"1234567890", kSerialNumberKey, nil];

	[self.kumoController createAP:requestParameters response: ^(NSError *error, id data) {
	    if ([self isInternetConnectionError:error]) {
	        XCTAssertNotNil(error);
		}
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

#pragma mark - AP Edit

/*
 * If request parameter is nil then editAP should return NSError object with
 * description "Request parameters not found"
 */

- (void)testEditAPScenario1 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPEdited];
	[self.kumoController editAP:nil response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter is empty then editAP
 * should return NSError object with description "Tenant name, venue name and AP information are required"
 */

- (void)testEditAPScenario2 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPEdited];
	NSDictionary *requestParameters = [[NSDictionary alloc] init];

	[self.kumoController editAP:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndVenueNameAndAPInfoNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request is available then editAP should return the response object
 */
- (void)testEditAPScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPEdited];
	NSDictionary *requestParameters = [[NSDictionary alloc] initWithObjectsAndKeys:@"kumomobile", kTenantNameKey, @"default", kVenueNameKey, @"1234567890", kSerialNumberKey, nil];

	[self.kumoController editAP:requestParameters response: ^(NSError *error, id data) {
	    if ([self isInternetConnectionError:error]) {
	        XCTAssertNotNil(error);
		}
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

#pragma mark - AP Delete

/*
 * If request parameter is nil then deleteAP should return NSError object with
 * description "Request parameters not found"
 */

- (void)testDeleteAPScenario1 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPDeleted];
	[self.kumoController deleteAP:nil response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter is empty then deleteAP
 * should return NSError object with description "Tenant name, venue name and AP information are required"
 */

- (void)testDeleteAPScenario2 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPDeleted];
	NSDictionary *requestParameters = [[NSDictionary alloc] init];

	[self.kumoController deleteAP:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndVenueNameAndAPInfoNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request is available then deleteAP should return the response object
 */
- (void)testDeleteAPScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPDeleted];
	NSDictionary *requestParameters = [[NSDictionary alloc] initWithObjectsAndKeys:@"kumomobile", kTenantNameKey, @"default", kVenueNameKey, @"1234567890", kSerialNumberKey, nil];

	[self.kumoController deleteAP:requestParameters response: ^(NSError *error, id data) {
	    if ([self isInternetConnectionError:error]) {
	        XCTAssertNotNil(error);
		}
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

#pragma mark - Events

/*
 * If request parameter and response block are not available then eventList should handle this scenanrio
 * without throwing any exception
 */
- (void)testEventListScenario1 {
	XCTAssertNoThrow([self.kumoController eventList:nil response:nil]);
}

/*
 * If request parameter is available but response block is not available then eventList should handle this
 * scenario without throwing any exception
 */
- (void)testEventListScenario2 {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	XCTAssertNoThrow([self.kumoController eventList:requestParameters response:nil]);
}

/*
 * If request parameter is not available but response block is available then eventList should return
 * NSError object with description "Request parameters not found"
 */

- (void)testEventListScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kEventListLoaded];

	[self.kumoController eventList:nil response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available but the request parameter dictionary
 * does not contain tenant name and  venue name then eventList should return NSError object
 * with description "Tenant name venue name and are required"
 */

- (void)testEventListScenario4 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kEventListLoaded];
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];

	[self.kumoController eventList:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndVenueNameNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter dictionary
 * contain tenant name but does not contain venue name then eventList should return NSError object
 * with description "Tenant name venue name and are required"
 */

- (void)testEventListScenario5 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kEventListLoaded];

	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];

	[self.kumoController eventList:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndVenueNameNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter dictionary
 * contains tenant name but does not contain venue name then eventList should return NSError object
 * with description "Tenant name venue name and are required"
 */

- (void)testEventListScenario6 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kEventListLoaded];

	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"default" forKey:kVenueNameKey];

	[self.kumoController eventList:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndVenueNameNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter dictionary
 * contains tenant name and venue name then eventList should return the response data
 */

- (void)testEventListScenario7 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kEventListLoaded];

	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];
	[requestParameters setObject:@"default" forKey:kVenueNameKey];

	[self.kumoController eventList:requestParameters response: ^(NSError *error, id data) {
	    if (OBJ_NOT_NIL(error)) {
	        XCTAssertNotNil(error);
		}
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter and response block are not available then EventDetails should handle this scenanrio
 * without throwing any exception
 */
- (void)testEventDetailsScenario1 {
	XCTAssertNoThrow([self.kumoController eventDetails:nil response:nil]);
}

/*
 * If request parameter is available but response block is not available then EventDetails should handle
 * this scenario without throwing any exception
 */
- (void)testEventDetailsScenario2 {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	XCTAssertNoThrow([self.kumoController eventDetails:requestParameters response:nil]);
}

/*
 * If request parameter is not available but response block is available then EventDetails should return
 * NSError object with description "Request parameters not found"
 */

- (void)testEventDetailsScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kEventDetailsLoaded];

	[self.kumoController eventDetails:nil response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If event list, filtering parameter and response block are nil and sortby parameter is not a valid input
 * then eventList should not throw any exception
 *
 */

- (void)testEventsListScenario1 {
    XCTAssertNoThrow([self.kumoController eventList:nil sortBy:3 filterOn:nil response:nil]);
}

/*
 * If request parameter and filtering parameter are nil, response block is available and sortby parameter is not a valid input
 * then eventList should return error
 *
 */
- (void)testEventsListScenario2 {
    __block XCTestExpectation *expectation = [self expectationWithDescription:kEventListLoaded];
    [self.kumoController eventList:nil sortBy:3 filterOn:nil response: ^(NSError *error, id data) {
        // TODO: Update this test case once the request url is available
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
        XCTAssertNil(error);
        expectation = nil;
    }];
}

/*
 * If filtering parameter is nil, response block and request parameter are available and sortby parameter is not a valid input
 * then eventList should return error
 *
 */
- (void)testEventsListScenario3 {
    NSMutableDictionary *requestParams = [[NSMutableDictionary alloc] init];
    
    [requestParams setObject:[NSNumber numberWithInt:2] forKey:kLimit];
    [requestParams setObject:[NSNumber numberWithInt:1] forKey:kOffset];
    
    __block XCTestExpectation *expectation = [self expectationWithDescription:kEventListLoaded];
    [self.kumoController eventList:requestParams sortBy:2 filterOn:nil response: ^(NSError *error, id data) {
            // TODO: Update this test case once the request url is available
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
        XCTAssertNil(error);
        expectation = nil;
    }];
}

/*
 * If request parameter is nil, response block and  filtering parameter are available and sortby parameter is a valid input
 * then eventList should return error
 *
 */
- (void)testEventsListScenario4 {
    NSMutableDictionary *filter = [[NSMutableDictionary alloc] init];
    // TODO: Update this test case once the request url is available
}

/*
 * If request parameter, response block and  filtering parameter are available and sortby parameter is a valid input
 * then eventList should return event list
 *
 */
- (void)testEventsListScenario5 {
    // Create request parameter dictionary
    NSMutableDictionary *requestParams = [[NSMutableDictionary alloc] init];
    [requestParams setObject:[NSNumber numberWithInt:2] forKey:kLimit];
    [requestParams setObject:[NSNumber numberWithInt:1] forKey:kOffset];
    
    // Create filter dictionary
    NSMutableDictionary *filter = [[NSMutableDictionary alloc] init];
    // TODO: update this test case once the date formate is available
    
}


#pragma mark - Client

#pragma mark - clientList


/*
 * If request parameter and response block are not available then clientList should
 * handle this scenanrio without throwing any exception
 */
- (void)testClientListScenario1 {
	XCTAssertNoThrow([self.kumoController clientList:nil response:nil]);
}

/*
 * If request parameter is available but response block is not available then clientList should
 * handle this scenario without throwing any exception
 */
- (void)testClientListScenario2 {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	XCTAssertNoThrow([self.kumoController clientList:requestParameters response:nil]);
}

/*
 * If request parameter is not available but response block is available then clientList should return
 * NSError object with description "Request parameters not found"
 */

- (void)testClientListScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kClientListLoaded];

	[self.kumoController clientList:nil response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available but the request parameter dictionary
 * does not contain tenant name then clientList should return NSError object with description
 * "Tenant name is required"
 */

- (void)testClientListScenario4 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kClientListLoaded];
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];

	[self.kumoController clientList:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantNameNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter dictionary
 * contains tenant name then clientList should return the response data
 */

- (void)testClientListScenario5 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kClientListLoaded];
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];

	[self.kumoController clientList:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

#pragma mark - active/blocked Client

/*
 * If request parameter and response block are nil then clientList should not throwing any exception
 */
- (void)testClientListScenario6 {
	XCTAssertNoThrow([self.kumoController clientList:nil clientType:0 response:nil]);
}

/*
 * If request parameter is available,clientType is 0 but response block is not available then clientList should
 * handle this scenario without throwing any exception
 */
- (void)testClientListScenario7 {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	XCTAssertNoThrow([self.kumoController clientList:requestParameters clientType:0 response:nil]);
}

/*
 * If request parameter is not available, clientType is 0 but response block is available then clientList should return NSError object with description "Request parameters not found"
 */

- (void)testClientListScenario8 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kClientListLoaded];
	[self.kumoController clientList:nil clientType:0 response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];
	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter is blank, clientType is 0 but response block is available then clientList should return NSError object with description "Tenant name is required"
 */
- (void)testClientListScenario9 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kClientListLoaded];
	[self.kumoController clientList:[NSDictionary dictionary] clientType:0 response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantNameNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];
	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter is blank, clientType is 1 but response block is available then clientList should return NSError object with description "Tenant name is required"
 */
- (void)testClientListScenario10 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kClientListLoaded];
	[self.kumoController clientList:[NSDictionary dictionary] clientType:1 response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantNameNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];
	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

#pragma mark - clientDetails

/*
 * If request parameter and response block are not available then clientDetails should handle this
 * scenanrio without throwing any exception
 */
- (void)testClientDetailsScenario1 {
	XCTAssertNoThrow([self.kumoController clientDetails:nil response:nil]);
}

/*
 * If request parameter is available but response block is not available then clientDetails should handle
 * this scenario without throwing any exception
 */
- (void)testClientDetailsScenario2 {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	XCTAssertNoThrow([self.kumoController clientDetails:requestParameters response:nil]);
}

/*
 * If request parameter is not available but response block is available then clientDetails should return
 * NSError object with description "Request parameters not found"
 */

- (void)testClientDetailsScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kClientDetailsLoaded];

	[self.kumoController clientDetails:nil response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForRequestParamNotFoundKeyDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available but the request parameter dictionary
 * does not contain tenant name and  mac address then clientDetails should return NSError object
 * with description "Tenant name and mac address are required"
 */

- (void)testClientDetailsScenario4 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kClientDetailsLoaded];
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];

	[self.kumoController clientDetails:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndMacAddressNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter dictionary
 * contain tenant name but does not contain mac address name then clientDetails should return NSError
 * object with description "Tenant name and mac address are required"
 */

- (void)testClientDetailsScenario5 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kClientDetailsLoaded];

	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];

	[self.kumoController clientDetails:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndMacAddressNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter dictionary
 * contains tenant name but does not contain mac address then clientDetails should return NSError object
 * with description "Tenant name and mac address are required"
 */

- (void)testClientDetailsScenario6 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kClientDetailsLoaded];

	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"20:C9:D0:8C:68:9F" forKey:kMacAddressInfoKey];

	[self.kumoController clientDetails:requestParameters response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	    XCTAssertEqual(kKUMOCodeForTenantAndMacAddressNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNil(data);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If request parameter dictionary and response block are available and the request parameter dictionary
 * contains tenant name and mac address then clientDetails should return the response data
 */

- (void)testClientDetailsScenario7 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kClientDetailsLoaded];

	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	[requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];
	[requestParameters setObject:@"20:C9:D0:8C:68:9F" forKey:kMacAddressInfoKey];

	[self.kumoController clientDetails:requestParameters response: ^(NSError *error, id data) {
	    if (OBJ_NOT_NIL(error)) {
	        XCTAssertNotNil(error);
		}
	    else {
	        XCTAssertNil(error);
	        XCTAssertNotNil(data);
		}
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

#pragma mark - Login

/*
 * If request parameter and response block are not available then loginRequestWithParameters should
 * handle this scenanrio without throwing any exception
 */
- (void)testLoginScenario1 {
	XCTAssertNoThrow([self.kumoController loginRequestWithParameters:nil response:nil]);
}

/*
 * If request parameter is available but response block is not available then loginRequestWithParameters
 * should handle this scenario without throwing any exception
 */
- (void)testLoginScenario2 {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	XCTAssertNoThrow([self.kumoController loginRequestWithParameters:requestParameters response:nil]);

	//TODO: More cases will be added once login request is implemented
}

#pragma mark - Logout

/*
 * If request parameter and response block are not available then loginRequestWithParameters should
 * handle this scenanrio without throwing any exception
 */
- (void)testLogoutScenario1 {
	XCTAssertNoThrow([self.kumoController logoutRequestWithResponse:nil]);
}

/*
 * If request parameter is available but response block is not available then loginRequestWithParameters
 * should handle this scenario without throwing any exception
 */
- (void)testLogoutScenario2 {
	NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
	XCTAssertNoThrow([self.kumoController logoutRequestWithResponse:nil]);

	//TODO: More cases will be added once logout request is implemented
}

- (BOOL)isInternetConnectionError:(NSError *)error {
	if (OBJ_NOT_NIL(error) && OBJ_NOT_NIL(error.userInfo) && [[error.userInfo objectForKey:kLocalizedDescription] containsString:kCouldNotConnectToServer]) {
		return YES;
	}
	return NO;
}

#pragma mark - Image upload/download

/*
 * If Image, image url and response block are nil then uploadImage should not throw any exception
 */

- (void)testUploadImageScenario1 {
	XCTAssertNoThrow([self.kumoController uploadImage:nil imageUrl:nil response:nil]);
}

/*
 * If image is available but upload url and response block are not available then uploadImage
 * should not throw any exception
 */

- (void)testUploadImageScenario2 {
	NSData *data = [[NSData alloc] init];
	XCTAssertNoThrow([self.kumoController uploadImage:data imageUrl:nil response:nil]);
}

/*
 * If image url is not available but image data and response block are not available then
 * uploadImage method should not throw any exception
 */

- (void)testUploadImageScenario3 {
	XCTAssertNoThrow([self.kumoController uploadImage:nil imageUrl:kEmptyText response:nil]);
}

/*
 * If image url and image data are not available but response block is available then
 * uploadImage should return NSError instance with description "KUMO Error: Image upload url and
 * image are required"
 */

- (void)testUploadImageScenario4 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kImageUploaded];

	[self.kumoController uploadImage:nil imageUrl:nil response: ^(NSError *error, id data) {
	    XCTAssertEqual(kKUMOCodeForImageAndUrlNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If upload image and upload url are available but response block is not available then
 * uploadImage should not throw any exception
 */

- (void)testUploadImageScenario5 {
	XCTAssertNoThrow([self.kumoController uploadImage:[[NSData alloc] init] imageUrl:kEmptyText response:nil]);
}

/*
 * If upload image and response block are available but image url is not available then uploadImage
 * should return NSError instance with description "KUMO Error: Image upload url and image are required"
 */

- (void)testUploadImageScenario6 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kImageUploaded];

	[self.kumoController uploadImage:[[NSData alloc] init] imageUrl:nil response: ^(NSError *error, id data) {
	    XCTAssertEqual(kKUMOCodeForImageAndUrlNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If image url and response block are available but image data is not available then uploadImage
 * should return NSError instance with description "KUMO Error: Image upload url and image are required"
 */

- (void)testUploadImageScenario7 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kImageUploaded];

	[self.kumoController uploadImage:nil imageUrl:kEmptyText response: ^(NSError *error, id data) {
	    XCTAssertEqual(kKUMOCodeForImageAndUrlNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	}];
}

/*
 * If image url, image data and response block are available then uploadImage should return upload response
 * data from the server
 */

- (void)testUploadImageScenario8 {
	//Update this test case once the request to upload image is implemented
}

/*
 * If image url and response block are not available then downloadImage method shoud not
 * throw any exception
 */

- (void)testDownloadImageScenario1 {
	XCTAssertNoThrow([self.kumoController downloadImage:nil response:nil]);
}

/*
 * If image url is available but the response block is not available then downloadImage method should
 * not throw any exception
 */

- (void)testDownloadImageScenario2 {
	XCTAssertNoThrow([self.kumoController downloadImage:kEmptyText response:nil]);
}

/*
 * If image url is not available but the response block is available then downloadImage method
 * should return NSError object with description "KUMO Error: Image download url is required"
 */

- (void)testDownloadImageScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kImageDownloaded];
	[self.kumoController downloadImage:nil response: ^(NSError *error, id data) {
	    XCTAssertEqual(kKUMOCodeForImageUrlNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If image url and response block are available then downloadImage method should return the image
 */
- (void)testDownloadImageScenario4 {
	// TODO: update this test case once download image request is implemented
}

@end
