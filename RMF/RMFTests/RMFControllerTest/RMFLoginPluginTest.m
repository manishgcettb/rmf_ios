/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "RMFLoginPlugin.h"
#import "RMFSystemManager.h"
#import "RMFConfiguration.h"
#import "RMFMacro.h"

#define TEST_PORT                   @"8080"
#define TEST_IP                     @"10.199.4.106"

#define  kLoginRequestCompleted     @"Login request competed"
#define  kLogoutRequestCompleted    @"Logout request completed"

@interface RMFLoginPluginTest : XCTestCase

// Stores the instance of RMFConfiguration
@property (nonatomic, strong) RMFConfiguration *configuration;

// Stores the instance of RMFWlanPlugin
@property (nonatomic, strong) RMFLoginPlugin *loginPlugin;

// Stores the request parameters
@property (nonatomic, strong) NSMutableDictionary *requestParameters;

@end

@implementation RMFLoginPluginTest

- (void)setUp {
	[super setUp];
	[self initialConfiguration];
	self.loginPlugin = [RMFLoginPlugin sharedRMFLoginPlugin];
	[self initRequestParameters];
}

- (void)tearDown {
	[super tearDown];
}

/*
 * initialise RMFConfiguration instance with system type, system IP and system port
 */

- (void)initialConfiguration {
	self.configuration = [RMFConfiguration sharedRMFConfiguration];
	self.configuration.system = kSystemTypeKUMO;
	self.configuration.systemIP = TEST_IP;
	self.configuration.port = TEST_PORT;
}

/*
 * initialise request parameter dictionary
 */

- (void)initRequestParameters {
	self.requestParameters = [[NSMutableDictionary alloc] init];
	if (self.requestParameters) {
		[self.requestParameters setObject:@"username" forKey:kUserNameKey];
		[self.requestParameters setObject:@"password" forKey:kPasswordKey];
	}
}

/*
 *  sharedRMFLogin should return a valid instance
 */

- (void)testSharedRMFLoginPlugin {
	XCTAssertNotNil(self.loginPlugin);
}

/*
 * [[RMFLoginPlugin alloc] init] should return a valid instance
 */

- (void)testRMFLoginPluginUsingAlloc {
	XCTAssertNotNil([[RMFLoginPlugin alloc] init]);
}

/*
 * [[RMFLoginPlugin alloc] init] and sharedRMFLogin should return same instance
 */

- (void)testIssharedRMFLoginPluginAndAllocReturnsSameObject {
	XCTAssertEqual([[RMFLoginPlugin alloc] init], self.loginPlugin);
}

/*
 * If request parameters and response block are nil then loginRequestWithParameters should not throw any
 * exception
 */

- (void)testLoginScenario1 {
	XCTAssertNoThrow([self.loginPlugin loginRequestWithParameters:nil response:nil]);
}

/*
 * If request parameter dictionary is available but the response block is nil then loginRequestWithParameters
 * should not throw any exception
 */

- (void)testLoginScenario2 {
	XCTAssertNoThrow([self.loginPlugin loginRequestWithParameters:self.requestParameters response:nil]);
}

/*
 * If request parameter dictionary is nil but the response block is available then
 * loginRequestWithParameters should return NSError object with description "Request parameters not found"
 */

- (void)testLoginScenario3 {
	//TODO: Write this case once login request is implemented
}

/*
 * If correct request parameters and response block are available then loginRequestWithParameters should
 * return response data
 */
- (void)testLoginScenario4 {
	//TODO: Write this case once login request is implemented
}

/*
 * If response block is nil then logoutRequestWithResponse should not throw any
 * exception
 */

- (void)testLogoutScenario1 {
	XCTAssertNoThrow([self.loginPlugin logoutRequestWithResponse:nil]);
}

/*
 * If response block is available then logoutRequestWithResponse should
 * return response data
 */
- (void)testLogoutScenario2 {
	//TODO: Write this case once logout request is implemented
}

@end
