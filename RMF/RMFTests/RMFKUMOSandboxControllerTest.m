/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "RMFKUMOSandboxController.h"
#import "RMFMacro.h"


#define RMFBUNDLE_TEST           [NSBundle bundleWithPath:[[[NSBundle mainBundle] resourcePath]stringByAppendingPathComponent:@"RMFBundle.bundle"]]


#define JSON_FILE_LOADED @"local json file should be loaded"


@interface RMFKUMOSandboxController (test)
- (void)jsonForFileName:(NSString *)filename response:(void (^)(NSError *error, id data))completion;
@end


@interface RMFKUMOSandboxControllerTest : XCTestCase

// Stores and returns the instance of RMFKumoController
@property (nonatomic, strong) RMFKUMOSandboxController *rmfKUMOSandboxController;
@end

@implementation RMFKUMOSandboxControllerTest

- (void)setUp {
	[super setUp];
	// Put setup code here. This method is called before the invocation of each test method in the class.
	self.rmfKUMOSandboxController = [RMFKUMOSandboxController sharedInstance];
}

- (void)tearDown {
	// Put teardown code here. This method is called after the invocation of each test method in the class.
	[super tearDown];
}

/*
 * Checks whether the instance of RMFKUMOSandboxController is available or not
 */
- (void)testRMFKUMOSandboxControllerScenario1 {
	XCTAssertNotNil(self.rmfKUMOSandboxController);
}

/*
 * Checks whether the instance created with [RMFKUMOSandboxController alloc] init] is available
 * or not
 */
- (void)testInitScenario1 {
	XCTAssertNotNil([[RMFKUMOSandboxController alloc] init]);
}

/*
 * If block is nil then apList should not throw any exception
 */
- (void)testApListScenario1 {
	XCTAssertNoThrow([self.rmfKUMOSandboxController apList:nil]);
}

/*
 * If block is not nil then apList should return objects
 */
- (void)testApListScenario2 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:JSON_FILE_LOADED];
	[self.rmfKUMOSandboxController apList: ^(NSError *error, id data) {
	    if (RMFBUNDLE_TEST != nil) {
	        XCTAssertNotNil(data);
	        XCTAssertNil(error);
		}
	    else {
	        XCTAssertNil(data);
	        XCTAssertNotNil(error);
		}
	    [expectation fulfill];
	}];
	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If block is nil then apDetail should not throw any exception
 */
- (void)testApDetailScenario1 {
	XCTAssertNoThrow([self.rmfKUMOSandboxController apDetail:nil]);
}

/*
 * If block is not nil then apDetail should return objects
 */
- (void)testApDetailScenario2 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:JSON_FILE_LOADED];
	[self.rmfKUMOSandboxController apDetail: ^(NSError *error, id data) {
	    if (RMFBUNDLE_TEST != nil) {
	        XCTAssertNotNil(data);
	        XCTAssertNil(error);
		}
	    else {
	        XCTAssertNil(data);
	        XCTAssertNotNil(error);
		}
	    [expectation fulfill];
	}];
	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If block is nil then wlanList should not throw any exception
 */
- (void)testWlanListScenario1 {
	XCTAssertNoThrow([self.rmfKUMOSandboxController wlanList:nil]);
}

/*
 * If block is not nil then wlanList should return objects
 */
- (void)testWlanListScenario2 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:JSON_FILE_LOADED];
	[self.rmfKUMOSandboxController wlanList: ^(NSError *error, id data) {
	    if (RMFBUNDLE_TEST != nil) {
	        XCTAssertNotNil(data);
	        XCTAssertNil(error);
		}
	    else {
	        XCTAssertNil(data);
	        XCTAssertNotNil(error);
		}
	    [expectation fulfill];
	}];
	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If block is nil then wlanDetails should not throw any exception
 */
- (void)testWlanDetailsScenario1 {
	XCTAssertNoThrow([self.rmfKUMOSandboxController wlanDetails:nil]);
}

/*
 * If block is not nil then wlanDetails should return objects
 */
- (void)testWlanDetailsScenario2 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:JSON_FILE_LOADED];

	[self.rmfKUMOSandboxController wlanDetails: ^(NSError *error, id data) {
	    if (RMFBUNDLE_TEST != nil) {
	        XCTAssertNotNil(data);
	        XCTAssertNil(error);
		}
	    else {
	        XCTAssertNil(data);
	        XCTAssertNotNil(error);
		}
	    [expectation fulfill];
	}];
	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If block is nil then wlanStatus should not throw any exception
 */
- (void)testWlanStatusScenario1 {
	XCTAssertNoThrow([self.rmfKUMOSandboxController wlanStatus:nil]);
}

/*
 * If block is not nil then wlanStatus should return objects
 */
- (void)testWlanStatusScenario2 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:JSON_FILE_LOADED];
	[self.rmfKUMOSandboxController wlanStatus: ^(NSError *error, id data) {
	    if (RMFBUNDLE_TEST != nil) {
	        XCTAssertNotNil(data);
	        XCTAssertNil(error);
		}
	    else {
	        XCTAssertNil(data);
	        XCTAssertNotNil(error);
		}
	    [expectation fulfill];
	}];
	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If both parameter is nil then jsonForFileName should not throw any exception
 */
- (void)testJsonForFileNameScenario1 {
	XCTAssertNoThrow([self.rmfKUMOSandboxController jsonForFileName:nil response:nil]);
}

/*
 * If filename is nil then jsonForFileName should return error
 */
- (void)testJsonForFileNameScenario2 {
	[self.rmfKUMOSandboxController jsonForFileName:nil response: ^(NSError *error, id data) {
	    XCTAssertNotNil(error);
	}];
}

/*
 * If filename is not nil then jsonForFileName should return object
 */
- (void)testJsonForFileNameScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:JSON_FILE_LOADED];
	[self.rmfKUMOSandboxController jsonForFileName:@"aplist.json" response: ^(NSError *error, id data) {
	    if (RMFBUNDLE_TEST != nil) {
	        XCTAssertNotNil(data);
	        XCTAssertNil(error);
		}
	    else {
	        XCTAssertNil(data);
	        XCTAssertNotNil(error);
		}
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

@end
