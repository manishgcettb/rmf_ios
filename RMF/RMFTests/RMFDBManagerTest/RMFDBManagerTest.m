/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFDBManager.h"
#import "RMFDBAccessLayer.h"
#import "RMFNotifications.h"
#import "RMFRootAssociation.h"
#import <CoreData/CoreData.h>


@interface RMFDBManagerTest : XCTestCase
{
	RMFDBAccessLayer *databaseManager;
	NSManagedObjectContext *managedObjectContext;
	NSManagedObjectModel *managedObjectModel;
}

@end

@interface RMFDBAccessLayer (PrivateMethods)
- (void)saveChanges;
- (void)createDatabaseManager;
- (NSString *)getPathToDatabase;
@end

@implementation RMFDBManagerTest

- (void)setUp {
	[super setUp];
	// Put setup code here. This method is called before the invocation of each test method in the class.
	databaseManager = [RMFDBAccessLayer sharedRMFDBAccessLayer];
	XCTAssertNotNil(databaseManager, @"not nil");
}

- (void)tearDown {
	// Put teardown code here. This method is called after the invocation of each test method in the class.
	[super tearDown];
}

//test to insert notifications
- (void)testInsertNotifications {
	RMFNotifications *notifications = [databaseManager insertNotifications];
	if (notifications != nil) {
		notifications.message = @"test Message";
		notifications.notificationTime = [NSDate date];
		notifications.type = @"1";
		notifications.readStatus = false;
		if (notifications) {
			XCTAssertNotNil([databaseManager saveNotificationObject:notifications]);
		}
	}
}

/**
 * This method tests that Notifications are fetched successfully from the database.
 */

- (void)testGetNotifications {
	NSArray *actualNotificationListInDatabase = [databaseManager getNotifications];
	RMFNotifications *notifications = [databaseManager insertNotifications];

	XCTAssertNotNil([databaseManager saveNotificationObject:notifications]);
	NSArray *notificationListInDatabase = [databaseManager getNotifications];
	if (actualNotificationListInDatabase != nil) {
		XCTAssertFalse(notificationListInDatabase.count == (actualNotificationListInDatabase.count + 1), @"There should be notification present in the database");
	}
}

//test to insert notifications
- (void)testInsertAssosciation {
	RMFRootAssociation *association = [databaseManager insertRootAssociation];
	if (association != nil) {
		association.parent = @"test parent";
		association.current = @"test current";
		association.childs = @"1";

		if (association) {
			XCTAssertNotNil([databaseManager saveRootAssociationObject:association]);
		}
	}
}

/**
 * This method tests that Notifications are fetched successfully from the database.
 */

- (void)testGetAssociation {
	NSArray *actualAssociationListInDatabase = [databaseManager getRootAssociation:@"test"];
	RMFRootAssociation *association = [databaseManager insertRootAssociation];

	XCTAssertNotNil([databaseManager saveRootAssociationObject:association]);
	NSArray *associationListInDatabase = [databaseManager getRootAssociation:@"test"];
	if (actualAssociationListInDatabase != nil) {
		XCTAssertFalse(associationListInDatabase.count == (actualAssociationListInDatabase.count + 1), @"There should be notification present in the database");
	}
}

- (void)testInsertUser {
	RMFUser *user = [[RMFUser alloc] init];
	user.userName = @"testName";
	user.roleType = 1;
	user.phoneNumber = @"123123";
	user.address = @"test address";
	user.fullName = @"test full name";
	user.password = @"test password";
	user.email = @"email@email.com";
	XCTAssertNotNil([databaseManager insertUser:user]);
}

- (void)testGetUser {
	XCTAssertNil([databaseManager getUser]);
}

- (void)testdeleteUser {
	XCTAssertNil([databaseManager deleteUser]);
}

@end
