/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "RMFDBAccessLayer.h"
#import "RMFErrorHandler.h"
#import "RMFMacro.h"


@interface RMFDBAccessLayer (Tests)
- (NSMutableArray *)mapAPManagedObjectToRFMAPModel:(NSArray *)managedObjectArray;
@end

@interface RMFDBLayerTest : XCTestCase

// Stores/returns RMFDBAccessLayer instance
@property (nonatomic, strong) RMFDBAccessLayer *dbAccessLayer;

@end

@implementation RMFDBLayerTest

- (void)setUp {
	[super setUp];
	[[RMFConfiguration sharedRMFConfiguration] setSystem:kSystemTypeKUMO];
	self.dbAccessLayer = [RMFDBAccessLayer sharedRMFDBAccessLayer];
}

- (void)tearDown {
	// Put teardown code here. This method is called after the invocation of each test method in the class.
	[super tearDown];
}

// Test apConstraintsParameter
- (void)testAPConstraintsParameter {
	[[RMFDBAccessLayer sharedRMFDBAccessLayer] setApConstraintsParameter:kAPConstraintsParameterSerialNumber];
	XCTAssertEqual([[RMFDBAccessLayer sharedRMFDBAccessLayer] apConstraintsParameter], kAPConstraintsParameterSerialNumber);
}

#pragma  mark  - insertAP
/*
 * If input parameter is nil then insertAP should return NSError object with description "Input parameter can not be nil"
 */
- (void)testInsertAPScenario1 {
	NSError *error = [[RMFDBAccessLayer sharedRMFDBAccessLayer] insertAP:nil];
	XCTAssertEqual(kKUMOCodeForDatabaseInputParameterNotFound, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
}

/*
 * If input parameter is not nil and database file not found then insertAP should return NSError object with description "Database instance not found"
 */
- (void)testInsertAPScenario2 {
	RMFAccessPoint *rmfAccessPoint = [[RMFAccessPoint alloc] init];
	NSError *error = [[RMFDBAccessLayer sharedRMFDBAccessLayer] insertAP:rmfAccessPoint];
	if ([self isDatabaseAvailable]) {
		XCTAssertEqual(kKUMOCodeForDatabaseInstanceNotFound, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	}
}

#pragma  mark  - updateAP

/*
 * If input parameter is nil then updateAP should return NSError object with description "Input parameter can not be nil"
 */
- (void)testUpdateAPScenario1 {
	NSError *error = [[RMFDBAccessLayer sharedRMFDBAccessLayer] updateAP:nil];
	XCTAssertEqual(kKUMOCodeForDatabaseInputParameterNotFound, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
}

/*
 * If input parameter is not nil and database file not found then updateAP should throw exception
 */
- (void)testUpdateAPScenario2 {
	RMFAccessPoint *rmfAccessPoint = [[RMFAccessPoint alloc] init];
	if ([self isDatabaseAvailable]) {
		XCTAssertNoThrow([[RMFDBAccessLayer sharedRMFDBAccessLayer] updateAP:rmfAccessPoint]);
	}
	else {
		XCTAssertNoThrow([[RMFDBAccessLayer sharedRMFDBAccessLayer] updateAP:rmfAccessPoint]);
	}
}

/*
 * If input parameter is not nil, apConstraintsParameter is kAPConstraintsParameterSerialNumber and database file not found then updateAP should throw exception
 */
- (void)testUpdateAPScenario3 {
	RMFAccessPoint *rmfAccessPoint = [[RMFAccessPoint alloc] init];
	rmfAccessPoint.serialNumber = @"";
	[[RMFDBAccessLayer sharedRMFDBAccessLayer] setApConstraintsParameter:kAPConstraintsParameterSerialNumber];

	XCTAssertNil([[RMFDBAccessLayer sharedRMFDBAccessLayer] updateAP:rmfAccessPoint]);
}

/*
 * If input parameter is not nil, apConstraintsParameter is kAPConstraintsParameterMacAddress and database file not found then updateAP should throw exception
 */
- (void)testUpdateAPScenario4 {
	RMFAccessPoint *rmfAccessPoint = [[RMFAccessPoint alloc] init];
	RMFAPStatus *apStatus = [[RMFAPStatus alloc] init];
	apStatus.macAddress = @"";
	rmfAccessPoint.rmfAPStatus = apStatus;
	[[RMFDBAccessLayer sharedRMFDBAccessLayer] setApConstraintsParameter:kAPConstraintsParameterMacAddress];
	XCTAssertNil([[RMFDBAccessLayer sharedRMFDBAccessLayer] updateAP:rmfAccessPoint]);
}

#pragma  mark  - apList
/*
 * If database file not found then apList should return nil
 */
- (void)testAPListScenario {
	if (![self isDatabaseAvailable]) {
		XCTAssertNil([[RMFDBAccessLayer sharedRMFDBAccessLayer] apList]);
	}
}

#pragma  mark  - apDetail

/*
 * If input parameter is nil then apDetail should return nil
 */
- (void)testGetAPDetailScenario1 {
	XCTAssertNil([[RMFDBAccessLayer sharedRMFDBAccessLayer] apDetail:nil]);
}

/*
 * If input parameter is nil then apDetail should return nil
 */
- (void)testGetAPDetailScenario2 {
	[[RMFDBAccessLayer sharedRMFDBAccessLayer] setApConstraintsParameter:kAPConstraintsParameterSerialNumber];
	XCTAssertNil([[RMFDBAccessLayer sharedRMFDBAccessLayer] apDetail:@""]);
}

/*
 * If input parameter is nil then apDetail should return nil
 */
- (void)testGetAPDetailScenario3 {
	[[RMFDBAccessLayer sharedRMFDBAccessLayer] setApConstraintsParameter:kAPConstraintsParameterMacAddress];
	XCTAssertNil([[RMFDBAccessLayer sharedRMFDBAccessLayer] apDetail:@""]);
}

#pragma  mark  - clearAllAP
/*
 * If database file not found then clearAllAP should return NSError nil
 */
- (void)testClearAllAPScenario {
	if (![self isDatabaseAvailable]) {
		XCTAssertNil([[RMFDBAccessLayer sharedRMFDBAccessLayer] deleteAllAP]);
	}
}

#pragma  mark  - deleteAP
/*
 * If input parameter is nil then deleteAP should return NSError object with description "Input parameter can not be nil"
 */
- (void)testDeleteAPScenario1 {
	NSError *error = [[RMFDBAccessLayer sharedRMFDBAccessLayer] deleteAP:nil];
	XCTAssertEqual(kKUMOCodeForDatabaseInputParameterNotFound, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
}

/*
 * If input parameter is not nil and database file not found then updateAP should throw exception
 */
- (void)testDeleteAPScenario2 {
	XCTAssertNil([[RMFDBAccessLayer sharedRMFDBAccessLayer] deleteAP:@""]);
}

#pragma  mark  - mapAPManagedObjectToRFMAPModel
/*
 * If input parameter is nil then mapAPManagedObjectToRFMAPModel should return nil
 */
- (void)testMapAPManagedObjectToRFMAPModelScenario1 {
	XCTAssertNil([[RMFDBAccessLayer sharedRMFDBAccessLayer] mapAPManagedObjectToRFMAPModel:nil]);
}

/*
 * If input parameter is nil then mapAPManagedObjectToRFMAPModel should return nil
 */
- (void)testMapAPManagedObjectToRFMAPModelScenario2 {
	XCTAssertNotNil([[RMFDBAccessLayer sharedRMFDBAccessLayer] mapAPManagedObjectToRFMAPModel:[NSArray array]]);
}

#pragma  mark  -


/*
 * Check database is available of not
 */
- (BOOL)isDatabaseAvailable {
	if (OBJ_NOT_NIL(RMFBUNDLE)) {
		if ([[RMFBUNDLE resourcePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.momd", DATABASE_NAME]]) {
			return YES;
		}
	}
	return NO;
}

#pragma  mark  - insertWlan

/*
 * If input parameter is nil then insertWlan should return NSError object with description "Input parameter can not be nil"
 */
- (void)testInsertWlanScenario1 {
	NSError *error = [[RMFDBAccessLayer sharedRMFDBAccessLayer] insertWlan:nil];
	XCTAssertEqual(kKUMOCodeForDatabaseInputParameterNotFound, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
}

/*
 * If input parameter is not nil and database file not found then insertWlan should return NSError object with description "Database instance not found"
 */
- (void)testInsertWlanScenario2 {
	RMFWlanService *wlan = [[RMFWlanService alloc] init];
	NSError *error = [[RMFDBAccessLayer sharedRMFDBAccessLayer] insertWlan:wlan];
	if ([self isDatabaseAvailable]) {
		XCTAssertEqual(kKUMOCodeForDatabaseInstanceNotFound, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	}
}

#pragma  mark  - updateWlan

/*
 * If input parameter is nil then insertWlan should return NSError object with description "Input parameter can not be nil"
 */
- (void)testUpdateWlanScenario1 {
	NSError *error = [[RMFDBAccessLayer sharedRMFDBAccessLayer] updateWlan:nil];
	XCTAssertEqual(kKUMOCodeForWlanSsidNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
}

/*
 * If input parameter is not nil and database file not found then insertWlan should return NSError object with description "Database instance not found"
 */
- (void)testUpdateWlanScenario2 {
	RMFWlanService *wlan = [[RMFWlanService alloc] init];
	NSError *error = [[RMFDBAccessLayer sharedRMFDBAccessLayer] updateWlan:wlan];
	if ([self isDatabaseAvailable]) {
		XCTAssertEqual(kKUMOCodeForDatabaseInstanceNotFound, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	}
}

#pragma  mark  - wlanList
/*
 * If database file not found then apList should return nil
 */
- (void)testWlanListScenario {
	if (![self isDatabaseAvailable]) {
		XCTAssertNil([[RMFDBAccessLayer sharedRMFDBAccessLayer] wlanList]);
	}
}

#pragma  mark  - wlanDetails

/*
 * If input parameter is nil then wlanDetail should return nil
 */
- (void)testWlanDetailScenario1 {
	XCTAssertNil([[RMFDBAccessLayer sharedRMFDBAccessLayer] wlanDetails:nil]);
}

/*
 * If input parameter is not nil then wlanDetails should not throw any exception
 */
- (void)testWlanDetailsScenario3 {
	if ([self isDatabaseAvailable]) {
		XCTAssertNoThrow([[RMFDBAccessLayer sharedRMFDBAccessLayer] wlanDetails:@""]);
	}
}

#pragma  mark  - deleteAllWlans
/*
 * If database file not found then deleteAllWlans should return NSError nil
 */
- (void)testDeleteAllWlanScenario {
	if (![self isDatabaseAvailable]) {
		XCTAssertNil([[RMFDBAccessLayer sharedRMFDBAccessLayer] deleteAllWlans]);
	}
}

#pragma  mark  - deleteWlan
/*
 * If input parameter is nil then deleteWlan should return NSError object with description "Input parameter can not be nil"
 */
- (void)testDeleteWlanScenario1 {
	NSError *error = [[RMFDBAccessLayer sharedRMFDBAccessLayer] deleteAP:nil];
	XCTAssertEqual(kKUMOCodeForDatabaseInputParameterNotFound, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
}

/*
 * If input parameter is not nil and database file not found then updateAP should not throw exception
 */
- (void)testDeleteWlanScenario2 {
	if ([self isDatabaseAvailable]) {
		XCTAssertNoThrow([[RMFDBAccessLayer sharedRMFDBAccessLayer] deleteWlan:@""]);
	}
}

#pragma  mark  - insertEvent

/*
 * If input parameter is nil then insertEvent should return NSError object with description "Input parameter can not be nil"
 */
- (void)testInsertEventScenario1 {
	NSError *error = [[RMFDBAccessLayer sharedRMFDBAccessLayer] insertEvent:nil];
	XCTAssertEqual(kKUMOCodeForDatabaseInputParameterNotFound, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
}

/*
 * If input parameter is not nil and database file not found then insertWlan should return NSError object with description "Database instance not found"
 */
- (void)testInsertEventScenario2 {
	RMFEvent *event = [[RMFEvent alloc] init];
	NSError *error = [[RMFDBAccessLayer sharedRMFDBAccessLayer] insertEvent:event];
	if ([self isDatabaseAvailable]) {
		XCTAssertEqual(kKUMOCodeForDatabaseInstanceNotFound, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	}
}

#pragma  mark  - updateEvent

/*
 * If input parameter is nil then updateEvent should return NSError object with description "Input parameter can not be nil"
 */
- (void)testUpdateEventScenario1 {
	NSError *error = [[RMFDBAccessLayer sharedRMFDBAccessLayer] updateEvent:nil];
	XCTAssertEqual(kKUMOCodeForWlanSsidNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
}

/*
 * If input parameter is not nil and database file not found then updateEvent should return NSError object with description "Database instance not found"
 */
- (void)testUpdateEventScenario2 {
	RMFEvent *event = [[RMFEvent alloc] init];
	NSError *error = [[RMFDBAccessLayer sharedRMFDBAccessLayer] updateEvent:event];
	if ([self isDatabaseAvailable]) {
		XCTAssertEqual(kKUMOCodeForDatabaseInstanceNotFound, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	}
}

#pragma  mark  - eventList
/*
 * If database file not found then eventList should return nil
 */
- (void)testEventListScenario {
	if (![self isDatabaseAvailable]) {
		XCTAssertNil([[RMFDBAccessLayer sharedRMFDBAccessLayer] eventList]);
	}
}

#pragma  mark  - eventDetails

/*
 * If input parameter is nil then eventDetails should not throw any exception
 */
- (void)testEventDetailsScenario1 {
	if (![self isDatabaseAvailable]) {
		XCTAssertNil([[RMFDBAccessLayer sharedRMFDBAccessLayer] eventDetails:0]);
	}
}

#pragma  mark  - deleteAllEvents
/*
 * If database file not found then deleteAllEvents should return NSError nil
 */
- (void)testDeleteAllEventsScenario {
	if (![self isDatabaseAvailable]) {
		XCTAssertNil([[RMFDBAccessLayer sharedRMFDBAccessLayer] deleteAllEvents]);
	}
}

#pragma  mark  - deleteEvent

/*
 * If called deleteEvent should throw exception
 */
- (void)testDeleteEventScenario1 {
	if ([self isDatabaseAvailable]) {
		XCTAssertNoThrow([[RMFDBAccessLayer sharedRMFDBAccessLayer] deleteEvent:0]);
	}
}

@end
