/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFZDParser.h"

@interface RMFZDParserTest : XCTestCase

@end

@implementation RMFZDParserTest

- (void)setUp {
	[super setUp];
}

- (void)tearDown {
	[super tearDown];
}

/*
 *  sharedRMFZDParser should return a valid instance
 */
- (void)testsharedRMFZDParser {
	XCTAssertNotNil([RMFZDParser sharedRMFZDParser]);
}

/*
 * [[RMFZDParser alloc] init] should return a valid instance
 */
- (void)testSharedRMFScgParserUsingAlloc {
	XCTAssertNotNil([[RMFZDParser alloc] init]);
}

/*
 * [[RMFZDParser alloc] init] and sharedRMFZDParser should return same instance
 */
- (void)testIsSharedRMFScgParserAndAllocReturnsSameObject {
	XCTAssertEqual([[RMFZDParser alloc] init], [RMFZDParser sharedRMFZDParser]);
}

@end
