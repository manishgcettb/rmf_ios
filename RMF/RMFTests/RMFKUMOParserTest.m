/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFKUMOParser.h"
#import "RMFMacro.h"
#import "RMFWlanService.h"
#import "RMFConfiguration.h"

#define  kEventListLoaded            @"Event list should be loaded"
#define  kWlanServiceListLoaded      @"Wlan service list should be loaded"
#define  kWlanServiceStatusLoaded    @"Wlan service status should be loaded"
#define  kWlanServiceDetailsLoaded   @"Wlan service details should be loaded"

#define  kAPListLoaded            @"AP list should be loaded"
#define  kAPFrameLoaded            @"AP frame should be loaded"
#define  kAPDetailLoaded            @"AP detal should be loaded"
#define  kAPCreated            @"AP should be created"
#define  kAPEdited            @"AP should be edited"
#define  kAPDeleted            @"AP should be deleted"
#define  kAPCreated            @"AP should be created"



@interface RMFKUMOParserTest : XCTestCase
@property (nonatomic, strong) RMFKUMOParser *rmfKumoParser;
@end

@implementation RMFKUMOParserTest

- (void)setUp {
	[super setUp];
	self.rmfKumoParser = [RMFKUMOParser sharedRMFKUMOParser];
	[RMFConfiguration sharedRMFConfiguration].system = kSystemTypeKUMO;
}

- (void)tearDown {
	[super tearDown];
}

/*
 *  sharedRMFKUMOParser should return a valid instance
 */
- (void)testsharedRMFZDParser {
	XCTAssertNotNil([RMFKUMOParser sharedRMFKUMOParser]);
}

/*
 * [[RMFKumoParser alloc] init] should return a valid instance
 */
- (void)testSharedRMFScgParserUsingAlloc {
	XCTAssertNotNil([[RMFKUMOParser alloc] init]);
}

/*
 * [[RMFKumoParser alloc] init] and sharedRMFKUMOParser should return same instance
 */
- (void)testIsSharedRMFScgParserAndAllocReturnsSameObject {
	XCTAssertEqual([[RMFKUMOParser alloc] init], [RMFKUMOParser sharedRMFKUMOParser]);
}

#pragma mark - AP List

/*
 * If input and response block both are nil then parseAPListWithInput should not throw any exception
 */
- (void)testParseAPListWithInputScenario1 {
	XCTAssertNoThrow([self.rmfKumoParser parseAPListWithInput:nil output:nil]);
}

/*
 * If input is not nil but response block is nil then parseAPListWithInput should not throw any exception
 */
- (void)testParseAPListWithInputScenario2 {
	NSDictionary *input = [[NSDictionary alloc] init];
	XCTAssertNoThrow([self.rmfKumoParser parseAPListWithInput:input output:nil]);
}

/*
 * If input is nil and response block is not nil then parseAPListWithInput should return
 *  NSError object with description "Parsing failure error"
 */
- (void)testParseAPListWithInputScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPListLoaded];
	[self.rmfKumoParser parseAPListWithInput:nil output: ^(NSError *error, id data) {
	    XCTAssertNil(data);
	    XCTAssertEqual(kKUMOCodeForParsingFailedErrorDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNotNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If input is empty NSDictionary object and response block is not nil then parseAPListWithInput should return NSError object with description "Parsing failure error"
 */
- (void)testParseAPListWithInputScenario4 {
	NSDictionary *input = [[NSDictionary alloc] init];

	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPListLoaded];
	[self.rmfKumoParser parseAPListWithInput:input output: ^(NSError *error, id data) {
	    XCTAssertNil(data);
	    XCTAssertEqual(kKUMOCodeForParsingFailedErrorDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNotNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If input (NSArray of empty object -> NSDictionary of ap -> NSDictionary of aps) and response block is not nil then parseAPListWithInput should return NSError object with description "Parsing failure error"
 */
- (void)testParseAPListWithInputScenario5 {
	NSArray *ap = [NSArray arrayWithObject:@""];
	NSDictionary *aps = [NSDictionary dictionaryWithObjectsAndKeys:ap, kAPKey, nil];
	NSDictionary *input = [NSDictionary dictionaryWithObjectsAndKeys:aps, kApsKey, nil];

	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPListLoaded];
	[self.rmfKumoParser parseAPListWithInput:input output: ^(NSError *error, id data) {
	    XCTAssertNil(data);
	    XCTAssertEqual(kKUMOCodeForParsingFailedErrorDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNotNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If input (NSDictionary of serial number -> NSArray of serial number dictionary -> NSDictionary of ap -> NSDictionary of aps) and response block is not nil then parseAPListWithInput should return NSError object with description "Parsing failure error"
 */
- (void)testParseAPListWithInputScenario6 {
	NSDictionary *serialNumbers = [NSDictionary dictionaryWithObjectsAndKeys:@"161205002059", kSerialNumberKey, nil];
	NSArray *ap = [NSArray arrayWithObject:serialNumbers];
	NSDictionary *aps = [NSDictionary dictionaryWithObjectsAndKeys:ap, kAPKey, nil];
	NSDictionary *input = [NSDictionary dictionaryWithObjectsAndKeys:aps, kApsKey, nil];

	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPListLoaded];
	[self.rmfKumoParser parseAPListWithInput:input output: ^(NSError *error, id data) {
	    XCTAssertNotNil(data);
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

#pragma mark - AP Detail

/*
 * If input and response block both are nil then parseAPDetailWithInput should not throw any exception
 */
- (void)testParseAPDetailWithInputScenario1 {
	XCTAssertNoThrow([self.rmfKumoParser parseAPDetailWithInput:nil output:nil]);
}

/*
 * If input is not nil but response block is nil then parseAPDetailWithInput should not throw any exception
 */
- (void)testParseAPDetailWithInputScenario2 {
	NSDictionary *input = [[NSDictionary alloc] init];
	XCTAssertNoThrow([self.rmfKumoParser parseAPDetailWithInput:input output:nil]);
}

/*
 * If input is nil and response block is not nil then parseAPDetailWithInput should return
 *  NSError object with description "Parsing failure error"
 */
- (void)testParseAPDetailWithInputScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPDetailLoaded];
	[self.rmfKumoParser parseAPDetailWithInput:nil output: ^(NSError *error, id data) {
	    XCTAssertNil(data);
	    XCTAssertEqual(kKUMOCodeForParsingFailedErrorDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNotNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If input is empty NSDictionary object and response block is not nil then parseAPDetailWithInput should return NSError object with description "Parsing failure error"
 */
- (void)testParseAPDetailWithInputScenario4 {
	NSDictionary *input = [[NSDictionary alloc] init];

	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPDetailLoaded];
	[self.rmfKumoParser parseAPDetailWithInput:input output: ^(NSError *error, id data) {
	    XCTAssertNil(data);
	    XCTAssertEqual(kKUMOCodeForParsingFailedErrorDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNotNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If input value of parent keys is nil and response block is not nil then parseAPDetailWithInput should return RMFAccessPoint object
 */
- (void)testParseAPDetailWithInputScenario5 {
	NSDictionary *ap = [NSDictionary dictionaryWithObjectsAndKeys:nil, kSerialNumberKey, nil, kAPWlanStatus, nil, kAPExtendedStatus, nil, kAPRadioStatus, nil, kAPStatus, nil];
	NSDictionary *input = [NSDictionary dictionaryWithObjectsAndKeys:ap, kAPKey, nil];

	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPDetailLoaded];
	[self.rmfKumoParser parseAPDetailWithInput:input output: ^(NSError *error, id data) {
	    XCTAssertNotNil(data);
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If input value of leaf node is nil  and response block is not nil then parseAPDetailWithInput should return RMFAccessPoint object
 */
- (void)testParseAPDetailWithInputScenario6 {
	NSString *serialNumber = nil;
	NSDictionary *wlanSatus = [NSDictionary dictionaryWithObjectsAndKeys:nil, kAPWlanCount, nil, kAPWlans, nil];
	NSDictionary *extendedStatus = [NSDictionary dictionaryWithObjectsAndKeys:nil, kAPInternalIP, nil, kAPExternalIP, nil, kAPHops, nil, kAPTunnelType, nil, kAPName, nil, kAPUptime, nil];
	NSDictionary *radioStatus = [NSDictionary dictionaryWithObjectsAndKeys:nil, kAPTxPower, nil, kAPChannel, nil, kAPChannelWidth, nil, kAPWlanCount, nil, kAPMode, nil];
	NSDictionary *status = [NSDictionary dictionaryWithObjectsAndKeys:nil, kAPModel, nil, kAPMacAddress, nil, kAPRegistrationStatus, nil, kAPConnectionStatus, nil, kAPConfigStatus, nil, kAPLastSeenByVscg, nil, kAPSWver, nil, kAPLocation, nil];

	NSDictionary *ap = [NSDictionary dictionaryWithObjectsAndKeys:serialNumber, kSerialNumberKey, wlanSatus, kAPWlanStatus, extendedStatus, kAPExtendedStatus, radioStatus, kAPRadioStatus, status, kAPStatus, nil];
	NSDictionary *input = [NSDictionary dictionaryWithObjectsAndKeys:ap, kAPKey, nil];


	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPDetailLoaded];
	[self.rmfKumoParser parseAPDetailWithInput:input output: ^(NSError *error, id data) {
	    XCTAssertNotNil(data);
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If input value of leaf node is not nil  and response block is not nil then parseAPDetailWithInput should return RMFAccessPoint object
 */
- (void)testParseAPDetailWithInputScenario7 {
	NSString *serialNumber = @"161205002059";
	NSDictionary *wlanSatus = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:1], kAPWlanCount, nil, kAPWlans, nil];
	NSDictionary *extendedStatus = [NSDictionary dictionaryWithObjectsAndKeys:@"172.16.110.248", kAPInternalIP, @"12.217.161.130", kAPExternalIP, @"0", kAPHops, @"0", kAPTunnelType, @"AP1", kAPName, [NSNumber numberWithLong:710971], kAPUptime, nil];
	NSDictionary *radioStatus = [NSDictionary dictionaryWithObjectsAndKeys:@"max", kAPTxPower, [NSNumber numberWithInt:5], kAPChannel, [NSNumber numberWithInt:0], kAPChannelWidth, [NSNumber numberWithInt:1], kAPWlanCount, @"11ng", kAPMode, nil];
	NSDictionary *status = [NSDictionary dictionaryWithObjectsAndKeys:@"ZF7321", kAPModel, @"C0:8A:DE:1D:89:E0", kAPMacAddress, @"Approved", kAPRegistrationStatus, @"Connected", kAPConnectionStatus, @"completed", kAPConfigStatus, @"2015-05-13T23:47:18.252+00:00", kAPLastSeenByVscg, @"3.0.3.0.457", kAPSWver, @"Here", kAPLocation, nil];

	NSDictionary *ap = [NSDictionary dictionaryWithObjectsAndKeys:serialNumber, kSerialNumberKey, wlanSatus, kAPWlanStatus, extendedStatus, kAPExtendedStatus, radioStatus, kAPRadioStatus, status, kAPStatus, nil];
	NSDictionary *input = [NSDictionary dictionaryWithObjectsAndKeys:ap, kAPKey, nil];


	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPDetailLoaded];
	[self.rmfKumoParser parseAPDetailWithInput:input output: ^(NSError *error, id data) {
	    XCTAssertNotNil(data);
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

#pragma mark - create AP

/*
 * If input and response block both are nil then parseCreateAPWithInput should not throw any exception
 */
- (void)testParseCreateAPWithInputScenario1 {
	XCTAssertNoThrow([self.rmfKumoParser parseCreateAPWithInput:nil output:nil]);
}

/*
 * If input is not nil but response block is nil then parseCreateAPWithInput should not throw any exception
 */
- (void)testParseCreateAPWithInputScenario2 {
	NSDictionary *input = [[NSDictionary alloc] init];
	XCTAssertNoThrow([self.rmfKumoParser parseCreateAPWithInput:input output:nil]);
}

/*
 * If input is nil and response block is not nil then parseCreateAPWithInput should return
 *  NSError object with description "Parsing failure error"
 */
- (void)testParseCreateAPWithInputScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPCreated];
	[self.rmfKumoParser parseCreateAPWithInput:nil output: ^(NSError *error, id data) {
	    XCTAssertEqual(kKUMOCodeForParsingFailedErrorDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNotNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If input is empty NSDictionary object and response block is not nil then parseCreateAPWithInput should return NSError object with description "Parsing failure error"
 */
- (void)testParseCreateAPWithInputScenario4 {
	NSDictionary *input = [[NSDictionary alloc] init];

	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPCreated];
	[self.rmfKumoParser parseCreateAPWithInput:input output: ^(NSError *error, id data) {
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

#pragma mark - AP frame

/*
 * If input and response block both are nil then parseAPWithInput should not throw any exception
 */
- (void)testparseAPWithInputScenario1 {
	XCTAssertNoThrow([self.rmfKumoParser parseAPWithInput:nil output:nil]);
}

/*
 * If input is not nil but response block is nil then parseAPWithInput should not throw any exception
 */
- (void)testparseAPWithInputScenario2 {
	NSDictionary *input = [[NSDictionary alloc] init];
	XCTAssertNoThrow([self.rmfKumoParser parseAPWithInput:input output:nil]);
}

/*
 * If input is nil and response block is not nil then parseAPWithInput should return
 *  NSError object with description "Parsing failure error"
 */
- (void)testparseAPWithInputScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPFrameLoaded];
	[self.rmfKumoParser parseAPWithInput:nil output: ^(NSError *error, id data) {
	    XCTAssertNil(data);
	    XCTAssertEqual(kKUMOCodeForParsingFailedErrorDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNotNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If input is empty NSDictionary object and response block is not nil then parseAPWithInput should return NSError object with description "Parsing failure error"
 */
- (void)testparseAPWithInputScenario4 {
	NSDictionary *input = [[NSDictionary alloc] init];

	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPFrameLoaded];
	[self.rmfKumoParser parseAPWithInput:input output: ^(NSError *error, id data) {
	    XCTAssertNil(data);
	    XCTAssertEqual(kKUMOCodeForParsingFailedErrorDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNotNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If input and response block is not nil but input is empty then parseAPWithInput should return empty list
 */
- (void)testparseAPWithInputScenario5 {
	NSArray *result = [NSArray arrayWithObject:@""];
	NSDictionary *queryResult = [NSDictionary dictionaryWithObjectsAndKeys:result, kResult, nil];
	NSDictionary *input = [NSDictionary dictionaryWithObjectsAndKeys:queryResult, kQueryResult, nil];

	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPFrameLoaded];
	[self.rmfKumoParser parseAPWithInput:input output: ^(NSError *error, id data) {
	    XCTAssertNotNil(data);
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If input and response block is not nil then parseAPWithInput should return list of RMFAccessPoint
 */
- (void)testparseAPWithInputScenario6 {
	NSDictionary *selectName = [NSDictionary dictionaryWithObjectsAndKeys:kSelectLabelName, kSelectParametersLabel, @"", kSelectResultParametersValue, nil];
	NSDictionary *selectSN = [NSDictionary dictionaryWithObjectsAndKeys:kSelectLabelSerialNumber, kSelectParametersLabel, @"", kSelectResultParametersValue, nil];

	NSDictionary *selectMac = [NSDictionary dictionaryWithObjectsAndKeys:kSelectLabelMacAddress, kSelectParametersLabel, @"", kSelectResultParametersValue, nil];

	NSDictionary *selectSSID = [NSDictionary dictionaryWithObjectsAndKeys:kSelectLabelSSID, kSelectParametersLabel, @"", kSelectResultParametersValue, nil];
	NSDictionary *selectWLAN = [NSDictionary dictionaryWithObjectsAndKeys:kSelectLabelWLANCount, kSelectParametersLabel, @"", kSelectResultParametersValue, nil];

	NSArray *selectQueryList = [NSArray arrayWithObjects:selectName, selectSN, selectMac, selectSSID, selectWLAN, nil];


	NSDictionary *select = [NSDictionary dictionaryWithObject:selectQueryList forKey:kSelect];
	NSArray *result = [NSArray arrayWithObjects:select, select, nil];

	NSDictionary *queryResult = [NSDictionary dictionaryWithObjectsAndKeys:result, kResult, nil];
	NSDictionary *input = [NSDictionary dictionaryWithObjectsAndKeys:queryResult, kQueryResult, nil];

	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPFrameLoaded];
	[self.rmfKumoParser parseAPWithInput:input output: ^(NSError *error, id data) {
	    XCTAssertNotNil(data);
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

#pragma mark - edit AP

/*
 * If input and response block both are nil then parseEditAPWithInput should not throw any exception
 */
- (void)testParseEditAPWithInputScenario1 {
	XCTAssertNoThrow([self.rmfKumoParser parseEditAPWithInput:nil output:nil]);
}

/*
 * If input is not nil but response block is nil then parseEditAPWithInput should not throw any exception
 */
- (void)testParseEditAPWithInputScenario2 {
	NSDictionary *input = [[NSDictionary alloc] init];
	XCTAssertNoThrow([self.rmfKumoParser parseEditAPWithInput:input output:nil]);
}

/*
 * If input is nil and response block is not nil then parseEditAPWithInput should return
 *  NSError object with description "Parsing failure error"
 */
- (void)testParseEditAPWithInputScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPCreated];
	[self.rmfKumoParser parseEditAPWithInput:nil output: ^(NSError *error, id data) {
	    XCTAssertEqual(kKUMOCodeForParsingFailedErrorDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNotNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If input is empty NSDictionary object and response block is not nil then parseEditAPWithInput should return NSError object with description "Parsing failure error"
 */
- (void)testParseEditAPWithInputScenario4 {
	NSDictionary *input = [[NSDictionary alloc] init];

	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPCreated];
	[self.rmfKumoParser parseEditAPWithInput:input output: ^(NSError *error, id data) {
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

#pragma mark - delete AP

/*
 * If input and response block both are nil then parseDeleteAPWithInput should not throw any exception
 */
- (void)testParseDeleteAPWithInputScenario1 {
	XCTAssertNoThrow([self.rmfKumoParser parseDeleteAPWithInput:nil output:nil]);
}

/*
 * If input is not nil but response block is nil then parseDeleteAPWithInput should not throw any exception
 */
- (void)testParseDeleteAPWithInputScenario2 {
	NSDictionary *input = [[NSDictionary alloc] init];
	XCTAssertNoThrow([self.rmfKumoParser parseDeleteAPWithInput:input output:nil]);
}

/*
 * If input is nil and response block is not nil then parseDeleteAPWithInput should return
 *  NSError object with description "Parsing failure error"
 */
- (void)testParseDeleteAPWithInputScenario3 {
	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPCreated];
	[self.rmfKumoParser parseDeleteAPWithInput:nil output: ^(NSError *error, BOOL success) {
	    XCTAssertEqual(kKUMOCodeForParsingFailedErrorDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
	    XCTAssertNotNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If input is empty NSDictionary object and response block is not nil then parseDeleteAPWithInput should return NSError object with description "Parsing failure error"
 */
- (void)testParseDeleteAPWithInputScenario4 {
	NSDictionary *input = [[NSDictionary alloc] init];

	__block XCTestExpectation *expectation = [self expectationWithDescription:kAPCreated];
	[self.rmfKumoParser parseDeleteAPWithInput:input output: ^(NSError *error, BOOL success) {
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

#pragma mark - WLAN list

/*
 * If input and response block both are nil then parseWlanServiceListWithInput should not throw any exception
 */

- (void)testparseWlanServiceListWithInputScenario1 {
	XCTAssertNoThrow([self.rmfKumoParser parseWlanServiceListWithInput:nil output:nil]);
}

/*
 * If input is available and response block is nil then parseWlanServiceListWithInput should not throw any exception
 */

- (void)testparseWlanServiceListWithInputScenario2 {
	NSDictionary *inputDictionary = @{
		@"services": @{
			@"wlan-service": @[
			    @{
			        @"name": @"kumomobile"
				}
			]
		}
	};

	XCTAssertNoThrow([self.rmfKumoParser parseWlanServiceListWithInput:inputDictionary output:nil]);
}

/*
 * If input and response block are available then parseWlanServiceListWithInput should return wlan list
 */

- (void)testparseWlanServiceListWithInputScenario3 {
	NSDictionary *inputDictionary = @{
		@"services": @{
			@"wlan-service": @[
			    @{
			        @"name": @"kumomobile"
				}
			]
		}
	};

	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceListLoaded];

	[self.rmfKumoParser parseWlanServiceListWithInput:inputDictionary output: ^(NSError *error, NSArray *wlanServiceList) {
	    XCTAssertNotNil(wlanServiceList);
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If input and response block are available but input is invalid then parseWlanServiceListWithInput should
 * not throw any exception
 */

- (void)testparseWlanServiceListWithInputScenario4 {
	NSDictionary *inputDictionary = @{
		@"serces": @{
			@"wlan-sice": @[
			    @{
			        @"nme": @"kumomobile"
				}
			]
		}
	};

	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceListLoaded];

	[self.rmfKumoParser parseWlanServiceListWithInput:inputDictionary output: ^(NSError *error, NSArray *wlanServiceList) {
	    XCTAssertNotNil(error);
	    XCTAssertNil(wlanServiceList);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

#pragma mark - WlanServiceStatusWithInput
/*
 * If input and response block both are nil then parseWlanServiceStatusWithInput should not throw any exception
 */

- (void)testParseWlanServiceStatusScenario1 {
	XCTAssertNoThrow([self.rmfKumoParser parseWlanServiceStatusWithInput:nil output:nil]);
}

/*
 * If input is available and response block is nil then parseWlanServiceStatusWithInput should not throw
 * any exception
 */

- (void)testParseWlanServiceStatusScenario2 {
	NSDictionary *inputDictionary = @{
		@"wlan-status": @{
			@"total-count": @1,
			@"wlans": @[
			    @{
			        @"bssid": @"C0:8A:DE:27:61:A8"
				}
			]
		}
	};

	XCTAssertNoThrow([self.rmfKumoParser parseWlanServiceStatusWithInput:inputDictionary output:nil]);
}

/*
 * If input and response block are available then parseWlanServiceListWithInput should return the
 * instance of RMFWlanServiceStatus initialise with parsed data
 */

- (void)testParseWlanServiceStatusScenario3 {
	NSDictionary *inputDictionary = @{
		@"wlan-status": @{
			@"total-count": @1,
			@"wlans": @[
			    @{
			        @"bssid": @"C0:8A:DE:27:61:A8"
				}
			]
		}
	};

	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceListLoaded];

	[self.rmfKumoParser parseWlanServiceStatusWithInput:inputDictionary output: ^(NSError *error, RMFWlanServiceStatus *wlanServiceStatus) {
	    XCTAssertNotNil(wlanServiceStatus);
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If input and response block are available but input is invalid then parseWlanServiceStatusWithInput should
 * not throw any exception
 */

- (void)testParseWlanServiceStatusScenario4 {
	NSDictionary *inputDictionary = @{
		@"serces": @{
			@"wlan-sice": @[
			    @{
			        @"nme": @"kumomobile"
				}
			]
		}
	};

	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceListLoaded];

	[self.rmfKumoParser parseWlanServiceStatusWithInput:inputDictionary output: ^(NSError *error, RMFWlanServiceStatus *wlanServiceStatus) {
	    XCTAssertNotNil(error);
	    XCTAssertNil(wlanServiceStatus);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

#pragma mark - Wlan service detail

/*
 * If input and response block both are nil then parseWlanServiceDetailsWithInput should not throw any exception
 */

- (void)testParseWlanServiceDetailsScenario1 {
	XCTAssertNoThrow([self.rmfKumoParser parseWlanServiceDetailsWithInput:nil output:nil]);
}

/*
 * If input is available and response block is nil then parseWlanServiceDetailsWithInput should not throw
 * any exception
 */

- (void)testParseWlanServiceDetailsScenario2 {
	NSDictionary *inputDictionary = @{
		@"wlan-service": @{
			@"name": @"kumomobile",
			@"ssid": @"kumomobile",
			@"wlan-type": @"private",
			@"wlan-security": @"WPA2-Personal",
			@"basic-network-customization": @{
				@"vlanId": @1
			},
			@"advanced-customization": @{
				@"max-clients-onWlan-perRadio": @100,
				@"enableBandBalancing": @false
			},
			@"psk-passphrase": @"Password"
		}
	};

	XCTAssertNoThrow([self.rmfKumoParser parseWlanServiceDetailsWithInput:inputDictionary output:nil]);
}

/*
 * If input and response block are available then parseWlanServiceDetailsWithInput should return the instance
 * of RMFWlanService initialise with parsed data
 */

- (void)testParseWlanServiceDetailsScenario3 {
	NSDictionary *inputDictionary = @{
		@"wlan-service": @{
			@"name": @"kumomobile",
			@"ssid": @"kumomobile",
			@"wlan-type": @"private",
			@"wlan-security": @"WPA2-Personal",
			@"basic-network-customization": @{
				@"vlanId": @1
			},
			@"advanced-customization": @{
				@"max-clients-onWlan-perRadio": @100,
				@"enableBandBalancing": @false
			},
			@"psk-passphrase": @"Password"
		}
	};

	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceListLoaded];

	[self.rmfKumoParser parseWlanServiceDetailsWithInput:inputDictionary output: ^(NSError *error, RMFWlanService *wlanService) {
	    XCTAssertNotNil(wlanService);
	    XCTAssertNil(error);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If input and response block are available but input is invalid then parseWlanServiceDetailsWithInput should
 * not throw any exception
 */

- (void)testParseWlanServiceDetailsScenario4 {
	NSDictionary *inputDictionary = @{
		@"wlan-service": @{
			@"nme": @"kumomobile",
			@"ssd": @"kumomobile",
			@"wl-type": @"private",
			@"wlan-urity": @"WPA2-Personal",
			@"basictwork-customization": @{
				@"vlanId": @1
			},
			@"advanccustomization": @{
				@"max-clients-onWlan-perRadio": @100,
				@"enableBandBalancing": @false
			},
			@"psk-pahrase": @"Password"
		}
	};

	__block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceListLoaded];

	[self.rmfKumoParser parseWlanServiceDetailsWithInput:inputDictionary output: ^(NSError *error, RMFWlanService *wlanService) {
	    XCTAssertNotNil(error);
	    XCTAssertNil(wlanService);
	    [expectation fulfill];
	}];

	[self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
	    XCTAssertNil(error);
	    expectation = nil;
	}];
}

/*
 * If input and response block both are nil then parseWlanListWithInput should not throw any exception
 */
- (void)testparseWlanListWithInputScenario1 {
    XCTAssertNoThrow([self.rmfKumoParser parseWlanListWithInput:nil output:nil]);
}

/*
 * If input is not nil but response block is nil then parseWlanListWithInput should not throw any exception
 */
- (void)testparseWlanListWithInputScenario2 {
    NSDictionary *input = [[NSDictionary alloc] init];
    XCTAssertNoThrow([self.rmfKumoParser parseWlanListWithInput:input output:nil]);
}

/*
 * If input is nil and response block is not nil then parseWlanListWithInput should return
 *  NSError object with description "Parsing failure error"
 */
- (void)testparseWlanListWithInputScenario3 {
    __block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceListLoaded];
    [self.rmfKumoParser parseWlanListWithInput:nil output: ^(NSError *error, id data) {
        XCTAssertNil(data);
        XCTAssertEqual(kKUMOCodeForParsingFailedErrorDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
        XCTAssertNotNil(error);
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
        XCTAssertNil(error);
        expectation = nil;
    }];
}

/*
 * If input is empty NSDictionary object and response block is not nil then parseWlanListWithInput should return NSError object with description "Parsing failure error"
 */
- (void)testparseWlanListWithInputScenario4 {
    NSDictionary *input = [[NSDictionary alloc] init];
    
    __block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceListLoaded];
    [self.rmfKumoParser parseWlanListWithInput:input output: ^(NSError *error, id data) {
        XCTAssertNil(data);
        XCTAssertEqual(kKUMOCodeForParsingFailedErrorDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
        XCTAssertNotNil(error);
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
        XCTAssertNil(error);
        expectation = nil;
    }];
}

/*
 * If input and response block is not nil but input is empty then parseWlanListWithInput should return empty list
 */
- (void)testparseWlanListWithInputScenario5 {
    NSArray *result = [NSArray arrayWithObject:kEmptyText];
    NSDictionary *queryResult = [NSDictionary dictionaryWithObjectsAndKeys:result, kResult, nil];
    NSDictionary *input = [NSDictionary dictionaryWithObjectsAndKeys:queryResult, kQueryResult, nil];
    
    __block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceListLoaded];
    [self.rmfKumoParser parseWlanListWithInput:input output: ^(NSError *error, id data) {
        XCTAssertNotNil(data);
        XCTAssertNil(error);
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
        XCTAssertNil(error);
        expectation = nil;
    }];
}

/*
 * If input and response block is not nil then parseAPWithInput should return list of parseWlanListWithInput
 */
- (void)testparseWlanListWithInputScenario6 {

    NSDictionary *input =
    @{
      @"query-result":@{
              @"result":@[
                      @{
                          @"select":@[
                                  @{
                                      @"label":@"name",
                                      @"value": @"KumoTest1",
                                      },
                                  @{
                                      @"label":@"description",
                                      @"value":@"",
                                      },
                                  @{
                                      @"label": @"ssid",
                                      @"value": @"kumotest",
                                      },
                                  @{
                                      @"label":@"enabled",
                                      @"value":@"true",
                                      },
                                  @{
                                      @"label":@"wlan-type",
                                      @"value":@"private",
                                      },
                                  @{
                                      @"label":@"psk-passphrase",
                                      @"value": @"Password22!",
                                      },
                                  @{
                                      @"label":@"wlan-security",
                                      @"value":@"WPA-Personal",
                                      }
                                  ]
                          }
                      ]
              }
      };

    
    __block XCTestExpectation *expectation = [self expectationWithDescription:kWlanServiceListLoaded];
    [self.rmfKumoParser parseWlanListWithInput:input output: ^(NSError *error, id data) {
        XCTAssertNotNil(data);
        XCTAssertNil(error);
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler: ^(NSError *error) {
        XCTAssertNil(error);
        expectation = nil;
    }];
}

/*
 * If input and response block are not available then parseEditWlanResponseWithInput should not throw exception
 */

-(void)testParseEditWlanScenario1{
    XCTAssertNoThrow([self.rmfKumoParser parseEditWlanResponseWithInput:nil output:nil]);
    //TODO: Add more test cases once edit wlan response is known
}

/*
 * If input and response block are not available then parseDeleteWlanResponseWithInput should not throw exception
 */

-(void)testParseDeleteWlanScenario1{
    XCTAssertNoThrow([self.rmfKumoParser parseDeleteWlanResponseWithInput:nil output:nil]);
    //TODO: Add more test cases once delete wlan response is known
}

/*
 * If input and response block are not available then parseCreateWlanResponseWithInput should not throw exception
 */

-(void)testParseCreateWlanScenario1{
    XCTAssertNoThrow([self.rmfKumoParser parseCreateWlanResponseWithInput:nil output:nil]);
    //TODO: Add more test cases once create wlan response is known
}

#pragma mark - Event

/*
 * If input and response block both are nil then parseEventListWithInput should not throw any exception
 */

- (void)testParseEventListScenario1 {
	XCTAssertNoThrow([self.rmfKumoParser parseEventListWithInput:nil output:nil]);
	//TODO: Add more test cases once Event list response is known
}

/*
 * If input and response block both are nil then parseEventDetailsWithInput should not throw any exception
 */

- (void)testParseEventDetailsScenario1 {
	XCTAssertNoThrow([self.rmfKumoParser parseEventDetailsWithInput:nil output:nil]);
	//TODO: Add more test cases once Event detail response is known
}

@end
