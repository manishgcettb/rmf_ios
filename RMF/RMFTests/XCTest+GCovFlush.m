/**
 * Copyright 2013 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */


#import "XCTest+GCovFlush.h"
#import <objc/runtime.h>

extern void __gcov_flush();

@implementation XCTest (GCovFlush)

+ (void)load {
	Method original, swizzled;

	original = class_getInstanceMethod(self, @selector(tearDown));
	swizzled = class_getInstanceMethod(self, @selector(_swizzledTearDown));
	method_exchangeImplementations(original, swizzled);
}

- (void)_swizzledTearDown {
	if (__gcov_flush) {
		__gcov_flush();
	}
	[self _swizzledTearDown];
}

@end
