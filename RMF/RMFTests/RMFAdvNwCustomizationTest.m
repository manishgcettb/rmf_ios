/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFWlanAdvNwCustomization.h"

@interface RMFAdvNwCustomizationTest : XCTestCase
@property (nonatomic, strong) RMFWlanAdvNwCustomization *wlanAdvNwCustomization;
@end



@implementation RMFAdvNwCustomizationTest

- (void)setUp {
	[super setUp];

	// Create an instance of RMFWlanAdvNwCustomization
	self.wlanAdvNwCustomization = [[RMFWlanAdvNwCustomization alloc] init];
}

- (void)tearDown {
	[super tearDown];
}

// Test if RMFWlanAdvNwCustomization instance is not nil
- (void)testRMFWlanAdvNwCustomizationObject {
	XCTAssertNotNil(self.wlanAdvNwCustomization);
}

// Property maxClientsOnWlanPerRadio should store/return an integer value
- (void)testMaxClientsOnWlanPerRadio {
	NSInteger testMaxClientsOnWlanPerRadio = 4;
	if (self.wlanAdvNwCustomization) {
		self.wlanAdvNwCustomization.maxClientsOnWlanPerRadio = testMaxClientsOnWlanPerRadio;

		XCTAssertTrue(self.wlanAdvNwCustomization.maxClientsOnWlanPerRadio == testMaxClientsOnWlanPerRadio);
	}
}

// Property enableBandBalancing should store/return boolean value
- (void)testEnableBandBalancing {
	BOOL testEnableBandBalancing = YES;
	self.wlanAdvNwCustomization.enableBandBalancing = testEnableBandBalancing;

	if (self.wlanAdvNwCustomization) {
		XCTAssertTrue(self.wlanAdvNwCustomization.enableBandBalancing == testEnableBandBalancing);
	}
}

@end
