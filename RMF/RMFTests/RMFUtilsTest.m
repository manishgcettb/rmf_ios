/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFUtils.h"
#import "RMFMacro.h"
#import "RMFConfiguration.h"

#define TEST_PORT                   @"8080"
#define TEST_IP                     @"10.199.4.106"

@interface RMFUtilsTest : XCTestCase

// Stores/returns the instance of RMFConfiguration
@property (nonatomic, strong) RMFConfiguration *configuration;

@end

@implementation RMFUtilsTest

- (void)setUp {
    [super setUp];
    [self initialConfiguration];
}

- (void)tearDown {
    [super tearDown];
}

/*
 * initialise RMFConfiguration instance with system ip and system type and port number
 */
- (void)initialConfiguration {
    self.configuration = [RMFConfiguration sharedRMFConfiguration];
    self.configuration.systemIP = TEST_IP;
    self.configuration.port = TEST_PORT;
    self.configuration.system = kSystemTypeKUMO;
}


/*
 * If exception type and description are nil then raiseException should not throw any exception
 */

-(void)testRaiseExceptionScenario1{
    XCTAssertNoThrow([RMFUtils raiseException:nil WithDescription:nil]);
}

/*
 * If exception type is available but description is not available then raiseException should not
 * throw any exception
 */

-(void)testRaiseExceptionScenario2{
    XCTAssertNoThrow([RMFUtils raiseException:@"" WithDescription:nil]);
}

/*
 * If description is not available but exception type is not available then raiseException should not 
 * throw any exception
 */

-(void)testRaiseExceptionScenario3{
    XCTAssertNoThrow([RMFUtils raiseException:nil WithDescription:@""]);
}

/*
 * If exception type and description are available then raiseException should throw exception
 */

-(void)testRaiseExceptionScenario4{
    XCTAssertThrowsSpecificNamed([RMFUtils raiseException:NSInternalInconsistencyException WithDescription:kActiveParserNotFound],NSException, NSInternalInconsistencyException);
}

/*
 * errorWithCode should return NSError object
 */
-(void)testErrorWithCodeScenario1{
    [RMFUtils errorWithCode:0];
}

/*
 * If RMFSystemManager instance is not available then isSystemManagerConfigured should throw exception
 * of type NSInternalInconsistencyException
 */

- (void)testIsSystemConfiguredScenario1 {
    XCTAssertThrowsSpecificNamed([RMFUtils isSystemManagerConfigured:nil], NSException, NSInternalInconsistencyException);
}

/*
 * If RMFSystemManager instance is available but activeController is not available then
 * isSystemManagerConfigured should throw exception of type NSInternalInconsistencyException
 */
- (void)testIsSystemConfiguredScenario2 {
    
    RMFSystemManager *systemManager = [RMFSystemManager sharedRMFSystemManager];
    
    if (systemManager && !systemManager.activeController) {
        XCTAssertThrowsSpecificNamed([RMFUtils isSystemManagerConfigured:[RMFSystemManager sharedRMFSystemManager]], NSException, NSInternalInconsistencyException);
    }
}

/*
 * If active parser and active controller instance are available then isSystemManagerConfigured should
 * return true
 */

- (void)testIsSystemConfiguredScenario3 {
        XCTAssertTrue([RMFUtils isSystemManagerConfigured:[RMFSystemManager sharedRMFSystemManager]]);
}

@end
