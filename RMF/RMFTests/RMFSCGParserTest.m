/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFSCGParser.h"

@interface RMFSCGParserTest : XCTestCase

@end

@implementation RMFSCGParserTest

- (void)setUp {
	[super setUp];
}

- (void)tearDown {
	[super tearDown];
}

/*
 *  sharedRMFSCGParser should return a valid instance
 */
- (void)testsharedRMFSCGParser {
	XCTAssertNotNil([RMFSCGParser sharedRMFSCGParser]);
}

/*
 * [[RMFScgParser alloc] init] should return a valid instance
 */
- (void)testSharedRMFScgParserUsingAlloc {
	XCTAssertNotNil([[RMFSCGParser alloc] init]);
}

/*
 * [[RMFScgParser alloc] init] and sharedRMFSCGParser should return same instance
 */
- (void)testIsSharedRMFScgParserAndAllocReturnsSameObject {
	XCTAssertEqual([[RMFSCGParser alloc] init], [RMFSCGParser sharedRMFSCGParser]);
}

@end
