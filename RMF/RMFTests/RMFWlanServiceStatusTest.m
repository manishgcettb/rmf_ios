/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFWlanServiceStatus.h"

@interface RMFWlanServiceStatusTest : XCTestCase
@property (nonatomic, strong) RMFWlanServiceStatus *wlanServiceStatus;
@end



@implementation RMFWlanServiceStatusTest

- (void)setUp {
	[super setUp];

	// Create an instance of RMFWlanServiceStatusTest
	self.wlanServiceStatus = [[RMFWlanServiceStatus alloc] init];
}

- (void)tearDown {
	[super tearDown];
}

// Test if RMFWlanServiceStatusTest instance is not not nil
- (void)testRMFWlanServiceStatusObject {
	XCTAssertNotNil(self.wlanServiceStatus);
}

// Property total count should store/return an integer
- (void)testTotalCount {
	NSInteger testTotalCount = 3;
	if (self.wlanServiceStatus) {
		self.wlanServiceStatus.totalCount = testTotalCount;
		XCTAssertTrue(self.wlanServiceStatus.totalCount == testTotalCount);
	}
}

// Property bssid array should store/return as array
- (void)testBssidArray {
	NSMutableArray *testArray = [[NSMutableArray alloc] initWithObjects:@"One", @"two", @"three", nil];

	if (self.wlanServiceStatus) {
		self.wlanServiceStatus.bssidArray = testArray;
		XCTAssertEqualObjects(testArray, self.wlanServiceStatus.bssidArray);
	}
}

@end
