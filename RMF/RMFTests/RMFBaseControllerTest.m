/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "RMFBaseController.h"
#import "RMFRequestOperationManager.h"
#import "RMFConfiguration.h"
#import "RMFMacro.h"

#define TEST_PORT    @"8080"
#define TEST_IP      @"10.199.4.106"
#define TEST_URL     @"www.ruckuswireless.com"

@interface RMFBaseControllerTest : XCTestCase

// Stores/returns the instance of RMFConfiguration
@property (nonatomic, strong) RMFConfiguration *configuration;

// Stores/returns the instance of RMFBaseController
@property (nonatomic, strong) RMFBaseController *baseController;

@end

@implementation RMFBaseControllerTest

- (void)setUp {
	[super setUp];
	self.baseController = [[RMFBaseController alloc] init];
	self.configuration = [RMFConfiguration sharedRMFConfiguration];
}

- (void)tearDown {
	[super tearDown];
}

/*
 * Checks whether the instance of RMFScgController is available or not
 */
- (void)testRMFScgControllerScenario1 {
	XCTAssertNotNil(self.baseController);
}

/*
 * Checks whether requestOperationManager Stores and return the instance of
 * RMFRequestOperationManager
 */
- (void)testRMFRequestOperationManagerScenario1 {
	RMFRequestOperationManager *testRequestOperationManager = [[RMFRequestOperationManager alloc] init];
	self.baseController.requestOperationManager = testRequestOperationManager;
	XCTAssertEqualObjects(testRequestOperationManager, self.baseController.requestOperationManager);
}

/*
 * If system IP is and config URL are not available then createBaseUrl method should throw
 * exception of type NSInternalInconsistencyException
 */
- (void)testCreateBaseUrlScenario1 {
	self.configuration.systemIP = nil;
	self.configuration.configUrl = nil;

	XCTAssertThrowsSpecificNamed([self.baseController createBaseUrl], NSException, NSInternalInconsistencyException);
}

/*
 * If system IP is empty text and config URL is not available then createBaseUrl method should throw
 * exception of type NSInternalInconsistencyException
 */
- (void)testCreateBaseUrlScenario2 {
	self.configuration.systemIP = kEmptyText;
	self.configuration.configUrl = nil;

	XCTAssertThrowsSpecificNamed([self.baseController createBaseUrl], NSException, NSInternalInconsistencyException);
}

/*
 * If config URL is empty text and system IP is not available then createBaseUrl method should throw
 * exception of type NSInternalInconsistencyException
 */
- (void)testCreateBaseUrlScenario3 {
	self.configuration.systemIP = nil;
	self.configuration.configUrl = kEmptyText;

	XCTAssertThrowsSpecificNamed([self.baseController createBaseUrl], NSException, NSInternalInconsistencyException);
}

/*
 * If system IP and config URL are empty text then createBaseUrl method should throw exception of
 * type NSInternalInconsistencyException
 */
- (void)testCreateBaseUrlScenario4 {
	self.configuration.systemIP = kEmptyText;
	self.configuration.configUrl = kEmptyText;
	XCTAssertThrowsSpecificNamed([self.baseController createBaseUrl], NSException, NSInternalInconsistencyException);
}

/*
 * If system IP is not available and config URL is non empty text then createBaseUrl method should
 * return config url as the base url
 */
- (void)testCreateBaseUrlScenario5 {
	self.configuration.systemIP = nil;
	self.configuration.configUrl = TEST_URL;
	XCTAssertEqualObjects([self.baseController createBaseUrl], self.configuration.configUrl);
}

/*
 * If system IP is empty text and config URL is non empty text then createBaseUrl method should
 * return config url as the base url
 */
- (void)testCreateBaseUrlScenario6 {
	self.configuration.systemIP = kEmptyText;
	self.configuration.configUrl = TEST_URL;
	XCTAssertEqualObjects([self.baseController createBaseUrl], self.configuration.configUrl);
}

/*
 * If system IP is non empty text and config url and port is not available then createBaseUrl
 * method should throw exception of type NSInternalInconsistencyException
 */
- (void)testCreateBaseUrlScenario7 {
	self.configuration.systemIP = TEST_IP;
	self.configuration.port = nil;
	self.configuration.configUrl = nil;
	XCTAssertThrowsSpecificNamed([self.baseController createBaseUrl], NSException, NSInternalInconsistencyException);
}

/*
 * If system IP is non empty text, config url is empty text and port is not available then
 * createBaseUrl method should throw exception of type NSInternalInconsistencyException
 */
- (void)testCreateBaseUrlScenario8 {
	self.configuration.systemIP = TEST_IP;
	self.configuration.port = nil;
	self.configuration.configUrl = kEmptyText;
	XCTAssertThrowsSpecificNamed([self.baseController createBaseUrl], NSException, NSInternalInconsistencyException);
}

/*
 * If system IP and config url are non empty text and port is not available then
 * createBaseUrl method should return config url as the base url
 */
- (void)testCreateBaseUrlScenario9 {
	self.configuration.systemIP = TEST_IP;
	self.configuration.port = nil;
	self.configuration.configUrl = TEST_URL;
	XCTAssertEqualObjects([self.baseController createBaseUrl], self.configuration.configUrl);
}

/*
 * If system IP is non empty text and port is empty text then createBaseUrl method should
 * throw exception of type NSInternalInconsistencyException
 */
- (void)testCreateBaseUrlScenario10 {
	self.configuration.systemIP = TEST_IP;
	self.configuration.port = kEmptyText;
	self.configuration.configUrl = nil;
	XCTAssertThrowsSpecificNamed([self.baseController createBaseUrl], NSException, NSInternalInconsistencyException);
}

/*
 * If system IP is non empty text and port is empty text then createBaseUrl method should
 * throw exception of type NSInternalInconsistencyException
 */
- (void)testCreateBaseUrlScenario11 {
	self.configuration.systemIP = TEST_IP;
	self.configuration.port = kEmptyText;
	self.configuration.configUrl = kEmptyText;
	XCTAssertThrowsSpecificNamed([self.baseController createBaseUrl], NSException, NSInternalInconsistencyException);
}

/*
 * If system IP is non empty text and port is empty text then createBaseUrl method should
 * return config url as the base url
 */
- (void)testCreateBaseUrlScenario12 {
	self.configuration.systemIP = TEST_IP;
	self.configuration.port = kEmptyText;
	self.configuration.configUrl = TEST_URL;
	XCTAssertEqualObjects([self.baseController createBaseUrl], self.configuration.configUrl);
}

/*
 * If system IP and port are non empty text and config url is not available then createBaseUrl method
 * should return "http://ip:port" as the base url
 */
- (void)testCreateBaseUrlScenario13 {
	self.configuration.systemIP = TEST_IP;
	self.configuration.port = TEST_PORT;
	self.configuration.configUrl = nil;
	NSString *baseUrl = [NSString stringWithFormat:@"http://%@:%@", self.configuration.systemIP, self.configuration.port];
	XCTAssertEqualObjects([self.baseController createBaseUrl], baseUrl);
}

/*
 * If system IP and port are non empty text and config url is empty text then createBaseUrl method
 * should return "http://ip:port" as the base url
 */
- (void)testCreateBaseUrlScenario14 {
	self.configuration.systemIP = TEST_IP;
	self.configuration.port = TEST_PORT;
	self.configuration.configUrl = kEmptyText;
	NSString *baseUrl = [NSString stringWithFormat:@"http://%@:%@", self.configuration.systemIP, self.configuration.port];
	XCTAssertEqualObjects([self.baseController createBaseUrl], baseUrl);
}

/*
 * If system IP, config url and port are non empty text then createBaseUrl method should
 * return "http://ip:port" as the base url
 */
- (void)testCreateBaseUrlScenario15 {
	self.configuration.systemIP = TEST_IP;
	self.configuration.port = TEST_PORT;
	self.configuration.configUrl = TEST_URL;
	NSString *baseUrl = [NSString stringWithFormat:@"http://%@:%@", self.configuration.systemIP, self.configuration.port];
	XCTAssertEqualObjects([self.baseController createBaseUrl], baseUrl);
}

@end
