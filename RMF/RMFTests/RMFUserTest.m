//
//  RMFUserTest.m
//  RMF
//
//  Created by Divyam on 6/25/15.
//  Copyright (c) 2015 Ruckus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "RMFUser.h"
#import "RMFMacro.h"

@interface RMFUserTest : XCTestCase
@property (nonatomic,retain) RMFUser *rmfUser;
@end

@implementation RMFUserTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    self.rmfUser = [[RMFUser alloc] init];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

// Test RMFAPExtendedStatus object for nil and validate it
- (void)testRMFAPExtendedStatusObjectForNil {
    XCTAssertNotNil(self.rmfUser);
    XCTAssertTrue([self.rmfUser isKindOfClass:[RMFUser class]]);
}

// Test property roletype
- (void)testInternalIp {
    if (OBJ_NOT_NIL(self.rmfUser)) {
        self.rmfUser.roleType = 1;
        XCTAssertEqual(1,self.rmfUser.roleType);
    }
    
}

// Test property user name
- (void)testUserName {
    if (OBJ_NOT_NIL(self.rmfUser)) {
        self.rmfUser.userName = @"test";
        XCTAssertEqualObjects(@"test", self.rmfUser.userName);
    }
    else {
        XCTAssertNil(self.rmfUser.userName);
    }
}

// Test property email
- (void)testEmail {
    if (OBJ_NOT_NIL(self.rmfUser)) {
        self.rmfUser.email = @"test";
        XCTAssertEqualObjects(@"test", self.rmfUser.email);
    }
    else {
        XCTAssertNil(self.rmfUser.email);
    }
}

// Test property fullname
- (void)testFullname {
    if (OBJ_NOT_NIL(self.rmfUser)) {
        self.rmfUser.fullName = @"test";
        XCTAssertEqualObjects(@"test", self.rmfUser.fullName);
    }
    else {
        XCTAssertNil(self.rmfUser.fullName);
    }
}

// Test property address
- (void)testAddress {
    if (OBJ_NOT_NIL(self.rmfUser)) {
        self.rmfUser.address = @"test";
        XCTAssertEqualObjects(@"test", self.rmfUser.address);
    }
    else {
        XCTAssertNil(self.rmfUser.address);
    }
}

// Test property phno
- (void)testPhNo {
    if (OBJ_NOT_NIL(self.rmfUser)) {
        self.rmfUser.phoneNumber = @"123123123";
        XCTAssertEqualObjects(@"123123123", self.rmfUser.phoneNumber);
    }
    else {
        XCTAssertNil(self.rmfUser.phoneNumber);
    }
}


@end
