/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMF.h"
#import "RMFMacro.h"


@interface RMFConfiguration (Tests)
- (NSString *)findValueForKey:(NSString *)key;
- (NSArray *)findArrayForKey:(NSString *)key;
- (NSDictionary *)findDictionaryForKey:(NSString *)key;
- (NSString *)configBlankJson;
@end

@interface RMFConfigurationTest : XCTestCase
@property (nonatomic, strong) RMFConfiguration *rmfConfiguration;

@end


@implementation RMFConfigurationTest

- (void)setUp {
	[super setUp];
	// Put setup code here. This method is called before the invocation of each test method in the class.
	self.rmfConfiguration = [RMFConfiguration sharedRMFConfiguration];
}

- (void)tearDown {
	// Put teardown code here. This method is called after the invocation of each test method in the class.
	[super tearDown];
}

#pragma mark - Test case for RMFConfiguration


// Test RMFConfiguration object for nil and validate it
- (void)testConfigObjectForNil {
	XCTAssertNotNil(self.rmfConfiguration);
	XCTAssertTrue([self.rmfConfiguration isKindOfClass:[RMFConfiguration class]]);
}

// Test method findValueForKey by passing nil and invalid value parameter
- (void)testConfigFindValueForKey {
	XCTAssertNotNil([self.rmfConfiguration findValueForKey:nil]);
	XCTAssertNotNil([self.rmfConfiguration findValueForKey:@"test"]);
}

// Test method findDictionaryForKey by passing nil and invalid value parameter
- (void)testConfigFindDicForKey {
	XCTAssertNil([self.rmfConfiguration findDictionaryForKey:nil]);
	XCTAssertNil([self.rmfConfiguration findDictionaryForKey:@"test"]);
}

// Test method findArrayForKey by passing nil and invalid value parameter
- (void)testConfigFindArrayForKey {
	XCTAssertNil([self.rmfConfiguration findArrayForKey:nil]);
	XCTAssertNil([self.rmfConfiguration findArrayForKey:@"test"]);
}

// Test property rmfConfig for both the case configuration file is available and not available
- (void)testRMFConfigDic {
	if ([self isConfigValueAvailable] && OBJ_NOT_NIL(self.rmfConfiguration)) {
		XCTAssertNotNil(self.rmfConfiguration.rmfConfig);
	}
	else {
		XCTAssertNil(self.rmfConfiguration.rmfConfig);
	}
}

// Test property isDatabaseEnabled
- (void)testIsDatabaseEnabled {
	if (OBJ_NOT_NIL(self.rmfConfiguration)) {
		self.rmfConfiguration.isDatabaseEnabled = YES;
		XCTAssertTrue(self.rmfConfiguration.isDatabaseEnabled);
	}
	else {
		XCTAssertFalse(self.rmfConfiguration.isDatabaseEnabled);
	}
}

// Test property isSandboxEnabled
- (void)testIsSandboxEnabled {
	if (OBJ_NOT_NIL(self.rmfConfiguration)) {
		self.rmfConfiguration.isSandboxEnabled = YES;
		XCTAssertTrue(self.rmfConfiguration.isSandboxEnabled);
	}
	else {
		XCTAssertFalse(self.rmfConfiguration.isSandboxEnabled);
	}
}

// Test property isAnalyticsEnabled
- (void)testIsAnalyticsEnabled {
	if (OBJ_NOT_NIL(self.rmfConfiguration)) {
		self.rmfConfiguration.isAnalyticsEnabled = YES;
		XCTAssertTrue(self.rmfConfiguration.isAnalyticsEnabled);
	}
	else {
		XCTAssertFalse(self.rmfConfiguration.isAnalyticsEnabled);
	}
}

// Test property aboutUs for both case configuration file is available and not available
- (void)testAboutUs {
	if ([self isConfigValueAvailable] && OBJ_NOT_NIL(self.rmfConfiguration)) {
		XCTAssertNotNil(self.rmfConfiguration.aboutUs);
	}
	else {
		XCTAssertNil(self.rmfConfiguration.aboutUs);
	}
}

// Test property help for both case configuration file is available and not available

- (void)testHelp {
	if ([self isConfigValueAvailable] && OBJ_NOT_NIL(self.rmfConfiguration)) {
		XCTAssertNotNil(self.rmfConfiguration.help);
	}
	else {
		XCTAssertNil(self.rmfConfiguration.help);
	}
}

// Test property privacyPolicy for both case configuration file is available and not available

- (void)testPrivacyPolicy {
	if ([self isConfigValueAvailable] && OBJ_NOT_NIL(self.rmfConfiguration)) {
		XCTAssertNotNil(self.rmfConfiguration.privacyPolicy);
	}
	else {
		XCTAssertNil(self.rmfConfiguration.privacyPolicy);
	}
}

// Check resource bundle is available or not
- (BOOL)isConfigValueAvailable {
	BOOL isAvail = false;
	NSBundle *rmfLibBundle = RMFBUNDLE;
	if (OBJ_NOT_NIL(rmfLibBundle)) {
		NSString *configFilePath =  [[rmfLibBundle resourcePath] stringByAppendingPathComponent:CONFIG_FILE_NAME];
		if (OBJ_NOT_NIL(configFilePath)) {
			isAvail = YES;
		}
	}
	if (!isAvail && OBJ_NOT_NIL(self.rmfConfiguration) && [self.rmfConfiguration respondsToSelector:@selector(configBlankJson)]) {
		if (STRING_NOT_NIL([self.rmfConfiguration configBlankJson])) {
			isAvail = YES;
		}
	}

	return isAvail;
}

//Test method screenNavigationFlowForFirstTime in both the case when system type defined and not defined
- (void)testScreenNavigationFlowForFirstTime {
	if (OBJ_NOT_NIL(self.rmfConfiguration)) {
		if (self.rmfConfiguration.system == kSystemTypeNone) {
			XCTAssertThrows([self.rmfConfiguration screenNavigationFlowForFirstTime]);
		}
		else {
			XCTAssertNotNil([self.rmfConfiguration screenNavigationFlowForFirstTime]);
		}

		self.rmfConfiguration.system = kSystemTypeKUMO;
		XCTAssertNotNil([self.rmfConfiguration screenNavigationFlowForFirstTime]);
	}
}

// Test method screenNavigationFlowForRole for posible cases for both the user role when system type defined or not defined
- (void)testScreenNavigationFlowForRole {
	if (OBJ_NOT_NIL(self.rmfConfiguration)) {
		if (self.rmfConfiguration.system == kSystemTypeNone) {
			XCTAssertThrows([self.rmfConfiguration screenNavigationFlowForRole:kUserRoleSuper]);
			XCTAssertThrows([self.rmfConfiguration screenNavigationFlowForRole:kUserRoleNormal]);
		}
		else {
			XCTAssertNotNil([self.rmfConfiguration screenNavigationFlowForRole:kUserRoleSuper]);
			XCTAssertNotNil([self.rmfConfiguration screenNavigationFlowForRole:kUserRoleNormal]);
		}

		self.rmfConfiguration.system = kSystemTypeKUMO;
		XCTAssertNotNil([self.rmfConfiguration screenNavigationFlowForRole:kUserRoleSuper]);
		self.rmfConfiguration.system = kSystemTypeNone;
	}
}

//Test method sponsoredUrlForOrientation in both orientation
- (void)testSponsoredUrlForOrientation {
	if (OBJ_NOT_NIL(self.rmfConfiguration)) {
		if (self.rmfConfiguration.sponsoredURL != nil) {
			XCTAssertNotNil([self.rmfConfiguration sponsoredUrlForOrientation:kScreenOrientationPortrait]);
			XCTAssertNotNil([self.rmfConfiguration sponsoredUrlForOrientation:kScreenOrientationLandscape]);
		}
	}
}

// Test property aboutUs for both case configuration file is available and not available
- (void)testKeepMeLogin {
	if ([self isConfigValueAvailable] && OBJ_NOT_NIL(self.rmfConfiguration)) {
		if (self.rmfConfiguration.keepMeLogin) {
			XCTAssertTrue(self.rmfConfiguration.keepMeLogin);
		}
		else {
			XCTAssertFalse(self.rmfConfiguration.keepMeLogin);
		}
	}
	else {
		XCTAssertFalse(self.rmfConfiguration.keepMeLogin);
	}
}

//Test property rmfConfiguration for both case configuration file is available and not available
- (void)testNotificationEnabled {
	if ([self isConfigValueAvailable] && OBJ_NOT_NIL(self.rmfConfiguration)) {
		if (self.rmfConfiguration.notificationEnabled) {
			XCTAssertTrue(self.rmfConfiguration.notificationEnabled);
		}
		else {
			XCTAssertFalse(self.rmfConfiguration.notificationEnabled);
		}
	}
	else {
		XCTAssertFalse(self.rmfConfiguration.notificationEnabled);
	}
}

- (void)testSupportedSocialLinks {
	if ([self isConfigValueAvailable] && OBJ_NOT_NIL(self.rmfConfiguration)) {
		XCTAssertNotNil(self.rmfConfiguration.supportedSocialLinks);
	}
	else {
		XCTAssertNil(self.rmfConfiguration.supportedSocialLinks);
	}
}

- (void)testConfigUrl {
	if (OBJ_NOT_NIL(self.rmfConfiguration)) {
		self.rmfConfiguration.configUrl = @"test_url";
		XCTAssertEqualObjects(@"test_url", self.rmfConfiguration.configUrl);
	}
}

- (void)testCompanyUrl {
	if ([self isConfigValueAvailable] && OBJ_NOT_NIL(self.rmfConfiguration)) {
		XCTAssertNotNil(self.rmfConfiguration.companyUrl);
	}
	else {
		XCTAssertNil(self.rmfConfiguration.companyUrl);
	}
}

- (void)testCompanyName {
	if ([self isConfigValueAvailable] && OBJ_NOT_NIL(self.rmfConfiguration)) {
		XCTAssertNotNil(self.rmfConfiguration.companyName);
	}
	else {
		XCTAssertNil(self.rmfConfiguration.companyName);
	}
}

- (void)testAppVersion {
	if ([self isConfigValueAvailable] && OBJ_NOT_NIL(self.rmfConfiguration)) {
		XCTAssertNotNil(self.rmfConfiguration.appVersion);
	}
	else {
		XCTAssertNil(self.rmfConfiguration.appVersion);
	}
}

- (void)testPromoURL {
	if ([self isConfigValueAvailable] && OBJ_NOT_NIL(self.rmfConfiguration)) {
		XCTAssertNotNil(self.rmfConfiguration.promoURL);
	}
	else {
		XCTAssertNil(self.rmfConfiguration.promoURL);
	}
}

- (void)testSupportEmail {
	if ([self isConfigValueAvailable] && OBJ_NOT_NIL(self.rmfConfiguration)) {
		XCTAssertNotNil(self.rmfConfiguration.supportEmail);
	}
	else {
		XCTAssertNil(self.rmfConfiguration.supportEmail);
	}
}

- (void)testSupportPhone {
	if ([self isConfigValueAvailable] && OBJ_NOT_NIL(self.rmfConfiguration)) {
		XCTAssertNotNil(self.rmfConfiguration.supportPhone);
	}
	else {
		XCTAssertNil(self.rmfConfiguration.supportPhone);
	}
}

- (void)testSystemVersion {
	if ([self isConfigValueAvailable] && OBJ_NOT_NIL(self.rmfConfiguration)) {
		XCTAssertNotNil(self.rmfConfiguration.systemVersion);
	}
	else {
		XCTAssertNil(self.rmfConfiguration.systemVersion);
	}
}

- (void)testSponsoredURL {
	if ([self isConfigValueAvailable] && OBJ_NOT_NIL(self.rmfConfiguration)) {
		XCTAssertNotNil(self.rmfConfiguration.sponsoredURL);
	}
	else {
		XCTAssertNil(self.rmfConfiguration.sponsoredURL);
	}
}

- (void)testPortInfo {
	if ([self isConfigValueAvailable] && OBJ_NOT_NIL(self.rmfConfiguration)) {
		XCTAssertNotNil(self.rmfConfiguration.portInfo);
	}
	else {
		XCTAssertNil(self.rmfConfiguration.portInfo);
	}
}

- (void)testMimeType {
	if ([self isConfigValueAvailable] && OBJ_NOT_NIL(self.rmfConfiguration)) {
		XCTAssertNotNil(self.rmfConfiguration.mimeType);
	}
	else {
		XCTAssertNil(self.rmfConfiguration.mimeType);
	}
}

- (void)testRmfVersion {
	if ([self isConfigValueAvailable] && OBJ_NOT_NIL(self.rmfConfiguration)) {
		XCTAssertNotNil(self.rmfConfiguration.rmfVersion);
	}
	else {
		XCTAssertNil(self.rmfConfiguration.rmfVersion);
	}
}

- (void)testRmfName {
	if ([self isConfigValueAvailable] && OBJ_NOT_NIL(self.rmfConfiguration)) {
		XCTAssertNotNil(self.rmfConfiguration.rmfName);
	}
	else {
		XCTAssertNil(self.rmfConfiguration.rmfName);
	}
}

- (void)testRmfDescription {
	if ([self isConfigValueAvailable] && OBJ_NOT_NIL(self.rmfConfiguration)) {
		XCTAssertNotNil(self.rmfConfiguration.rmfDescription);
	}
	else {
		XCTAssertNil(self.rmfConfiguration.rmfDescription);
	}
}

- (void)testDefaultZome {
	if ([self isConfigValueAvailable] && OBJ_NOT_NIL(self.rmfConfiguration)) {
		XCTAssertNotNil(self.rmfConfiguration.defaultZome);
	}
	else {
		XCTAssertNil(self.rmfConfiguration.defaultZome);
	}
}

- (void)testTermOfUseURL {
	if ([self isConfigValueAvailable] && OBJ_NOT_NIL(self.rmfConfiguration)) {
		XCTAssertNotNil(self.rmfConfiguration.termOfUseURL);
	}
	else {
		XCTAssertNil(self.rmfConfiguration.termOfUseURL);
	}
}

- (void)testPrivacyPolicyURL {
	if ([self isConfigValueAvailable] && OBJ_NOT_NIL(self.rmfConfiguration)) {
		XCTAssertNotNil(self.rmfConfiguration.privacyPolicyURL);
	}
	else {
		XCTAssertNil(self.rmfConfiguration.privacyPolicyURL);
	}
}

- (void)testIPAddressBlankJson {
	if (OBJ_NOT_NIL(self.rmfConfiguration) && [self.rmfConfiguration respondsToSelector:@selector(configBlankJson)]) {
		XCTAssertNotNil([self.rmfConfiguration configBlankJson]);
	}
}

- (void)testSystemIP {
	if (OBJ_NOT_NIL(self.rmfConfiguration)) {
		self.rmfConfiguration.systemIP = @"test_ip";
		XCTAssertEqualObjects(@"test_ip", self.rmfConfiguration.systemIP);
	}
}

- (void)testPort {
	if (OBJ_NOT_NIL(self.rmfConfiguration)) {
		self.rmfConfiguration.port = @"test_port";
		XCTAssertEqualObjects(@"test_port", self.rmfConfiguration.port);
	}
}

- (void)testNotificationTypes {
	if ([self isConfigValueAvailable] && OBJ_NOT_NIL(self.rmfConfiguration)) {
		XCTAssertNotNil(self.rmfConfiguration.notificationTypes);
	}
	else {
		XCTAssertNil(self.rmfConfiguration.notificationTypes);
	}
}

// Property userType should store/returns an integer value
- (void)testUserTypeScenario1 {
	NSInteger testUserType = kUserRoleSuper;

	if (self.rmfConfiguration) {
		self.rmfConfiguration.userType = testUserType;
		XCTAssertEqual(self.rmfConfiguration.userType, testUserType);
	}
}

// Property userAssociation should store/returns an string value
- (void)testUserAssociationScenario1 {
	NSString *testUserAssociation = @"kumomobile";

	if (self.rmfConfiguration) {
		self.rmfConfiguration.userAssociation = testUserAssociation;
		XCTAssertEqualObjects(self.rmfConfiguration.userAssociation, testUserAssociation);
	}
}

@end
