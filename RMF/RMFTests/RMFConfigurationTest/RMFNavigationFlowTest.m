/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFNavigationFlow.h"
#import "RMFMacro.h"

@interface RMFNavigationFlow (Tests)
- (NSString *)navigationFlowForRole:(int)userRole navigationFlowDic:(NSDictionary *)navigationFlowDic;
@end



@interface RMFNavigationFlowTest : XCTestCase
@property (nonatomic, strong) RMFNavigationFlow *rmfNavigationFlow;
@end

@implementation RMFNavigationFlowTest

- (void)setUp {
	[super setUp];
	// Put setup code here. This method is called before the invocation of each test method in the class.

	self.rmfNavigationFlow = [[RMFNavigationFlow alloc] init];
}

- (void)tearDown {
	// Put teardown code here. This method is called after the invocation of each test method in the class.
	[super tearDown];
}

/*
 * Test method screenNavigationFlowForRole:navigationFlowDic:systemType for possible cases for both the user role and system type
 */
- (void)testScreenNavigationFlowForRole {
	if (OBJ_NOT_NIL(self.rmfNavigationFlow)) {
		NSDictionary *flowDic = [NSDictionary dictionaryWithObjectsAndKeys:@"", NAVIGATION_FLOW_FIRST_TIME, @"", NAVIGATION_FLOW_FOR_ADMIN, @"", NAVIGATION_FLOW_FOR_NORMAL, nil];
		NSDictionary *kumoDic = [NSDictionary dictionaryWithObjectsAndKeys:flowDic, KUMO, nil];
		NSDictionary *scgDic = [NSDictionary dictionaryWithObjectsAndKeys:flowDic, SCG, nil];
		NSDictionary *zdDic = [NSDictionary dictionaryWithObjectsAndKeys:flowDic, ZD, nil];

		XCTAssertNotNil([self.rmfNavigationFlow screenNavigationFlowForRole:kUserRoleSuper navigationFlowDic:nil systemType:kSystemTypeKUMO]);
		XCTAssertNotNil([self.rmfNavigationFlow screenNavigationFlowForRole:kUserRoleNormal navigationFlowDic:nil systemType:kSystemTypeKUMO]);
		XCTAssertNotNil([self.rmfNavigationFlow screenNavigationFlowForRole:kUserRoleSuper navigationFlowDic:kumoDic systemType:kSystemTypeKUMO]);

		XCTAssertNotNil([self.rmfNavigationFlow screenNavigationFlowForRole:kUserRoleSuper navigationFlowDic:nil systemType:kSystemTypeSCG]);
		XCTAssertNotNil([self.rmfNavigationFlow screenNavigationFlowForRole:kUserRoleNormal navigationFlowDic:nil systemType:kSystemTypeSCG]);
		XCTAssertNotNil([self.rmfNavigationFlow screenNavigationFlowForRole:kUserRoleSuper navigationFlowDic:scgDic systemType:kSystemTypeSCG]);

		XCTAssertNotNil([self.rmfNavigationFlow screenNavigationFlowForRole:kUserRoleSuper navigationFlowDic:nil systemType:kSystemTypeZD]);
		XCTAssertNotNil([self.rmfNavigationFlow screenNavigationFlowForRole:kUserRoleNormal navigationFlowDic:nil systemType:kSystemTypeZD]);
		XCTAssertNotNil([self.rmfNavigationFlow screenNavigationFlowForRole:kUserRoleSuper navigationFlowDic:zdDic systemType:kSystemTypeZD]);
	}
}

/*
 * Test method screenNavigationFlowForFirstTimeFromNavigationDic:systemType for all system type
 */
- (void)testScreenNavigationFlowForFirstTimeFromNavigationDic {
	if (OBJ_NOT_NIL(self.rmfNavigationFlow)) {
		XCTAssertNotNil([self.rmfNavigationFlow screenNavigationFlowForFirstTimeFromNavigationDic:nil systemType:kSystemTypeKUMO]);
		XCTAssertNotNil([self.rmfNavigationFlow screenNavigationFlowForFirstTimeFromNavigationDic:nil systemType:kSystemTypeSCG]);
		XCTAssertNotNil([self.rmfNavigationFlow screenNavigationFlowForFirstTimeFromNavigationDic:nil systemType:kSystemTypeZD]);

		NSDictionary *flowDic = [NSDictionary dictionaryWithObjectsAndKeys:@"", NAVIGATION_FLOW_FIRST_TIME, @"", NAVIGATION_FLOW_FOR_ADMIN, @"", NAVIGATION_FLOW_FOR_NORMAL, nil];
		NSDictionary *kumoDic = [NSDictionary dictionaryWithObjectsAndKeys:flowDic, KUMO, nil];
		NSDictionary *scgDic = [NSDictionary dictionaryWithObjectsAndKeys:flowDic, SCG, nil];
		NSDictionary *zdDic = [NSDictionary dictionaryWithObjectsAndKeys:flowDic, ZD, nil];

		XCTAssertNotNil([self.rmfNavigationFlow screenNavigationFlowForFirstTimeFromNavigationDic:kumoDic systemType:kSystemTypeKUMO]);
		XCTAssertNotNil([self.rmfNavigationFlow screenNavigationFlowForFirstTimeFromNavigationDic:scgDic systemType:kSystemTypeSCG]);
		XCTAssertNotNil([self.rmfNavigationFlow screenNavigationFlowForFirstTimeFromNavigationDic:zdDic systemType:kSystemTypeZD]);
	}
}

- (void)testNavigationFlowForRole {
	if (OBJ_NOT_NIL(self.rmfNavigationFlow)) {
		XCTAssertNotNil([self.rmfNavigationFlow navigationFlowForRole:kUserRoleSuper navigationFlowDic:[NSDictionary dictionary]]);
		XCTAssertNotNil([self.rmfNavigationFlow navigationFlowForRole:kUserRoleNormal navigationFlowDic:[NSDictionary dictionary]]);
	}
}

@end
