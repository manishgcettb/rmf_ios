/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFConfiguration.h"
#import "RMFImageLoader.h"
#import "RMFMacro.h"

#define  kImageDownloaded            @"Image should be downloaded"
#define  kImageUploaded            @"Image should be uploaded"

@interface RMFImageLoaderTest : XCTestCase

// Stores the instance of RMFConfiguration
@property (nonatomic, strong) RMFConfiguration *configuration;

// Stores the instance of RMFImageLoader
@property (nonatomic, strong) RMFImageLoader *imageLoader;

@end

#define TEST_PORT                   @"8080"
#define TEST_IP                     @"10.199.4.106"

@implementation RMFImageLoaderTest

- (void)setUp {
    [super setUp];
    [self initialConfiguration];
    self.imageLoader = [RMFImageLoader sharedRMFImageLoader];
}

- (void)tearDown {
    [super tearDown];
}

/*
 * initialise RMFConfiguration instance with system type, system IP and system port
 */
- (void)initialConfiguration {
    self.configuration = [RMFConfiguration sharedRMFConfiguration];
    self.configuration.system = kSystemTypeKUMO;
    self.configuration.systemIP = TEST_IP;
    self.configuration.port = TEST_PORT;
}


/*
 *  sharedRMFImageLoader should return a valid instance
 */
- (void)testSharedRMFImageLoader {
    XCTAssertNotNil(self.imageLoader);
}

/*
 * [[RMFImageLoader alloc] init] should return a valid instance
 */
- (void)testSharedRMFImageLoaderUsingAlloc {
    XCTAssertNotNil([[RMFImageLoader alloc] init]);
}

/*
 * [[RMFImageLoader alloc] init] and sharedRMFImageLoader should return same instance
 */
- (void)testIsSharedRMFScgParserAndAllocReturnsSameObject {
    XCTAssertEqual([[RMFImageLoader alloc] init], self.imageLoader);
}

/*
 * If Image, image url and response block are nil then uploadImage should not throw any exception
 */

-(void)testUploadImageScenario1{
    XCTAssertNoThrow([self.imageLoader uploadImage:nil imageUrl:nil response:nil]);
}

/*
 * If image is available but upload url and response block are not available then uploadImage
 * should not throw any exception
 */

-(void)testUploadImageScenario2{
    NSData* data = [[NSData alloc] init];
    XCTAssertNoThrow([self.imageLoader uploadImage:data imageUrl:nil response:nil]);
}

/*
 * If image url is not available but image data and response block are not available then
 * uploadImage method should not throw any exception
 */

-(void)testUploadImageScenario3{
    XCTAssertNoThrow([self.imageLoader uploadImage:nil imageUrl:kEmptyText response:nil]);
}

/*
 * If image url and image data are not available but response block is available then
 * uploadImage should return NSError instance with description "KUMO Error: Image upload url and
 * image are required"
 */

-(void)testUploadImageScenario4{
    
    __block XCTestExpectation *expectation = [self expectationWithDescription:kImageUploaded];
    
    [self.imageLoader uploadImage:nil imageUrl:nil response:^(NSError *error, id data) {
        XCTAssertEqual(kKUMOCodeForImageAndUrlNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler:^(NSError *error) {
        XCTAssertNil(error);
        expectation = nil;
    }];
}

/*
 * If upload image and upload url are available but response block is not available then
 * uploadImage should not throw any exception
 */

-(void) testUploadImageScenario5{
    XCTAssertNoThrow([self.imageLoader uploadImage:[[NSData alloc] init] imageUrl:kEmptyText response:nil]);
}

/*
 * If upload image and response block are available but image url is not available then uploadImage
 * should return NSError instance with description "KUMO Error: Image upload url and image are required"
 */

-(void)testUploadImageScenario6{
    __block XCTestExpectation *expectation = [self expectationWithDescription:kImageUploaded];
    
    [self.imageLoader uploadImage:[[NSData alloc] init] imageUrl:nil response:^(NSError *error, id data) {
        XCTAssertEqual(kKUMOCodeForImageAndUrlNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler:^(NSError *error) {
        XCTAssertNil(error);
        expectation = nil;
    }];
}

/*
 * If image url and response block are available but image data is not available then uploadImage
 * should return NSError instance with description "KUMO Error: Image upload url and image are required"
 */

-(void)testUploadImageScenario7{
    
    __block XCTestExpectation *expectation = [self expectationWithDescription:kImageUploaded];
    
    [self.imageLoader uploadImage:nil imageUrl:kEmptyText response:^(NSError *error, id data) {
        XCTAssertEqual(kKUMOCodeForImageAndUrlNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler:^(NSError *error) {
        XCTAssertNil(error);
    }];
    
}

/*
 * If image url, image data and response block are available then uploadImage should return upload response
 * data from the server
 */

-(void)testUploadImageScenario8{
    //Update this test case once the request to upload image is implemented
}

/*
 * If image url and response block are not available then downloadImage method shoud not
 * throw any exception
 */

-(void)testDownloadImageScenario1{
    XCTAssertNoThrow([self.imageLoader downloadImage:nil response:nil]);
}

/*
 * If image url is available but the response block is not available then downloadImage method should
 * not throw any exception
 */

-(void)testDownloadImageScenario2{
    XCTAssertNoThrow([self.imageLoader downloadImage:kEmptyText response:nil]);
}

/*
 * If image url is not available but the response block is available then downloadImage method
 * should return NSError object with description "KUMO Error: Image download url is required"
 */

-(void)testDownloadImageScenario3{
    
    __block XCTestExpectation *expectation = [self expectationWithDescription:kImageDownloaded];
    [self.imageLoader downloadImage:nil response:^(NSError *error, id data) {
        XCTAssertEqual(kKUMOCodeForImageUrlNotFoundDesc, [error.userInfo objectForKey:kLocalizedDescriptionKey]);
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:kRequestTimeOutInterval handler:^(NSError *error) {
        XCTAssertNil(error);
        expectation = nil;
    }];
}

/*
 * If image url and response block are available then downloadImage method should return the image
 */
-(void)testDownloadImageScenario4{
    // TODO: update this test case once download image request is implemented
}

/*
 * clearImageCache should not throw any exception
 */
-(void)testclearImageCacheScenario1{
    [self.imageLoader clearImageCache];
}

@end
