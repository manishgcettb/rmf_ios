/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFSummaryStatistics.h"

@interface RMFSummaryStatisticsTest : XCTestCase

// Stores and returns summary statistics instance
@property (nonatomic, strong) RMFSummaryStatistics *summaryStatistics;

@end

@implementation RMFSummaryStatisticsTest

- (void)setUp {
    [super setUp];
    self.summaryStatistics = [[RMFSummaryStatistics alloc] init];
}

- (void)tearDown {
    [super tearDown];
}

/*
 * Test if RMFSummaryStatisticsTest instance is not nil
 */

-(void)testRMFSummaryStatisticsObject{
    XCTAssertNotNil(self.summaryStatistics);
}

/*
 * Property wlanCount should store/return an integer value
 */

-(void)testWlanCount {
    NSInteger testWlanCount = 5;
    self.summaryStatistics.wlanCount = testWlanCount;
    XCTAssertEqual(self.summaryStatistics.wlanCount, testWlanCount);
}

/*
 * Property apCount should store/return an integer value
 */

-(void)testAPCount {
    NSInteger testAPCount = 5;
    self.summaryStatistics.apCount = testAPCount;
    XCTAssertEqual(self.summaryStatistics.apCount, testAPCount);
}

/*
 * Property totalAlarmCount should store/return an integer value
 */

-(void)testTotalAlarmCount {
    NSInteger testTotalAlarmCount = 5;
    self.summaryStatistics.totalAlarmCount = testTotalAlarmCount;
    XCTAssertEqual(self.summaryStatistics.totalAlarmCount, testTotalAlarmCount);
}

/*
 * Property testMajorAlarmCount should store/return an integer value
 */

-(void)testMajorAlarmCount {
    NSInteger testMajorAlarmCount = 5;
    self.summaryStatistics.majorAlarmCount = testMajorAlarmCount;
    XCTAssertEqual(self.summaryStatistics.majorAlarmCount, testMajorAlarmCount);
}

/*
 * Property testMajorAlarmCount should store/return an integer value
 */

-(void)testMinorAlarmCount {
    NSInteger testMinorAlarmCount = 5;
    self.summaryStatistics.minorAlarmCount = testMinorAlarmCount;
    XCTAssertEqual(self.summaryStatistics.minorAlarmCount, testMinorAlarmCount);
}

/*
 * Property testMajorAlarmCount should store/return an integer value
 */

-(void)testCriticalAlarmCount {
    NSInteger testCriticalAlarmCount = 5;
    self.summaryStatistics.criticalAlarmCount = testCriticalAlarmCount;
    XCTAssertEqual(self.summaryStatistics.criticalAlarmCount, testCriticalAlarmCount);
}

/*
 * Property testMajorAlarmCount should store/return an integer value
 */

-(void)testClientCount {
    NSInteger testClientCount = 5;
    self.summaryStatistics.clientCount = testClientCount;
    XCTAssertEqual(self.summaryStatistics.clientCount, testClientCount);
}

@end
