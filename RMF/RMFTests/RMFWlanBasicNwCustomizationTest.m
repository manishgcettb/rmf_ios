/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <XCTest/XCTest.h>
#import "RMFWlanBasicNwCustomization.h"

@interface RMFWlanBasicNwCustomizationTest : XCTestCase
@property (nonatomic, strong) RMFWlanBasicNwCustomization *wlanBasicNwCustomization;
@end



@implementation RMFWlanBasicNwCustomizationTest

- (void)setUp {
	[super setUp];

	// Create an instance of RMFWlanBasicNwCustomization
	self.wlanBasicNwCustomization = [[RMFWlanBasicNwCustomization alloc] init];
}

- (void)tearDown {
	[super tearDown];
}

- (void)testExample {
	XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
}

// Test if RMFWlanBasicNwCustomization instance is not nil
- (void)testRMFWlanBasicNwCustomizationObject {
	XCTAssertNotNil(self.wlanBasicNwCustomization);
}

// Property vlan Id should store/ return an integer value
- (void)testValnId {
	NSInteger testValnId = 3;
	if (self.wlanBasicNwCustomization) {
		self.wlanBasicNwCustomization.vlanId = testValnId;
		XCTAssertTrue(self.wlanBasicNwCustomization.vlanId == testValnId);
	}
}

@end
