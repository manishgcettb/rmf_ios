/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "RMFAnalyticsKit.h"

@implementation RMFAnalyticsKit

NSString* const AnalyticsKitEventTimeSeconds = @"AnalyticsKitEventTimeSeconds";

static NSArray *_loggers = nil;

+(void)initialize {
    _loggers = [[NSArray alloc] init];
}

+(void)initializeLoggers:(NSArray *)loggers {
#if !__has_feature(objc_arc)
    [loggers retain];
    [_loggers release];
#endif
    _loggers = loggers;
}

+(NSArray *)loggers {
    return _loggers;
}

+(void)logAppEnterForegroundEvent {
    for (id<RMFAnalyticsKitProvider> logger in _loggers) {
        [logger logAppEnterForegroundEvent];
    }
}

+(void)logAppEnterBackgroundEvent {
    for (id<RMFAnalyticsKitProvider> logger in _loggers) {
        [logger logAppEnterBackgroundEvent];
    }
}

+(void)logAppTerminateEvent {
    for (id<RMFAnalyticsKitProvider> logger in _loggers) {
        [logger logAppTerminateEvent];
    }
}

+(void)uncaughtException:(NSException *)exception {
    for (id<RMFAnalyticsKitProvider> logger in _loggers) {
        [logger uncaughtException:exception];
    }
    
}

+(void)logScreen:(NSString *)screenName {
    for (id<RMFAnalyticsKitProvider> logger in _loggers) {
        [logger logScreen:screenName];
    }
    
}

+(void)logEvent:(NSString *)event {
    for (id<RMFAnalyticsKitProvider> logger in _loggers) {
        [logger logEvent:event];
    }
}


+(void)logClickEvent:(NSString *)event withAction:(NSString *)action
{
    if (event == nil) event = @"nil";
    if (action == nil) action = @"nil";
    for (id<RMFAnalyticsKitProvider> logger in _loggers) {
        [logger logClickEvent:event withAction:action];
    }
    
}

+(void) logNavigationEvent:(NSString *)event withAction:(NSString *)action
{
    if (event == nil) event = @"nil";
    if (action == nil) action = @"nil";
    for (id<RMFAnalyticsKitProvider> logger in _loggers) {
        [logger logNavigationEvent:event withAction:action];
    }
    
}
+(void) logOrientationChangeEvent:(NSString *)event withAction:(NSString *)action
{
    if (event == nil) event = @"nil";
    if (action == nil) action = @"nil";
    for (id<RMFAnalyticsKitProvider> logger in _loggers) {
        [logger logOrientationChangeEvent:event withAction:action];
    }
}

+(void)logEvent:(NSString *)eventName timed:(BOOL)timed{
    for (id<RMFAnalyticsKitProvider> logger in _loggers) {
        [logger logEvent:eventName timed:timed];
    }
}


+(void)endTimedEvent:(NSString *)eventName withProperties:(NSDictionary *)dict{
    for (id<RMFAnalyticsKitProvider> logger in _loggers) {
        [logger endTimedEvent:eventName withProperties:dict];
    }
}

+(void)logError:(NSString *)name message:(NSString *)message exception:(NSException *)exception {
    for (id<RMFAnalyticsKitProvider> logger in _loggers) {
        [logger logError:name message:message exception:exception];
    }
}

+(void)logError:(NSString *)name message:(NSString *)message error:(NSError *)error {
    for (id<RMFAnalyticsKitProvider> logger in _loggers) {
        [logger logError:name message:message error:error];
    }
}

+(void) stopRecordingAnalytics
{
    for (id<RMFAnalyticsKitProvider> logger in _loggers) {
        [logger stopRecordingAnalytics];
    }

    
}


@end
