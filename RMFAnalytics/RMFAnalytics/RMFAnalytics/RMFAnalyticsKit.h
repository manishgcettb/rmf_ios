/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <Foundation/Foundation.h>
#define kRMFClickEvent          @"This event occured due to click gesture"
#define kRMFNavigationEvent     @"Thus event occured due to navigation gesture"
#define kRMFTransitionEvent     @"This event occured due to app changing states i.e from foreground to background or vice versa"
#define kRMFOrientationEvent    @"This event occured due to app changing orientations i.e.from landscape to portrait or vice versa"



@protocol RMFAnalyticsKitProvider <NSObject>

/* Event handling for logging screen name */
-(void)logScreen:(NSString *)screenName;

/* Event handling for logging event name */
-(void)logEvent:(NSString *)event;

/* Event handling for logging click event */
-(void)logClickEvent:(NSString *)event withAction:(NSString *)action;

/* Event handling for logging navigation event */
-(void)logNavigationEvent:(NSString *)event withAction:(NSString *)action;

/* Event handling for logging orientation change event */
-(void)logOrientationChangeEvent:(NSString *)event withAction:(NSString *)action;

/* Event handling for logging timed event */
-(void)logEvent:(NSString *)event timed:(BOOL)timed;

/* Event handling for logging timed event */
-(void)endTimedEvent:(NSString *)event withProperties:(NSDictionary *)dict;
-(void)logError:(NSString *)name message:(NSString *)message exception:(NSException *)exception;
-(void)logError:(NSString *)name message:(NSString *)message error:(NSError *)error;


/* Event handling for lifecycle events */
- (void) logAppEnterForegroundEvent;
- (void) logAppEnterBackgroundEvent;
- (void) logAppTerminateEvent;
- (void) uncaughtException:(NSException *)exception;

- (void) stopRecordingAnalytics;



@end

@interface RMFAnalyticsKit : NSObject

OBJC_EXPORT NSString* const AnalyticsKitEventTimeSeconds;

+(void)initialize;
+(void)initializeLoggers:(NSArray *)loggers;
+(NSArray *)loggers;
+(void)logAppEnterForegroundEvent;
+(void)logAppEnterBackgroundEvent;
+(void)logAppTerminateEvent;
+(void)uncaughtException:(NSException *)exception;

+(void)logEvent:(NSString *)event;
+(void)logClickEvent:(NSString *)event withAction:(NSString *)action;
+(void)logScreen:(NSString *)screenName;
+(void)logNavigationEvent:(NSString *)event withAction:(NSString *)action;
+(void)logOrientationChangeEvent:(NSString *)event withAction:(NSString *)action;
+(void)logEvent:(NSString *)event timed:(BOOL)timed;
+(void)endTimedEvent:(NSString *)event withProperties:(NSDictionary *)dict;
+(void)logError:(NSString *)name message:(NSString *)message exception:(NSException *)exception;
+(void)logError:(NSString *)name message:(NSString *)message error:(NSError *)error;
+(void) stopRecordingAnalytics;

@end
