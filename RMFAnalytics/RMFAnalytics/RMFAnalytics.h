/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <UIKit/UIKit.h>
#import "RMFGoogleAnalyticsProvider.h"
//! Project version number for RMFAnalytics.
FOUNDATION_EXPORT double RMFAnalyticsVersionNumber;

//! Project version string for RMFAnalytics.
FOUNDATION_EXPORT const unsigned char RMFAnalyticsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <RMFAnalytics/PublicHeader.h>


