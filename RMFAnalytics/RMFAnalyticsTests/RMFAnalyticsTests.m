/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */
#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "RMFAnalyticsKit.h"

@interface RMFAnalyticsTests : XCTestCase

@end

@implementation RMFAnalyticsTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}


-(void)testLogAppEnterForegroundEvent{
    XCTAssertNoThrow([RMFAnalyticsKit logAppEnterBackgroundEvent]);
}
-(void)testLogAppEnterBackgroundEvent
{
    XCTAssertNoThrow([RMFAnalyticsKit logAppEnterForegroundEvent]);
}
-(void)testLogAppTerminateEvent;
{
    XCTAssertNoThrow([RMFAnalyticsKit logAppTerminateEvent]);
}
-(void)testUncaughtException;
{
    XCTAssertNoThrow([RMFAnalyticsKit uncaughtException:[[NSException alloc] initWithName:@"test Exception" reason:@"test reason" userInfo:nil]]);
}

-(void)testLogEvent
{
    XCTAssertNoThrow([RMFAnalyticsKit logEvent:@"test event"]);
}
-(void)testLogClickEvent
{
    XCTAssertNoThrow([RMFAnalyticsKit logClickEvent:@"test Click" withAction:@"clicked"]);
}
-(void)testLogScreen
{
    XCTAssertNoThrow([RMFAnalyticsKit logScreen:@"test Screen"]);
}
-(void)testLogNavigationEvent{
    XCTAssertNoThrow([RMFAnalyticsKit logNavigationEvent:@"test Event" withAction:@"test Action"]);
}
-(void)testLogOrientationChangeEvent
{
    XCTAssertNoThrow([RMFAnalyticsKit logOrientationChangeEvent:@"test Event" withAction:@"test Action"]);
}
-(void)testLogEventForTime{
    XCTAssertNoThrow([RMFAnalyticsKit logEvent:@"test event" timed:YES]);
}
-(void)testEndTimedEvent
{
    XCTAssertNoThrow([RMFAnalyticsKit endTimedEvent:@"test event" withProperties:[[NSDictionary alloc] init]]);
}
-(void)testLogErrorWithException
{
    XCTAssertNoThrow([RMFAnalyticsKit logError:@"test event" message:@"test message" exception:nil]);
}
-(void)testLogError{
    XCTAssertNoThrow([RMFAnalyticsKit logError:@"test event" message:@"test message" error:nil]);
}


/*
 NSArray *providers = @[
  [[RMFGoogleAnalyticsProvider alloc] initWithTrackingID:nil],
 ];
 [RMFAnalyticsKit initializeLoggers:providers];
 
 
 [RMFAnalyticsKit logEvent:@"foo"];
 

 
 */

@end
