/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#ifndef DemoRMF_DemoMacro_h
#define DemoRMF_DemoMacro_h

#define TEST_IP              @"10.199.4.106"
#define TEST_PORT            @"8080"

#define kVenueNameKey        @"venue_name"
#define kTenantNameKey       @"tenant_name"
#define kSerialNumberKey     @"serial-number"
#define kWlanNameKey         @"name"
#define kNewWlanInfoKey      @"wlan_info"
#define kNewAPInfoKey        @"ap_info"
#define kNewVenueInfoKey     @"venue_info"
#define kMacAddressInfoKey   @"mac_address"

#endif
