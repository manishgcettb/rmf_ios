/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import "DemoViewController.h"
#import "DemoMacro.h"


@interface DemoViewController ()

// Stores and returns request parameters
@property (nonatomic, strong) NSMutableDictionary *requestParameters;

// Stores and returns the instance of RMFConfiguration
@property (nonatomic, strong) RMFConfiguration *configuration;

@end

@implementation DemoViewController

- (void)viewDidLoad {
	[super viewDidLoad];

	// initialise Configuration instance
	[self initRmfConfiguration];

	// initialise request parameter dictionary
	[self initRequestParameters];

	// Get an instance of RMFDBAccessLayer
	[self initDatabaseManager];

	// Make request to add a new AP
	[self createAP];

	// Make request to fetch AP list
	[self apList];

	// Make request to fetch AP details
	[self apDetails];

	// Make request to fetch WLAN list
	[self wlanServiceList];

	// Make request to fetch WLANs count
	[self wlanCount];

	// Make request to fetch WLAN service detail
	[self wlanServiceDetail];

	// Make request to fetch WLAN status
	[self wlanStatus];

	// initialise logger
	[self configureLogger];

	// client related operations...
	[self clientList];

	[self clientDetail];

	// client list sort and filter
	[self clientListWithSortAndFilter];

	// Make request to fetch AP sorted and filtered list
	[self apListWithSortAndFilter];
}

/*
 * This method creates and initialise request parameter dictionary with suitable values
 */

- (void)initRequestParameters {
	// Create the instance of request parameter dictionary
	self.requestParameters = [[NSMutableDictionary alloc] init];

	if (self.requestParameters) {
		// initialise request parameter dictionary with suitable values
		[self.requestParameters setObject:@"default" forKey:kVenueNameKey];
		[self.requestParameters setObject:@"kumomobile" forKey:kTenantNameKey];
		[self.requestParameters setObject:@"401205000278" forKey:kSerialNumberKey];
		[self.requestParameters setObject:@"kumomobile" forKey:kWlanNameKey];
		[self.requestParameters setObject:@"20:C9:D0:8C:68:9F" forKey:kMacAddressInfoKey];
	}
}

/*
 * This method creates and initialise RMFConfiguration instance with suitable values
 */

- (void)initRmfConfiguration {
	// Get the instance of RMFConfiguration
	self.configuration = [RMFConfiguration sharedRMFConfiguration];

	if (self.configuration) {
		// initialise the RMFConfiguration instance with system IP and port
		self.configuration.systemIP = TEST_IP; // setting the IP address
		self.configuration.port = TEST_PORT; // setting port
		self.configuration.system = kSystemTypeKUMO; // type of system
	}
}

/*
 * This method creates  RMFDBAccessLayer instance
 */

- (void)initDatabaseManager {
	databaseManager = [RMFDBAccessLayer sharedRMFDBAccessLayer];
}

#pragma mark - Wlan test methods
/*
 * This method requests RMFWlanPlugin instance to make a request to fetch WLAN list
 */

- (void)wlanServiceList {
	// Get the instance of RMFWlanPlugin
	RMFWlanPlugin *wlanPlugin = [RMFWlanPlugin sharedRMFWlanPlugin];

	// Make request to fetch WLAN list
	[wlanPlugin wlanList:self.requestParameters response: ^(NSError *error, NSArray *wlanServiceList) {
	    NSLog(@" Wlan Service list: %@", wlanServiceList);
	}];
}

/*
 * This method requests RMFWlanPlugin instance to make a request to fetch WLAN service detail
 */

- (void)wlanServiceDetail {
	// Get the instance of RMFWlanPlugin
	RMFWlanPlugin *wlanPlugin = [RMFWlanPlugin sharedRMFWlanPlugin];

	// Make request to fetch WLAN detail
	[wlanPlugin wlanDetails:self.requestParameters response: ^(NSError *error, RMFWlanService *wlanService) {
	    NSLog(@"error: %@", error);

	    NSLog(@"  wlanService.name: %@", wlanService.name);
	    NSLog(@"  wlanService.ssid: %@", wlanService.ssid);
	    NSLog(@"  wlanService.wlanType: %@", wlanService.wlanType);
	    NSLog(@"  wlanService.wlanSecurity: %@", wlanService.wlanSecurity);
	    NSLog(@"  wlanService.pskPassPhrase: %@", wlanService.pskPassPhrase);
	    NSLog(@"  wlanService.wlanAdvCustmization.enableBandBalancing: %@", wlanService.wlanAdvCustmization.enableBandBalancing ? @"YES" : @"NO");
	    NSLog(@"  wlanService.wlanAdvCustmization.maxClientsOnWlanPerRadio: %@", wlanService.wlanAdvCustmization.maxClientsOnWlanPerRadio ? @"YES" : @"NO");
	    NSLog(@"  wlanService.wlanBasicNwCustomization.vlanId: %@", wlanService.wlanBasicNwCustomization.vlanId ? @"YES" : @"NO");
	}];
}

/*
 * This method requests RMFWlanPlugin instance to make a request to fetch WLAN status
 */
- (void)wlanStatus {
	// Get the instance of RMFWlanPlugin
	RMFWlanPlugin *wlanPlugin = [RMFWlanPlugin sharedRMFWlanPlugin];

	// Make request to fetch WLAN status
	[wlanPlugin wlanStatus:self.requestParameters response: ^(NSError *error, RMFWlanServiceStatus *wlanServiceStatus) {
	    NSLog(@"error: %@", error);

	    NSLog(@"wlanServiceStatus.totalCount: %ld", (long)wlanServiceStatus.totalCount);
	    NSLog(@"wlanServiceStatus.bssidArray: %@", wlanServiceStatus.bssidArray);
	}];
}

/*
 * This method requests RMFWlanPlugin instance to make a request to fetch WLANs count
 */
- (void)wlanCount {
	// Get the instance of RMFWlanPlugin
	RMFWlanPlugin *wlanPlugin = [RMFWlanPlugin sharedRMFWlanPlugin];

	// Make request to fetch WLAN count
	[wlanPlugin wlanCount:self.requestParameters response: ^(NSError *error, NSInteger wlanCount) {
	    NSLog(@"error: %@", error);

	    NSLog(@"Wlancount: %ld", (long)wlanCount);
	}];
}

#pragma mark - AP

/*
 * This method requests RMFAPPlugin instance to make a request to add new AP
 */

- (void)createAP {
	NSDictionary *createAPData = [[NSDictionary alloc] initWithObjectsAndKeys:@"kumomobile", kTenantNameKey, @"default", kVenueNameKey, @"111111111111", kSerialNumberKey, nil];

	[[RMFAPPlugin sharedRMFAPPlugin] createAP:createAPData response: ^(NSError *error, id data) {
	    NSError *serializeError;
	    if (error && error.userInfo && [error.userInfo objectForKey:@"com.alamofire.serialization.response.error.data"]) {
	        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:[error.userInfo objectForKey:@"com.alamofire.serialization.response.error.data"]
	                                                             options:kNilOptions
	                                                               error:&serializeError];

	        NSLog(@"Error: %@", json);
		}
	}];
}

/*
 * This method requests RMFAPPlugin instance to make a request to fetch AP list
 */

- (void)apList {
	[[RMFAPPlugin sharedRMFAPPlugin] apList:self.requestParameters response: ^(NSError *error, NSArray *apList) {
	    NSLog(@"error = %@", error);

	    if (apList != nil) {
	        for (RMFAccessPoint *rmfAccessPoint in apList) {
	            NSLog(@"serialNumber : %@ ", rmfAccessPoint.serialNumber);
			}
		}
	}];
}

/*
 * This method requests RMFAPPlugin instance to make a request to fetch AP list with sorting and filtring
 */

- (void)apListWithSortAndFilter {
	NSMutableDictionary *requestDic = [self.requestParameters mutableCopy];
	[requestDic setObject:@"1" forKey:@"offset"];
	[requestDic setObject:@"5" forKey:@"limit"];

	[[RMFAPPlugin sharedRMFAPPlugin] apList:requestDic sortBy:kSortingParameterName filterOn:[NSDictionary dictionaryWithObjectsAndKeys:@"111111111111", @"serial-number", nil] response: ^(NSError *error, NSArray *apList) {
	    NSLog(@"apList  : %@", apList);
	    NSLog(@"error  : %@", error);
	}];
}

/*
 * This method requests RMFAPPlugin instance to make a request to fetch AP details
 */

- (void)apDetails {
	[[RMFAPPlugin sharedRMFAPPlugin] apDetail:self.requestParameters response: ^(NSError *error, RMFAccessPoint *rmfAccessPoint) {
	    NSLog(@"error = %@ rmfAccessPoint = %@", error, rmfAccessPoint);

	    if (rmfAccessPoint != nil) {
	        NSLog(@"rmfAccessPoint.serialNumber = %@", rmfAccessPoint.serialNumber);
		}
	}];
}

/*
 * This method tests that Notifications are fetched successfully from the database.
 */

- (void)getNotificationsforDummy {
	NSArray *actualNotificationListInDatabase = [databaseManager getNotifications];
	NSLog(@"actualNotificationListInDatabase %@", actualNotificationListInDatabase);
}

- (void)insertNotificationsforDummy {
	RMFNotifications *notifications = [databaseManager insertNotifications];
	if (notifications != nil) {
		notifications.message = @"test Message";
		notifications.notificationTime = [NSDate date];
		notifications.type = @"1";
		notifications.readStatus = [NSNumber numberWithInt:1];
		[databaseManager saveNotificationObject:notifications];
	}
}




- (void)insertUser {
    
    RMFUser *user = [[RMFUser alloc] init];
    user.userName = @"testName";
    user.roleType = 1;
    user.phoneNumber = @"123123";
    user.address = @"test address";
    user.fullName = @"test full name";
    user.password = @"test password";
    user.email = @"email@email.com";
    [databaseManager insertUser:user];
}
- (void)getUser{
    [databaseManager getUser];
}

#pragma mark - Logger

- (void)configureLogger {
	// set log level
	[RMFConfiguration sharedRMFConfiguration].logLevelRMF = DDLogLevelVerbose;

	// create logger instance
	RMFLogger *logger = [RMFLogger sharedRMFLogger];

	[logger enableRMFLogger];
}

- (NSMutableArray *)errorLogData {
	NSUInteger maximumLogFilesToReturn = 5; // default logs file..
	NSMutableArray *errorLogFiles = [NSMutableArray arrayWithCapacity:maximumLogFilesToReturn];
	DDFileLogger *logger = [RMFLogger sharedRMFLogger].fileLogger;
	NSArray *sortedLogFileInfos = [logger.logFileManager sortedLogFileInfos];
	for (int i = 0; i < MIN(sortedLogFileInfos.count, maximumLogFilesToReturn); i++) {
		DDLogFileInfo *logFileInfo = [sortedLogFileInfos objectAtIndex:i];
		NSData *fileData = [NSData dataWithContentsOfFile:logFileInfo.filePath];
		[errorLogFiles addObject:fileData];
	}
	return errorLogFiles;
}

- (void)composeEmailWithLogsAttachment {
	if ([MFMailComposeViewController canSendMail]) {
		MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
		mailViewController.mailComposeDelegate = self;
		NSMutableData *errorLogData = [NSMutableData data];
		for (NSData *errorLogFileData in[self errorLogData]) {
			[errorLogData appendData:errorLogFileData];
		}
		[mailViewController addAttachmentData:errorLogData mimeType:@"text/plain" fileName:@"errorLog.txt"];
		[mailViewController setSubject:NSLocalizedString(@"Logs sheet - RMF", @"")];
		[mailViewController setToRecipients:[NSArray arrayWithObject:@"rakesh9.kumar@aricent.com"]];

		[self presentViewController:mailViewController animated:YES completion:nil];
	}

	else {
		NSString *message = NSLocalizedString(@"Sorry, your issue can't be reported right now. This is most likely because no mail accounts are set up on your mobile device.", @"");
		[[[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"") otherButtonTitles:nil] show];
	}
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
	UIAlertView *alert;
	switch (result) {
		case MFMailComposeResultCancelled:
			break;

		case MFMailComposeResultSaved:
			alert = [[UIAlertView alloc] initWithTitle:@"Draft Saved" message:@"Composed Mail is saved in draft." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
			[alert show];
			break;

		case MFMailComposeResultSent:
			alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Logs sent" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
			[alert show];
			break;

		case MFMailComposeResultFailed:
			alert = [[UIAlertView alloc] initWithTitle:@"Failed" message:@"Sorry! Failed to send." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
			[alert show];
			break;

		default:
			break;
	}

	// Close the Mail Interface
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
}

- (void)getJSON {
	[RMFConfiguration sharedRMFConfiguration];
}

- (void)onNotificationReceive {
}

#pragma mark - Client methods
/*
 * This method requests RMFClientPlugin instance to make a request to fetch AP list
 */
- (void)clientList {
	[[RMFAPClientPlugin sharedRMFClientPlugin] clientList:self.requestParameters response: ^(NSError *error, NSArray *clientList) {
	    NSLog(@"error = %@", error);

	    if (clientList != nil) {
	        NSLog(@" client List: %@", clientList);
		}
	}];
}

- (void)clientDetail {
	[[RMFAPClientPlugin sharedRMFClientPlugin] clientDetails:self.requestParameters response: ^(NSError *error, RMFClientDetail *clientDetail) {
	    NSLog(@"error = %@", error);

	    // Display details of a client..
	    NSLog(@"  clientDetail: %@", clientDetail.authMethod);
	    NSLog(@"  clientDetail: %@", clientDetail.connectionDuration);
	    NSLog(@"  clientDetail: %@", clientDetail.ip);
	    NSLog(@"  clientDetail: %@", clientDetail.macAddress);
	    NSLog(@"  clientDetail: %@", clientDetail.osType);
	    NSLog(@"  clientDetail: %@", clientDetail.status);
	    NSLog(@"  clientDetail: %@", clientDetail.wlan);
	}];
}

/*
 * This method requests RMFClientPlugin instance to make a request to fetch client list with sorting and filtring
 */

- (void)clientListWithSortAndFilter {
	NSMutableDictionary *requestDic = [self.requestParameters mutableCopy];

	NSArray *sortParamsArray  = [[NSArray alloc] initWithObjects:@"mac-address", nil];

	[[RMFAPClientPlugin sharedRMFClientPlugin] clientListFilteredBy:requestDic sortBy:sortParamsArray limit:[NSNumber numberWithInteger:5] offset:[NSNumber numberWithInteger:1] response: ^(NSError *error, id response) {
	    //
	}];
}

#pragma mark - test button tapped

- (IBAction)testAPIButtonTapped:(id)sender {
	[self composeEmailWithLogsAttachment];
}

@end
