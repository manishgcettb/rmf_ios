/**
 * Copyright 2015 Ruckus Wireless, Inc. All rights reserved.
 *
 * RUCKUS WIRELESS, INC. CONFIDENTIAL -
 * This is an unpublished, proprietary work of Ruckus Wireless, Inc., and is
 * fully protected under copyright and trade secret laws. You may not view,
 * use, disclose, copy, or distribute this file or any information contained
 * herein except pursuant to a valid license from Ruckus.
 */

#import <UIKit/UIKit.h>
#import <RMF/RMF.h>

#import <MessageUI/MessageUI.h>

@interface DemoViewController : UIViewController<MFMailComposeViewControllerDelegate>
{
	RMFDBAccessLayer *databaseManager;
	NSManagedObjectContext *managedObjectContext;
	NSManagedObjectModel *managedObjectModel;
}

@property (weak, nonatomic) IBOutlet UIButton *testAPIButton;
@property (weak, nonatomic) IBOutlet UITextView *outputTextView;
- (IBAction)testAPIButtonTapped:(id)sender;

@end
