//
//  main.m
//  DemoRMF
//
//  Created by Deepak Bammi on 21/04/15.
//  Copyright (c) 2015 Ruckus. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
